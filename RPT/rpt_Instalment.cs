﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ByStro.Clases;

namespace ByStro.RPT
{
    public partial class rpt_Instalment : rpt_Master
    {
        public rpt_Instalment()
        {
            InitializeComponent();
            lbl_PrinDate.Text = DateTime.Now.ToString();
            lbl_UserName.Text = CurrentSession.user.UserName;
            lbl_companyName.Text = CurrentSession.Company.CombanyName;
            lbl_Cpmpany_Phone.Text = CurrentSession.Company.PhoneNamber;

        }
        public override void LoadData()
        {

            cell_Index.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Index"));
            cell_DueDate.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "DueDate"));
            cell_Amount.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Amount"));
            cell_PayDate.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "PayDate"));
            xrCheckBox1.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Checked", "IsPaid")); 


            lbl_ID.DataBindings.Add("Text", DataSource, "ID");
            xrBarCode1.DataBindings.Add("Text", DataSource, "ID");
            lbl_PartName.DataBindings.Add("Text", DataSource, "Customer");
            lbl_PartType.DataBindings.Add("Text", DataSource, "PartTypeText");
            lbl_Date.DataBindings.Add("Text", DataSource, "Date"); 
            lbl_Benfite.DataBindings.Add("Text", DataSource, "Benfite");
            lbl_Total.DataBindings.Add("Text", DataSource, "Net");
            lbl_Amount.DataBindings.Add("Text", DataSource, "Amount");
            lbl_NetAsString.DataBindings.Add("Text", DataSource, "NetText");
            lbl_Paid.DataBindings.Add("Text", DataSource, "Paid");
            lbl_Remains.DataBindings.Add("Text", DataSource, "Remains");  
            lbl_Address.DataBindings.Add("Text", DataSource, "Address");
            lbl_Phone.DataBindings.Add("Text", DataSource, "Phone");
            lbl_Mobile.DataBindings.Add("Text", DataSource, "Mobile");
            lbl_Period.DataBindings.Add("Text", DataSource, "Period");
            lbl_Count .DataBindings.Add("Text", DataSource, "Count");
            lbl_AdvancedPay.DataBindings.Add("Text", DataSource, "AdvancedPay");
            try
            {
                xrPictureBox1.Image = MasterClass.GetImageFromByte(CurrentSession.Company.Imge.ToArray());
            }
            catch (Exception)
            {
            }

            base.LoadData();
        }
        public static void Print(object ds)
        {
            rpt_Instalment report = new rpt_Instalment();
            report.LoadTemplete(); 
            report.DataSource = ds;
            report.DataMember = ""; 
            report.DetailReport.DataSource = report.DataSource;
            report.DetailReport.DataMember = "Details";

            //     report.DetailReport_Expence .DataSource = report.DataSource;
            //  report.DetailReport_Expence.DataMember = "HeadOtherCharges";
            report.LoadData();

            //switch (CurrentSession.user.WhenPrintShowMode)
            //{
            //    case 0: report.ShowPreview(); break;
            //    case 1: report.PrintDialog(); break;
            //    case 2: report.Print(); break;
            //    default: report.PrintDialog(); break;
            //}
            report.ShowPreview();

        }

    }
}
