﻿namespace ByStro.PL
{
    partial class CloseData_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CloseData_Form));
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblPrcent = new System.Windows.Forms.Label();
            this.butData = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.BWorkerCustomers = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMessage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblMessage.Location = new System.Drawing.Point(68, 272);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(587, 19);
            this.lblMessage.TabIndex = 27;
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(14, 271);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(54, 17);
            this.lblStatus.TabIndex = 26;
            this.lblStatus.Text = "Status :";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(17, 110);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(630, 25);
            this.progressBar.Step = 20;
            this.progressBar.TabIndex = 23;
            // 
            // lblPrcent
            // 
            this.lblPrcent.AutoSize = true;
            this.lblPrcent.BackColor = System.Drawing.Color.Transparent;
            this.lblPrcent.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrcent.Location = new System.Drawing.Point(296, 147);
            this.lblPrcent.Name = "lblPrcent";
            this.lblPrcent.Size = new System.Drawing.Size(33, 19);
            this.lblPrcent.TabIndex = 24;
            this.lblPrcent.Text = "0 %";
            // 
            // butData
            // 
            this.butData.BackColor = System.Drawing.Color.Yellow;
            this.butData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.butData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butData.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butData.ForeColor = System.Drawing.Color.Red;
            this.butData.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butData.Location = new System.Drawing.Point(212, 176);
            this.butData.Name = "butData";
            this.butData.Size = new System.Drawing.Size(185, 36);
            this.butData.TabIndex = 25;
            this.butData.Text = "Close Years";
            this.butData.UseVisualStyleBackColor = false;
            this.butData.Click += new System.EventHandler(this.butData_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(659, 94);
            this.label1.TabIndex = 22;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // BWorkerCustomers
            // 
            this.BWorkerCustomers.WorkerReportsProgress = true;
            this.BWorkerCustomers.WorkerSupportsCancellation = true;
            this.BWorkerCustomers.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BWorkerCustomers_DoWork);
            this.BWorkerCustomers.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BWorkerCustomers_ProgressChanged);
            this.BWorkerCustomers.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BWorkerCustomers_RunWorkerCompleted);
            // 
            // CloseData_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 304);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lblPrcent);
            this.Controls.Add(this.butData);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "CloseData_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فتح فترة جديدة ";
            this.Load += new System.EventHandler(this.CloseData_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblPrcent;
        private System.Windows.Forms.Button butData;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker BWorkerCustomers;
    }
}