﻿namespace ByStro.PL
{
    partial class Employee_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Employee_frm));
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.txtEmployeeName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.combSocialStatus = new System.Windows.Forms.ComboBox();
            this.chType = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.txtEducationalQualification = new System.Windows.Forms.TextBox();
            this.txtNationalID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEndContract = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtStartContract = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.txtWebSite = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPhone2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtAccountNumber = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtServiceEndDate = new System.Windows.Forms.MaskedTextBox();
            this.txtServiceStartDate = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPlaceOfService = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMilitaryService = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtSalary = new System.Windows.Forms.NumericUpDown();
            this.txtNumberMonth = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPartCompanyID = new System.Windows.Forms.TextBox();
            this.txtWorkPartCompanyID = new System.Windows.Forms.TextBox();
            this.txtWorkingHours = new System.Windows.Forms.NumericUpDown();
            this.combPartCompany = new System.Windows.Forms.ComboBox();
            this.combWorkPartCompany = new System.Windows.Forms.ComboBox();
            this.txtStartWork = new System.Windows.Forms.DateTimePicker();
            this.txtTimeToLeave = new System.Windows.Forms.DateTimePicker();
            this.txtTimeAttendance = new System.Windows.Forms.DateTimePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btnGetData = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkingHours)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "كود الموظف :";
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.BackColor = System.Drawing.Color.White;
            this.txtEmployeeID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmployeeID.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmployeeID.Location = new System.Drawing.Point(107, 9);
            this.txtEmployeeID.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.ReadOnly = true;
            this.txtEmployeeID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtEmployeeID.Size = new System.Drawing.Size(213, 24);
            this.txtEmployeeID.TabIndex = 1;
            this.txtEmployeeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEmployeeID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNationalID_KeyPress);
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmployeeName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmployeeName.Location = new System.Drawing.Point(107, 36);
            this.txtEmployeeName.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Size = new System.Drawing.Size(522, 24);
            this.txtEmployeeName.TabIndex = 0;
            this.txtEmployeeName.TextChanged += new System.EventHandler(this.txtCompanyName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "اسم الموظف :";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(5, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(656, 317);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.combSocialStatus);
            this.tabPage1.Controls.Add(this.chType);
            this.tabPage1.Controls.Add(this.radioButton2);
            this.tabPage1.Controls.Add(this.txtEducationalQualification);
            this.tabPage1.Controls.Add(this.txtNationalID);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtEndContract);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Controls.Add(this.Label5);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtRemarks);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.txtStartContract);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(648, 288);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "البيانات الاساسية";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // combSocialStatus
            // 
            this.combSocialStatus.FormattingEnabled = true;
            this.combSocialStatus.Items.AddRange(new object[] {
            "متزوج",
            "اعزب",
            "ارمل",
            "مطلق"});
            this.combSocialStatus.Location = new System.Drawing.Point(331, 136);
            this.combSocialStatus.Name = "combSocialStatus";
            this.combSocialStatus.Size = new System.Drawing.Size(291, 24);
            this.combSocialStatus.TabIndex = 4;
            // 
            // chType
            // 
            this.chType.AutoSize = true;
            this.chType.Checked = true;
            this.chType.Location = new System.Drawing.Point(124, 35);
            this.chType.Name = "chType";
            this.chType.Size = new System.Drawing.Size(46, 21);
            this.chType.TabIndex = 1;
            this.chType.TabStop = true;
            this.chType.Text = "ذكر";
            this.chType.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(55, 35);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(52, 21);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "انثي";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // txtEducationalQualification
            // 
            this.txtEducationalQualification.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducationalQualification.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEducationalQualification.Location = new System.Drawing.Point(194, 35);
            this.txtEducationalQualification.Margin = new System.Windows.Forms.Padding(4);
            this.txtEducationalQualification.MaxLength = 50;
            this.txtEducationalQualification.Name = "txtEducationalQualification";
            this.txtEducationalQualification.Size = new System.Drawing.Size(428, 23);
            this.txtEducationalQualification.TabIndex = 0;
            // 
            // txtNationalID
            // 
            this.txtNationalID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNationalID.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNationalID.Location = new System.Drawing.Point(331, 187);
            this.txtNationalID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNationalID.MaxLength = 50;
            this.txtNationalID.Name = "txtNationalID";
            this.txtNationalID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNationalID.Size = new System.Drawing.Size(291, 24);
            this.txtNationalID.TabIndex = 5;
            this.txtNationalID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNationalID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNationalID_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(539, 166);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 17);
            this.label9.TabIndex = 419;
            this.label9.Text = " رقم البطاقة :";
            // 
            // txtEndContract
            // 
            this.txtEndContract.Location = new System.Drawing.Point(27, 187);
            this.txtEndContract.Mask = "00 / 00 / 0000";
            this.txtEndContract.Name = "txtEndContract";
            this.txtEndContract.Size = new System.Drawing.Size(274, 24);
            this.txtEndContract.TabIndex = 7;
            this.txtEndContract.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(504, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 17);
            this.label3.TabIndex = 413;
            this.label3.Text = "الحالة الاجتماعية :";
            // 
            // txtAddress
            // 
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.Location = new System.Drawing.Point(27, 84);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddress.MaxLength = 60;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(595, 23);
            this.txtAddress.TabIndex = 3;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(562, 63);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(54, 17);
            this.Label5.TabIndex = 408;
            this.Label5.Text = "العنوان :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(194, 165);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 17);
            this.label11.TabIndex = 412;
            this.label11.Text = "نهاية مدة العقد :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(564, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 410;
            this.label4.Text = "ملاحظة :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.Location = new System.Drawing.Point(27, 239);
            this.txtRemarks.MaxLength = 50;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(595, 23);
            this.txtRemarks.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(210, 115);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 17);
            this.label17.TabIndex = 415;
            this.label17.Text = "بداية التعامل :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(124, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 17);
            this.label14.TabIndex = 414;
            this.label14.Text = "النوع :";
            // 
            // txtStartContract
            // 
            this.txtStartContract.Location = new System.Drawing.Point(27, 136);
            this.txtStartContract.Mask = "00 / 00 / 0000";
            this.txtStartContract.Name = "txtStartContract";
            this.txtStartContract.Size = new System.Drawing.Size(274, 24);
            this.txtStartContract.TabIndex = 6;
            this.txtStartContract.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(505, 13);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "المؤهل الدراسي :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtFax);
            this.tabPage2.Controls.Add(this.txtWebSite);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.txtPhone2);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.txtEmail);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txtPhone);
            this.tabPage2.Controls.Add(this.txtAccountNumber);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(648, 288);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "بيانات الاتصال";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtFax
            // 
            this.txtFax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFax.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFax.Location = new System.Drawing.Point(27, 72);
            this.txtFax.Margin = new System.Windows.Forms.Padding(4);
            this.txtFax.MaxLength = 50;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(274, 23);
            this.txtFax.TabIndex = 3;
            // 
            // txtWebSite
            // 
            this.txtWebSite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWebSite.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSite.Location = new System.Drawing.Point(27, 168);
            this.txtWebSite.Margin = new System.Windows.Forms.Padding(4);
            this.txtWebSite.MaxLength = 50;
            this.txtWebSite.Name = "txtWebSite";
            this.txtWebSite.Size = new System.Drawing.Size(274, 23);
            this.txtWebSite.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(192, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 17);
            this.label10.TabIndex = 381;
            this.label10.Text = "موقع الالكتروني :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(246, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 17);
            this.label12.TabIndex = 380;
            this.label12.Text = "فاكس :";
            // 
            // txtPhone2
            // 
            this.txtPhone2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhone2.Location = new System.Drawing.Point(348, 119);
            this.txtPhone2.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhone2.MaxLength = 15;
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Size = new System.Drawing.Size(274, 23);
            this.txtPhone2.TabIndex = 1;
            this.txtPhone2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(534, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 17);
            this.label13.TabIndex = 379;
            this.label13.Text = "رقم هاتف 2 :";
            // 
            // txtEmail
            // 
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(27, 120);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(274, 23);
            this.txtEmail.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(247, 99);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 17);
            this.label15.TabIndex = 377;
            this.label15.Text = "الاميل :";
            // 
            // txtPhone
            // 
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhone.Location = new System.Drawing.Point(348, 72);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(274, 23);
            this.txtPhone.TabIndex = 0;
            this.txtPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone_KeyPress);
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccountNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccountNumber.Location = new System.Drawing.Point(350, 168);
            this.txtAccountNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtAccountNumber.MaxLength = 50;
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(272, 23);
            this.txtAccountNumber.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(540, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 17);
            this.label16.TabIndex = 378;
            this.label16.Text = "رقم الهاتف :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(529, 147);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 17);
            this.label18.TabIndex = 382;
            this.label18.Text = "رقم الحساب :";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtServiceEndDate);
            this.tabPage3.Controls.Add(this.txtServiceStartDate);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.txtPlaceOfService);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.txtMilitaryService);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(648, 288);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "الخدمة العسكرية";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtServiceEndDate
            // 
            this.txtServiceEndDate.Location = new System.Drawing.Point(333, 204);
            this.txtServiceEndDate.Mask = "00/00/0000";
            this.txtServiceEndDate.Name = "txtServiceEndDate";
            this.txtServiceEndDate.Size = new System.Drawing.Size(301, 24);
            this.txtServiceEndDate.TabIndex = 3;
            this.txtServiceEndDate.ValidatingType = typeof(System.DateTime);
            // 
            // txtServiceStartDate
            // 
            this.txtServiceStartDate.Location = new System.Drawing.Point(333, 154);
            this.txtServiceStartDate.Mask = "00/00/0000";
            this.txtServiceStartDate.Name = "txtServiceStartDate";
            this.txtServiceStartDate.Size = new System.Drawing.Size(301, 24);
            this.txtServiceStartDate.TabIndex = 2;
            this.txtServiceStartDate.ValidatingType = typeof(System.DateTime);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label20.Location = new System.Drawing.Point(513, 181);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 17);
            this.label20.TabIndex = 412;
            this.label20.Text = "تاريخ انتهاء الخدمة :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label19.Location = new System.Drawing.Point(515, 132);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 17);
            this.label19.TabIndex = 410;
            this.label19.Text = "تاريخ بداية الخدمة :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label21.Location = new System.Drawing.Point(546, 83);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 17);
            this.label21.TabIndex = 408;
            this.label21.Text = "مكان الخدمة :";
            // 
            // txtPlaceOfService
            // 
            this.txtPlaceOfService.BackColor = System.Drawing.Color.White;
            this.txtPlaceOfService.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPlaceOfService.Location = new System.Drawing.Point(333, 104);
            this.txtPlaceOfService.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPlaceOfService.Name = "txtPlaceOfService";
            this.txtPlaceOfService.Size = new System.Drawing.Size(301, 24);
            this.txtPlaceOfService.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label22.Location = new System.Drawing.Point(519, 34);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(115, 17);
            this.label22.TabIndex = 406;
            this.label22.Text = "الخدمة العسكرية :";
            // 
            // txtMilitaryService
            // 
            this.txtMilitaryService.BackColor = System.Drawing.Color.White;
            this.txtMilitaryService.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtMilitaryService.Location = new System.Drawing.Point(333, 55);
            this.txtMilitaryService.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMilitaryService.Name = "txtMilitaryService";
            this.txtMilitaryService.Size = new System.Drawing.Size(301, 24);
            this.txtMilitaryService.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtSalary);
            this.tabPage4.Controls.Add(this.txtNumberMonth);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.txtPartCompanyID);
            this.tabPage4.Controls.Add(this.txtWorkPartCompanyID);
            this.tabPage4.Controls.Add(this.txtWorkingHours);
            this.tabPage4.Controls.Add(this.combPartCompany);
            this.tabPage4.Controls.Add(this.combWorkPartCompany);
            this.tabPage4.Controls.Add(this.txtStartWork);
            this.tabPage4.Controls.Add(this.txtTimeToLeave);
            this.tabPage4.Controls.Add(this.txtTimeAttendance);
            this.tabPage4.Controls.Add(this.label32);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(648, 288);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "بيانات المرتب";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(334, 206);
            this.txtSalary.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(301, 24);
            this.txtSalary.TabIndex = 3;
            this.txtSalary.ValueChanged += new System.EventHandler(this.txtSalary_ValueChanged);
            // 
            // txtNumberMonth
            // 
            this.txtNumberMonth.Location = new System.Drawing.Point(21, 46);
            this.txtNumberMonth.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.txtNumberMonth.Name = "txtNumberMonth";
            this.txtNumberMonth.Size = new System.Drawing.Size(301, 24);
            this.txtNumberMonth.TabIndex = 4;
            this.txtNumberMonth.ValueChanged += new System.EventHandler(this.txtNumberMonth_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label7.Location = new System.Drawing.Point(200, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 17);
            this.label7.TabIndex = 117;
            this.label7.Text = "عدد ايام الشهر :";
            // 
            // txtPartCompanyID
            // 
            this.txtPartCompanyID.Location = new System.Drawing.Point(6, 69);
            this.txtPartCompanyID.Name = "txtPartCompanyID";
            this.txtPartCompanyID.ReadOnly = true;
            this.txtPartCompanyID.Size = new System.Drawing.Size(10, 24);
            this.txtPartCompanyID.TabIndex = 116;
            this.txtPartCompanyID.Visible = false;
            // 
            // txtWorkPartCompanyID
            // 
            this.txtWorkPartCompanyID.Location = new System.Drawing.Point(6, 39);
            this.txtWorkPartCompanyID.Name = "txtWorkPartCompanyID";
            this.txtWorkPartCompanyID.ReadOnly = true;
            this.txtWorkPartCompanyID.Size = new System.Drawing.Size(10, 24);
            this.txtWorkPartCompanyID.TabIndex = 115;
            this.txtWorkPartCompanyID.Visible = false;
            // 
            // txtWorkingHours
            // 
            this.txtWorkingHours.Location = new System.Drawing.Point(21, 206);
            this.txtWorkingHours.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.txtWorkingHours.Name = "txtWorkingHours";
            this.txtWorkingHours.Size = new System.Drawing.Size(301, 24);
            this.txtWorkingHours.TabIndex = 7;
            this.txtWorkingHours.ValueChanged += new System.EventHandler(this.txtWorkingHours_ValueChanged);
            // 
            // combPartCompany
            // 
            this.combPartCompany.FormattingEnabled = true;
            this.combPartCompany.Location = new System.Drawing.Point(337, 100);
            this.combPartCompany.Name = "combPartCompany";
            this.combPartCompany.Size = new System.Drawing.Size(297, 24);
            this.combPartCompany.TabIndex = 1;
            this.combPartCompany.SelectedIndexChanged += new System.EventHandler(this.combPartCompany_SelectedIndexChanged);
            this.combPartCompany.TextChanged += new System.EventHandler(this.combPartCompany_TextChanged);
            // 
            // combWorkPartCompany
            // 
            this.combWorkPartCompany.FormattingEnabled = true;
            this.combWorkPartCompany.Location = new System.Drawing.Point(337, 47);
            this.combWorkPartCompany.Name = "combWorkPartCompany";
            this.combWorkPartCompany.Size = new System.Drawing.Size(297, 24);
            this.combWorkPartCompany.TabIndex = 0;
            this.combWorkPartCompany.SelectedIndexChanged += new System.EventHandler(this.combWorkPartCompany_SelectedIndexChanged);
            this.combWorkPartCompany.TextChanged += new System.EventHandler(this.combWorkPartCompany_TextChanged);
            // 
            // txtStartWork
            // 
            this.txtStartWork.CustomFormat = "dd /MM /yyyy";
            this.txtStartWork.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStartWork.Location = new System.Drawing.Point(21, 100);
            this.txtStartWork.Name = "txtStartWork";
            this.txtStartWork.Size = new System.Drawing.Size(301, 24);
            this.txtStartWork.TabIndex = 5;
            // 
            // txtTimeToLeave
            // 
            this.txtTimeToLeave.CustomFormat = "";
            this.txtTimeToLeave.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.txtTimeToLeave.Location = new System.Drawing.Point(21, 155);
            this.txtTimeToLeave.Name = "txtTimeToLeave";
            this.txtTimeToLeave.Size = new System.Drawing.Size(301, 24);
            this.txtTimeToLeave.TabIndex = 6;
            // 
            // txtTimeAttendance
            // 
            this.txtTimeAttendance.CustomFormat = "";
            this.txtTimeAttendance.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.txtTimeAttendance.Location = new System.Drawing.Point(337, 153);
            this.txtTimeAttendance.Name = "txtTimeAttendance";
            this.txtTimeAttendance.Size = new System.Drawing.Size(297, 24);
            this.txtTimeAttendance.TabIndex = 2;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label32.Location = new System.Drawing.Point(581, 77);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(56, 17);
            this.label32.TabIndex = 108;
            this.label32.Text = "القسم :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label29.Location = new System.Drawing.Point(226, 133);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(94, 17);
            this.label29.TabIndex = 102;
            this.label29.Text = "وقت الانصراف :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label28.Location = new System.Drawing.Point(200, 184);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(122, 17);
            this.label28.TabIndex = 101;
            this.label28.Text = "عدد ساعات العمل :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label27.Location = new System.Drawing.Point(545, 183);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 17);
            this.label27.TabIndex = 100;
            this.label27.Text = "المرتب الثابت :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label26.Location = new System.Drawing.Point(553, 130);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(85, 17);
            this.label26.TabIndex = 98;
            this.label26.Text = "وقت الحضور :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label25.Location = new System.Drawing.Point(241, 78);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 17);
            this.label25.TabIndex = 97;
            this.label25.Text = "بداية العمل :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label24.Location = new System.Drawing.Point(579, 24);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 17);
            this.label24.TabIndex = 96;
            this.label24.Text = "الوظيفة :";
            // 
            // btnGetData
            // 
            this.btnGetData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnGetData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetData.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetData.ForeColor = System.Drawing.Color.White;
            this.btnGetData.Location = new System.Drawing.Point(667, 230);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(91, 33);
            this.btnGetData.TabIndex = 411;
            this.btnGetData.Text = "بحث";
            this.btnGetData.UseVisualStyleBackColor = false;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(667, 195);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 33);
            this.btnDelete.TabIndex = 410;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(667, 265);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 33);
            this.btnClose.TabIndex = 412;
            this.btnClose.Text = "اغلاق";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(667, 160);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 33);
            this.btnUpdate.TabIndex = 409;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(667, 125);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 33);
            this.btnSave.TabIndex = 408;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.White;
            this.btnNew.Location = new System.Drawing.Point(667, 90);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(91, 33);
            this.btnNew.TabIndex = 407;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // Employee_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(766, 385);
            this.Controls.Add(this.btnGetData);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtEmployeeName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEmployeeID);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Employee_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "اضافة موظف جديد";
            this.Load += new System.EventHandler(this.Company_Add_Form_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Customers_frm_KeyDown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkingHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtEmployeeID;
        public System.Windows.Forms.TextBox txtEmployeeName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MaskedTextBox txtEndContract;
        internal System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtAddress;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtStartContract;
        public System.Windows.Forms.TextBox txtFax;
        public System.Windows.Forms.TextBox txtWebSite;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtPhone2;
        internal System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox txtPhone;
        public System.Windows.Forms.TextBox txtAccountNumber;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Button btnGetData;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnNew;
        public System.Windows.Forms.TextBox txtNationalID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtEducationalQualification;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtPlaceOfService;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox txtMilitaryService;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DateTimePicker txtStartWork;
        private System.Windows.Forms.DateTimePicker txtTimeToLeave;
        private System.Windows.Forms.DateTimePicker txtTimeAttendance;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.RadioButton chType;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ComboBox combSocialStatus;
        private System.Windows.Forms.ComboBox combPartCompany;
        private System.Windows.Forms.ComboBox combWorkPartCompany;
        private System.Windows.Forms.NumericUpDown txtWorkingHours;
        private System.Windows.Forms.TextBox txtWorkPartCompanyID;
        private System.Windows.Forms.TextBox txtPartCompanyID;
        private System.Windows.Forms.MaskedTextBox txtServiceEndDate;
        private System.Windows.Forms.MaskedTextBox txtServiceStartDate;
        private System.Windows.Forms.NumericUpDown txtNumberMonth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown txtSalary;
    }
}