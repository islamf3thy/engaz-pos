﻿using System;
using System.Data;

class Employee_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Employees()
    {
        return Execute_SQL("select ISNULL (MAX(EmployeeID)+1,1) from Employees", CommandType.Text);
    }

    //Search 
    public DataTable Search_Employees(string Search)
    {
        return ExecteRader("Select *  from Employees Where convert(nvarchar,EmployeeID)+EmployeeName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    //Delete
    public void Delete_Employees(string EmployeeID)
    {
        Execute_SQL("Delete  From Employees where EmployeeID=@EmployeeID", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID));
    }

    //Details ID
    public DataTable Details_Employees(string EmployeeID)
    {
        return ExecteRader(@"SELECT  dbo.Employees.EmployeeID, dbo.Employees.EmployeeName, dbo.Employees.EducationalQualification, dbo.Employees.Type, dbo.Employees.Adress, dbo.Employees.SocialStatus, dbo.Employees.StartContract, 
                         dbo.Employees.EndContract, dbo.Employees.NationalID, dbo.Employees.Remarks, dbo.Employees.Phone, dbo.Employees.Phone2, dbo.Employees.AccountNumber, dbo.Employees.Fax, dbo.Employees.Email, 
                         dbo.Employees.WebSite, dbo.Employees.MilitaryService, dbo.Employees.PlaceOfService, dbo.Employees.ServiceStartDate, dbo.Employees.ServiceEndDate, dbo.Employees.WorkPartCompanyID, 
                         dbo.WorkPartCompany.WorkPartCompanyName, dbo.Employees.PartCompanyID, dbo.Employees.NumberMonth, dbo.PartCompany.PartCompanyName, dbo.Employees.StartWork, dbo.Employees.TimeAttendance, 
                         dbo.Employees.TimeToLeave, dbo.Employees.WorkingHours, dbo.Employees.Salary
FROM            dbo.Employees INNER JOIN
                         dbo.WorkPartCompany ON dbo.Employees.WorkPartCompanyID = dbo.WorkPartCompany.WorkPartCompanyID INNER JOIN
                         dbo.PartCompany ON dbo.Employees.PartCompanyID = dbo.PartCompany.PartCompanyID
Where EmployeeID=@EmployeeID", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID));
    }
    //Details ID
    public DataTable NameSearch_Employees(String EmployeeName)
    {
        return ExecteRader("Select EmployeeName  from Employees Where EmployeeName=@EmployeeName", CommandType.Text,
        Parameter("@EmployeeName", SqlDbType.NVarChar, EmployeeName));
    }


    // Insert
    public void Insert_Employees(String EmployeeID, String EmployeeName, String EducationalQualification, Boolean Type, String Adress, String SocialStatus, String StartContract, String EndContract, String NationalID, String Remarks, String Phone, String Phone2, String AccountNumber, String Fax, String Email, String WebSite, String MilitaryService, String PlaceOfService, String ServiceStartDate, String ServiceEndDate, String WorkPartCompanyID, String PartCompanyID,string NumberMonth, DateTime StartWork, String TimeAttendance, String TimeToLeave, String WorkingHours, String Salary)
    {
        Execute_SQL("insert into Employees(EmployeeID ,EmployeeName ,EducationalQualification ,Type ,Adress ,SocialStatus ,StartContract ,EndContract ,NationalID ,Remarks ,Phone ,Phone2 ,AccountNumber ,Fax ,Email ,WebSite ,MilitaryService ,PlaceOfService ,ServiceStartDate ,ServiceEndDate ,WorkPartCompanyID ,PartCompanyID ,NumberMonth,StartWork ,TimeAttendance ,TimeToLeave ,WorkingHours ,Salary )Values (@EmployeeID ,@EmployeeName ,@EducationalQualification ,@Type ,@Adress ,@SocialStatus ,@StartContract ,@EndContract ,@NationalID ,@Remarks ,@Phone ,@Phone2 ,@AccountNumber ,@Fax ,@Email ,@WebSite ,@MilitaryService ,@PlaceOfService ,@ServiceStartDate ,@ServiceEndDate ,@WorkPartCompanyID ,@PartCompanyID ,@NumberMonth,@StartWork ,@TimeAttendance ,@TimeToLeave ,@WorkingHours ,@Salary )", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@EmployeeName", SqlDbType.NVarChar, EmployeeName),
        Parameter("@EducationalQualification", SqlDbType.NVarChar, EducationalQualification),
        Parameter("@Type", SqlDbType.Bit, Type),
        Parameter("@Adress", SqlDbType.NText, Adress),
        Parameter("@SocialStatus", SqlDbType.NVarChar, SocialStatus),
        Parameter("@StartContract", SqlDbType.NVarChar, StartContract),
        Parameter("@EndContract", SqlDbType.NVarChar, EndContract),
        Parameter("@NationalID", SqlDbType.NVarChar, NationalID),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Phone2", SqlDbType.NVarChar, Phone2),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Email", SqlDbType.NVarChar, Email),
        Parameter("@WebSite", SqlDbType.NVarChar, WebSite),
        Parameter("@MilitaryService", SqlDbType.NVarChar, MilitaryService),
        Parameter("@PlaceOfService", SqlDbType.NVarChar, PlaceOfService),
        Parameter("@ServiceStartDate", SqlDbType.NVarChar, ServiceStartDate),
        Parameter("@ServiceEndDate", SqlDbType.NVarChar, ServiceEndDate),
        Parameter("@WorkPartCompanyID", SqlDbType.Int, WorkPartCompanyID),
        Parameter("@PartCompanyID", SqlDbType.Int, PartCompanyID),
        Parameter("@NumberMonth", SqlDbType.Int, NumberMonth),
        Parameter("@StartWork", SqlDbType.Date, StartWork),
        Parameter("@TimeAttendance", SqlDbType.NVarChar, TimeAttendance),
        Parameter("@TimeToLeave", SqlDbType.NVarChar, TimeToLeave),
        Parameter("@WorkingHours", SqlDbType.Int, WorkingHours),
        Parameter("@Salary", SqlDbType.Float, Salary));
    }

    //Update
    public void Update_Employees(string EmployeeID, String EmployeeName, String EducationalQualification, Boolean Type, string Adress, String SocialStatus, String StartContract, String EndContract, String NationalID, string Remarks, String Phone, String Phone2, String AccountNumber, String Fax, String Email, String WebSite, String MilitaryService, String PlaceOfService, String ServiceStartDate, String ServiceEndDate, string WorkPartCompanyID, string PartCompanyID, string NumberMonth, DateTime StartWork, String TimeAttendance, String TimeToLeave, string WorkingHours, string Salary)
    {
        Execute_SQL("Update Employees Set EmployeeID=@EmployeeID ,EmployeeName=@EmployeeName ,EducationalQualification=@EducationalQualification ,Type=@Type ,Adress=@Adress ,SocialStatus=@SocialStatus ,StartContract=@StartContract ,EndContract=@EndContract ,NationalID=@NationalID ,Remarks=@Remarks ,Phone=@Phone ,Phone2=@Phone2 ,AccountNumber=@AccountNumber ,Fax=@Fax ,Email=@Email ,WebSite=@WebSite ,MilitaryService=@MilitaryService ,PlaceOfService=@PlaceOfService ,ServiceStartDate=@ServiceStartDate ,ServiceEndDate=@ServiceEndDate ,WorkPartCompanyID=@WorkPartCompanyID ,PartCompanyID=@PartCompanyID,NumberMonth=@NumberMonth ,StartWork=@StartWork ,TimeAttendance=@TimeAttendance ,TimeToLeave=@TimeToLeave ,WorkingHours=@WorkingHours ,Salary=@Salary  where EmployeeID=@EmployeeID", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@EmployeeName", SqlDbType.NVarChar, EmployeeName),
        Parameter("@EducationalQualification", SqlDbType.NVarChar, EducationalQualification),
        Parameter("@Type", SqlDbType.Bit, Type),
        Parameter("@Adress", SqlDbType.NText, Adress),
        Parameter("@SocialStatus", SqlDbType.NVarChar, SocialStatus),
        Parameter("@StartContract", SqlDbType.NVarChar, StartContract),
        Parameter("@EndContract", SqlDbType.NVarChar, EndContract),
        Parameter("@NationalID", SqlDbType.NVarChar, NationalID),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Phone2", SqlDbType.NVarChar, Phone2),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Email", SqlDbType.NVarChar, Email),
        Parameter("@WebSite", SqlDbType.NVarChar, WebSite),
        Parameter("@MilitaryService", SqlDbType.NVarChar, MilitaryService),
        Parameter("@PlaceOfService", SqlDbType.NVarChar, PlaceOfService),
        Parameter("@ServiceStartDate", SqlDbType.NVarChar, ServiceStartDate),
        Parameter("@ServiceEndDate", SqlDbType.NVarChar, ServiceEndDate),
        Parameter("@WorkPartCompanyID", SqlDbType.Int, WorkPartCompanyID),
        Parameter("@PartCompanyID", SqlDbType.Int, PartCompanyID),
          Parameter("@NumberMonth", SqlDbType.Int, NumberMonth),
        Parameter("@StartWork", SqlDbType.Date, StartWork),
        Parameter("@TimeAttendance", SqlDbType.NVarChar, TimeAttendance),
        Parameter("@TimeToLeave", SqlDbType.NVarChar, TimeToLeave),
        Parameter("@WorkingHours", SqlDbType.Int, WorkingHours),
        Parameter("@Salary", SqlDbType.Float, Salary));
    }




}
