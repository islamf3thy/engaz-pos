﻿using ByStro.RPT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MaintenanceRecived_frm : Form
    {
        public MaintenanceRecived_frm()
        {
            InitializeComponent();
        }
        Maintenance_cls cls = new Maintenance_cls();
        private void MaintenanceRecived_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);
         
        }


    


        public void FillDeviceType(string id)
        {

            DeviceType_cls DeviceType_cls = new DeviceType_cls();
            combDeviceType.DataSource = DeviceType_cls.Details_DeviceTypeCompeny(id);
            combDeviceType.DisplayMember = "DeviceTypeName";
            combDeviceType.ValueMember = "DeviceTypeID";
            combDeviceType.Text = null;

        }


        public void FillDamagedType()
        {

            DamagedType_cls DamagedType_cls = new DamagedType_cls();
            combDamagedType.DataSource = DamagedType_cls.Search__DamagedType("");
            combDamagedType.DisplayMember = "DamagedTypeName";
            combDamagedType.ValueMember = "DamagedTypeID";
            combDamagedType.Text = null;

        }


        public void FillCustomerName()
        {
            string cusName = combCustomerName.Text;
            Customers_cls Suppliers_cls = new Customers_cls();
            combCustomerName.DataSource = Suppliers_cls.Search_Customers("");
            combCustomerName.DisplayMember = "CustomerName";
            combCustomerName.ValueMember = "CustomerID";
            combCustomerName.Text = cusName;

        }

        public void FillCombMaintenanceCompeny()
        {

            MaintenanceCompeny_cls Compeny_cls = new MaintenanceCompeny_cls();
            combMaintenanceCompeny.DataSource = Compeny_cls.Search_MaintenanceCompeny("");
            combMaintenanceCompeny.DisplayMember = "CompenyName";
            combMaintenanceCompeny.ValueMember = "CompenyID";
            combMaintenanceCompeny.Text = null;

        }



        private void MaintenanceRecived_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                FillDamagedType();
                FillCustomerName();
                FillCombMaintenanceCompeny();
                combDeviceType.DataSource = null;
                combDeviceType.Items.Clear();
                txtMaintenanceID.Text = cls.MaxID_Maintenance();
                D_RecivedData.Value = DateTime.Now;
                D_ReturnData.Value = DateTime.Now;
                txtSerialNumber.Text = "";
                combDeviceStatus.Text = null;
                combCustomerName.Text = null;
                txtTotalPrice.Text = "0";
                txtRemark.Text = "";
                btnSave.Enabled = true;
                button3.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                combMaintenanceCompeny.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (combDeviceType.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي ادخال نوع الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceType.Focus();
                    return;
                }

                if (combDamagedType.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد نوع العطل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDamagedType.Focus();
                    return;
                }

                if (combDeviceStatus.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد حالة الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceStatus.Focus();
                    return;
                }
                if (combCustomerName.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي ادخال اسم العميل ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combCustomerName.Focus();
                    return;
                }
                if (txtTotalPrice.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي تحديد المبلغ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTotalPrice.Focus();
                    return;
                }

                int DeviceStatus = combDeviceStatus.SelectedIndex;

                Double total = 0;
                if (!string.IsNullOrEmpty(txtTotalPrice.Text))
                {
                    total = Convert.ToDouble(txtTotalPrice.Text);
                }
     
                txtMaintenanceID.Text = cls.MaxID_Maintenance();
                cls.Insert_Maintenance(txtMaintenanceID.Text, D_RecivedData.Value, D_ReturnData.Value, txtSerialNumber.Text, combDeviceType.SelectedValue.ToString(), combDamagedType.SelectedValue.ToString(), DeviceStatus.ToString(), combCustomerName.SelectedValue.ToString(), total, txtRemark.Text, Properties.Settings.Default.UserID);

               // print(false);
                MessageBox.Show("تم تسجيل الجهاز بنجاح", "بنجاح", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (combDeviceType.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي ادخال نوع الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceType.Focus();
                    return;
                }

                if (combDamagedType.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد نوع العطل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDamagedType.Focus();
                    return;
                }

                if (combDeviceStatus.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد حالة الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceStatus.Focus();
                    return;
                }
                if (combCustomerName.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي ادخال اسم العميل ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combCustomerName.Focus();
                    return;
                }
                if (txtTotalPrice.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي تحديد المبلغ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTotalPrice.Focus();
                    return;
                }

                int DeviceStatus = combDeviceStatus.SelectedIndex;

                Double total = 0;

                if (!string.IsNullOrEmpty(txtTotalPrice.Text))
                {
                    total = Convert.ToDouble(txtTotalPrice.Text);
                }
               
                cls.Update_Maintenance(txtMaintenanceID.Text, D_RecivedData.Value, D_ReturnData.Value, txtSerialNumber.Text, combDeviceType.SelectedValue.ToString(), combDamagedType.SelectedValue.ToString(), DeviceStatus.ToString(), combCustomerName.SelectedValue.ToString(), total, txtRemark.Text);

              //  cls_Treasury_Movement.UpdateTreasuryMovement(TreasuryID, "", "no", txtMaintenanceID.Text, txtTypeID.Text, this.Text, D_RecivedData.Value, txtRemark.Text, txtPaid.Text,"0", txtRemark.Text, "0", "0");

                
                MessageBox.Show("تم التعديل بنجاح", "تعديل", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_Maintenance(txtMaintenanceID.Text);

                    btnNew_Click(null, null);

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {
                MaintenanceRecivedSearch_frm frm = new MaintenanceRecivedSearch_frm();
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }

                DataTable dt = cls.Details_Maintenance(frm.DGV1.CurrentRow.Cells["MaintenanceID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMaintenanceID.Text = Dr["MaintenanceID"].ToString();
                D_RecivedData.Value = Convert.ToDateTime(Dr["RecivedData"].ToString());
                D_ReturnData.Value = Convert.ToDateTime(Dr["ReturnData"].ToString());
                txtSerialNumber.Text = Dr["SerialNumber"].ToString();
                combMaintenanceCompeny.Text = Dr["CompenyName"].ToString();
                combDeviceType.Text = Dr["DeviceTypeName"].ToString();
                combDamagedType.Text = Dr["DamagedTypeName"].ToString();


                combDeviceStatus.SelectedIndex = int.Parse(Dr["DeviceStatus"].ToString());

                combCustomerName.Text = Dr["CustomerName"].ToString();
                txtTotalPrice.Text = Dr["TotalPrice"].ToString();
               
                txtRemark.Text = Dr["Remark"].ToString();



                btnSave.Enabled = false;
                button3.Enabled = false;

                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtRest_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtTotalPrice_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtPaid_TextChanged(object sender, EventArgs e)
        {
          
        }

      
        private void combMaintenanceCompeny_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combMaintenanceCompeny.SelectedIndex >= 0)
            {
                FillDeviceType(combMaintenanceCompeny.SelectedValue.ToString());
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DeviceType_frm frm = new DeviceType_frm();
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Customers_frm cus = new Customers_frm();
            cus.ShowDialog();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

       
       

        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

 

        
      
        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


     

  
        private void combCustomerName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (string.IsNullOrEmpty(combCustomerName.Text.Trim()))
                    {
                        return;
                    }
                    Customers_cls cls = new Customers_cls();
                    DataTable dt = cls.Search_Customers(combCustomerName.Text);
                    if (dt.Rows.Count > 0)
                    {
                        combCustomerName.Text = dt.Rows[0]["CustomerName"].ToString();
                    }
                    else
                    {

                        Customers_frm cus = new Customers_frm();
                        cus.txtPhone.Text = combCustomerName.Text; //txtCustomerName.Text = "";
                        cus.ClearData = false;
                        cus.ShowDialog(this);
                        if (cus.SaveData == true)
                        {
                            combCustomerName.Text = cus.txtCustomerName.Text;
                        }

                    }
                }
            }
            catch 
            {

               
            }
        }
  string DeviceStatusName;
        private void button3_Click_1(object sender, EventArgs e)
             
        {
            try
            {

                if (combDeviceType.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي ادخال نوع الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceType.Focus();
                    return;
                }

                if (combDamagedType.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد نوع العطل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDamagedType.Focus();
                    return;
                }

                if (combDeviceStatus.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد حالة الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceStatus.Focus();
                    return;
                }
                if (combCustomerName.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي ادخال اسم العميل ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combCustomerName.Focus();
                    return;
                }
                if (txtTotalPrice.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي تحديد المبلغ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTotalPrice.Focus();
                    return;
                }

                int DeviceStatus = combDeviceStatus.SelectedIndex;

                Double total = 0;
                if (!string.IsNullOrEmpty(txtTotalPrice.Text))
                {
                    total = Convert.ToDouble(txtTotalPrice.Text);
                }

                txtMaintenanceID.Text = cls.MaxID_Maintenance();
                cls.Insert_Maintenance(txtMaintenanceID.Text, D_RecivedData.Value, D_ReturnData.Value, txtSerialNumber.Text, combDeviceType.SelectedValue.ToString(), combDamagedType.SelectedValue.ToString(), DeviceStatus.ToString(), combCustomerName.SelectedValue.ToString(), total, txtRemark.Text, Properties.Settings.Default.UserID);

             //   print(true);
                MessageBox.Show("تم تسجيل الجهاز بنجاح", "بنجاح", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            
              
        }



        //private void print(Boolean ISPrint)
        //{
        //    if (ISPrint==false)
        //    {
        //        return;
        //    }
        //    DeviceStatusName = combDeviceStatus.Text;


        //    try
        //    {

        //        if (BillSetting_cls.PrintSize == 1)
        //        {
        //            PrintSizeA4();
        //        }
        //        else if (BillSetting_cls.PrintSize == 2)
        //        {
        //            PrintSize1_2A4();
        //        }
        //        else
        //        {
        //            PrintSize80MM();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}




     //   private void PrintSizeA4()
     //   {
     //       

     //       BillSetting_cls cls_print = new BillSetting_cls();

     //       DataTable dt_Print = cls.Details_Maintenance(txtMaintenanceID.Text);

     //       Reportes_Form frm = new Reportes_Form();

     //       var report = new RptMaintenance_A4();
     //       report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
     //       report.SetParameterValue(0, this.Text);
     //       report.SetParameterValue(1, DeviceStatusName);

     //       frm.crystalReportViewer1.ReportSource = report;
     //       frm.crystalReportViewer1.Refresh();


     //       #region"Crystal Report | Printing | Default Printer"
     //       PrintDialog dialog = new PrintDialog();
     //       if (dialog.ShowDialog() == DialogResult.OK)
     //       {
     //           int copies = dialog.PrinterSettings.Copies;
     //           int fromPage = dialog.PrinterSettings.FromPage;
     //           int toPage = dialog.PrinterSettings.ToPage;
     //           bool collate = dialog.PrinterSettings.Collate;

     //           report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //           report.PrintToPrinter(copies, collate, fromPage, toPage);
     //       }


     //       //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //       //report.PrintToPrinter(1, false, 0, 0);
     //       #endregion




     //       

     //      // frm.ShowDialog();


     //   }

     //   private void PrintSize1_2A4()
     //   {
     //    

     //       BillSetting_cls cls_print = new BillSetting_cls();

     //       DataTable dt_Print = cls.Details_Maintenance(txtMaintenanceID.Text);

     //       Reportes_Form frm = new Reportes_Form();

     //       var report = new RptMaintenance();
     //       report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
     //       report.SetParameterValue(0, this.Text);
     //       report.SetParameterValue(1, DeviceStatusName);

     //       frm.crystalReportViewer1.ReportSource = report;
     //       frm.crystalReportViewer1.Refresh();
     //       #region"Crystal Report | Printing | Default Printer"
     //       PrintDialog dialog = new PrintDialog();
     //       if (dialog.ShowDialog() == DialogResult.OK)
     //       {
     //           int copies = dialog.PrinterSettings.Copies;
     //           int fromPage = dialog.PrinterSettings.FromPage;
     //           int toPage = dialog.PrinterSettings.ToPage;
     //           bool collate = dialog.PrinterSettings.Collate;

     //           report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //           report.PrintToPrinter(copies, collate, fromPage, toPage);
     //       }


     //       //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //       //report.PrintToPrinter(1, false, 0, 0);
     //       #endregion

     //      // frm.ShowDialog();


     //   }


     //   private void PrintSize80MM()
     //   {
     //

     //       BillSetting_cls cls_print = new BillSetting_cls();

     //       DataTable dt_Print = cls.Details_Maintenance(txtMaintenanceID.Text);

     //       Reportes_Form frm = new Reportes_Form();

     //       var report = new RptMaintenance80MM();
     //       report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
     //       report.SetParameterValue(0, this.Text);
     //       report.SetParameterValue(1, DeviceStatusName);

     //       frm.crystalReportViewer1.ReportSource = report;
     //       frm.crystalReportViewer1.Refresh();
     //       #region"Crystal Report | Printing | Default Printer"
     //       PrintDialog dialog = new PrintDialog();
     //       if (dialog.ShowDialog() == DialogResult.OK)
     //       {
     //           int copies = dialog.PrinterSettings.Copies;
     //           int fromPage = dialog.PrinterSettings.FromPage;
     //           int toPage = dialog.PrinterSettings.ToPage;
     //           bool collate = dialog.PrinterSettings.Collate;

     //           report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //           report.PrintToPrinter(copies, collate, fromPage, toPage);
     //       }


     //       //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //       //report.PrintToPrinter(1, false, 0, 0);
     //       #endregion

     //      // frm.ShowDialog();


     //   }

       
    }
}
