﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class RbtProdecuts_Action_frm : Form
    {
        public RbtProdecuts_Action_frm()
        {
            InitializeComponent();
        }
        ProdecutAction_cls cls = new ProdecutAction_cls();
        private void BalanseItemes_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);

                // FILL Combo Store item
                Store_cls Store_cls = new Store_cls();
                combStore.DataSource = Store_cls.Search_Stores("");
                combStore.DisplayMember = "StoreName";
                combStore.ValueMember = "StoreID";
                combStore.Text = "";
                txtStoreID.Text = "";
                //================================================================================
                // FILL Combo Store item
                Prodecut_cls Prodecut_cls = new Prodecut_cls();
                combProdecut.DataSource = Prodecut_cls.Search_Prodecuts("");
                combProdecut.DisplayMember = "ProdecutName";
                combProdecut.ValueMember = "ProdecutID";
                combProdecut.Text = "";
                txtProdecutID.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }









 

        

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = new DataTable();

                if (combProdecut.Text.Trim() == "" || txtProdecutID.Text.Trim() == "")
                {
                    combProdecut.Focus();
                    return;
                }  
                    if (combStore.Text.Trim()==""||txtStoreID.Text.Trim()=="")
                {


                    
                         dt.Dispose();
                    dt = null;
                    dt = cls.Search_Firist_Main(txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "رصيد أول المدة", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());
                    }



                    dt.Dispose();
                    dt = null;
                    //===================================================================================================================================================================================================================================================================================================================================================================
                    dt = cls.Search_Bay_Main(txtProdecutID.Text,D1.Value,D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()),"مشتريات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString() );
                        if (Convert.ToDouble(dt.Rows[i]["ReturnQuantity"].ToString()  )> 0)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مرتجع مشتريات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["ReturnQuantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["ReturnTotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    //=====Sales==============================================================================================================================================================================================================================================================================================================================================================
                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_Sales_Main(txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مبيعات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());
                        if (Convert.ToDouble(dt.Rows[i]["ReturnQuantity"].ToString()) > 0)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مرتجع مبيعات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["ReturnQuantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["ReturnTotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    //=====Search_ChangeQuantity_Main==============================================================================================================================================================================================================================================================================================================================================================
                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_ChangeQuantity_Main(txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dt.Rows[i]["SumType"].ToString()) ==true)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تسوية بالزيادة", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                        else
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تسوية بالعجز", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    //=====Search_ChangeQuantity_Main==============================================================================================================================================================================================================================================================================================================================================================
                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_ChangeStore_Main(txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dt.Rows[i]["SumType"].ToString()) == true)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تحويل إلي مخزن", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                        else
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تحويل من مخزن", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    

                }
                else
                {
                    //**********************************************************************************************************************************************************************

                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_Firist_Main(txtStoreID.Text,txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "رصيد أول المدة", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());
                    }
                    //===================================================================================================================================================================================================================================================================================================================================================================
                    dt = cls.Search_Bay_Main(txtStoreID.Text,txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مشتريات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());
                        if (Convert.ToDouble(dt.Rows[i]["ReturnQuantity"].ToString()) > 0)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مرتجع مشتريات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["ReturnQuantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["ReturnTotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    //=====Sales==============================================================================================================================================================================================================================================================================================================================================================
                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_Sales_Main(txtStoreID.Text, txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مبيعات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());
                        if (Convert.ToDouble(dt.Rows[i]["ReturnQuantity"].ToString()) > 0)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "مرتجع مبيعات", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["ReturnQuantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["ReturnTotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    //=====Search_ChangeQuantity_Main==============================================================================================================================================================================================================================================================================================================================================================
                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_ChangeQuantity_Main(txtStoreID.Text, txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dt.Rows[i]["SumType"].ToString()) == true)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تسوية بالزيادة", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                        else
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تسوية بالعجز", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                    //=====Search_ChangeQuantity_Main==============================================================================================================================================================================================================================================================================================================================================================
                    dt.Dispose();
                    dt = null;
                    dt = cls.Search_ChangeStore_Main(txtStoreID.Text, txtProdecutID.Text, D1.Value, D2.Value);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dt.Rows[i]["SumType"].ToString()) == true)
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تحويل إلي مخزن", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                        else
                        {
                            AddRowDgv(Convert.ToDateTime(dt.Rows[i]["MyDate"].ToString()), "تحويل من مخزن", dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), "0", dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["EmpName"].ToString());

                        }
                    }

                }


                lblCount.Text = DGV1.Rows.Count.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }

        private void AddRowDgv(DateTime MyDate, string TypeAction, string ProdecutName, string Unit, string Quantity, string Price, string Vat, string TotalPrice, string StoreName,string EmpName)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["MyDate"].Value = MyDate.ToShortDateString();
                DGV1.Rows[lastRows].Cells["TypeAction"].Value = TypeAction;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["Vat"].Value = Vat;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;
                DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;
                DGV1.Rows[lastRows].Cells["EmpName"].Value = EmpName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void combStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;
                txtStoreID.Text = combStore.SelectedValue.ToString();

            }
            catch
            {

                txtStoreID.Text = "";

            }
        }

        private void combStore_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;

                if (combStore.Text == "")
                {
                    txtStoreID.Text = "";
                }
                else
                {
                    txtStoreID.Text = combStore.SelectedValue.ToString();
                }
            }
            catch
            {
                txtStoreID.Text = "";
            }
        }

        private void combProdecut_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combProdecut.BackColor = Color.White;
                txtProdecutID.Text = combProdecut.SelectedValue.ToString();

            }
            catch
            {

                txtProdecutID.Text = "";

            }
        }

        private void combProdecut_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combProdecut.BackColor = Color.White;

                if (combProdecut.Text == "")
                {
                    txtProdecutID.Text = "";
                }
                else
                {
                    txtProdecutID.Text = combProdecut.SelectedValue.ToString();
                }
            }
            catch
            {
                txtProdecutID.Text = "";
            }
        }
    }
}
