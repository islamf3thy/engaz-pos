﻿using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq.Expressions;
using DevExpress.XtraBars;
using ByStro.DAL;
using ByStro.Clases;
using System.Collections.ObjectModel;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;

namespace ByStro.Forms
{
    public partial class frm_DeliveryOrder : frm_Master 
    {
        DeliveryOrder order;
        internal override bool IsNew
        {
            get => base.IsNew; set
            {
                base.IsNew = value;


                if (value == true)
                    this.Name = "امر توصيل جديد";
                else
                    this.Name = string.Format("تعديل امر توصيل رقم {0}  - {1} ", order.ID , gridLookUpEdit1 .Text);
                btn_Delete.Visibility = value ? BarItemVisibility.Never : BarItemVisibility.Always;
            }
        }
    
        public frm_DeliveryOrder(int? ID = null)
        {
            InitializeComponent();
            
            if (ID == null)
            {
                New();
            }
            else
            {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                order  = db.DeliveryOrders.Where(x => x.ID == ID).SingleOrDefault();
                if (order == null)
                    New();
            }
            GetData();
        } 
        BindingList<TempOrderInvoicesObject> OrderInvoices;

        class TempOrderInvoicesObject
        {
            public int InvoiceID { get; set; }
            public int ItemCount { get; set; }
            public double Amount { get; set; }
            public bool  Selected { get; set; }

        }
        
        private bool ValidData()
        {

            if (gridLookUpEdit1.EditValue.ValidAsIntNonZero() == false )
            {
                gridLookUpEdit1.ErrorText = "";
                gridLookUpEdit1.Focus();
                return false;
            }
           
            if (checkEdit1.Checked && dateEdit2.DateTime.Year <1950)
            {
                dateEdit2.ErrorText = Clases.MasterClass.EmptyFieldErrorText;
                dateEdit2.Focus();
                return false;
            }  
            if((gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected)
              .Count() <= 0)
            {
                XtraMessageBox.Show("يجب اختيار  فاتوره واحده علي الاقل ");
                return false;
            }
            return true;
        }
        public override void OpenList()
        {
            base.OpenList();
            new frm_DeliveryOrderList(true).Show();
        }
        public override void Save()
        {

            if (!ValidData()) { return; }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew)
            {
                order = new DeliveryOrder();
                db.DeliveryOrders.InsertOnSubmit(order);
            }
            else
            {
                order = db.DeliveryOrders.Where(s => s.ID == order.ID).First();
            }
            SetData();
            db.SubmitChanges();

            //db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.DeliveryOrderCollect ).ToString() && x.VoucherID == order.ID.ToString()));
            //db.SubmitChanges();

            if(order.HasReturned)
            {
                var Orders = db.DeliveryOrders.FirstOrDefault(d => d.ID == order.ID && d.DriverID == Convert.ToInt32(gridLookUpEdit1.EditValue));
                var stringList = "";
                if (Orders != null)
                    stringList = Orders.Orders; 
                var ordersInvoicesList = stringList.Split(new char[] { Convert.ToChar(",") }, options: StringSplitOptions.RemoveEmptyEntries);
                var invoices = db.Inv_Invoices.Where(x =>
                  x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice &&
               ordersInvoicesList.Contains(x.ID.ToString()));
                foreach (var item in invoices)
                {
                    var TreasuryMovement =  db.TreasuryMovements.SingleOrDefault(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.SalesInvoice).ToString()
                    && x.VoucherID == item.ID.ToString());
                    TreasuryMovement.Income = TreasuryMovement. Credit =  TreasuryMovement.Debit; 
                    item.Paid = item.Net; 
                    db.SubmitChanges();
                }

            }

            //var msg = string.Format("تحصيل امر توصيل رقم {0} من سائق {1}", order.ID, gridLookUpEdit1.Text);
            //TreasuryMovement movement = new TreasuryMovement()
            //{
            //    ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
            //    AccountID = order.DriverID .ToString(),
            //    ISCusSupp = "0",
            //    VoucherID = order.ID.ToString(),
            //    Type = ((int)MasterClass.TreasuryMovementTypes.DeliveryOrderCollect).ToString(),
            //    VoucherType = "تحصيل امر توصيل",
            //    VoucherDate = order.ReturnDate ,
            //    Description = msg,
            //    Income = order.RecivedAmount ,
            //    Export = 0,
            //    Credit = 0,
            //    Debit = 0,
            //    ISShow = true,
            //    UserAdd = Properties.Settings.Default.UserID,
            //    Remark = msg,
            //};
            //db.TreasuryMovements.InsertOnSubmit(movement);
            db.SubmitChanges();
            base.Save();
        }
        public override void Delete()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew) return;
         
            if (XtraMessageBox.Show(text: "تأكيد الحذف", caption: "هل تريد الحذف ؟", buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.DeliveryOrderCollect).ToString() && x.VoucherID == order.ID.ToString()));
                db.SubmitChanges();

                order = db.DeliveryOrders.Where(c => c.ID == order.ID).First();
                db.DeliveryOrders.DeleteOnSubmit(order);
                db.SubmitChanges();
                base.Delete();
                New();
            }
        }
        public override void New()
        {
            order = new DeliveryOrder()
            {
               OutDate = DateTime.Now
            };
            GetData();
            base.New();
            IsNew = true;
            ChangesMade = false;
        }


        bool IsLoadingData;


        private void GetData()
        {
            IsLoadingData = true;
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);


            gridLookUpEdit1.EditValue = order.DriverID;
            spinEdit1.EditValue = order.OrdersAmount;
            textEdit2.Text = order.OrdersCount.ToString();
            textEdit1.Text = order.ID .ToString();
            dateEdit1.EditValue = order.OutDate;
            spinEdit2.EditValue = order.RecivedAmount;
            dateEdit2.EditValue = order.ReturnDate;
            checkEdit1.Checked  =order.HasReturned ;
            IsLoadingData = false;
        }

        void SetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            order.DriverID = gridLookUpEdit1.EditValue.ToInt();
            order.HasReturned = checkEdit1.Checked;
            var ordersIDs = "";
            (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).ToList().Where (x=>x.Selected ).ToList().ForEach(i =>
            {
                ordersIDs += i.InvoiceID + ",";
            });

            order.Orders = ordersIDs;
            order.OrdersAmount = Convert.ToDouble(spinEdit1.EditValue);
            order.OrdersCount = (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected).Count();
            order.OutDate = dateEdit1.DateTime;
            order.RecivedAmount = Convert.ToDouble(spinEdit2.EditValue);
            order.ReturnDate = dateEdit2.EditValue as DateTime?; 
        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            gridLookUpEdit1.Properties.DataSource = db.Drivers.Select(x => new { x.Name, x.ID, State = (db.DeliveryOrders.Where(d => d.HasReturned == false  && d.DriverID == x.ID).Count() > 0) }).ToList();
            gridLookUpEdit1.Properties.ValueMember = "ID";
            gridLookUpEdit1.Properties.DisplayMember = "Name";
            var view = gridLookUpEdit1.Properties.View;
            view.PopulateColumns(gridLookUpEdit1.Properties.DataSource);
            view.Columns["State"].Caption = "الحاله";
            view.Columns["State"].ColumnEdit = new RepositoryItemTextEdit();
            view.Columns["ID"].Visible = false;
            view.Columns["Name"].Caption = "الاسم";
            view.RowCellStyle += View_RowCellStyle;
            view.CustomColumnDisplayText += View_CustomColumnDisplayText;
            base.RefreshData();
        }
        private void View_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "State")
            {
                e.DisplayText = (e.Value is bool d && d == true) ? "مشغول" : "متاح";
            }
        }

        private void View_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "State")
            {
                if (e.CellValue is bool d && d == true)
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
                }
                else
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;

                }
            }
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            layoutControl1.AllowCustomization = false;
            btn_Print.Visibility = BarItemVisibility.Never;

         
            gridView1.CellValueChanged += GridView1_CellValueChanged;
            gridView1.RowCountChanged += GridView1_RowCountChanged;
            #region DataChanged
            spinEdit1 .EditValueChanged += DataChanged;
            spinEdit2.EditValueChanged += DataChanged;
            dateEdit1.EditValueChanged += DataChanged;
            dateEdit2.EditValueChanged += DataChanged;
            #endregion 
        }

        private void GridView1_RowCountChanged(object sender, EventArgs e)
        {
            GridView1_CellValueChanged(sender, null);
        }

        private void GridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if(gridView1.DataSource != null)
            {

                spinEdit1 .Text = (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected)
                .Sum(x => x.Amount).ToString();
            textEdit2.Text = (gridView1.DataSource as BindingList<TempOrderInvoicesObject>).Where(x => x.Selected)
              .Count().ToString();
            }
            else
            {
                spinEdit1.Text = textEdit2.Text = "0";
            }
        }

        private void gridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
           if(gridLookUpEdit1.EditValue.ValidAsIntNonZero() == false)
            {
                gridControl1.DataSource = null;
                return;
            }
            if (IsNew)
            {
                var oldOrders = db.DeliveryOrders.FirstOrDefault(d => d.DriverID == Convert.ToInt32(gridLookUpEdit1.EditValue));
                var stringList = "";
                if (oldOrders != null)
                    stringList = oldOrders.Orders;
                var ordersInvoicesList = stringList.Split(new char[] {Convert.ToChar( ",") },options: StringSplitOptions.RemoveEmptyEntries);
                gridControl1.DataSource = db.Inv_Invoices .Where(x =>
                x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice&&
                x.Driver  == Convert.ToInt32(gridLookUpEdit1.EditValue) &&
               !ordersInvoicesList.Contains(x.ID .ToString())
               ).Select(x=>new TempOrderInvoicesObject 
               {
                   Amount = x.Net ,
                   InvoiceID =(int)x.ID,
                   ItemCount = db.Inv_InvoiceDetails  .Where(d=>d.InvoiceID  == x.ID   ).Count()
               });
            }
            else
            {
                var oldOrders = db.DeliveryOrders.FirstOrDefault(d => d.ID == order.ID && d.DriverID == Convert.ToInt32(gridLookUpEdit1.EditValue));
                var stringList = "";
                if (oldOrders != null)
                    stringList = oldOrders.Orders;


                var ordersInvoicesList = stringList.Split(new char[] { Convert.ToChar(",") }, options: StringSplitOptions.RemoveEmptyEntries);
                gridControl1.DataSource = db.Inv_Invoices.Where(x =>
                  x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice &&
               ordersInvoicesList.Contains(x.ID.ToString())
               ).Select(x => new TempOrderInvoicesObject
               {
                   Amount = x.Net ,
                   InvoiceID = (int)x.ID ,
                   ItemCount = db.Inv_InvoiceDetails   .Where(d => d.InvoiceID    == x.ID  ).Count() ,
                   Selected =true 
               });
            }
            gridView1.Columns["Selected"].OptionsColumn.AllowEdit = (IsNew);
            gridView1.Columns["Amount"].Caption = "قيمه الفاتوره";
            gridView1.Columns["Selected"].Caption = "توصيل";
            gridView1.Columns["InvoiceID"].Caption = "الفاتوره";
            gridView1.Columns["ItemCount"].Caption = "عدد الاصناف";
            gridView1.Columns["Amount"].OptionsColumn.AllowEdit =
            gridView1.Columns["InvoiceID"].OptionsColumn.AllowEdit =
            gridView1.Columns["ItemCount"].OptionsColumn.AllowEdit = false;
        }
        public override void Print()
        {
           
        }

        private void textEdit2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            spinEdit2.Enabled = checkEdit1.Checked;
            dateEdit2 .Enabled = checkEdit1.Checked;
            if(IsLoadingData == false && checkEdit1.Checked)
            {
                spinEdit2.EditValue = spinEdit1.EditValue;
                dateEdit2.EditValue = DateTime.Now;

            }
        }
    }
}
