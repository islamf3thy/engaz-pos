﻿using System;
using System.Data;


class Store_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Stores()
    {
        return Execute_SQL("select ISNULL (MAX(StoreID)+1,1) from Stores", CommandType.Text);
    }

    // Insert
    public void Insert_Stores(int StoreID, String StoreName, String phone, String phone2, String Place, String Note)
    {
        Execute_SQL("insert into Stores(StoreID ,StoreName  ,phone ,phone2 ,Place ,Note )Values (@StoreID ,@StoreName ,@phone ,@phone2 ,@Place ,@Note )", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@StoreName", SqlDbType.NVarChar, StoreName),

        Parameter("@phone", SqlDbType.NVarChar, phone),
        Parameter("@phone2", SqlDbType.NVarChar, phone2),
        Parameter("@Place", SqlDbType.NVarChar, Place),
        Parameter("@Note", SqlDbType.NVarChar, Note));
    }

    //Update
    public void Update_Stores(int StoreID, String StoreName, String phone, String phone2, String Place, String Note)
    {
        Execute_SQL("Update Stores Set StoreID=@StoreID ,StoreName=@StoreName,phone=@phone ,phone2=@phone2 ,Place=@Place ,Note=@Note  where StoreID=@StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@StoreName", SqlDbType.NVarChar, StoreName),
        Parameter("@phone", SqlDbType.NVarChar, phone),
        Parameter("@phone2", SqlDbType.NVarChar, phone2),
        Parameter("@Place", SqlDbType.NVarChar, Place),
        Parameter("@Note", SqlDbType.NVarChar, Note));
    }

    //Delete
    public void Delete_Stores(int StoreID)
    {
        Execute_SQL("Delete  From Stores where StoreID=@StoreID", CommandType.Text,
         Parameter("@StoreID", SqlDbType.Int, StoreID));
    }

    //Details ID
    public DataTable Details_Stores(int StoreID)
    {
        return ExecteRader("Select *  From Stores where StoreID=@StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }

    //Details ID
    public DataTable NameSearch_Stores(String StoreName)
    {
        return ExecteRader("Select StoreName  From Stores where StoreName=@StoreName", CommandType.Text,
        Parameter("@StoreName", SqlDbType.NVarChar, StoreName));
    }

    //Search
    public DataTable Search_Stores(String Search)
    {
        String SQL = "Select *  from Stores where convert(nvarchar, StoreID) + StoreName + phone like '%' + @Search + '%'";
        if (DataAccessLayer.TypeUser == false)
        {
            SQL = @"SELECT        dbo.Stores.*, dbo.StoreUsers.UserID FROM dbo.StoreUsers INNER JOIN
                         dbo.Stores ON dbo.StoreUsers.StoreID = dbo.Stores.StoreID where dbo.StoreUsers.UserID=" + ByStro.Properties.Settings.Default.UserID + " and   StoreName  like '%' + @Search + '%'";
        }
        return ExecteRader(SQL, CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    //Search
    public DataTable Search_StoreUsers(int UserID)
    {
        return ExecteRader(@" SELECT dbo.Stores.*, dbo.StoreUsers.UserID
    FROM            dbo.Stores INNER JOIN
                             dbo.StoreUsers ON dbo.Stores.StoreID = dbo.StoreUsers.StoreID where dbo.StoreUsers.UserID = @UserID", CommandType.Text,
        Parameter("@UserID", SqlDbType.Int, UserID));
    }







    // Delete 
    //Details ID
    public DataTable Details_Store_Prodecut(string StoreID)
    {
        return ExecteRader("Select StoreID  from Store_Prodecut Where StoreID=@StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }



    //=======================================================================================================================================================================================

    // Insert
    public void Insert_StoreUsers(int StoreID, int UserID)
    {
        Execute_SQL2("insert into StoreUsers(StoreID ,UserID )Values (@StoreID ,@UserID )", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@UserID", SqlDbType.Int, UserID));
    }

    //Delete
    public void Delete_StoreUsers(int StoreID)
    {
        Execute_SQL("Delete  From StoreUsers where StoreID=@StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }

    //Details ID
    public DataTable Details_StoreUsers(int StoreID)
    {
        return ExecteRader(@"SELECT        dbo.StoreUsers.StoreID, dbo.StoreUsers.UserID, dbo.UserPermissions.EmpName
FROM            dbo.UserPermissions INNER JOIN
                         dbo.StoreUsers ON dbo.UserPermissions.ID = dbo.StoreUsers.UserID Where StoreID=@StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }









}

