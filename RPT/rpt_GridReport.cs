﻿using System; 
using DevExpress.XtraReports.UI; 
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using ByStro.Clases;
using System.Windows.Forms;

namespace ByStro.RPT
{
 
 
    public partial class rpt_GridReport : rpt_Master
    {
        public rpt_GridReport()
        {
            InitializeComponent();
        }
        public static void Print(GridControl gridControl1, string ReportName, string filter, bool rtll)
        {

            GridView view = gridControl1.MainView as GridView;


            PrintableComponentLink pcLink1 = new PrintableComponentLink();
            pcLink1.Component = gridControl1;
            rpt_GridReport rpt = new  rpt_GridReport();
            rpt.LoadTemplete();
         

           rpt.printableComponentContainer1.PrintableComponent = pcLink1;
            try
            {
               rpt.xrPictureBox1.Image = MasterClass.GetImageFromByte(CurrentSession.Company.Imge .ToArray());
            }
            catch (Exception)
            {
            }

            rpt.lbl_filter.Text = filter;
            rpt.lbl_PrinDate.Text = DateTime.Now.ToString();
            rpt.lbl_UserName.Text = CurrentSession.user.UserName;

           rpt.lbl_PrinDate.Text = DateTime.Now.ToString();
           rpt.lbl_UserName.Text = CurrentSession.user.UserName;
           rpt.lbl_companyName.Text = CurrentSession.Company.CombanyName;
        
             
            rpt.lbl_rptName.Text = ReportName;
            rpt.ShowPreview();
            //switch (CurrentSession.user.WhenPrintShowMode)
            //{
            //    case 0:  break;
            //    case 1: rpt.PrintDialog(); break;
            //    case 2: rpt.Print(); break;
            //    default: rpt.PrintDialog(); break;
            //}
        }
    }

}

