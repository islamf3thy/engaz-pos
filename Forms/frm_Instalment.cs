﻿using System;
using System.Collections.Generic; 
using System.Data; 
using System.Linq; 
using System.Windows.Forms;
using DevExpress.XtraEditors; 
using System.Linq.Expressions;
using DevExpress.XtraBars;
using ByStro.DAL;
using ByStro.Clases;
using System.Collections.ObjectModel;

namespace ByStro.Forms
{

    public partial class frm_Instalment : frm_Master
    {
        Instalment instalment;
        internal override bool IsNew { get => base.IsNew; set { base.IsNew = value;


                if (value == true) 
                    this.Name = "ملف قسط جديد";
                else
                    this.Name = string.Format("ملف قسط رقم -{0} - {1} عميل : {2}",instalment.ID , instalment.Code ,LookUpEdit_Customer.Text );
                btn_Delete.Visibility = value ? BarItemVisibility.Never : BarItemVisibility.Always;
            }
        }
         
        public frm_Instalment(int? ID = null)
        {
            InitializeComponent();
          
            if (ID == null)
            {
                New();
            }
            else
            {
                DBDataContext  db = new DBDataContext(Properties.Settings.Default.Connection_String);
                instalment = db.Instalments.Where(x => x.ID == ID).SingleOrDefault();
                if (instalment == null)
                    New();
            }
            GetData(); 
        }
        public frm_Instalment(int  CustomerID, int InvoiceID )
        {
            InitializeComponent();
          
            New();
            GetData();
            LookUpEdit_Customer.EditValue = CustomerID;
            LookUpEdit_Invoice.EditValue = InvoiceID;

        }


        public bool CheckIfCodeIsUsed(string Code)
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var obj = from s in db.Instalments where this.TextEdit_ID.Text != s.ID.ToString() && s.Code == Code select s.ID;
            return (obj.Count() > 0);
        }

        private bool ValidData()
        {

            if (TextEdit_ID.EditValue == null || String.IsNullOrEmpty(TextEdit_ID.Text))
            {
                TextEdit_ID.ErrorText = "";
                TextEdit_ID.Focus();
                return false;
            }
            if ( String.IsNullOrEmpty(TextEdit_Code.Text)  )
            {
                TextEdit_Code.ErrorText = Clases.MasterClass.EmptyFieldErrorText ;
                TextEdit_Code.Focus();
                return false;
            }
            if (DateEdit_Date.DateTime .Year <1910)
            {
                DateEdit_Date.ErrorText = Clases.MasterClass.EmptyFieldErrorText ;
                DateEdit_Date.Focus();
                return false;
            }
            if (LookUpEdit_Customer.EditValue == null || String.IsNullOrEmpty(LookUpEdit_Customer.Text))
            {
                LookUpEdit_Customer.ErrorText = Clases.MasterClass.EmptyFieldErrorText ;
                LookUpEdit_Customer.Focus();
                return false;
            }
            if (DateEdit_StartDate.EditValue == null || String.IsNullOrEmpty(DateEdit_StartDate.Text))
            {
                DateEdit_StartDate.ErrorText = Clases.MasterClass.EmptyFieldErrorText ;
                DateEdit_StartDate.Focus();
                return false;
            }
          
            if (SpinEdit_Amount.EditValue is decimal d && d <=0)
            {
                SpinEdit_Amount.ErrorText = Clases.MasterClass.EmptyFieldErrorText ;
                SpinEdit_Amount.Focus();
                return false;
            }
            if (SpinEdit_InstalmentsCount.EditValue == null || String.IsNullOrEmpty(SpinEdit_InstalmentsCount.Text))
            {
                SpinEdit_InstalmentsCount.ErrorText = Clases.MasterClass.EmptyFieldErrorText ;
                SpinEdit_InstalmentsCount.Focus();
                return false;
            }
  
        
         
            return true;
        }
        public override void Save()
        {
            
            if (!ValidData()) { return; }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew)
            {
                instalment = new Instalment();
                db.Instalments.InsertOnSubmit(instalment);
            }
            else
            {
                instalment = db.Instalments.Where(s => s.ID == instalment.ID).First();
            }
            SetData();
            db.SubmitChanges();

            db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.InstalmentAdvancedPay ).ToString() && x.VoucherID == instalment.ID.ToString()));
            db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.InstalmentBenfites).ToString() && x.VoucherID == instalment.ID.ToString()));
            db.SubmitChanges();
            if (instalment.AdvancedPay > 0)
            {
                TreasuryMovement movement = new TreasuryMovement()
                {
                    ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
                    AccountID = LookUpEdit_Customer.EditValue.ToInt().ToString(),
                    ISCusSupp = "Cus",
                    VoucherID = instalment.ID.ToString(),
                    Type = ((int)MasterClass.TreasuryMovementTypes.InstalmentAdvancedPay).ToString(),
                    VoucherType = "مقدم  قسط",
                    VoucherDate = instalment.Date,
                    Description = string.Format("مقدم  قسط رقم {0} لعميل {1}", instalment.ID, LookUpEdit_Customer.Text),
                    Income = instalment.AdvancedPay,
                    Export = 0,
                    Credit = instalment.AdvancedPay,
                    Debit = 0,
                    ISShow = true,
                    UserAdd = Properties.Settings.Default.UserID,
                    Remark = string.Format("مقدم  قسط رقم {0} لعميل {1}", instalment.ID, LookUpEdit_Customer.Text),
                };
                db.TreasuryMovements.InsertOnSubmit(movement);
                db.SubmitChanges();

            }
            if (instalment.BenfitesValue > 0)
            {
                TreasuryMovement movement = new TreasuryMovement()
                {
                    ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
                    AccountID = LookUpEdit_Customer.EditValue.ToInt().ToString(),
                    ISCusSupp = "Cus",
                    VoucherID = instalment.ID.ToString(),
                    Type = ((int)MasterClass.TreasuryMovementTypes.InstalmentBenfites).ToString(),
                    VoucherType = "فوائد قسط",
                    VoucherDate = instalment.Date,
                    Description = string.Format("فوائد  قسط  لعميل {1}", instalment.ID, LookUpEdit_Customer.Text),
                    Income = 0,
                    Export = 0,
                    Credit =0,
                    Debit =  instalment.BenfitesValue * (instalment.Net - instalment.AdvancedPay ),
                    ISShow = true,
                    UserAdd = Properties.Settings.Default.UserID,
                    Remark = string.Format("فوائد  قسط  لعميل {1}", instalment.ID, LookUpEdit_Customer.Text),
                };
                db.TreasuryMovements.InsertOnSubmit(movement);
                db.SubmitChanges();

            } 

            foreach (var item in gridView1.DataSource as Collection<Instalment_Detail>)
                item.InstalmentID = instalment.ID;
            FixedDB.SubmitChanges();
            base.Save();
        }
        public override void OpenList()
        {
            base.OpenList();
            if (frm_InstalmentList.Instance.IsDisposed == false)
                frm_InstalmentList.Instance.Close();
            frm_InstalmentList.Instance.instalmentInstaice = this;
            frm_InstalmentList.Instance.ShowDialog();

        }
        public override void Delete()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew) return;
            var paid = db.Instalment_Details.Where(x => x.InstalmentID == instalment.ID && x.IsPaid == true).Count();

            if(paid > 0)
            {
                XtraMessageBox.Show(text: "لا يمكن الحذف", caption: "لا يمكن حذف القسط بعد سداد دفعات منه ", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Exclamation);
                return;
            }
            if (XtraMessageBox.Show(text:"تأكيد الحذف" , caption: "هل تريد الحذف ؟" , buttons : MessageBoxButtons.YesNo , icon : MessageBoxIcon.Warning) == DialogResult.Yes)
            {

                db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.InstalmentAdvancedPay).ToString() && x.VoucherID == instalment.ID.ToString()));
                db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.InstalmentBenfites).ToString() && x.VoucherID == instalment.ID.ToString()));
                db.SubmitChanges();
                instalment = db.Instalments.Where(c => c.ID == instalment.ID).First();
                db.Instalment_Details.DeleteAllOnSubmit(db.Instalment_Details.Where(x => x.InstalmentID == instalment.ID));
                db.SubmitChanges();
                db.Instalments.DeleteOnSubmit(instalment); 
                db.SubmitChanges();
                base.Delete();
                New();
            }
        }
        DAL.DBDataContext FixedDB = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

        void SetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            instalment.Code = TextEdit_Code.Text ;
            instalment.Date = DateEdit_Date.DateTime;
            instalment.Customer = LookUpEdit_Customer.EditValue.ToInt();
            instalment.StartDate = DateEdit_StartDate.DateTime;
            var multe = 1;
            if (comboBoxEdit1.SelectedIndex == 1) multe = 30;
            if (comboBoxEdit1.SelectedIndex == 2) multe = 365;
            instalment.InstallPeriod  =Convert.ToInt32( Convert.ToDouble( spinEdit_InstallPeriod.EditValue) * multe  );
            instalment.Net = SpinEdit_Net.EditValue.ToInt();
            instalment.AdvancedPay = spinEdit_AdvancedPay.EditValue.ToInt();
            instalment.Amount =Convert.ToDouble( SpinEdit_Amount.EditValue);
            instalment.InstalmentsCount = SpinEdit_InstalmentsCount.EditValue.ToInt();
            instalment.Notes = MemoEdit_Notes.Text;
            instalment.Invoice = LookUpEdit_Invoice.EditValue as int?;
            instalment.BenfitesValue = Convert.ToDouble(spinEdit_BenfietValu.EditValue);
        }

        public override void New()
        {
              
            DBDataContext FixedDB = new DBDataContext(Properties.Settings.Default.Connection_String);

            var maxCode = FixedDB.Instalments.Max(x => x.Code);
            if(string.IsNullOrEmpty(maxCode))
                maxCode  = "INST00000000";
            instalment = new Instalment()
            {
                Code = MasterClass. GetNextNumberInString(maxCode),
                Date = DateTime .Now,
                StartDate  = DateTime.Now,
                InstallPeriod  = 365,
                InstalmentsCount = 2,
            };
            base.New();
            IsNew = true;
            ChangesMade = false;
        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            LookUpEdit_Customer.Properties.DataSource = db.Customers.Select(x => new { ID =x.CustomerID,Name =  x.CustomerName }).ToList();
            base.RefreshData();
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            layoutControl1.AllowCustomization = false;
           // btn_Print.Visibility = BarItemVisibility.Never;



            LookUpEdit_Customer.Properties.DisplayMember = "Name";
            LookUpEdit_Customer.Properties.ValueMember = "ID";

            gridView1.Columns[nameof( nameInstance.ID)].Visible =
            gridView1.Columns[nameof(nameInstance.InstalmentID)].Visible =
            gridView1.Columns[nameof(nameInstance.InstalmentID)].OptionsColumn.ShowInCustomizationForm =
            gridView1.Columns[nameof(nameInstance.ID)].OptionsColumn.ShowInCustomizationForm = false;


            gridView1.Columns[nameof(nameInstance.DatePaid)].OptionsColumn.AllowFocus = 
            gridView1.Columns[nameof(nameInstance.IsPaid)].OptionsColumn.AllowFocus = false;

            gridView1.Columns[nameof(nameInstance.Amount)].Caption = "المبلغ";
            gridView1.Columns[nameof(nameInstance.DatePaid)].Caption = "تاريخ السداد";
            gridView1.Columns[nameof(nameInstance.IsPaid)].Caption = "مسدده";
            gridView1.Columns[nameof(nameInstance.number)].Caption = "تسلسل";
            gridView1.Columns[nameof(nameInstance.DueDate)].Caption = "تاريخ الاستحقاق";

            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            gridView1.FocusedColumnChanged += GridView1_FocusedColumnChanged;
            gridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            spinEdit_InstallPeriod.Enter += SpinEdit1_Enter;
            spinEdit_InstallPeriod.Leave += SpinEdit1_Leave;
            #region DataChanged

            TextEdit_ID.EditValueChanged += DataChanged;
            TextEdit_Code.EditValueChanged += DataChanged;
            DateEdit_Date.EditValueChanged += DataChanged;
            LookUpEdit_Customer.EditValueChanged += DataChanged;
            DateEdit_StartDate.EditValueChanged += DataChanged;
            spinEdit_InstallPeriod .EditValueChanged += DataChanged;
            SpinEdit_Amount.EditValueChanged += DataChanged;
            SpinEdit_InstalmentsCount.EditValueChanged += DataChanged;
            CheckEdit_ISPaid.EditValueChanged += DataChanged;
            MemoEdit_Notes.EditValueChanged += DataChanged;
            LookUpEdit_Invoice.EditValueChanged += DataChanged;
            #endregion 
        }

        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            GridView1_FocusedColumnChanged(sender, new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs(gridView1.FocusedColumn, gridView1.FocusedColumn));
        }

        Instalment_Detail nameInstance = new DAL.Instalment_Detail();

        private void GridView1_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            var row = gridView1.GetFocusedRow() as Instalment_Detail;
            if (row == null) return; // safe call 
            gridView1.Columns[nameof(nameInstance.Amount)].OptionsColumn.AllowFocus = !row.IsPaid;

        }

        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = e.Row as Instalment_Detail ;
            var list = ((Collection<Instalment_Detail>)gridView1.DataSource);

            if (row.Amount == 0)
            {
                e.Valid = false;
                gridView1.SetColumnError(gridView1.Columns[nameof(nameInstance.Amount)], "يجب ان يكون المبلغ اكبر من صفر ");
            }
            if (row.DatePaid < DateEdit_StartDate.DateTime )
            {
                e.Valid = false;
                gridView1.SetColumnError(gridView1.Columns[nameof(nameInstance.DatePaid)], "لا يمكن لتاريخ الاستحقاق ان يكون قبل موعد اول قسط");
            }else if(row.number > 1)
            {
                if(row.DueDate <= list[row.number-2].DueDate)
                {
                    e.Valid = false;
                    gridView1.SetColumnError(gridView1.Columns[nameof(nameInstance.DatePaid)], "يجب ان يكون تاريخ الاستحقاق اكبر من تاريخ استحقاق القسط السابق");
                }
            }
        }

        bool IsSpinEdit1Focesed;
        private void SpinEdit1_Leave(object sender, EventArgs e)
        {
            IsSpinEdit1Focesed = false;
        }

        private void SpinEdit1_Enter(object sender, EventArgs e)
        {
            IsSpinEdit1Focesed = true;

        }
        bool IsLoadingData;
        private void GetData()
        {
            IsLoadingData = true;
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            gridControl1.DataSource =
                FixedDB.Instalment_Details.Select<DAL.Instalment_Detail, DAL.Instalment_Detail>((Expression<Func<DAL.Instalment_Detail, DAL.Instalment_Detail>>)
                (x => x)).Where(x => x.InstalmentID == instalment.ID);
            TextEdit_ID.EditValue = instalment.ID;
            TextEdit_Code.EditValue = instalment.Code;
            DateEdit_Date.EditValue = instalment.Date;
            LookUpEdit_Customer.EditValue = instalment.Customer;
            DateEdit_StartDate.EditValue = instalment.StartDate;
            SpinEdit_Amount.EditValue = instalment.Amount;
            spinEdit_BenfietValu.EditValue = instalment.BenfitesValue;
            SpinEdit_Net.EditValue = (instalment.Net );
            SpinEdit_InstalmentsCount.EditValue = instalment.InstalmentsCount;
            spinEdit_AdvancedPay.EditValue = instalment.AdvancedPay;
            spinEdit_TotalBeforeAdvanced.EditValue = instalment.Net - instalment.AdvancedPay;
            CheckEdit_ISPaid.EditValue = instalment.ISPaid;
            MemoEdit_Notes.EditValue = instalment.Notes;
            LookUpEdit_Invoice.EditValue = instalment.Invoice;


            if (instalment.InstallPeriod > 400)
            {
                spinEdit_InstallPeriod.EditValue = instalment.InstallPeriod / 365;
                comboBoxEdit1.SelectedIndex = 2;
            }
            else  if(instalment.InstallPeriod > 120)
            {
                spinEdit_InstallPeriod.EditValue = instalment.InstallPeriod / 30;
                comboBoxEdit1.SelectedIndex = 1;
            }
            else  
            {
                spinEdit_InstallPeriod.EditValue = instalment.InstallPeriod ;
                comboBoxEdit1.SelectedIndex = 0;
            }




            IsLoadingData = false; 
        }
        public override void Print()
        {
            if (IsNew == true) return;
            Print(new List<int>() { instalment.ID });

        }
        public static void Print(List<int> ids)
        {


            

            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var installment = (from h in db.Instalments
                           from p in db.Customers.Where(x => x.CustomerID == h.Customer ).DefaultIfEmpty() 
                           where ids.Contains( h.ID ) 
                           select new
                           {
                               h.ID ,
                               Customer = p.CustomerName,
                               h.Date   ,
                               Benfite =  h.BenfitesValue  ,
                               Total = h.Amount /(1+h.BenfitesValue),
                               h.AdvancedPay ,
                               h.Amount ,
                               NetText = "",
                               h.Net,
                               Paid =db.Instalment_Details.Where(x=>x.InstalmentID == h.ID && x.IsPaid ==true ).Sum(x=>(double?) x.Amount ),
                               Remains = h.Amount- db.Instalment_Details.Where(x => x.InstalmentID == h.ID && x.IsPaid == true).Sum(x => (double?)x.Amount),
                               Period  = h.InstallPeriod ,
                               Count = h.InstalmentsCount ,
                               p.Address,
                               Mobile = p.Phone2,
                               p.Phone, 
                               Details = db.Instalment_Details .Where(x => x.InstalmentID  == h.ID).Select(x => new
                               {
                                   Index = 0,
                                   x.DueDate ,
                                   x.Amount ,
                                   PayDate =  x.DatePaid ,
                                   IsPaid = x.IsPaid 
                               }).ToList()
                           }).ToList();

        

            installment.ForEach(x =>
            {
                x.Set(i => i.NetText, MasterClass.ConvertMoneyToText(x.Amount.ToString()));
                int Index = 1;
                x.Details.ForEach(p =>
                    p.Set(i => i.Index, Index++));

            });
            RPT.rpt_Instalment .Print(installment);

        }

        private void SpinEdit_InstalmentsCount_EditValueChanged(object sender, EventArgs e)
        {
            if (IsLoadingData) return;
            var list = ((Collection<Instalment_Detail>)gridView1.DataSource);
            if (list == null) return;
            var count = SpinEdit_InstalmentsCount.EditValue.ToInt();
            if (list.Count() > count)
            {
                for (int i = list.Count - 1; i >=  count; i--)
                {
                    var row = list[i];
                    if (row.IsPaid == false)
                        list.Remove(row);
                }
            }
            else if (list.Count() < count)
            {
                for (int i = list.Count; i < count; i++)
                {
                    list.Add(new Instalment_Detail()
                    {
                        InstalmentID = instalment.ID,
                        number = i+1, 

                    });

                }
            }
            if (list.Count > 0)
            {
                var paidAmount = list.Where(x => x.IsPaid == true).Sum(x => x.Amount);
                var amountToDist = Convert.ToDouble(SpinEdit_Amount.EditValue) - paidAmount;
                var amountEach = amountToDist / list.Where(x => x.IsPaid == false).Count();
                var multe = 1;
                if (comboBoxEdit1.SelectedIndex == 1) multe = 30;
                if (comboBoxEdit1.SelectedIndex == 2) multe = 365;
                instalment.InstallPeriod = Convert.ToInt32(Convert.ToDouble(spinEdit_InstallPeriod.EditValue) * multe);

                var payPeriod = instalment.InstallPeriod / SpinEdit_InstalmentsCount.EditValue.ToInt();
                int i = 0;
                if(amountEach > 0)
                    foreach (var item in list.Where(x => x.IsPaid == false))
                    {
                        item.Amount = amountEach;
                        item.DueDate = DateEdit_StartDate.DateTime.AddDays(i);
                        i = i + payPeriod;
                    }
            }


        }

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            var editor = sender as CalcEdit ;
            if(IsSpinEdit1Focesed  )
            {
              //  DateEdit_EndDate.DateTime = DateEdit_StartDate.DateTime.AddDays(editor.EditValue.ToInt()); 
            }
        }

        private void spinEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (Convert.ToDouble(e.NewValue) <= 0)
                e.Cancel = true;

        }

        private void DateEdit_EndDate_EditValueChanged(object sender, EventArgs e)
        {
            if (IsSpinEdit1Focesed== false )
            {
              //  spinEdit1.EditValue = (DateEdit_EndDate.DateTime - DateEdit_StartDate.DateTime).TotalDays;
            }
        }

        private void DateEdit_StartDate_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (IsLoadingData) return;
            var dateEdit = sender as DateEdit;
            var newValue = e.NewValue as DateTime?;
            if (newValue == null || newValue.HasValue == false || newValue.Value.Year < 1950)
                e.NewValue = DateTime.Now;

            //if(DateEdit_StartDate.DateTime >= DateEdit_EndDate.DateTime)
            //{
            //    e.Cancel = true;
            //    XtraMessageBox.Show("لايمكن لتاريخ بدايه القسط ان يكون بعد تاريخ اخر قسط");
            //}
        }

        private void LookUpEdit_Customer_EditValueChanged(object sender, EventArgs e)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (LookUpEdit_Customer.EditValue.ValidAsIntNonZero()) {
                var invoices = db.Inv_Invoices .Where(x =>x.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice&&
                x.PartID  == Convert.ToInt32(LookUpEdit_Customer.EditValue) &&( x.Net - x.Paid) > 0 && 
                db.Instalments.Select(i=>i.Invoice??0 ).Contains((int)x.ID  ) == false ).Select(x => new {  x.ID , x.Date , x.Net ,Remaining = (x.Net - x.Paid) }).ToList();

                LookUpEdit_Invoice.Properties.DataSource = invoices;
                LookUpEdit_Invoice.Properties.DisplayMember = "ID";
                LookUpEdit_Invoice.Properties.ValueMember = "ID";
            }
            else
                LookUpEdit_Invoice.Properties.DataSource = null;
        }

        private void LookUpEdit_Invoice_EditValueChanged(object sender, EventArgs e)
        {
            if(LookUpEdit_Invoice.EditValue.ValidAsIntNonZero() && IsLoadingData == false ) {
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

                var invoice = db.Inv_Invoices .Where(x => x.ID  == Convert.ToInt32(LookUpEdit_Invoice.EditValue)).SingleOrDefault();
                if (invoice != null)
                    SpinEdit_Net.EditValue = invoice.Net  - invoice.Paid ;
            }
        }

        private void spinEdit_BenfietValu_EditValueChanged(object sender, EventArgs e)
        {
            if (IsLoadingData) return;
            spinEdit_TotalBeforeAdvanced.EditValue =Convert.ToDouble(SpinEdit_Net.EditValue)- Convert.ToDouble(spinEdit_AdvancedPay.EditValue);
            SpinEdit_Amount.EditValue = Convert.ToDouble(spinEdit_TotalBeforeAdvanced.EditValue) + (Convert.ToDouble(spinEdit_TotalBeforeAdvanced.EditValue) * Convert.ToDouble(spinEdit_BenfietValu.EditValue));
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SpinEdit_InstalmentsCount_EditValueChanged(null, null);
        }
    }
}

