﻿using ByStro.BL;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class capital_frm : Form
    {
        public capital_frm()
        {
            InitializeComponent();
        }
        Capital_cls cls = new Capital_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);

        }


        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
          
        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtDebit.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

  

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

              
                //"Conditional"
                if (txtDebit.Text.Trim() == "")
                {
                    txtDebit.BackColor = Color.Pink;
                    txtDebit.Focus();
                    return;
                }

        

                txtID.Text = cls.MaxID_Capital();
                 cls.Insert_Capital(txtID.Text, txtNotes.Text, txtDebit.Text.Trim(),txtCredit.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // "Conditional"
                if (txtDebit.Text.Trim() == "")
                {
                    txtDebit.BackColor = Color.Pink;
                    txtDebit.Focus();
                    return;
                }

                //"Conditional"



                cls.Update_Capital(txtID.Text, txtNotes.Text, txtDebit.Text.Trim(), txtCredit.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                                // fill combBox combParent
            
                //=============================================================================================

                txtID.Text = cls.MaxID_Capital();
                txtDebit.Text = "";
                txtCredit.Text = "";
                txtNotes.Text ="";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtDebit.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                if (Mass.Delete() == true)
                {
                    cls.Delete_Capital( txtID.Text);
                    btnNew_Click(null, null); 
                    LoadData = true;
                }  
             
            
            



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
           

            try
            {
                capital_Search_frm frm = new capital_Search_frm();
                frm.ShowDialog(this);
                if (frm.loaddata==false)
                {
                    return;
                }

                DataTable dt = cls.Details_Capital( frm.DGV1.CurrentRow.Cells["Id"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtID.Text = Dr["Id"].ToString();
                txtDebit.Text = Dr["Debit"].ToString();
                txtCredit.Text = Dr["Credit"].ToString();
                txtNotes.Text =Dr["Notes"].ToString();
     
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

     

      



  

       












    }
}
