﻿using ByStro.PL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.File
{
    public partial class frmStart : Form
    {
        public frmStart()
        {
            InitializeComponent();
        }
        DataAccessLayer dal = new DataAccessLayer();
        private void frmStart_Load(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.Connection_String == "")
                {
                    MainForm();
                }
                else
                {
                    if (dal.connSQLServer.State != ConnectionState.Open)
                    {
                        dal.connSQLServer.Open();
                    }
                    LoginUser_Form frm = new LoginUser_Form();
                    frm.Show();
                }


            }
            catch
            {
                MainForm();
            }
            finally
            {
                this.Hide();
                dal.connSQLServer.Close();
            }

        }


        private void MainForm()
        {
                Main_frm main = new Main_frm();
                main.CS_01.Visible = false;
                main.CS_10.Visible = false;
                main.CS_15.Visible = false;
                main.CS_19.Visible = false;
                main.CS_24.Visible = false;
                main.CS_29.Visible = false;
                main.CS_42.Visible = false;
                main.CS_46.Visible = false;
                main.CS_53.Visible = false;
                main.CS_58.Visible = false;
                main.CS_75.Visible = false;
                main.Username.Visible = false;
                main.Fills.Visible = true;
                main.Show();
        }


    }
}
