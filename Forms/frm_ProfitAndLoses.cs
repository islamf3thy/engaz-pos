﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.DAL;
using ByStro.Clases;
using DevExpress.LookAndFeel;

namespace ByStro.Forms
{
    public partial class frm_ProfitAndLoses : DevExpress.XtraEditors.XtraForm
    {
        public frm_ProfitAndLoses()
        {
            InitializeComponent();
        }

        private void dateEdit2_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEdit1.DateTime.Year < 1950 || dateEdit2.DateTime.Year < 1950 || dateEdit2.DateTime < dateEdit1.DateTime)
            {
               // XtraMessageBox.Show("يرجي اختيار التاريخ بشكل صحيح");
                return;
            }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            var SalesDetails = db.Inv_InvoiceDetails.
                Where(sd => db.Inv_Invoices.Where(s => s.Date.Date >= dateEdit1.DateTime.Date && s.Date.Date <= dateEdit2.DateTime.Date &&
                  s.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice).Select(s => s.ID).Contains(sd.InvoiceID)); 
            var TotalSoldItems = SalesDetails.Sum(sd =>(double ?) sd.TotalPrice)??0;
            var TotalSoldCost = SalesDetails.Sum(sd =>(double?) sd.TotalCostValue )??0;

            var SalesRetirnDetails = db.Inv_InvoiceDetails.
              Where(sd => db.Inv_Invoices.Where(s => s.Date.Date >= dateEdit1.DateTime.Date && s.Date.Date <= dateEdit2.DateTime.Date &&
                s.InvoiceType == (int)MasterClass.InvoiceType.SalesReturn ).Select(s => s.ID).Contains(sd.InvoiceID));
            var TotalReturnItems = SalesRetirnDetails.Sum(sd => (double?)sd.TotalPrice)??0;
            var TotalReturnCost = SalesRetirnDetails.Sum(sd => (double?)sd.TotalCostValue)??0;

            textEdit1.Text = TotalSoldItems.ToString();
            textEdit2.Text = TotalSoldCost.ToString();
            textEdit7.Text = TotalReturnItems.ToString();
            textEdit8.Text = TotalReturnCost.ToString();

            var SalesProfite = ((TotalSoldItems - TotalReturnItems) - (TotalSoldCost - TotalReturnCost));
            textEdit3.Text = SalesProfite.ToString();

            var TotalExpenses = db.Expenses.Where(s => s.MyDate .Value .Date >= dateEdit1.DateTime.Date && s.MyDate.Value.Date <= dateEdit2.DateTime.Date ).Sum(sd => sd.ExpenseValue) ?? 0;
            textEdit5.Text = TotalExpenses.ToString();
            var NetProfite = SalesProfite - TotalExpenses;
            textEdit6.Text = (Math.Abs(NetProfite)).ToString();
            textEdit4.Text = (NetProfite >= 0) ? "ربح" : "خساره";

            textEdit4.BackColor = (NetProfite >= 0)? DXSkinColors.FillColors.Success : DXSkinColors.FillColors.Danger;

        }

        private void frm_ProfitAndLoses_Load(object sender, EventArgs e)
        {
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit2_EditValueChanged);
            this.dateEdit2.EditValueChanged += new System.EventHandler(this.dateEdit2_EditValueChanged);
            dateEdit1.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            dateEdit2.DateTime = DateTime.Now.Date;
        }
    }
}