﻿using System; 
using System.Data; 
using System.Linq; 
using System.Windows.Forms; 
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls;
using System.Linq.Expressions;

namespace ByStro.Forms
{
    
    public partial class frm_Inv_Size : DevExpress.XtraEditors.XtraForm
    { 
        DAL.DBDataContext objDataContext = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
         
        public frm_Inv_Size()
        {
            InitializeComponent();
            gridView1.InvalidRowException += gridView1_InvalidRowException;
            gridView1.ValidateRow += gridView1_ValidateRow;
            gridView1.CellValueChanged += gridView1_CellValueChanged;
            gridView1.RowUpdated += gridView1_RowUpdated;
        }

        private void Inv_Color_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = objDataContext.Inv_Sizes.Select<DAL.Inv_Size, DAL.Inv_Size>((Expression<Func<DAL.Inv_Size, DAL.Inv_Size>>)(x => x));
            gridView1.Columns[0].Visible = false;
            gridView1.Columns[1].Caption ="الاحجام";
        }
        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            try
            {
                ColumnView clm = sender as ColumnView;
                if (clm.GetRowCellValue(e.RowHandle, "Name").ToString() == string.Empty)
                {
                    e.Valid = false;
                    clm.SetColumnError(clm.Columns["Name"], "*");
                }
            }
            catch (Exception)
            {

                e.Valid = false;
            }

        }

        private void gridView1_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            objDataContext.SubmitChanges();
        }

        private void gridView1_RowUpdated(object sender, RowObjectEventArgs e)
        {
            objDataContext.SubmitChanges();
        }
    }
}