﻿namespace ByStro.PL
{
    partial class BillSales2_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillSales2_frm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtMainID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.DGV1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitOperating = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BayPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalBayPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtStoreID = new System.Windows.Forms.TextBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtTotalPrice = new System.Windows.Forms.TextBox();
            this.combUnit = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtProdecutID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtUnitFactor = new System.Windows.Forms.TextBox();
            this.txtUnitOperating = new System.Windows.Forms.TextBox();
            this.txtBillTypeID = new System.Windows.Forms.TextBox();
            this.txtVAT = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAccountID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRestInvoise = new System.Windows.Forms.TextBox();
            this.txtpaid_Invoice = new System.Windows.Forms.TextBox();
            this.txtNetInvoice = new System.Windows.Forms.TextBox();
            this.txtTotalDescound = new System.Windows.Forms.TextBox();
            this.txtTotalInvoice = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtProdecutAvg = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtBalence = new System.Windows.Forms.TextBox();
            this.txtTotalBayPrice = new System.Windows.Forms.TextBox();
            this.combAccount = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnPayAgel = new System.Windows.Forms.Button();
            this.btnPayCash = new System.Windows.Forms.Button();
            this.txtVatValue = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMainVat = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtSumPayType = new System.Windows.Forms.ToolStripStatusLabel();
            this.DGVPayType = new System.Windows.Forms.DataGridView();
            this.PayTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Statement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStatement = new System.Windows.Forms.TextBox();
            this.txtPayValue = new System.Windows.Forms.TextBox();
            this.combPayType = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.txtCurrencyRate = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.combCurrency = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnx = new System.Windows.Forms.Button();
            this.txtQ = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave_Print = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMainID
            // 
            this.txtMainID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMainID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMainID.Location = new System.Drawing.Point(92, 31);
            this.txtMainID.Name = "txtMainID";
            this.txtMainID.ReadOnly = true;
            this.txtMainID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMainID.Size = new System.Drawing.Size(310, 28);
            this.txtMainID.TabIndex = 606;
            this.txtMainID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMainID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 21);
            this.label4.TabIndex = 605;
            this.label4.Text = "رقم السند :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 604;
            this.label2.Text = "البيـــان :";
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNote.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.Location = new System.Drawing.Point(92, 90);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(901, 27);
            this.txtNote.TabIndex = 601;
            // 
            // D1
            // 
            this.D1.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.D1.CustomFormat = "dd/MM/yyyy";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.D1.Location = new System.Drawing.Point(92, 4);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(154, 28);
            this.D1.TabIndex = 603;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 21);
            this.label1.TabIndex = 602;
            this.label1.Text = "تاريخ السند :";
            // 
            // DGV1
            // 
            this.DGV1.AllowUserToAddRows = false;
            this.DGV1.AllowUserToDeleteRows = false;
            this.DGV1.AllowUserToResizeColumns = false;
            this.DGV1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV1.ColumnHeadersHeight = 24;
            this.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.ProdecutID,
            this.ProdecutName,
            this.Unit,
            this.Quantity,
            this.XQuantity,
            this.CurrencyPrice,
            this.CurrencyVat,
            this.CurrencyTotal,
            this.StoreName,
            this.StoreID,
            this.UnitFactor,
            this.UnitOperating,
            this.BayPrice,
            this.TotalBayPrice,
            this.MainVat,
            this.CurrencyID,
            this.Price,
            this.Vat,
            this.TotalPrice,
            this.CurrencyName,
            this.CurrencyRate});
            this.DGV1.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGV1.EnableHeadersVisualStyles = false;
            this.DGV1.GridColor = System.Drawing.Color.Silver;
            this.DGV1.Location = new System.Drawing.Point(48, 55);
            this.DGV1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGV1.MultiSelect = false;
            this.DGV1.Name = "DGV1";
            this.DGV1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGV1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGV1.RowHeadersVisible = false;
            this.DGV1.RowHeadersWidth = 55;
            this.DGV1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.DGV1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DGV1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGV1.RowTemplate.Height = 23;
            this.DGV1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV1.Size = new System.Drawing.Size(752, 290);
            this.DGV1.TabIndex = 609;
            this.DGV1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV1_CellEndEdit);
            this.DGV1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGV1_EditingControlShowing);
            this.DGV1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGV1_KeyDown);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // ProdecutID
            // 
            this.ProdecutID.DataPropertyName = "ProdecutID";
            this.ProdecutID.HeaderText = "كود الصنف";
            this.ProdecutID.Name = "ProdecutID";
            this.ProdecutID.ReadOnly = true;
            this.ProdecutID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProdecutID.Visible = false;
            // 
            // ProdecutName
            // 
            this.ProdecutName.DataPropertyName = "ProdecutName";
            this.ProdecutName.FillWeight = 256.7848F;
            this.ProdecutName.HeaderText = "اسم الصنف";
            this.ProdecutName.Name = "ProdecutName";
            this.ProdecutName.ReadOnly = true;
            this.ProdecutName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProdecutName.Width = 290;
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "Unit";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Unit.DefaultCellStyle = dataGridViewCellStyle3;
            this.Unit.FillWeight = 96.3198F;
            this.Unit.HeaderText = "الوحدة";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            this.Quantity.FillWeight = 79.34152F;
            this.Quantity.HeaderText = "الكمية";
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Quantity.Width = 80;
            // 
            // XQuantity
            // 
            this.XQuantity.DataPropertyName = "XQuantity";
            this.XQuantity.HeaderText = "XQuantity";
            this.XQuantity.Name = "XQuantity";
            this.XQuantity.Visible = false;
            // 
            // CurrencyPrice
            // 
            this.CurrencyPrice.DataPropertyName = "CurrencyPrice";
            this.CurrencyPrice.HeaderText = "سعر بالعملة";
            this.CurrencyPrice.Name = "CurrencyPrice";
            this.CurrencyPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyPrice.Width = 101;
            // 
            // CurrencyVat
            // 
            this.CurrencyVat.DataPropertyName = "CurrencyVat";
            this.CurrencyVat.HeaderText = "Vat %";
            this.CurrencyVat.Name = "CurrencyVat";
            this.CurrencyVat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyVat.Width = 55;
            // 
            // CurrencyTotal
            // 
            this.CurrencyTotal.DataPropertyName = "CurrencyTotal";
            this.CurrencyTotal.HeaderText = "اجمالي بالعملة";
            this.CurrencyTotal.Name = "CurrencyTotal";
            this.CurrencyTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyTotal.Width = 121;
            // 
            // StoreName
            // 
            this.StoreName.DataPropertyName = "StoreName";
            this.StoreName.HeaderText = "المخزن";
            this.StoreName.Name = "StoreName";
            this.StoreName.ReadOnly = true;
            this.StoreName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StoreName.Width = 150;
            // 
            // StoreID
            // 
            this.StoreID.DataPropertyName = "StoreID";
            this.StoreID.HeaderText = "StoreID";
            this.StoreID.Name = "StoreID";
            this.StoreID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StoreID.Visible = false;
            // 
            // UnitFactor
            // 
            this.UnitFactor.DataPropertyName = "UnitFactor";
            this.UnitFactor.HeaderText = "UnitFactor";
            this.UnitFactor.Name = "UnitFactor";
            this.UnitFactor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UnitFactor.Visible = false;
            // 
            // UnitOperating
            // 
            this.UnitOperating.DataPropertyName = "UnitOperating";
            this.UnitOperating.HeaderText = "UnitOperating";
            this.UnitOperating.Name = "UnitOperating";
            this.UnitOperating.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UnitOperating.Visible = false;
            // 
            // BayPrice
            // 
            this.BayPrice.DataPropertyName = "BayPrice";
            this.BayPrice.HeaderText = "سعر التكلفة";
            this.BayPrice.Name = "BayPrice";
            this.BayPrice.ReadOnly = true;
            this.BayPrice.Visible = false;
            // 
            // TotalBayPrice
            // 
            this.TotalBayPrice.DataPropertyName = "TotalBayPrice";
            this.TotalBayPrice.HeaderText = "اجمالي سعر التكلفة";
            this.TotalBayPrice.Name = "TotalBayPrice";
            this.TotalBayPrice.ReadOnly = true;
            this.TotalBayPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalBayPrice.Visible = false;
            // 
            // MainVat
            // 
            this.MainVat.DataPropertyName = "MainVat";
            this.MainVat.HeaderText = "الضريبة %";
            this.MainVat.Name = "MainVat";
            this.MainVat.Visible = false;
            this.MainVat.Width = 80;
            // 
            // CurrencyID
            // 
            this.CurrencyID.DataPropertyName = "CurrencyID";
            this.CurrencyID.HeaderText = "كود العملة";
            this.CurrencyID.Name = "CurrencyID";
            this.CurrencyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyID.Visible = false;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.FillWeight = 116.319F;
            this.Price.HeaderText = "سعر الوحدة";
            this.Price.Name = "Price";
            this.Price.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Price.Visible = false;
            this.Price.Width = 101;
            // 
            // Vat
            // 
            this.Vat.DataPropertyName = "Vat";
            this.Vat.HeaderText = "VAT";
            this.Vat.Name = "Vat";
            this.Vat.ReadOnly = true;
            this.Vat.Visible = false;
            this.Vat.Width = 55;
            // 
            // TotalPrice
            // 
            this.TotalPrice.DataPropertyName = "TotalPrice";
            this.TotalPrice.HeaderText = "الاجمالي";
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.ReadOnly = true;
            this.TotalPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalPrice.Visible = false;
            this.TotalPrice.Width = 121;
            // 
            // CurrencyName
            // 
            this.CurrencyName.DataPropertyName = "CurrencyName";
            this.CurrencyName.HeaderText = "العملة";
            this.CurrencyName.Name = "CurrencyName";
            this.CurrencyName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyName.Visible = false;
            // 
            // CurrencyRate
            // 
            this.CurrencyRate.DataPropertyName = "CurrencyRate";
            this.CurrencyRate.HeaderText = "سعر الصرف";
            this.CurrencyRate.Name = "CurrencyRate";
            this.CurrencyRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyRate.Visible = false;
            // 
            // txtStoreName
            // 
            this.txtStoreName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStoreName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtStoreName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStoreName.Location = new System.Drawing.Point(732, 4);
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStoreName.Size = new System.Drawing.Size(229, 28);
            this.txtStoreName.TabIndex = 624;
            this.txtStoreName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStoreName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreName_KeyDown);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(669, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 21);
            this.label6.TabIndex = 623;
            this.label6.Text = "المخزن :";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(963, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 625;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtStoreID
            // 
            this.txtStoreID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStoreID.Location = new System.Drawing.Point(950, 1);
            this.txtStoreID.Name = "txtStoreID";
            this.txtStoreID.ReadOnly = true;
            this.txtStoreID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStoreID.Size = new System.Drawing.Size(10, 28);
            this.txtStoreID.TabIndex = 626;
            this.txtStoreID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStoreID.Visible = false;
            this.txtStoreID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Location = new System.Drawing.Point(508, 30);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSearch.Size = new System.Drawing.Size(292, 28);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // txtQuantity
            // 
            this.txtQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuantity.BackColor = System.Drawing.Color.White;
            this.txtQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuantity.Location = new System.Drawing.Point(328, 30);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtQuantity.Size = new System.Drawing.Size(78, 28);
            this.txtQuantity.TabIndex = 2;
            this.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            this.txtQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQty_KeyDown);
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtTotalPrice
            // 
            this.txtTotalPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalPrice.BackColor = System.Drawing.Color.White;
            this.txtTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalPrice.Location = new System.Drawing.Point(49, 30);
            this.txtTotalPrice.Name = "txtTotalPrice";
            this.txtTotalPrice.ReadOnly = true;
            this.txtTotalPrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalPrice.Size = new System.Drawing.Size(123, 28);
            this.txtTotalPrice.TabIndex = 5;
            this.txtTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalPrice.TextChanged += new System.EventHandler(this.txtPrise_TextChanged);
            this.txtTotalPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrise_KeyDown);
            this.txtTotalPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // combUnit
            // 
            this.combUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.combUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combUnit.FormattingEnabled = true;
            this.combUnit.Location = new System.Drawing.Point(407, 30);
            this.combUnit.Name = "combUnit";
            this.combUnit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.combUnit.Size = new System.Drawing.Size(100, 29);
            this.combUnit.TabIndex = 1;
            this.combUnit.SelectedIndexChanged += new System.EventHandler(this.combUnit_SelectedIndexChanged);
            this.combUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combUnit_KeyDown);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label10.Location = new System.Drawing.Point(508, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(292, 24);
            this.label10.TabIndex = 640;
            this.label10.Text = "اسم الصنف , الباركود , للبحث F1";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Location = new System.Drawing.Point(407, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 24);
            this.label11.TabIndex = 641;
            this.label11.Text = "الوحدة";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Location = new System.Drawing.Point(328, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 24);
            this.label12.TabIndex = 642;
            this.label12.Text = "الكمية";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProdecutID
            // 
            this.txtProdecutID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProdecutID.Location = new System.Drawing.Point(950, 1);
            this.txtProdecutID.Name = "txtProdecutID";
            this.txtProdecutID.Size = new System.Drawing.Size(10, 28);
            this.txtProdecutID.TabIndex = 645;
            this.txtProdecutID.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(226, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 24);
            this.label3.TabIndex = 648;
            this.label3.Text = "سعر الوحدة";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Location = new System.Drawing.Point(49, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 24);
            this.label5.TabIndex = 656;
            this.label5.Text = "الاجمالي";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPrice
            // 
            this.txtPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrice.BackColor = System.Drawing.Color.White;
            this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrice.Location = new System.Drawing.Point(226, 30);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPrice.Size = new System.Drawing.Size(101, 28);
            this.txtPrice.TabIndex = 3;
            this.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // txtUnitFactor
            // 
            this.txtUnitFactor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUnitFactor.Location = new System.Drawing.Point(950, 1);
            this.txtUnitFactor.Name = "txtUnitFactor";
            this.txtUnitFactor.Size = new System.Drawing.Size(10, 28);
            this.txtUnitFactor.TabIndex = 657;
            this.txtUnitFactor.Visible = false;
            // 
            // txtUnitOperating
            // 
            this.txtUnitOperating.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUnitOperating.Location = new System.Drawing.Point(950, 1);
            this.txtUnitOperating.Name = "txtUnitOperating";
            this.txtUnitOperating.Size = new System.Drawing.Size(10, 28);
            this.txtUnitOperating.TabIndex = 658;
            this.txtUnitOperating.Visible = false;
            // 
            // txtBillTypeID
            // 
            this.txtBillTypeID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBillTypeID.Location = new System.Drawing.Point(950, -2);
            this.txtBillTypeID.Name = "txtBillTypeID";
            this.txtBillTypeID.ReadOnly = true;
            this.txtBillTypeID.Size = new System.Drawing.Size(10, 28);
            this.txtBillTypeID.TabIndex = 659;
            this.txtBillTypeID.Text = "Sales";
            this.txtBillTypeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBillTypeID.Visible = false;
            // 
            // txtVAT
            // 
            this.txtVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVAT.BackColor = System.Drawing.Color.White;
            this.txtVAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVAT.Location = new System.Drawing.Point(173, 30);
            this.txtVAT.Name = "txtVAT";
            this.txtVAT.ReadOnly = true;
            this.txtVAT.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVAT.Size = new System.Drawing.Size(52, 28);
            this.txtVAT.TabIndex = 4;
            this.txtVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVAT.TextChanged += new System.EventHandler(this.txtVAT_TextChanged);
            this.txtVAT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVAT_KeyDown);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Location = new System.Drawing.Point(173, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 24);
            this.label8.TabIndex = 665;
            this.label8.Text = "VAT %";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 21);
            this.label9.TabIndex = 687;
            this.label9.Text = "العميل :";
            // 
            // txtAccountID
            // 
            this.txtAccountID.Location = new System.Drawing.Point(322, -1);
            this.txtAccountID.Name = "txtAccountID";
            this.txtAccountID.Size = new System.Drawing.Size(10, 28);
            this.txtAccountID.TabIndex = 691;
            this.txtAccountID.Visible = false;
            this.txtAccountID.TextChanged += new System.EventHandler(this.txtAccountID_TextChanged);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(435, 522);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(113, 24);
            this.label19.TabIndex = 767;
            this.label19.Text = "خصم";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRestInvoise
            // 
            this.txtRestInvoise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtRestInvoise.BackColor = System.Drawing.Color.White;
            this.txtRestInvoise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRestInvoise.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtRestInvoise.ForeColor = System.Drawing.Color.Blue;
            this.txtRestInvoise.Location = new System.Drawing.Point(835, 547);
            this.txtRestInvoise.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRestInvoise.Name = "txtRestInvoise";
            this.txtRestInvoise.ReadOnly = true;
            this.txtRestInvoise.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRestInvoise.Size = new System.Drawing.Size(158, 29);
            this.txtRestInvoise.TabIndex = 765;
            this.txtRestInvoise.Text = "0.00";
            this.txtRestInvoise.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtpaid_Invoice
            // 
            this.txtpaid_Invoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtpaid_Invoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpaid_Invoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtpaid_Invoice.ForeColor = System.Drawing.Color.Green;
            this.txtpaid_Invoice.Location = new System.Drawing.Point(706, 547);
            this.txtpaid_Invoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtpaid_Invoice.Name = "txtpaid_Invoice";
            this.txtpaid_Invoice.ReadOnly = true;
            this.txtpaid_Invoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtpaid_Invoice.Size = new System.Drawing.Size(128, 29);
            this.txtpaid_Invoice.TabIndex = 766;
            this.txtpaid_Invoice.Text = "0";
            this.txtpaid_Invoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtpaid_Invoice.TextChanged += new System.EventHandler(this.txtpaid_Invoice_TextChanged);
            // 
            // txtNetInvoice
            // 
            this.txtNetInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNetInvoice.BackColor = System.Drawing.Color.White;
            this.txtNetInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtNetInvoice.ForeColor = System.Drawing.Color.Red;
            this.txtNetInvoice.Location = new System.Drawing.Point(549, 547);
            this.txtNetInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNetInvoice.Name = "txtNetInvoice";
            this.txtNetInvoice.ReadOnly = true;
            this.txtNetInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNetInvoice.Size = new System.Drawing.Size(155, 29);
            this.txtNetInvoice.TabIndex = 764;
            this.txtNetInvoice.Text = "0.00";
            this.txtNetInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNetInvoice.TextChanged += new System.EventHandler(this.txtNetInvoice_TextChanged);
            // 
            // txtTotalDescound
            // 
            this.txtTotalDescound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTotalDescound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalDescound.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtTotalDescound.Location = new System.Drawing.Point(435, 547);
            this.txtTotalDescound.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalDescound.Name = "txtTotalDescound";
            this.txtTotalDescound.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalDescound.Size = new System.Drawing.Size(113, 29);
            this.txtTotalDescound.TabIndex = 760;
            this.txtTotalDescound.Text = "0";
            this.txtTotalDescound.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.txtTotalDescound, "خصم مسموح بة");
            this.txtTotalDescound.Click += new System.EventHandler(this.txtTotalDescound_Click);
            this.txtTotalDescound.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // txtTotalInvoice
            // 
            this.txtTotalInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTotalInvoice.BackColor = System.Drawing.Color.White;
            this.txtTotalInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalInvoice.Location = new System.Drawing.Point(185, 547);
            this.txtTotalInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalInvoice.Name = "txtTotalInvoice";
            this.txtTotalInvoice.ReadOnly = true;
            this.txtTotalInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalInvoice.Size = new System.Drawing.Size(132, 29);
            this.txtTotalInvoice.TabIndex = 759;
            this.txtTotalInvoice.Text = "0.00";
            this.txtTotalInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalInvoice.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(835, 522);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label24.Size = new System.Drawing.Size(158, 24);
            this.label24.TabIndex = 755;
            this.label24.Text = "المبلغ الباقي";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(549, 522);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(155, 24);
            this.label15.TabIndex = 758;
            this.label15.Text = "صافي الفاتورة";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(706, 522);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(128, 24);
            this.label18.TabIndex = 757;
            this.label18.Text = "المبلغ المسدد";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(185, 522);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label20.Size = new System.Drawing.Size(132, 24);
            this.label20.TabIndex = 754;
            this.label20.Text = "اجمالي الفاتورة";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblCount});
            this.statusStrip1.Location = new System.Drawing.Point(3, 337);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(795, 26);
            this.statusStrip1.TabIndex = 768;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(106, 21);
            this.toolStripStatusLabel1.Text = "عدد الاصناف :";
            // 
            // lblCount
            // 
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Red;
            this.lblCount.Name = "lblCount";
            this.lblCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCount.Size = new System.Drawing.Size(21, 21);
            this.lblCount.Text = "0";
            // 
            // txtProdecutAvg
            // 
            this.txtProdecutAvg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProdecutAvg.Location = new System.Drawing.Point(950, -2);
            this.txtProdecutAvg.Name = "txtProdecutAvg";
            this.txtProdecutAvg.ReadOnly = true;
            this.txtProdecutAvg.Size = new System.Drawing.Size(10, 28);
            this.txtProdecutAvg.TabIndex = 770;
            this.txtProdecutAvg.Visible = false;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(347, -1);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(10, 28);
            this.txtPhone.TabIndex = 772;
            this.txtPhone.Visible = false;
            // 
            // txtBalence
            // 
            this.txtBalence.Location = new System.Drawing.Point(366, -1);
            this.txtBalence.Name = "txtBalence";
            this.txtBalence.ReadOnly = true;
            this.txtBalence.Size = new System.Drawing.Size(10, 28);
            this.txtBalence.TabIndex = 773;
            this.txtBalence.Visible = false;
            // 
            // txtTotalBayPrice
            // 
            this.txtTotalBayPrice.Location = new System.Drawing.Point(390, -1);
            this.txtTotalBayPrice.Name = "txtTotalBayPrice";
            this.txtTotalBayPrice.ReadOnly = true;
            this.txtTotalBayPrice.Size = new System.Drawing.Size(10, 28);
            this.txtTotalBayPrice.TabIndex = 776;
            this.txtTotalBayPrice.Visible = false;
            // 
            // combAccount
            // 
            this.combAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combAccount.FormattingEnabled = true;
            this.combAccount.Location = new System.Drawing.Point(92, 59);
            this.combAccount.Name = "combAccount";
            this.combAccount.Size = new System.Drawing.Size(309, 29);
            this.combAccount.TabIndex = 777;
            this.combAccount.SelectedIndexChanged += new System.EventHandler(this.combAccount_SelectedIndexChanged);
            this.combAccount.TextChanged += new System.EventHandler(this.combAccount_TextChanged);
            this.combAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combAccount_KeyDown_1);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::ByStro.Properties.Resources.icons8_Plus_30;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(408, 59);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 25);
            this.button3.TabIndex = 689;
            this.toolTip1.SetToolTip(this.button3, "اضافة عميل جديد");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button5.BackgroundImage = global::ByStro.Properties.Resources.Search_20;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(449, 7);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(60, 33);
            this.button5.TabIndex = 607;
            this.toolTip1.SetToolTip(this.button5, "البحث في الفواتير السابقة F5");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnPayAgel
            // 
            this.btnPayAgel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPayAgel.BackColor = System.Drawing.Color.Yellow;
            this.btnPayAgel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPayAgel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPayAgel.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPayAgel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayAgel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayAgel.Location = new System.Drawing.Point(449, 45);
            this.btnPayAgel.Name = "btnPayAgel";
            this.btnPayAgel.Size = new System.Drawing.Size(60, 39);
            this.btnPayAgel.TabIndex = 814;
            this.btnPayAgel.Text = "اجل";
            this.toolTip1.SetToolTip(this.btnPayAgel, "البحث في الفواتير السابقة F5");
            this.btnPayAgel.UseVisualStyleBackColor = false;
            this.btnPayAgel.Click += new System.EventHandler(this.btnPayAgel_Click);
            // 
            // btnPayCash
            // 
            this.btnPayCash.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPayCash.BackColor = System.Drawing.Color.Yellow;
            this.btnPayCash.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPayCash.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPayCash.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPayCash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayCash.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayCash.Location = new System.Drawing.Point(515, 45);
            this.btnPayCash.Name = "btnPayCash";
            this.btnPayCash.Size = new System.Drawing.Size(124, 39);
            this.btnPayCash.TabIndex = 815;
            this.btnPayCash.Text = "نقدي";
            this.toolTip1.SetToolTip(this.btnPayCash, "البحث في الفواتير السابقة F5");
            this.btnPayCash.UseVisualStyleBackColor = false;
            this.btnPayCash.Click += new System.EventHandler(this.btnPayCash_Click);
            // 
            // txtVatValue
            // 
            this.txtVatValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtVatValue.BackColor = System.Drawing.Color.White;
            this.txtVatValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVatValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtVatValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtVatValue.Location = new System.Drawing.Point(318, 547);
            this.txtVatValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVatValue.Name = "txtVatValue";
            this.txtVatValue.ReadOnly = true;
            this.txtVatValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVatValue.Size = new System.Drawing.Size(116, 29);
            this.txtVatValue.TabIndex = 780;
            this.txtVatValue.Text = "0";
            this.txtVatValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVatValue.Click += new System.EventHandler(this.txtVatValue_Click);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(318, 522);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(116, 24);
            this.label16.TabIndex = 779;
            this.label16.Text = "اجمالي الضريبة";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainVat
            // 
            this.txtMainVat.Location = new System.Drawing.Point(215, 6);
            this.txtMainVat.Name = "txtMainVat";
            this.txtMainVat.ReadOnly = true;
            this.txtMainVat.Size = new System.Drawing.Size(10, 28);
            this.txtMainVat.TabIndex = 781;
            this.txtMainVat.Text = "0";
            this.txtMainVat.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(189, 117);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(809, 400);
            this.tabControl1.TabIndex = 782;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.DGV1);
            this.tabPage1.Controls.Add(this.txtMainVat);
            this.tabPage1.Controls.Add(this.txtPrice);
            this.tabPage1.Controls.Add(this.txtTotalPrice);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtVAT);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtQuantity);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.combUnit);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtSearch);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.statusStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(801, 366);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "الاصناف";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.button18);
            this.panel1.Controls.Add(this.button19);
            this.panel1.Controls.Add(this.button21);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(43, 334);
            this.panel1.TabIndex = 782;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.White;
            this.button17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button17.BackgroundImage")));
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button17.Location = new System.Drawing.Point(3, 282);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(37, 56);
            this.button17.TabIndex = 5;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Visible = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.White;
            this.button16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button16.BackgroundImage")));
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button16.Location = new System.Drawing.Point(3, 26);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(37, 56);
            this.button16.TabIndex = 4;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Visible = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.White;
            this.button18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button18.BackgroundImage")));
            this.button18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button18.Location = new System.Drawing.Point(3, 218);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(37, 56);
            this.button18.TabIndex = 7;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.White;
            this.button19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button19.BackgroundImage")));
            this.button19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button19.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button19.Location = new System.Drawing.Point(3, 90);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(37, 56);
            this.button19.TabIndex = 6;
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.White;
            this.button21.BackgroundImage = global::ByStro.Properties.Resources._5;
            this.button21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button21.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button21.Location = new System.Drawing.Point(3, 154);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(37, 56);
            this.button21.TabIndex = 8;
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(801, 366);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "انواع الدفع F4";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.statusStrip2);
            this.groupBox2.Controls.Add(this.DGVPayType);
            this.groupBox2.Controls.Add(this.txtStatement);
            this.groupBox2.Controls.Add(this.txtPayValue);
            this.groupBox2.Controls.Add(this.combPayType);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(795, 360);
            this.groupBox2.TabIndex = 781;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "انواع الدفع  ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.button10);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(3, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(43, 307);
            this.panel2.TabIndex = 789;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.White;
            this.button9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button9.BackgroundImage")));
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button9.Location = new System.Drawing.Point(3, 181);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(37, 56);
            this.button9.TabIndex = 7;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.White;
            this.button10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button10.BackgroundImage")));
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button10.Location = new System.Drawing.Point(3, 53);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(37, 56);
            this.button10.TabIndex = 6;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.White;
            this.button11.BackgroundImage = global::ByStro.Properties.Resources._5;
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button11.Location = new System.Drawing.Point(3, 117);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(37, 56);
            this.button11.TabIndex = 8;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Location = new System.Drawing.Point(538, 24);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(254, 24);
            this.label23.TabIndex = 788;
            this.label23.Text = "نوع الدفع";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Location = new System.Drawing.Point(399, 24);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(138, 24);
            this.label22.TabIndex = 787;
            this.label22.Text = "المبلغ";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Location = new System.Drawing.Point(50, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(348, 24);
            this.label25.TabIndex = 786;
            this.label25.Text = "البيان";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip2
            // 
            this.statusStrip2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.txtSumPayType});
            this.statusStrip2.Location = new System.Drawing.Point(3, 331);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(789, 26);
            this.statusStrip2.TabIndex = 785;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(83, 21);
            this.toolStripStatusLabel2.Text = "الاجمالي :";
            // 
            // txtSumPayType
            // 
            this.txtSumPayType.BackColor = System.Drawing.Color.Transparent;
            this.txtSumPayType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumPayType.ForeColor = System.Drawing.Color.Red;
            this.txtSumPayType.Name = "txtSumPayType";
            this.txtSumPayType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSumPayType.Size = new System.Drawing.Size(21, 21);
            this.txtSumPayType.Text = "0";
            // 
            // DGVPayType
            // 
            this.DGVPayType.AllowUserToAddRows = false;
            this.DGVPayType.AllowUserToDeleteRows = false;
            this.DGVPayType.AllowUserToResizeColumns = false;
            this.DGVPayType.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FloralWhite;
            this.DGVPayType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.DGVPayType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVPayType.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPayType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DGVPayType.ColumnHeadersHeight = 24;
            this.DGVPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGVPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PayTypeName,
            this.PayTypeID,
            this.CurrencyPrice2,
            this.Statement,
            this.CurrencyID2,
            this.CurrencyName2,
            this.CurrencyRate2,
            this.PayValue});
            this.DGVPayType.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.DefaultCellStyle = dataGridViewCellStyle9;
            this.DGVPayType.EnableHeadersVisualStyles = false;
            this.DGVPayType.GridColor = System.Drawing.Color.Silver;
            this.DGVPayType.Location = new System.Drawing.Point(49, 74);
            this.DGVPayType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGVPayType.MultiSelect = false;
            this.DGVPayType.Name = "DGVPayType";
            this.DGVPayType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGVPayType.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.DGVPayType.RowHeadersVisible = false;
            this.DGVPayType.RowHeadersWidth = 55;
            this.DGVPayType.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.DGVPayType.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.DGVPayType.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVPayType.RowTemplate.Height = 23;
            this.DGVPayType.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVPayType.Size = new System.Drawing.Size(742, 256);
            this.DGVPayType.TabIndex = 784;
            this.DGVPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPayType_CellEndEdit);
            // 
            // PayTypeName
            // 
            this.PayTypeName.DataPropertyName = "PayTypeName";
            this.PayTypeName.HeaderText = "نوع الدفع";
            this.PayTypeName.Name = "PayTypeName";
            this.PayTypeName.ReadOnly = true;
            this.PayTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeName.Width = 252;
            // 
            // PayTypeID
            // 
            this.PayTypeID.DataPropertyName = "PayTypeID";
            this.PayTypeID.HeaderText = "PayTypeID";
            this.PayTypeID.Name = "PayTypeID";
            this.PayTypeID.ReadOnly = true;
            this.PayTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeID.Visible = false;
            // 
            // CurrencyPrice2
            // 
            this.CurrencyPrice2.HeaderText = "السعر بالعملة";
            this.CurrencyPrice2.Name = "CurrencyPrice2";
            this.CurrencyPrice2.Width = 140;
            // 
            // Statement
            // 
            this.Statement.DataPropertyName = "Statement";
            this.Statement.HeaderText = "البيان";
            this.Statement.Name = "Statement";
            this.Statement.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Statement.Width = 348;
            // 
            // CurrencyID2
            // 
            this.CurrencyID2.HeaderText = "CurrencyID2";
            this.CurrencyID2.Name = "CurrencyID2";
            this.CurrencyID2.Visible = false;
            // 
            // CurrencyName2
            // 
            this.CurrencyName2.HeaderText = "العملة";
            this.CurrencyName2.Name = "CurrencyName2";
            // 
            // CurrencyRate2
            // 
            this.CurrencyRate2.HeaderText = "سعر التعادل";
            this.CurrencyRate2.Name = "CurrencyRate2";
            // 
            // PayValue
            // 
            this.PayValue.DataPropertyName = "Debit";
            this.PayValue.HeaderText = "السعر بعد التحويل";
            this.PayValue.Name = "PayValue";
            this.PayValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayValue.Width = 135;
            // 
            // txtStatement
            // 
            this.txtStatement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatement.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtStatement.ForeColor = System.Drawing.Color.Black;
            this.txtStatement.Location = new System.Drawing.Point(50, 49);
            this.txtStatement.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStatement.Size = new System.Drawing.Size(348, 29);
            this.txtStatement.TabIndex = 783;
            this.txtStatement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStatement.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStatement_KeyDown);
            // 
            // txtPayValue
            // 
            this.txtPayValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPayValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtPayValue.ForeColor = System.Drawing.Color.Black;
            this.txtPayValue.Location = new System.Drawing.Point(399, 49);
            this.txtPayValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPayValue.Name = "txtPayValue";
            this.txtPayValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPayValue.Size = new System.Drawing.Size(138, 29);
            this.txtPayValue.TabIndex = 780;
            this.txtPayValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPayValue.Click += new System.EventHandler(this.txtPayValue_Click);
            this.txtPayValue.TextChanged += new System.EventHandler(this.txtPayValue_TextChanged);
            this.txtPayValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValue_KeyDown);
            this.txtPayValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPayValue_KeyPress);
            // 
            // combPayType
            // 
            this.combPayType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.combPayType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combPayType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combPayType.FormattingEnabled = true;
            this.combPayType.Location = new System.Drawing.Point(538, 49);
            this.combPayType.Name = "combPayType";
            this.combPayType.Size = new System.Drawing.Size(253, 29);
            this.combPayType.TabIndex = 779;
            this.combPayType.TextChanged += new System.EventHandler(this.combPayType_TextChanged);
            this.combPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combPayType_KeyDown);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // txtCurrencyRate
            // 
            this.txtCurrencyRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurrencyRate.BackColor = System.Drawing.Color.White;
            this.txtCurrencyRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrencyRate.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCurrencyRate.Location = new System.Drawing.Point(732, 62);
            this.txtCurrencyRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrencyRate.Name = "txtCurrencyRate";
            this.txtCurrencyRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCurrencyRate.Size = new System.Drawing.Size(261, 29);
            this.txtCurrencyRate.TabIndex = 783;
            this.txtCurrencyRate.Text = "1";
            this.txtCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyRate_KeyPress);
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(644, 65);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 21);
            this.label21.TabIndex = 787;
            this.label21.Text = "سعر الصرف :";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(674, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 21);
            this.label17.TabIndex = 784;
            this.label17.Text = "العملة :";
            // 
            // combCurrency
            // 
            this.combCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.combCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combCurrency.FormattingEnabled = true;
            this.combCurrency.Location = new System.Drawing.Point(732, 33);
            this.combCurrency.Name = "combCurrency";
            this.combCurrency.Size = new System.Drawing.Size(261, 29);
            this.combCurrency.TabIndex = 809;
            this.combCurrency.SelectedIndexChanged += new System.EventHandler(this.combCurrency_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnx);
            this.groupBox1.Controls.Add(this.txtQ);
            this.groupBox1.Controls.Add(this.btnOK);
            this.groupBox1.Controls.Add(this.btnC);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.btn3);
            this.groupBox1.Controls.Add(this.btn2);
            this.groupBox1.Controls.Add(this.btn1);
            this.groupBox1.Controls.Add(this.btn6);
            this.groupBox1.Controls.Add(this.btn5);
            this.groupBox1.Controls.Add(this.btn4);
            this.groupBox1.Controls.Add(this.btn9);
            this.groupBox1.Controls.Add(this.btn8);
            this.groupBox1.Controls.Add(this.btn7);
            this.groupBox1.Location = new System.Drawing.Point(3, 349);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 231);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnx
            // 
            this.btnx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnx.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnx.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnx.Location = new System.Drawing.Point(135, 19);
            this.btnx.Name = "btnx";
            this.btnx.Size = new System.Drawing.Size(42, 30);
            this.btnx.TabIndex = 16;
            this.btnx.Text = "X";
            this.btnx.UseVisualStyleBackColor = false;
            this.btnx.Click += new System.EventHandler(this.btnx_Click);
            // 
            // txtQ
            // 
            this.txtQ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQ.Font = new System.Drawing.Font("Tahoma", 14F);
            this.txtQ.Location = new System.Drawing.Point(6, 19);
            this.txtQ.Name = "txtQ";
            this.txtQ.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtQ.Size = new System.Drawing.Size(123, 36);
            this.txtQ.TabIndex = 15;
            this.txtQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnOK.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(135, 55);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(42, 170);
            this.btnOK.TabIndex = 14;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnC
            // 
            this.btnC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnC.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnC.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btnC.Location = new System.Drawing.Point(92, 184);
            this.btnC.Name = "btnC";
            this.btnC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnC.Size = new System.Drawing.Size(37, 38);
            this.btnC.TabIndex = 13;
            this.btnC.Text = "c";
            this.btnC.UseVisualStyleBackColor = false;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button8.Location = new System.Drawing.Point(49, 184);
            this.button8.Name = "button8";
            this.button8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button8.Size = new System.Drawing.Size(37, 38);
            this.button8.TabIndex = 12;
            this.button8.Text = ".";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.button12.Location = new System.Drawing.Point(6, 184);
            this.button12.Name = "button12";
            this.button12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button12.Size = new System.Drawing.Size(37, 38);
            this.button12.TabIndex = 11;
            this.button12.Text = "0";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn3.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn3.Location = new System.Drawing.Point(92, 141);
            this.btn3.Name = "btn3";
            this.btn3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn3.Size = new System.Drawing.Size(37, 38);
            this.btn3.TabIndex = 10;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn2.Location = new System.Drawing.Point(49, 141);
            this.btn2.Name = "btn2";
            this.btn2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn2.Size = new System.Drawing.Size(37, 38);
            this.btn2.TabIndex = 9;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn1.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn1.Location = new System.Drawing.Point(6, 141);
            this.btn1.Name = "btn1";
            this.btn1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn1.Size = new System.Drawing.Size(37, 38);
            this.btn1.TabIndex = 8;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn6.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn6.Location = new System.Drawing.Point(92, 98);
            this.btn6.Name = "btn6";
            this.btn6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn6.Size = new System.Drawing.Size(37, 38);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn5.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn5.Location = new System.Drawing.Point(49, 98);
            this.btn5.Name = "btn5";
            this.btn5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn5.Size = new System.Drawing.Size(37, 38);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn4.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn4.Location = new System.Drawing.Point(6, 98);
            this.btn4.Name = "btn4";
            this.btn4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn4.Size = new System.Drawing.Size(37, 38);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn9.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn9.Location = new System.Drawing.Point(92, 55);
            this.btn9.Name = "btn9";
            this.btn9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn9.Size = new System.Drawing.Size(37, 38);
            this.btn9.TabIndex = 2;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn8.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn8.Location = new System.Drawing.Point(49, 55);
            this.btn8.Name = "btn8";
            this.btn8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn8.Size = new System.Drawing.Size(37, 38);
            this.btn8.TabIndex = 1;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn7.Location = new System.Drawing.Point(6, 55);
            this.btn7.Name = "btn7";
            this.btn7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn7.Size = new System.Drawing.Size(37, 38);
            this.btn7.TabIndex = 0;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnUpdate);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnSave_Print);
            this.groupBox3.Controls.Add(this.btnSave);
            this.groupBox3.Controls.Add(this.btnNew);
            this.groupBox3.Location = new System.Drawing.Point(3, 119);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 233);
            this.groupBox3.TabIndex = 810;
            this.groupBox3.TabStop = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpdate.Image = global::ByStro.Properties.Resources.Edit_25;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.Location = new System.Drawing.Point(7, 142);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(166, 41);
            this.btnUpdate.TabIndex = 20;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnDelete.Image = global::ByStro.Properties.Resources.Delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(8, 185);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(166, 41);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // btnSave_Print
            // 
            this.btnSave_Print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSave_Print.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnSave_Print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave_Print.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSave_Print.Image = global::ByStro.Properties.Resources.Save_and_New;
            this.btnSave_Print.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave_Print.Location = new System.Drawing.Point(6, 99);
            this.btnSave_Print.Name = "btnSave_Print";
            this.btnSave_Print.Size = new System.Drawing.Size(166, 41);
            this.btnSave_Print.TabIndex = 18;
            this.btnSave_Print.Text = "حفظ وطباعة   ";
            this.btnSave_Print.UseVisualStyleBackColor = false;
            this.btnSave_Print.Click += new System.EventHandler(this.btnSave_Print_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSave.Image = global::ByStro.Properties.Resources.Save_30px;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(6, 56);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(166, 41);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnNew.Image = global::ByStro.Properties.Resources.New;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.Location = new System.Drawing.Point(6, 13);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(166, 41);
            this.btnNew.TabIndex = 16;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // button6
            // 
            this.button6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button6.BackgroundImage")));
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(579, 7);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(60, 33);
            this.button6.TabIndex = 813;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(514, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 33);
            this.button2.TabIndex = 812;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BillSales2_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(246)))));
            this.ClientSize = new System.Drawing.Size(997, 581);
            this.Controls.Add(this.btnPayAgel);
            this.Controls.Add(this.btnPayCash);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.combCurrency);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtCurrencyRate);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtVatValue);
            this.Controls.Add(this.combAccount);
            this.Controls.Add(this.txtTotalBayPrice);
            this.Controls.Add(this.txtBalence);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.txtProdecutAvg);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtRestInvoise);
            this.Controls.Add(this.txtpaid_Invoice);
            this.Controls.Add(this.txtNetInvoice);
            this.Controls.Add(this.txtTotalDescound);
            this.Controls.Add(this.txtTotalInvoice);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.txtAccountID);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBillTypeID);
            this.Controls.Add(this.txtUnitOperating);
            this.Controls.Add(this.txtUnitFactor);
            this.Controls.Add(this.txtProdecutID);
            this.Controls.Add(this.txtStoreID);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtStoreName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtMainID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label16);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BillSales2_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فاتورة مبيعات";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrderProudect_frm_FormClosing);
            this.Load += new System.EventHandler(this.PureItemToStoreForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BillSales_frm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtMainID;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.DateTimePicker D1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView DGV1;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtStoreID;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtTotalPrice;
        private System.Windows.Forms.ComboBox combUnit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtProdecutID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtUnitFactor;
        private System.Windows.Forms.TextBox txtUnitOperating;
        private System.Windows.Forms.TextBox txtBillTypeID;
        private System.Windows.Forms.TextBox txtVAT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAccountID;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtRestInvoise;
        public System.Windows.Forms.TextBox txtpaid_Invoice;
        public System.Windows.Forms.TextBox txtNetInvoice;
        public System.Windows.Forms.TextBox txtTotalDescound;
        public System.Windows.Forms.TextBox txtTotalInvoice;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label20;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        private System.Windows.Forms.TextBox txtProdecutAvg;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtBalence;
        private System.Windows.Forms.TextBox txtTotalBayPrice;
        private System.Windows.Forms.ComboBox combAccount;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.TextBox txtVatValue;
        public System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtMainVat;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox combPayType;
        public System.Windows.Forms.TextBox txtStatement;
        public System.Windows.Forms.TextBox txtPayValue;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel txtSumPayType;
        public System.Windows.Forms.DataGridView DGVPayType;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.TextBox txtCurrencyRate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox combCurrency;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TextBox txtQ;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnx;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave_Print;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnPayCash;
        private System.Windows.Forms.Button btnPayAgel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn XQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreName;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitOperating;
        private System.Windows.Forms.DataGridViewTextBoxColumn BayPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBayPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn MainVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vat;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Statement;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayValue;
    }
}