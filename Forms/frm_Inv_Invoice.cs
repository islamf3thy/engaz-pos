using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using System.IO;
using DevExpress.Utils.VisualEffects; 
using System.Linq.Expressions;
using System.Collections.ObjectModel;
using System.Data.Linq;
using DevExpress.XtraLayout;
using ByStro.DAL;
using ByStro.Clases;
using System.Diagnostics;

namespace ByStro.Forms
{
 
	public partial class frm_Inv_Invoice : frm_Master
	{
		Inv_Invoice Invoice = new Inv_Invoice();
		RepositoryItemGridLookUpEdit repoItems = new RepositoryItemGridLookUpEdit();
		RepositoryItemLookUpEdit repoUOM = new RepositoryItemLookUpEdit();
		RepositoryItemLookUpEdit repoSize = new RepositoryItemLookUpEdit();
		RepositoryItemLookUpEdit repoColor = new RepositoryItemLookUpEdit();
		RepositoryItemDateEdit repoDate = new RepositoryItemDateEdit(); 
		RepositoryItemLookUpEdit repoUsers = new RepositoryItemLookUpEdit();
		RepositoryItemLookUpEdit repoStore = new RepositoryItemLookUpEdit();
		DAL.DBDataContext  DetailsDataContexst;
		public MasterClass.InvoiceType invoiceType; 
		Dictionary<int, string> CodesDictionary = new Dictionary<int, string>();
		struct ItemBalanceInStore
		{
			public int ItemID;
			public int StoreID;
			public int UnitID;
			public double Balance;
		}
		List<ItemBalanceInStore> itemsBalanceInStores = new List<ItemBalanceInStore>();
		void ViewItemBalanceInGrid(int itemID, int storeID)
		{



			double balance = 0;


			DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			var logSumIN = dbc.Inv_StoreLogs.Where(x => x.ItemID == itemID && x.StoreID == storeID).Sum(x => x.ItemQuIN) ?? 0;
			var logSumOut = dbc.Inv_StoreLogs.Where(x => x.ItemID == itemID && x.StoreID == storeID).Sum(x => x.ItemQuOut) ?? 0;
			balance = ((double)logSumIN - (double)logSumOut);

			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			var item = db.Prodecuts.Where(x => x.ProdecutID == Convert.ToInt32(itemID)).Single();

			var datasource = new[] {
				  new { Name = item.FiestUnit, ID = 1 ,Factor = item.FiestUnitFactor },
				  new { Name = item.SecoundUnit, ID = 2,Factor = item.SecoundUnitFactor   },
				  new { Name = item.ThreeUnit, ID = 3,Factor = item.ThreeUnitFactor  }  };  
			var itemunit = datasource.Where(x => string.IsNullOrEmpty(x.Name) == false && string.IsNullOrEmpty(x.Factor) == false).ToList(); 
			itemsBalanceInStores.RemoveAll(x => x.ItemID == itemID);
			itemunit.ForEach(u =>
			{
				itemsBalanceInStores.Add(new ItemBalanceInStore()
				{
					ItemID = itemID,
					StoreID = storeID,
					UnitID = u.ID,
					Balance = balance / Convert.ToDouble(u.Factor)
				});
			});


		}
		//   object CustomersList;
	 

		void LoadInvoiceType()
		{
			lyc_PartID.Visibility = lyc_Attintion .Visibility =   lyc_PriceLevel .Visibility = 
				layoutControlItem19.Visibility =  lyc_Source.Visibility=lyc_Net.Visibility = lyc_Paid.Visibility = lyc_Remains.Visibility
		     = lyc_Shiping.Visibility = lyg_Totals.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

			LookUpEdit_PartType.Properties.DataSource = new[]
					{
					  new { ID = 1 ,Name = "مورد" },
					  new { ID = 2 ,Name =  "عميل"  },
					};


			switch (invoiceType)
			{
				case MasterClass.InvoiceType.SalesInvoice:
					lyc_PartID.Visibility = lyc_Attintion.Visibility = lyc_PriceLevel.Visibility =
				layoutControlItem19.Visibility =  lyc_Net.Visibility = lyc_Paid.Visibility = lyc_Remains.Visibility 
				   = lyc_Profit.Visibility = lyc_Shiping.Visibility = lyg_Totals.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
		 
					this.Text = "فاتوره مبيعات";
					this.Name = "frm_SalesInvoice";
					LookUpEdit_PartType.EditValue = 2;
					break;
				case MasterClass.InvoiceType.PurchaseInvoice :
					lyc_PartID.Visibility = lyc_Attintion.Visibility = lyc_PriceLevel.Visibility =
				lyc_Net.Visibility = lyc_Paid.Visibility = lyc_Remains.Visibility
					  = lyc_Shiping.Visibility = lyg_Totals.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
					this.Text = "فاتوره مشتروات";
					this.Name = "frm_PurchaseInvoice";
					LookUpEdit_PartType.EditValue = 1;
					break;
				case MasterClass.InvoiceType.ItemOpenBalance : 
					this.Text = "رصيد افتتاحي للصنف";
					this.Name = "frm_OpenBalanceInvoice";
					LookUpEdit_PartType.EditValue = 0;
					btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
					break;
				case MasterClass.InvoiceType.ItemDamage:
					this.Text = "هالك اصناف";
					this.Name = "frm_ItemDamage";
					LookUpEdit_PartType.EditValue = 0;
					btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
					break;
				case MasterClass.InvoiceType.PurchaseReturn :
					lyc_PartID.Visibility = lyc_Attintion.Visibility = 
						lyc_Source.Visibility =  	lyc_Net.Visibility = lyc_Paid.Visibility = lyc_Remains.Visibility = lyc_Shiping.Visibility = lyg_Totals.Visibility
						= DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
					
					this.Text = "مردود مشتريات";
					this.Name = "frm_PurchaseReturnInvoice";
					LookUpEdit_PartType.EditValue = 1;
					break;

				case MasterClass.InvoiceType.SalesReturn :
					lyc_PartID.Visibility = lyc_Attintion.Visibility =
						lyc_Source.Visibility = lyc_Net.Visibility = lyc_Paid.Visibility = lyc_Remains.Visibility = lyc_Shiping.Visibility = lyg_Totals.Visibility
						= DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
					 
					this.Text = "مردود مبيعات";
					this.Name = "frm_SalesReturnInvoice";
					LookUpEdit_PartType.EditValue = 2;
					break;
				case MasterClass.InvoiceType.ItemStoreMove : 
					layoutControlItem5.Text = "من مخزن";
					lyc_ToStore.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
					this.Text = "سند نقل اصناف";
					this.Name = "frm_ItemStoreMove";
					LookUpEdit_PartType.EditValue = 0;
					btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
					break;
				default:
					throw new NotImplementedException();

			}
		}
		public frm_Inv_Invoice(MasterClass.InvoiceType _InvoiceType)
		{
			InitializeComponent();
			invoiceType = _InvoiceType;
			LoadInvoiceType();
			New();

		}
		public frm_Inv_Invoice(MasterClass.InvoiceType _InvoiceType , List<Inv_InvoiceDetail > Products )
		{
			InitializeComponent();
			invoiceType = _InvoiceType;
			LoadInvoiceType();
			New();
			var list = GridView_Items.DataSource as Collection<Inv_InvoiceDetail>;
			int RowHandle = 0;
			Products.ForEach(item =>
			{
				list.Add(item);
				GridView_Items_CellValueChanged(GridView_Items, new CellValueChangedEventArgs(RowHandle, GridView_Items.Columns[nameof(item.ItemID)], item.ItemID));
				RowHandle++;
			});
			
		}
		public frm_Inv_Invoice(int ID)
		{
			InitializeComponent();
			
			LoadItem(ID);
			
		}


		/// <summary>
		/// Creat new invoice and load data from outer source 
		/// </summary>
		/// <param name="ID">The source ID </param>
		/// <param name="TypeToLoad">The source of the invoice to load data from [  Ex: ord=Sales order , req=Sales Requst , pof=Price Offer ]  </param>
		/// <param name="lst"></param>
		public frm_Inv_Invoice(int ID, MasterClass.PartTypes _partType, MasterClass.InvoiceType _InvoiceType, List<int> lst = null)
		{
			InitializeComponent();
			invoiceType = _InvoiceType;
			LoadInvoiceType();
			New();
			Invoice.PartType = (byte)_partType;
			Invoice.PartID = ID;
		 

		}
		private void View_RowCellStyle(object sender, RowCellStyleEventArgs e)
		{
			if (e.Column.FieldName == "State")
			{
				if (e.CellValue is bool d && d == true)
				{
					e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
				}
				else
				{
					e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;

				}
			}
		}
		private void View_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName == "State")
			{
				e.DisplayText = (e.Value is bool d && d == true) ? "مشغول" : "متاح";
			}
		}
		/// <summary>
		/// This field is used to get the names of the columns exp:nameof(InvoDInsta.ItemID)
		/// </summary>
		Inv_InvoiceDetail InvoDInsta = new Inv_InvoiceDetail(); // instance of invoice detal to get the names from 
		public override void frm_Load(object sender, EventArgs e)
		{
			LoadInvoiceType();
			btn_SaveAndPrint.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
			GridView_Items.Columns.Add(new GridColumn() { VisibleIndex = 0, Name = "clmIndex", FieldName = "Index",Caption ="م" , UnboundType = DevExpress.Data.UnboundColumnType.Integer, MaxWidth = 25 });
			GridView_Items.Columns.Add(new GridColumn() { VisibleIndex = 1, Name = "clmCode", FieldName = "Code", UnboundType = DevExpress.Data.UnboundColumnType.String });
			GridView_Items.Columns.Add(new GridColumn() { VisibleIndex = 50, Name = "clmCurrentBalance", FieldName = "CurrentBalance", UnboundType = DevExpress.Data.UnboundColumnType.Object });
			MasterClass.RestoreGridLayout(GridView_Items, this);

			GridView_Items.Columns["Index"].OptionsColumn.AllowFocus =
				GridView_Items.Columns["CurrentBalance"].OptionsColumn.AllowFocus = false;


			var view = gridLookUpEdit1.Properties.View;
			gridLookUpEdit1.Properties.ValueMember = "ID";
			gridLookUpEdit1.Properties.DisplayMember = "Name";
			view.PopulateColumns(gridLookUpEdit1.Properties.DataSource);
			view.Columns["State"].Caption = "الحاله";
			view.Columns["State"].ColumnEdit = new RepositoryItemTextEdit();
			view.Columns["ID"].Visible = false;
			view.Columns["Name"].Caption = "الاسم";
			view.RowCellStyle += View_RowCellStyle;
			view.CustomColumnDisplayText += View_CustomColumnDisplayText;


			this.spn_Discount.Properties.Increment = 0.01M;
			this.spn_Discount.Properties.Mask.EditMask = "p";
			this.spn_Discount.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.spn_Discount.Properties.MaxValue = 1;

			RefreshData();

			repoItems.CloseUp += glkp_Customer_CloseUp;
			repoItems.ValidateOnEnterKey = true;
			repoItems.AllowNullInput = DefaultBoolean.False;
			repoItems.BestFitMode = BestFitMode.BestFitResizePopup;
			repoItems.ImmediatePopup = true;
			repoItems.EditValueChanged += RepoItems_EditValueChanged;
			repoItems.TextEditStyle = TextEditStyles.DisableTextEditor;

			lkp_Store.EditValueChanging += Lkp_Store_EditValueChanging;
	 
			LookUpEdit_PartID.Properties.PopupFormWidth = 300;
			GridView_Items.CustomUnboundColumnData += GridView_Items_CustomUnboundColumnData;
			this.FormClosing += frm_Inv_Invoice_FormClosing;

			var repositoryItemGridLookUpEdit1View = repoItems.View as GridView;
			repositoryItemGridLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
			repositoryItemGridLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = HorzAlignment.Far;
			repositoryItemGridLookUpEdit1View.Appearance.Row.Options.UseTextOptions = true;
			repositoryItemGridLookUpEdit1View.Appearance.Row.TextOptions.HAlignment = HorzAlignment.Near;
			repositoryItemGridLookUpEdit1View.FocusRectStyle = DrawFocusRectStyle.RowFocus;
			repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
			repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;
			repositoryItemGridLookUpEdit1View.OptionsBehavior.AutoSelectAllInEditor = false;
			repositoryItemGridLookUpEdit1View.OptionsBehavior.AutoUpdateTotalSummary = false;
			repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
			repositoryItemGridLookUpEdit1View.OptionsSelection.UseIndicatorForSelection = true;
			repositoryItemGridLookUpEdit1View.OptionsView.BestFitMaxRowCount = 10;
			repositoryItemGridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
			repositoryItemGridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
			repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
			repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;



			LookUpEdit_PartID.Properties.View.PopulateColumns(LookUpEdit_PartID.Properties.DataSource);
			 
			GridView_Items.Columns[nameof(InvoDInsta.Color)].Caption = "اللون";
			GridView_Items.Columns[nameof(InvoDInsta.Size)].Caption = "الحجم";
			GridView_Items.Columns[nameof(InvoDInsta.Serial)].Caption = "السيريال";



			GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Caption = "الصنف";
			GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].Caption = "الوحده";
			GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Caption = "الكميه";
			GridView_Items.Columns[nameof(InvoDInsta.Price)].Caption = "السعر";
			GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Caption = "الاجمالي";
			GridView_Items.Columns[nameof(InvoDInsta.Discount)].Caption = "ن خصم";
			GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].Caption = "ق خصم";
			GridView_Items.Columns[nameof(InvoDInsta.AddTax )].Caption = "VAT %";
			GridView_Items.Columns[nameof(InvoDInsta.AddTaxVal )].Caption = "VAT";
			GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].Caption = "الصلاحيه";
			if(invoiceType == MasterClass.InvoiceType.ItemStoreMove )
				GridView_Items.Columns[nameof(InvoDInsta.StoreID)].Caption = "من مخزن";
			else
				GridView_Items.Columns[nameof(InvoDInsta.StoreID)].Caption = "المخزن";

			GridView_Items.Columns[nameof(InvoDInsta.CostValue )].Caption = "سعر التكلفه";
			GridView_Items.Columns[nameof(InvoDInsta.TotalCostValue )].Caption = "اجمالي التكلفه";



			repositoryItemGridLookUpEdit1View.PopulateColumns(repoItems.DataSource);
			repositoryItemGridLookUpEdit1View.Columns["ID"].Visible = false;



			btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
		

			repoStore.NullText = repoItems.NullText = LookUpEdit_SourceID.Properties.NullText = repoUOM.NullText = repoColor.NullText = repoSize.NullText  = "";
			lkp_Store.Properties.ValueMember = "ID";
			lkp_Store.Properties.DisplayMember = "Name";

			lkp_ToStore .Properties.ValueMember = "ID";
			lkp_ToStore.Properties.DisplayMember = "Name";


			LookUpEdit_SourceID.Properties.ValueMember = "ID";
			LookUpEdit_SourceID.Properties.DisplayMember = "Name";


		
			LookUpEdit_PartID.Properties.ValueMember = "ID";
			LookUpEdit_PartID.Properties.DisplayMember = "Name";

			LookUpEdit_PartType.Properties.ValueMember = "ID";
			LookUpEdit_PartType.Properties.DisplayMember = "Name";
			LookUpEdit_PartType.Properties.ShowHeader = false;
			//LookUpEdit_PartType.Properties.Columns[0].Visible  = false ;


		


		


			LookUpEdit_PartID.Properties.ValueMember = "ID";
			LookUpEdit_PartID.Properties.DisplayMember = "Name";

			 

			repoItems.ValueMember = "ID";
			repoItems.DisplayMember = "Name";


			repoStore.ValueMember = "ID";
			repoStore.DisplayMember = "Name";



			repoItems.Buttons.Add(new EditorButton("Add", ButtonPredefines.Plus));
			repoItems.ButtonClick += RepoItems_ButtonClick;


			repoUOM.ValueMember = "ID";
			repoUOM.DisplayMember = "Name";
			repoUOM.PopulateColumns();
			repoUOM.Columns["ID"].Visible = false;

			repoSize.ValueMember = "ID";
			repoSize.DisplayMember = "Name";
			repoSize.PopulateColumns();
			repoSize.Columns["ID"].Visible = false;
			repoSize.Buttons.Add(new EditorButton("Add", ButtonPredefines.Plus));
			repoSize.ButtonClick += RepoSize_ButtonClick;
			repoColor.ValueMember = "ID";
			repoColor.DisplayMember = "Name";
			repoColor.PopulateColumns();
			repoColor.Columns["ID"].Visible = false;
			repoColor.Buttons.Add(new EditorButton("Add", ButtonPredefines.Plus));
			repoColor.ButtonClick += RepoColor_ButtonClick;

			repoDate.CalendarView = CalendarView.TouchUI;
			RepositoryItemSpinEdit repospin = new RepositoryItemSpinEdit();
			RepositoryItemSpinEdit repospinPrecintage = new RepositoryItemSpinEdit();

			repospinPrecintage.Increment = 0.01M;
			repospinPrecintage.Mask.EditMask = "p";
			repospinPrecintage.Mask.UseMaskAsDisplayFormat = true;
			repospinPrecintage.MaxValue = 1;

			GridControl_Items.RepositoryItems.AddRange(new RepositoryItem[]
			{ repoStore,repoItems , repoUOM, repospin,repoColor ,repoSize ,repoDate,repospinPrecintage });
		

			GridView_Items.Columns["ID"].OptionsColumn.AllowShowHide = GridView_Items.Columns["InvoiceID"].OptionsColumn.AllowShowHide =
				GridView_Items.Columns["ID"].Visible = GridView_Items.Columns["InvoiceID"].Visible = false;
			GridView_Items.Columns["CurrentBalance"].OptionsColumn.AllowEdit = false;
			GridView_Items.Columns[nameof(InvoDInsta.ItemID)].ColumnEdit = repoItems;
			GridView_Items.Columns[nameof(InvoDInsta.StoreID)].ColumnEdit = repoStore; 
			GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].ColumnEdit = repoUOM;
			GridView_Items.Columns[nameof(InvoDInsta.Price)].ColumnEdit = repospin;
			GridView_Items.Columns[nameof(InvoDInsta.Discount)].ColumnEdit = repospinPrecintage;
			GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].ColumnEdit = repospin;
			GridView_Items.Columns[nameof(InvoDInsta.AddTax)].ColumnEdit = repospinPrecintage;
			GridView_Items.Columns[nameof(InvoDInsta.AddTaxVal )].ColumnEdit = repospin;
			GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].ColumnEdit = repospin;
			GridView_Items.Columns[nameof(InvoDInsta.Size)].ColumnEdit = repoSize;
			GridView_Items.Columns[nameof(InvoDInsta.Color)].ColumnEdit = repoColor;
			GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].ColumnEdit = repoDate;
			GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].OptionsColumn.AllowFocus = false;
			GridView_Items.Columns[nameof(InvoDInsta.TotalCostValue)].OptionsColumn.AllowFocus = false;
			GridView_Items.Columns[nameof(InvoDInsta.CostValue)].OptionsColumn.AllowFocus = false;
			//GridView_Items.Columns["CurrentBalance"].OptionsColumn.ShowInCustomizationForm = (bool)CurrentSession.user.ShowItemBalance;
			//if (GridView_Items.Columns["CurrentBalance"].Visible && !(bool)CurrentSession.user.ShowItemBalance)
			//	GridView_Items.Columns["CurrentBalance"].Visible = false;

			RepositoryItemButtonEdit buttonEdit = new RepositoryItemButtonEdit();
			GridControl_Items.RepositoryItems.Add(buttonEdit);
			buttonEdit.Buttons.Clear();
			buttonEdit.Buttons.Add(new EditorButton(ButtonPredefines.Delete));
			buttonEdit.ButtonClick += ButtonEdit_ButtonClick;
			GridColumn clmnDelete = new GridColumn() { Name = "Delete", Caption = " ", FieldName = "Delete", ColumnEdit = buttonEdit, VisibleIndex = 100, Width = 15 };
			buttonEdit.TextEditStyle = TextEditStyles.HideTextEditor;
			GridView_Items.Columns.Add(clmnDelete);

			//GridView_Items.Columns[nameof(InvoDInsta.Serial)].Visible =
   //         GridView_Items.Columns[nameof(InvoDInsta.Serial)].OptionsColumn.ShowInCustomizationForm = false;


			GridView_Items.Columns["Index"].VisibleIndex = 0;
			GridView_Items.Columns["Code"].VisibleIndex = 1;
			GridView_Items.Columns[nameof(InvoDInsta.ItemID)].VisibleIndex = 2;
			GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].VisibleIndex = 3;
			GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].VisibleIndex = 4;
			GridView_Items.Columns[nameof(InvoDInsta.Price)].VisibleIndex = 5;
			GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].VisibleIndex = 6;
			GridView_Items.Columns[nameof(InvoDInsta.Discount)].VisibleIndex = 7;
			GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].VisibleIndex = 8;
			GridView_Items.Columns[nameof(InvoDInsta.AddTax )].VisibleIndex = 9;
			GridView_Items.Columns[nameof(InvoDInsta.AddTaxVal )].VisibleIndex = 10; 
			GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].VisibleIndex = 13;
			GridView_Items.Columns[nameof(InvoDInsta.StoreID)].VisibleIndex = 14;

			GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Summary.Clear();
			GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Summary.Add(DevExpress.Data.SummaryItemType.Count, nameof(InvoDInsta.ItemID), "{0}عدد ");

			GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Summary.Clear(); 
			GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.ItemQty), "{0} قطع");
			GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Summary.Clear();
			GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.TotalPrice), "{0}");

		 

			GridView_Items.Columns["CurrentBalance"].Caption = "الرصيد الحالي";
			MasterClass.RestoreGridLayout(gridView1, this);


			GridView_Items.CustomRowCellEditForEditing += GridView_Items_CustomRowCellEditForEditing;
			GridView_Items.FocusedRowChanged += GridView_Items_FocusedRowChanged;
			GridView_Items.FocusedColumnChanged += GridView_Items_FocusedColumnChanged;
			GridView_Items.CellValueChanged += GridView_Items_CellValueChanged;
			GridView_Items.InvalidRowException += GridView_Items_InvalidRowException;
			GridView_Items.ValidateRow += GridView_Items_ValidateRow;
			GridView_Items.RowUpdated += GridView_Items_RowUpdated;
			GridView_Items.RowCountChanged += GridView_Items_RowCountChanged;
			GridView_Items.CustomColumnDisplayText += GridView_Items_CustomColumnDisplayText;
			GridView_Items.CustomDrawCell += GridView_Items_CustomDrawCell;
			GridView_Items.CustomUnboundColumnData += GridView_Items_CustomUnboundColumnData;
			GridView_Items.InitNewRow += GridView_Items_InitNewRow;
			GridControl_Items.ProcessGridKey += gridControl1_ProcessGridKey;

			spn_Remains.EditValueChanged += new System.EventHandler(this.spn_Remains_EditValueChanged);
			spn_Net.EditValueChanged += new System.EventHandler(this.spn_Net_EditValueChanged);
			spn_Net.DoubleClick += new System.EventHandler(this.spn_Net_DoubleClick);
			spn_Discount.EditValueChanged += new System.EventHandler(this.spn_Precent_EditValueChanged);
			spn_DiscountVal.EditValueChanged += new System.EventHandler(this.spn_Val_EditValueChanged);
			spn_Total.EditValueChanged += new System.EventHandler(this.spn_Total_EditValueChanged);
			spn_TotalRevenue.EditValueChanged += new System.EventHandler(this.spn_Total_EditValueChanged);
			spn_Paid.EditValueChanged += new System.EventHandler(this.spn_Net_EditValueChanged);
			lkp_Store.EditValueChanged += new System.EventHandler(this.lkp_Store_EditValueChanged);
			LookUpEdit_PartID.Popup += new System.EventHandler(this.glkp_Customer_Popup);
			LookUpEdit_PartID.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.glkp_Customer_CloseUp);
			LookUpEdit_PartID.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.glkp_Customer_ButtonClick);
			LookUpEdit_PartID.EditValueChanged += new System.EventHandler(this.glkp_Customer_EditValueChanged);
			LookUpEdit_PartID.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LookUpEdit_PartID_EditValueChanging);
			LookUpEdit_PartType.EditValueChanged += new System.EventHandler(this.LookUpEdit_PartType_EditValueChanged);
			LookUpEdit_PartType.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LookUpEdit_PartID_EditValueChanging);
			 
			repoUsers.ValueMember = "ID";
			repoUsers.DisplayMember = "Name";
			repoUsers.NullText = "";


			lkp_Store_EditValueChanged(lkp_Store, null);



			#region DataChangedEventHandlers 
			dt_Date.EditValueChanged += DataChanged;
			memoEdit1.EditValueChanged += DataChanged;
			lkp_Store.EditValueChanged += DataChanged;
			txt_Code.EditValueChanged += DataChanged;
			LookUpEdit_PartID.EditValueChanged += DataChanged;
			LookUpEdit_SourceID.EditValueChanged += DataChanged;
			txt_Attn.EditValueChanged += DataChanged;
			txt_Code.EditValueChanged += DataChanged;
		 
			gridLookUpEdit1.EditValueChanged += DataChanged;
			memoEdit1.EditValueChanged += DataChanged;
			memoEdit_ShipTo.EditValueChanged += DataChanged;
			 
			spn_Discount.EditValueChanged += DataChanged;
			spn_DiscountVal.EditValueChanged += DataChanged;
			spn_Net.EditValueChanged += DataChanged;
			spn_Paid.EditValueChanged += DataChanged; 
			spn_TotalRevenue.EditValueChanged += DataChanged;



			#endregion
		}
		private void ButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			GridView view = ((GridControl)((ButtonEdit)sender).Parent).MainView as GridView;

			if (view.FocusedRowHandle >= 0)
			{
				view.DeleteSelectedRows();
			}

		}
		public override void OpenList()
		{
			new frm_Inv_InvoiceList(invoiceType).Show();
			base.OpenList();
		}
		List<DAL.Prodecut> _prodecuts;
		List<DAL.Prodecut> prodecuts {
			get {

				if (_prodecuts == null )
				{
				var db = new DBDataContext(Properties.Settings.Default.Connection_String); 
					_prodecuts = db.Prodecuts.ToList();
					
				}
				return _prodecuts;
			} }
		private void GridView_Items_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName == nameof(InvoDInsta.ItemUnitID))
			{
				GridView view = GridView_Items as GridView;
				int index = view.GetRowHandle(e.ListSourceRowIndex);
				DAL.Inv_InvoiceDetail  detail = view.GetRow(index) as DAL.Inv_InvoiceDetail;
				if (detail == null) return;
				var db = new DBDataContext(Properties.Settings.Default.Connection_String);
				
				var product = prodecuts.SingleOrDefault(x => x.ProdecutID == detail.ItemID);
				if (prodecuts == null) return;
				if (detail.ItemUnitID == 1)
					e.DisplayText = product.FiestUnit;
				else if (detail.ItemUnitID == 2)
					e.DisplayText = product.SecoundUnit;
				else if (detail.ItemUnitID == 3)
					e.DisplayText = product.ThreeUnit;
			}
		}
		private void GridView_Items_InitNewRow(object sender, InitNewRowEventArgs e)
		{
			GridView view = GridView_Items as GridView;
			int index = e.RowHandle;
			DAL.Inv_InvoiceDetail detail = view.GetRow(index) as DAL.Inv_InvoiceDetail; 
			if (BillSetting_cls.UseVat == true)
			{
				detail.AddTax = Convert.ToDouble(BillSetting_cls.Vat) / 100;
			}

		}


		private void LookUpEdit_PartType_EditValueChanged(object sender, EventArgs e)
		{
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			switch (LookUpEdit_PartType.EditValue.ToInt())
			{
				case 0:
					LookUpEdit_PartID.Properties.DataSource = null;
					LookUpEdit_PartID.EditValue = 0;
					break;
				case 1:
					LookUpEdit_PartID.Properties.DataSource =db.Suppliers .Select(c => new {ID = c.SupplierID ,Name =  c.SupplierName ,c.Address ,  c.Phone }).ToList();
					LookUpEdit_PartID.EditValue = db.Suppliers.Select(x=> ((int?)x.SupplierID)??0) .FirstOrDefault();
					lyc_PartID.Text ="مورد";
					break;
				case 2:
					LookUpEdit_PartID.Properties.DataSource = db.Customers .Select(c => new { ID = c.CustomerID , Name = c.CustomerName ,  c.Address, c.Phone }).ToList();
					LookUpEdit_PartID.EditValue = db.Customers.Select(x => ((int?)x.CustomerID ) ?? 0).FirstOrDefault();
					lyc_PartID.Text ="عميل";
					break;
				//case 3:
				//	LookUpEdit_PartID.Properties.DataSource = db.HR_Employees.Select(c => new { c.ID, c.Name }).ToList();
				//	LookUpEdit_PartID.EditValue = 0;
				//	layoutControlItem3.Text = LangResource.Employee;

				//	break;
				//case 4:
				//	LookUpEdit_PartID.Properties.DataSource = db.Acc_Accounts.Where(x => CurrentSession.UserAccessbileAccounts.Select(a => a.ID).Contains(x.ID) &&
				//	db.Acc_Accounts.Where(a => a.ParentID == x.ID).Count() == 0).Select(c => new { c.ID, c.Name }).ToList();
				//	LookUpEdit_PartID.EditValue = 0;
				//	layoutControlItem3.Text = LangResource.Account;

				//	break;

			}
		}

		private void GridView_Items_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
		{
			if (e.Column.FieldName == "Index")
			{
				if (e.IsGetData)
					e.Value = e.ListSourceRowIndex + 1;
			}
			else if (e.Column.FieldName == "Code")
			{
				if (e.IsGetData)
				{

					var code = "";
					CodesDictionary.TryGetValue(GridView_Items.GetRowHandle(e.ListSourceRowIndex), out code);
					e.Value = code;

				}
				else
				{
					CodesDictionary.Remove(GridView_Items.GetRowHandle(e.ListSourceRowIndex));
					if (e.Value != null)
						CodesDictionary.Add(GridView_Items.GetRowHandle(e.ListSourceRowIndex), e.Value.ToString());
				}
			}
			else if (e.Column.FieldName == "CurrentBalance")
			{
				if (e.IsGetData)
				{
					var row = e.Row as Inv_InvoiceDetail;
					if (row == null || row.ItemID == 0) return;

					var balance = itemsBalanceInStores.Where(x => x.ItemID == row.ItemID && x.UnitID == row.ItemUnitID && x.StoreID == (row.StoreID ?? lkp_Store.EditValue.ToInt())).FirstOrDefault();
					if (balance.ItemID == 0)
					{
						ViewItemBalanceInGrid(row.ItemID, (row.StoreID ?? lkp_Store.EditValue.ToInt()));
						return;
					}

					e.Value = balance.Balance;
				}

			}

		}
 
	
		public override void DataChanged(object sender, EventArgs e)
		{
			base.DataChanged(sender, e);
		}
		

		


		private void Lkp_Store_EditValueChanging(object sender, ChangingEventArgs e)
		{
		 
			for (int i = 0; i <= GridView_Items.RowCount - 1; i++)
			{
				if (GridView_Items.GetRowCellValue(i, "StoreID").ToString() == e.OldValue.ToString())
					GridView_Items.SetRowCellValue(i, "StoreID", e.NewValue);
			}
		}
		private void RepoColor_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
				return;

			string btnName = e.Button.Tag.ToString();
			if (btnName == "Add")
			{
				DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String); 
				new frm_Inv_Color().ShowDialog();
				repoColor.DataSource = (from c in db.Inv_Colors select c).ToList();
			}
		}
		private void RepoSize_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
				return;

			string btnName = e.Button.Tag.ToString();
			if (btnName == "Add")
			{
				DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

				new frm_Inv_Size().ShowDialog();

				repoSize.DataSource = (from s in db.Inv_Sizes select s).ToList();

			}
		}
		private void RepoItems_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
				return;

			string btnName = e.Button.Tag.ToString();
			if (btnName == "Add")
			{
				new PL.Prodecut_frm().ShowDialog();
				RefreshData();

			}
		}
		private void RepoItems_EditValueChanged(object sender, EventArgs e)
		{
			GridView_Items.PostEditor();
		}




		

		public override void RefreshData()
		{
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			var store = (from s in db.Stores  select new { ID = s.StoreID , Name = s.StoreName }).ToList();
			lkp_Store.Properties.DataSource = store; 
			repoSize.DataSource = (from s in db.Inv_Sizes select s).ToList();
			repoColor.DataSource = (from c in db.Inv_Colors select c).ToList(); 
			repoUsers.DataSource = db.UserPermissions .Select(x => new {  x.ID, Name = x.UserName }).ToList(); 
			repoStore.DataSource = db.Stores.Select(x => new { ID = x.StoreID, Name = x.StoreName }).ToList();
			repoItems.DataSource = db.Prodecuts.Select(x => new {
				ID = x.ProdecutID,
				Name = x.ProdecutName,
				Category = db.Categories.SingleOrDefault(c => c.CategoryID == x.CategoryID).CategoryName ?? ""
			}).ToList();
			repoUOM.DataSource = db.Units.Select(x => new { ID = x.UnitID, Name = x.UnitName }).ToList();
			gridLookUpEdit1.Properties.DataSource = db.Drivers.Select(x => new { x.Name, x.ID, State = (db.DeliveryOrders.Where(d => d.HasReturned == false && d.DriverID == x.ID).Count() > 0) }).ToList();
			LookUpEdit_PartType_EditValueChanged(null, null);
		}


		private bool ValidData()
		{
			if (GridView_Items.RowCount == 0)
			{
				XtraMessageBox.Show("يجب تسجيل صنف واحد علي الاقل ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			if (GridView_Items.HasColumnErrors)
			{
				return false;
			}
			//if (Convert.ToDouble(spn_Remains.EditValue) > 0 &&
			//	 Convert.ToDouble(txt_MaxCredit.Text) < (CustomerBalance.Balance * -1)
			//	 && invoiceType == MasterClass.InvoiceType.SalesInvoice)
			//{
			//	if (CurrentSession.user.WhenSellingToMaxCreditCustomer == 1 &&
			//		   XtraMessageBox.Show(LangResource.WarningCustomerRachedThereMaxCredit, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			//	{
			//		return false;
			//	}
			//	if (CurrentSession.user.WhenSellingToMaxCreditCustomer == 2)
			//	{
			//		XtraMessageBox.Show(LangResource.ErrorCantSellToThatCustomerRechedMaxCredit, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
			//		return false;
			//	}
			//}
			if(invoiceType == MasterClass.InvoiceType .SalesInvoice ||
				invoiceType == MasterClass.InvoiceType.PurchaseInvoice ||
				invoiceType == MasterClass.InvoiceType.SalesReturn ||
				invoiceType == MasterClass.InvoiceType.PurchaseReturn ) 
			if (LookUpEdit_PartID.EditValue .ValidAsIntNonZero () == false )
			{
				LookUpEdit_PartID.ErrorText = "هذا الحقل مطلوب";
				return false;
			}

			if (lkp_Store.EditValue.ValidAsIntNonZero() == false)
			{
				lkp_Store.ErrorText  = "هذا الحقل مطلوب";
				return false;
			}
			if (invoiceType == MasterClass.InvoiceType.ItemStoreMove && lkp_ToStore .EditValue.ValidAsIntNonZero() == false)
			{
				lkp_ToStore.ErrorText = "هذا الحقل مطلوب";
				return false;
			}

			//if ( 
			//   invoiceType == MasterClass.InvoiceType.SalesReturn ||
			//   invoiceType == MasterClass.InvoiceType.PurchaseReturn)
			//	if (LookUpEdit_SourceID .EditValue.ValidAsIntNonZero() == false)
			//	{
			//		LookUpEdit_SourceID.ErrorText = "هذا الحقل مطلوب";
			//		return false;
			//	}
			return true;

		}
		int GetNextID()
		{
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			try
			{
				return (int)db.Inv_Invoices.Max(n => n.ID) + 1;
			}
			catch
			{
				return (int)1;
			}
		}
		  
		public override void Save()
		{
			// if (IsNew && !CanAdd) { XtraMessageBox.Show(LangResource.CantAddNoPermission, "", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
		 
			if (!ValidData()) { return; }
			if (GridView_Items.FocusedRowHandle < 0)
			{
				GridView_Items.DeleteRow(GridView_Items.FocusedRowHandle);
				//GridView_Items_ValidateRow(GridView_Items, new ValidateRowEventArgs(GridView_Items.FocusedRowHandle, GridView_Items.GetRow(GridView_Items.FocusedRowHandle)));
			}
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			if (IsNew)
			{
				Invoice = new Inv_Invoice();
				db.Inv_Invoices.InsertOnSubmit(Invoice);
				Invoice.UserID = CurrentSession.user.ID;
			}
			else
			{
				Invoice = db.Inv_Invoices.Where(x => x.ID == Invoice.ID).First();
				if (Invoice == null)
				{
					Invoice = new Inv_Invoice();
					db.Inv_Invoices.InsertOnSubmit(Invoice);
					Invoice.UserID = CurrentSession.user.ID;
				}
				Invoice.LastUpdateUserID = CurrentSession.user.ID;
				Invoice.LastUpdateDate = DateTime.Now; 
			}

			SetData();

			 
			db.SubmitChanges();
			var invoiceDetails = ((Collection<Inv_InvoiceDetail>)GridView_Items.DataSource);
 
			foreach (var item in invoiceDetails)
				item.InvoiceID = Invoice.ID; 
			DetailsDataContexst.SubmitChanges();

			 
			var partTypeName = LookUpEdit_PartType.Text;
			var PartID = LookUpEdit_PartID.EditValue.ToInt(); 


			switch (invoiceType)
			{
				case MasterClass.InvoiceType.SalesInvoice:
					db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.SalesInvoice).ToString() && x.VoucherID ==Invoice .ID.ToString()));
					db.SubmitChanges();
					TreasuryMovement movement = new TreasuryMovement()
					{
						ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
						AccountID = PartID.ToString(),
						ISCusSupp = "Cus",
						VoucherID = Invoice.ID.ToString(),
						Type = ((int)MasterClass.TreasuryMovementTypes.SalesInvoice).ToString(),
						VoucherType = "فاتوره مبيعات",
						VoucherDate = Invoice.Date,
						Description = string.Format("فاتوره مبيعات رقم {0} الي عميل {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
						Income = (Invoice.Paid> Invoice.Net )?Invoice.Net : Invoice.Paid,
						Export = 0,
						Credit = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						Debit =Invoice.Net,
						ISShow = true,
						UserAdd = Properties.Settings.Default.UserID,
						Remark = string.Format("فاتوره مبيعات رقم {0} الي عميل {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
					};
					db.TreasuryMovements.InsertOnSubmit(movement);
					db.SubmitChanges();
					break;
				case MasterClass.InvoiceType.PurchaseInvoice :
					db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.PurchaseInvoice).ToString() && x.VoucherID == Invoice.ID.ToString()));
					db.SubmitChanges();
					 movement = new TreasuryMovement()
					{
						ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
						AccountID = PartID.ToString(),
						ISCusSupp = "Supp",
						VoucherID = Invoice.ID.ToString(),
						Type = ((int)MasterClass.TreasuryMovementTypes.PurchaseInvoice ).ToString(),
						VoucherType = "فاتوره مشتريات",
						VoucherDate = Invoice.Date,
						Description = string.Format("فاتوره مشتريات رقم {0} من مورد {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
						Income =0,
						Export = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						Credit = Invoice.Net,
						Debit = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						ISShow = true,
						UserAdd = Properties.Settings.Default.UserID,
						Remark = string.Format("فاتوره مشتريات رقم {0} من مورد {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
					 };
					db.TreasuryMovements.InsertOnSubmit(movement);
					db.SubmitChanges();
					break;
				case MasterClass.InvoiceType.PurchaseReturn :
					db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.PurchaseReturn).ToString() && x.VoucherID == Invoice.ID.ToString()));
					db.SubmitChanges();
					movement = new TreasuryMovement()
					{
						ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
						AccountID = PartID.ToString(),
						ISCusSupp = "Supp",
						VoucherID = Invoice.ID.ToString(),
						Type = ((int)MasterClass.TreasuryMovementTypes.PurchaseReturn ).ToString(),
						VoucherType = "فاتوره مردود مشتريات",
						VoucherDate = Invoice.Date,
						Description = string.Format("فاتوره مردود مشتريات رقم {0} من مورد {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
						Income = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						Export =0,
						Credit = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						Debit = Invoice.Net,
						ISShow = true,
						UserAdd = Properties.Settings.Default.UserID,
						Remark = string.Format("فاتوره مردود مشتريات رقم {0} من مورد {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
					};
					db.TreasuryMovements.InsertOnSubmit(movement);
					db.SubmitChanges();
					break;
				case MasterClass.InvoiceType.SalesReturn :
					db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.SalesReturn ).ToString() && x.VoucherID == Invoice.ID.ToString()));
					db.SubmitChanges();
					movement = new TreasuryMovement()
					{
						ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
						AccountID = PartID.ToString(),
						ISCusSupp = "Cus",
						VoucherID = Invoice.ID.ToString(),
						Type = ((int)MasterClass.TreasuryMovementTypes.SalesReturn).ToString(),
						VoucherType = "فاتوره مردود مبيعات",
						VoucherDate = Invoice.Date,
						Description = string.Format("فاتوره مردود مبيعات رقم {0} من عميل {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
						Income = 0,
						Export = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						Credit = Invoice.Net,
						Debit = (Invoice.Paid > Invoice.Net) ? Invoice.Net : Invoice.Paid,
						ISShow = true,
						UserAdd = Properties.Settings.Default.UserID,
						Remark = string.Format("فاتوره مردود مبيعات رقم {0} من عميل {1}  ", Invoice.ID, LookUpEdit_PartID.Text),
					};
					db.TreasuryMovements.InsertOnSubmit(movement);
					db.SubmitChanges();
					break;
				case MasterClass.InvoiceType.ItemOpenBalance:
				case MasterClass.InvoiceType.ItemDamage:
				case MasterClass.InvoiceType.ItemStoreMove:
					break;
				default:
					throw new NotImplementedException();

			}




			if (GridView_Items.FocusedRowHandle < 0)
			{
				GridView_Items_ValidateRow(GridView_Items, new ValidateRowEventArgs(GridView_Items.FocusedRowHandle, GridView_Items.GetRow(GridView_Items.FocusedRowHandle)));

			}
			 
			  
			 
			db.Inv_StoreLogs.DeleteAllOnSubmit(db.Inv_StoreLogs.Where(x => x.Type == (int)invoiceType  && invoiceDetails.Select(d => d.ID).Contains(x.TypeID ?? 0)));
			db.SubmitChanges();
			int index = 1;
			bool IsStockOut;
			var MainMessage = ""; 
			switch (invoiceType)
			{
				case MasterClass.InvoiceType.SalesInvoice:
					IsStockOut = true;
					MainMessage = string.Format("فاتوره مبيعات رقم {0}", Invoice.ID);
					break;
				case MasterClass.InvoiceType.PurchaseInvoice:
					IsStockOut = false;
					MainMessage = string.Format("فاتوره مشتريات رقم {0}", Invoice.ID);
					break;
				case MasterClass.InvoiceType.ItemOpenBalance:
					IsStockOut = false;
					MainMessage = string.Format("رصيد افتتاحي للاصناف رقم {0}", Invoice.ID);
					break;
				case MasterClass.InvoiceType.ItemDamage:
					IsStockOut = true;
					MainMessage = string.Format("سند هالك اصناف رقم {0}", Invoice.ID);
					break;
				case MasterClass.InvoiceType.PurchaseReturn:
					IsStockOut = true;
					MainMessage = string.Format("فاتوره مردود مشتريات رقم {0}", Invoice.ID);
					break;
				case MasterClass.InvoiceType.SalesReturn:
					IsStockOut = false;
					MainMessage = string.Format("فاتوره مردود مبيعات رقم {0}", Invoice.ID);
					break;
				case MasterClass.InvoiceType.ItemStoreMove:
					IsStockOut = true;
					MainMessage = string.Format("سند نقل اصناف {0}", Invoice.ID);
					break;
				default:
					throw new NotImplementedException();
			}
			foreach (var item in invoiceDetails)
			{
				var product = db.Prodecuts.Single(x => x.ProdecutID == item.ItemID);
			
				double Factor = 1;
				
				switch (item.ItemUnitID)
				{
					case 1:
						Factor = Convert.ToDouble(product.FiestUnitFactor);
						break;
					case 2:
						Factor = Convert.ToDouble(product.SecoundUnitFactor); 
						break;
					case 3:
						Factor = Convert.ToDouble(product.ThreeUnitFactor); 
						break;
				}
		 

				
				
				db.Inv_StoreLogs.InsertOnSubmit(new Inv_StoreLog()
				{
					Type = (int)invoiceType,
					TypeID = item.ID,
					Batch = "",
					BuyPrice = item.CostValue   ,
					date = Invoice.Date.AddMilliseconds(index),
					ItemID = item.ItemID,
					ItemQuIN = IsStockOut ? 0 : item.ItemQty * Factor,
					ItemQuOut = IsStockOut ? item.ItemQty * Factor : 0,
					Note = MainMessage,
					SellPrice = item.Price ,
					Serial = item.Serial , 
					ExpDate = item.ExpDate,
					Color = item.Color,
					Size = item.Size,
					StoreID = item.StoreID
				});

				if(invoiceType == MasterClass.InvoiceType.ItemStoreMove)
				{

					db.Inv_StoreLogs.InsertOnSubmit(new Inv_StoreLog()
					{
						Type = (int)invoiceType,
						TypeID = item.ID,
						Batch = "",
						BuyPrice = item.CostValue,
						date = Invoice.Date.AddMilliseconds(index + 1),
						ItemID = item.ItemID,
						ItemQuIN = (!IsStockOut) ? 0 : item.ItemQty * Factor,
						ItemQuOut = (!IsStockOut) ? item.ItemQty * Factor : 0,
						Note = MainMessage  ,
						SellPrice = item.Price,
						Serial = item.Serial,
						ExpDate = item.ExpDate,
						Color = item.Color,
						Size = item.Size,
						StoreID = lkp_ToStore.EditValue.ToInt()
					});
				}
				index++;
			}
			db.SubmitChanges();
			if (invoiceType == MasterClass.InvoiceType.SalesInvoice &&
				Invoice.Paid < Invoice.Net  && XtraMessageBox.Show(text: "هل تريد انشاء ملف قسط لهذه الفاتوره", caption: "", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				var frm = new Forms.frm_Instalment(Invoice.PartID ,Invoice.ID );
				frm.Show();
			}
			base.Save();
			New();

		} 
		public override void New()
		{
			if (ChangesMade && !SaveChangedData()) return;
			RefreshData();
			itemsBalanceInStores = new List<ItemBalanceInStore>();
			Invoice = new Inv_Invoice()
			{
			 
				InvoiceType = (byte)invoiceType, 
				Date = DateTime.Now, 
				Code = DateTime.Now.ToString("yyyyMMdd") + Invoice.ID.ToString() + GetNextID().ToString(), 
			};
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

			switch (invoiceType)
			{
				case MasterClass.InvoiceType.SalesInvoice:
				case MasterClass.InvoiceType.SalesReturn:
					Invoice.PartType = (int)MasterClass.PartTypes.Customer;
					Invoice.PartID = BillSetting_cls.CustomerID.ToInt();
					Invoice.StoreID = BillSetting_cls.StoreID.ToInt();
					break;
				case MasterClass.InvoiceType.PurchaseInvoice:
				case MasterClass.InvoiceType.PurchaseReturn:
					Invoice.PartType = (int)MasterClass.PartTypes.Vendor; 
					Invoice.StoreID = BillSetting_cls.StoreID.ToInt();
					break;
				case MasterClass.InvoiceType.ItemOpenBalance:
				case MasterClass.InvoiceType.ItemDamage:
				case MasterClass.InvoiceType.ItemStoreMove:
					Invoice.StoreID = BillSetting_cls.StoreID.ToInt(); 
					break;
				default:
					throw new NotImplementedException();


			}
			GetData();
			base.New();
			IsNew = true;
			ChangesMade = false;
		}
		public override void Delete()
		{
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			//if (!CanPerformDelete()) return;
			if (IsNew) return;
		 
			if (MasterClass.AskForDelete())
			{
				Delete(new List<int>() { Invoice.ID }, this.Name, invoiceType);
				New();
			}

		}
		void SetData()
		{
	 
			Invoice.Code = txt_Code.Text;
			Invoice.PartID = Convert.ToInt32(LookUpEdit_PartID.EditValue); 
			Invoice.StoreID = Convert.ToInt32(lkp_Store.EditValue); 
			Invoice.Date = dt_Date.DateTime;  
			Invoice.SourceID = (LookUpEdit_SourceID.EditValue != null) ? Convert.ToInt32(LookUpEdit_SourceID.EditValue) : 0;  
			Invoice.Notes = memoEdit1.Text;
			Invoice.DiscountRatio = Convert.ToDouble(spn_Discount.EditValue);
			Invoice.DiscountValue = Convert.ToDouble(spn_DiscountVal.EditValue);
			Invoice.Total = Convert.ToDouble(spn_Total.EditValue);
			Invoice.TotalRevenue = Convert.ToDouble(spn_TotalRevenue.EditValue); 
			Invoice.Net = Convert.ToDouble(spn_Net.EditValue); 
			Invoice.Paid = Convert.ToDouble(spn_Paid .EditValue);
			Invoice.Driver = Convert.ToInt32(gridLookUpEdit1.EditValue);
			Invoice.ShippingAddress = memoEdit_ShipTo.Text;
			Invoice.InvoiceType = (byte)invoiceType;
			Invoice.PartType = (byte)LookUpEdit_PartType.EditValue.ToInt(); 

			if(invoiceType == MasterClass.InvoiceType.ItemStoreMove )
				Invoice.SourceID = Convert.ToInt32(lkp_ToStore .EditValue);
		}

		bool IsLoadingData = false;

		void GetData()
		{
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			DetailsDataContexst = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			invoiceType = (MasterClass.InvoiceType)Invoice.InvoiceType;
			IsLoadingData = true;
			txt_ID.Text = Invoice.ID.ToString();
			txt_Code.Text = Invoice.Code;

			IsPaidChangedByUser = true; // to diable auto inserting value into paied spenEdit
			LookUpEdit_PartID.EditValue = Invoice.PartID; 
			lkp_Store.EditValue = Invoice.StoreID;
			// chk_PostState.Checked = Invoice.PostState;
			if (invoiceType == MasterClass.InvoiceType.ItemStoreMove)
			lkp_ToStore.EditValue =	Invoice.SourceID  ;

			dt_Date.DateTime = Invoice.Date;
		  
			LookUpEdit_PartType.EditValue = (int)Invoice.PartType;
			 
			LookUpEdit_SourceID.EditValue = Invoice.SourceID;
		 
			memoEdit1.Text = Invoice.Notes;
			spn_Discount.EditValue = Invoice.DiscountRatio;
			spn_DiscountVal.EditValue = Invoice.DiscountValue;
			spn_Total.EditValue = Invoice.Total;
			spn_TotalRevenue.EditValue = Invoice.TotalRevenue;
		 
			spn_Net.EditValue = Invoice.Net;
			spn_Paid.EditValue = Invoice.Paid;
			gridLookUpEdit1.EditValue = Invoice.Driver;
			memoEdit_ShipTo.Text = Invoice.ShippingAddress;
			
 


			txt_InsertUser.Text = db.UserPermissions.Where(x => x.ID == Invoice.UserID).Select(x => x.UserName).FirstOrDefault();
			txt_UpdateUser.Text = db.UserPermissions.Where(x => x.ID == Invoice.LastUpdateUserID).Select(x => x.UserName).FirstOrDefault();
			txt_LastUpdate.Text = (Invoice.LastUpdateDate != null) ? ((DateTime)Invoice.LastUpdateDate).ToString("yyyy-MM-dd dddd hh:mm tt") : "";




			IQueryable<Inv_InvoiceDetail> InvoiceDetails =
				DetailsDataContexst.Inv_InvoiceDetails.Select<DAL.Inv_InvoiceDetail, DAL.Inv_InvoiceDetail>((Expression<Func<DAL.Inv_InvoiceDetail, DAL.Inv_InvoiceDetail>>)
				(x => x)).Where(x => x.InvoiceID == Invoice.ID);
			GridControl_Items.DataSource = InvoiceDetails;


		
			IsPaidChangedByUser = false;

			ChangesMade = false;
			IsLoadingData = false;

		}
		 
	 
		private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
		{
			try
			{

				GridControl gridControl = sender as GridControl;
				GridView focusedView = gridControl.FocusedView as GridView;
				if (e.KeyCode == Keys.Return)
				{
					//GridColumn focusedColumn = (gridControl.FocusedView as ColumnView).FocusedColumn;
					int focusedRowHandle = (gridControl.FocusedView as ColumnView).FocusedRowHandle;

					var FocusedColumnName = "";
					if (focusedView.FocusedColumn == focusedView.Columns["Code"] || focusedView.FocusedColumn == focusedView.Columns["ItemID"])
					{
						FocusedColumnName = focusedView.FocusedColumn.FieldName;
						gridControl1_ProcessGridKey(sender, new KeyEventArgs(Keys.Tab));
					}
					if (focusedView.FocusedRowHandle < 0)
					{
						focusedView.AddNewRow();
						if (FocusedColumnName != "")
							focusedView.FocusedColumn = focusedView.Columns[FocusedColumnName];
						else
							focusedView.FocusedColumn = focusedView.Columns["Code"];
					}
					else
					{
						focusedView.FocusedRowHandle = focusedRowHandle + 1;
						if (FocusedColumnName != "")
							focusedView.FocusedColumn = focusedView.Columns[FocusedColumnName];
						else
							focusedView.FocusedColumn = focusedView.Columns["Code"];
					}
					e.Handled = true;
				}
				else if (e.KeyCode == Keys.Tab && e.Modifiers != Keys.Shift)
				{
					if (focusedView.FocusedColumn.VisibleIndex == 0)
						focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.VisibleColumns.Count - 1];
					else
						focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.FocusedColumn.VisibleIndex - 1];
					e.Handled = true;
				}
				else if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
					focusedView.DeleteSelectedRows();
				//else if (e.KeyCode == Keys.Tab && e.Modifiers == Keys.Shift)
				//{
				//    if (focusedView.FocusedColumn.VisibleIndex == focusedView.VisibleColumns.Count)
				//        focusedView.FocusedColumn = focusedView.VisibleColumns[0];
				//    else
				//        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.FocusedColumn.VisibleIndex + 1];
				//    e.Handled = true;
				//}
				//else
				//{
				//    //if (e.KeyCode != Keys.Up || focusedView.GetFocusedRow() == null || !(focusedView.GetFocusedRow() as DataRowView).IsNew || focusedView.GetFocusedRowCellValue("ItemId") != null && !(focusedView.GetFocusedRowCellValue("ItemId").ToString() == string.Empty))
				//    //    return;
				//    //focusedView.DeleteRow(focusedView.FocusedRowHandle);
				//}
			}
			catch
			{
			}
		}

		private void GridView_Items_ValidateRow(object sender, ValidateRowEventArgs e)
		{
			GridView view = sender as GridView;
			ColumnView columnView = sender as ColumnView;
			int index = e.RowHandle;
			Inv_InvoiceDetail detail = e.Row as Inv_InvoiceDetail;

			if (detail == null || detail.ItemID <= 0)
			{
				e.Valid = false;
				return;
			}
			if (detail.ItemID.ValidAsIntNonZero() == false)
			{
				e.Valid = false;
				view.SetColumnError(view.Columns[nameof(detail.ItemID)], "يجب اختيار الصنف ");
			}
			// if (columnView.get)
			DAL.Prodecut item = prodecuts.Where(x => x.ProdecutID == detail.ItemID).Single(); 
			if (detail.ItemQty <= 0)
			{
				e.Valid = false;
				view.SetColumnError(view.Columns[nameof(detail.ItemQty)], "يجب ان تكون الكميه اكبر من صفر ");
			}
			if (invoiceType == MasterClass.InvoiceType.SalesInvoice)
			{
				if (detail.CostValue  > detail.Price)
				{
					if (MessageBox.Show("سعر البيـع اقل من سعر الشـراء بمقدار :  " + (detail.Price - detail.CostValue ).ToString() + Environment.NewLine + "هل تريد الاستمــرار ؟؟ ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
					{
						view.DeleteRow(e.RowHandle);

					} 
				}
			
				else if (detail.CostValue  == detail.Price)
				{
					if (MessageBox.Show("سعر البيع يساوي سعر الشراء : هل تريد البيع بسعر التكلفة", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
					{
						view.DeleteRow(e.RowHandle); 

					}
				}
			}



			if (item.HasSerial &&string.IsNullOrEmpty( detail.Serial))
			{
				e.Valid = false;
				view.SetColumnError(view.Columns[nameof(detail.Serial)], "يجب اختيار الرقم التسلسلي");
				view.Columns[nameof(detail.Serial)].Visible = true;

			}
			if (item.HasSerial && string.IsNullOrEmpty(detail.Serial) == false)
			{
				DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String); 
				var log = db.Inv_StoreLogs.Where(x => x.Serial == detail.Serial && x.TypeID != detail.ID  );
				switch (invoiceType)
				{ 
					case MasterClass.InvoiceType.SalesInvoice:
					case MasterClass.InvoiceType.PurchaseReturn:
					case MasterClass.InvoiceType.ItemDamage:
					case MasterClass.InvoiceType.ItemStoreMove:
						if (log.Count() == 0)
						{
							e.Valid = false;
							view.SetColumnError(view.Columns[nameof(detail.Serial)], "هذا الرقم التسلسلي غير موجود");
							view.Columns[nameof(detail.Serial)].Visible = true;
						}
						else if(  log.OrderByDescending(x => x.date).First().ItemQuIN == 0)
						{
							e.Valid = false;
							view.SetColumnError(view.Columns[nameof(detail.Serial)], "هذا الرقم التسلسلي خرج من المخزن ولا يمكن صرفه مره اخري");
							view.Columns[nameof(detail.Serial)].Visible = true;
						}
						else if(  ((Collection<Inv_InvoiceDetail>)GridView_Items.DataSource).Where(x => x.Serial == detail.Serial).Count() > 1)
						{
							e.Valid = false;
							view.SetColumnError(view.Columns[nameof(detail.Serial)], "هذا الرقم التسلسلي  مسجل بالجدول بالفعل");
							view.Columns[nameof(detail.Serial)].Visible = true;
						}
						break;
					case MasterClass.InvoiceType.PurchaseInvoice:
					case MasterClass.InvoiceType.ItemOpenBalance:
						if (log.Count() > 0 ||((Collection<Inv_InvoiceDetail > )GridView_Items.DataSource ).Where(x => x.Serial == detail.Serial).Count() > 1)
						{
							e.Valid = false;
							view.SetColumnError(view.Columns[nameof(detail.Serial)], "هذا الرقم التسلسلي   موجود مسبقاً");
							view.Columns[nameof(detail.Serial)].Visible = true;
						}
						break;

					case MasterClass.InvoiceType.SalesReturn :

						break;
					default:
						throw new NotImplementedException();
				}


			}
			if (item.HasColor  && detail.Color.ValidAsIntNonZero() == false)
			{
				e.Valid = false;
				view.SetColumnError(view.Columns[nameof(detail.Color)], "يجب اختيار الللون");
				view.Columns[nameof(detail.Color)].Visible = true;

			}
			if (item.HasSize  && detail.Size.ValidAsIntNonZero() == false)
			{
				e.Valid = false;
				view.SetColumnError(view.Columns[nameof(detail.Size)],"يجب اختيار الحجم");
				view.Columns[nameof(detail.Size)].Visible = true;

			}
			if (item.HasExpire  && detail.ExpDate.HasValue == false)
			{
				view.Columns[nameof(detail.ExpDate)].Visible = true;
				e.Valid = false;
				view.SetColumnError(view.Columns[nameof(detail.ExpDate)], "يجب اختيار تاريخ الصلاحيه");
			}
			ViewItemBalanceInGrid(item.ProdecutID, lkp_Store.EditValue.ToInt()); 
			if ((invoiceType ==  MasterClass.InvoiceType.SalesInvoice ||
				invoiceType == MasterClass.InvoiceType.PurchaseReturn || invoiceType == MasterClass.InvoiceType.ItemStoreMove ) &&
				itemsBalanceInStores.Where(x => x.ItemID == item.ProdecutID  ).Sum(x => x.Balance) < detail.ItemQty)
			{
				if(XtraMessageBox.Show("لا يوجد رصيد بالمخزن , هل تريد الاستمرار ؟ ","",MessageBoxButtons.YesNo ,MessageBoxIcon.Warning ) == DialogResult.No)
				{
					view.DeleteRow(e.RowHandle);
				}
			}
			if (item.HasExpire && detail.ExpDate.HasValue && IsNew )
			{
				int relationship = DateTime.Compare(detail.ExpDate.Value, DateTime.Now.Date);
				if (relationship <= 0)
				{
					if (XtraMessageBox.Show("صلاحيه الصنف منتهيه , هل تريد الاستمرار", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
					{
						view.DeleteRow(e.RowHandle); 
					} 
				}
			}




		}

		private void GridView_Items_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
		{
			 
			if (e.Row == null || (e.Row as Inv_InvoiceDetail).ItemID == 0)
				e.ExceptionMode = ExceptionMode.Ignore;
			else
				e.ExceptionMode = ExceptionMode.NoAction;
		}

		private void GridView_Items_RowCountChanged(object sender, EventArgs e)
		{

			GridView_Items_RowUpdated(sender, new RowObjectEventArgs(0, "Index"));

		}
		void LoadItem(int ID)
		{
			DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			Invoice = (from i in dbc.Inv_Invoices where i.ID == ID select i).First();
			invoiceType =(MasterClass.InvoiceType ) Invoice.InvoiceType ;
			RefreshData();
			GetData();
			IsNew = false;
		}
		private void GridView_Items_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
		{
			GridView view = sender as GridView;
			if (view == null || view.Columns.Count == 0) return;
			Inv_InvoiceDetail detail = view.GetRow(e.FocusedRowHandle) as Inv_InvoiceDetail;
			if (detail == null)
				return;
			DAL.Prodecut  item =prodecuts .Where(x => x.ProdecutID  == detail.ItemID).SingleOrDefault();
			if (item == null) return;

			int h = e.FocusedRowHandle;
			view.Columns[nameof(InvoDInsta.Size)].OptionsColumn.AllowEdit = item.HasSize ;
			view.Columns[nameof(InvoDInsta.Color)].OptionsColumn.AllowEdit = item.HasColor;
			view.Columns[nameof(InvoDInsta.ExpDate)].OptionsColumn.AllowEdit = item.HasExpire ;
			view.Columns[nameof(InvoDInsta.ItemQty)].OptionsColumn.AllowEdit = !item.HasSerial;
			view.Columns[nameof(InvoDInsta.ItemUnitID)].OptionsColumn.AllowEdit = !item.HasSerial;
			view.Columns[nameof(InvoDInsta.Serial)].OptionsColumn.AllowEdit = item.HasSerial;
			if (!view.Columns[nameof(InvoDInsta.ItemQty)].OptionsColumn.AllowEdit)
				view.SetRowCellValue(h, nameof(InvoDInsta.ItemQty), 1);

			if (item.HasSerial)
			{
				switch (invoiceType)
				{
					case MasterClass.InvoiceType.SalesInvoice:
					case MasterClass.InvoiceType.PurchaseReturn:
					case MasterClass.InvoiceType.ItemDamage:
					case MasterClass.InvoiceType.ItemStoreMove:
						view.Columns[nameof(InvoDInsta.Size)].OptionsColumn.AllowEdit = false ;
						view.Columns[nameof(InvoDInsta.Color)].OptionsColumn.AllowEdit = false ;
						break;
					case MasterClass.InvoiceType.PurchaseInvoice:
					case MasterClass.InvoiceType.ItemOpenBalance:
					case MasterClass.InvoiceType.SalesReturn:
						view.Columns[nameof(InvoDInsta.Size)].OptionsColumn.AllowEdit = item.HasSize;
						view.Columns[nameof(InvoDInsta.Color)].OptionsColumn.AllowEdit = item.HasColor;
						break;
					default:
						throw new NotImplementedException();
				}
			}

		}


		private void GridView_Items_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
		{
			GridView_Items_FocusedRowChanged(sender, new FocusedRowChangedEventArgs(0, GridView_Items.FocusedRowHandle));

		}
		private void frm_Inv_Invoice_FormClosing(object sender, FormClosingEventArgs e)
		{
			MasterClass.SaveGridLayout(GridView_Items, this);
			MasterClass.SaveGridLayout(gridView1, this); 
			MasterClass.SaveGridLayout(gridLookUpEdit1View, this);
		}
		private void GridView_Items_RowUpdated(object sender, RowObjectEventArgs e)
		{
			var items = GridView_Items.DataSource as Collection<Inv_InvoiceDetail>;
			if (items == null)
				spn_Total.EditValue = 0;
			else
				spn_Total.EditValue = items.Sum(x => x.TotalPrice);
			ChangesMade = true;
		}

		private void GridView_Items_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
		{
			var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);

			GridView Senderview = sender as GridView;

			if (e.Column.FieldName == "ItemUnitID")
			{
				RepositoryItemGridLookUpEdit UOMRepoEXP = new RepositoryItemGridLookUpEdit();
				GridView view = new GridView();
				UOMRepoEXP.View = view;
				UOMRepoEXP.NullText = "";

				DAL.Inv_InvoiceDetail  detail = Senderview.GetRow(e.RowHandle) as DAL.Inv_InvoiceDetail;
			 
				if (detail == null ||detail.ItemID.ValidAsIntNonZero() == false)
				{ e.RepositoryItem = new RepositoryItem(); return; }
				var item = dbc.Prodecuts.Where(x => x.ProdecutID == Convert.ToInt32(Senderview.GetRowCellValue(e.RowHandle, "ItemID"))).Single();
				var datasource = new[] { new { Name = item.FiestUnit, ID = 1 }, new { Name = item.SecoundUnit, ID = 2 }, new { Name = item.ThreeUnit, ID = 3 } };


				var itemunit = datasource.Where(x => string.IsNullOrEmpty(x.Name) == false).ToList();

				UOMRepoEXP.DataSource = itemunit;
				UOMRepoEXP.DisplayMember = "Name";
				UOMRepoEXP.ValueMember = "ID";
				UOMRepoEXP.View.PopulateColumns(itemunit.ToList());
				view.Columns["ID"].Visible = false;
				e.RepositoryItem = UOMRepoEXP;
			}
		}
		MasterClass.ItemLog SharedLog;
		private void GridView_Items_CellValueChanged(object sender, CellValueChangedEventArgs e)
		{
			if (e.RowHandle > GridView_Items.RowCount - 1) return;
			GridControl gridControl = sender as GridControl;
			GridView View = GridView_Items as GridView;
			DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			int index = e.RowHandle;
			Inv_InvoiceDetail detail = View.GetRow(index) as Inv_InvoiceDetail;
			if (detail == null)
				return;
			var item = dbc.Prodecuts.SingleOrDefault(x => x.ProdecutID == detail.ItemID); 
			if (e.Column == null) return;
			switch (e.Column.FieldName)
			{
				case "Code":
					if (e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty) View.DeleteRow(index);
				//	SharedLog = MasterClass.SearchForItem(e.Value.ToString(), (lkp_Store.EditValue == null) ? 0 : Convert.ToInt32(lkp_Store.EditValue));

					if (e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty) View.DeleteRow(index);
					if (CurrentSession.SystemSettings.UseScalBarcode && e.Value.ToString().Length == CurrentSession.SystemSettings.WeightCodeLength + CurrentSession.SystemSettings.ItemCodeLength)
					{
						var itemCode = e.Value.ToString().Substring(0, CurrentSession.SystemSettings.ItemCodeLength);
						var weightText = e.Value.ToString().Substring(CurrentSession.SystemSettings.ItemCodeLength, CurrentSession.SystemSettings.WeightCodeLength);

						double weight = 0;
						double.TryParse(weightText, out weight);
						SharedLog = MasterClass.SearchForItem(itemCode, (lkp_Store.EditValue == null) ? 0 : Convert.ToInt32(lkp_Store.EditValue));
						if (weight > 0)
						{
							weight = weight / (double)CurrentSession.SystemSettings.DivideWeightBy;
							detail.ItemQty = weight;
						}
					}
					else
					{
						SharedLog = MasterClass.SearchForItem(e.Value.ToString(), (lkp_Store.EditValue == null) ? 0 : Convert.ToInt32(lkp_Store.EditValue));


					}

					if (SharedLog.ItemID == 0)
					{
						View.DeleteRow(index);
						break;
					}
					detail.ItemID = SharedLog.ItemID;
					detail.Serial = SharedLog.Serial;
					detail.StoreID = SharedLog.StoreID;
					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Serial)], detail.Serial)); 
					if (CodesDictionary.ContainsKey(index))
						CodesDictionary.Remove(index);
					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemID)], detail.ItemID));
					break;
				case nameof(InvoDInsta.Serial):
					if (string.IsNullOrEmpty(detail.Serial))
						return;
					switch (invoiceType)
					{
						case MasterClass.InvoiceType.SalesInvoice:
						case MasterClass.InvoiceType.PurchaseReturn:
						case MasterClass.InvoiceType.SalesReturn:
						case MasterClass.InvoiceType.ItemDamage:
						case MasterClass.InvoiceType.ItemStoreMove:
							var serialData = dbc.Inv_StoreLogs.FirstOrDefault(x => x.Serial == detail.Serial && x.ItemQuIN == 1);
							if(serialData != null)
							{
								detail.Color = serialData.Color;
								detail.Size = serialData.Size;
							} 
							break;
						case MasterClass.InvoiceType.PurchaseInvoice:
						case MasterClass.InvoiceType.ItemOpenBalance:

							break;
						default:
							throw new NotImplementedException();
					}
				
					break;
				case nameof(InvoDInsta.ItemID):

					if (detail.ItemID == 0)   //e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty)
					{
						View.DeleteRow(index);
						break;
					}
					if (item == null) return;
					if (SharedLog.ItemID == 0)
						SharedLog = MasterClass.GetNextItemForSell(detail.ItemID, Convert.ToInt32(lkp_Store.EditValue));
					if (detail.ItemUnitID.ValidAsIntNonZero() == false)
						detail.ItemUnitID = item.UnitDefoult ?? 1;
					if(detail.StoreID .ValidAsIntNonZero() == false)
						detail.StoreID = SharedLog.StoreID == 0 ? lkp_Store.EditValue.ToInt() : SharedLog.StoreID;
					  if (detail.ItemQty <= 0)
                        detail.ItemQty = 1;

					switch (invoiceType)
					{
						case MasterClass.InvoiceType.SalesInvoice:
						case MasterClass.InvoiceType.PurchaseReturn:
						case MasterClass.InvoiceType.ItemDamage:
						case MasterClass.InvoiceType.ItemStoreMove:
							detail.Color = SharedLog.Color;
							detail.ExpDate = SharedLog.ExpirDate;
							detail.Size = SharedLog.Size;
							break;
						case MasterClass.InvoiceType.PurchaseInvoice:
						case MasterClass.InvoiceType.ItemOpenBalance:
						case MasterClass.InvoiceType.SalesReturn:


							break;
						default:
							throw new NotImplementedException();
					}


					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemUnitID)], detail.ItemUnitID));
					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemQty)], detail.ItemQty));
					break;
				case nameof(InvoDInsta.ItemUnitID):
					if (detail.ItemUnitID <= 0) break;
					if (item == null) return; 
					switch (invoiceType)
					{
					 
						case MasterClass.InvoiceType.SalesInvoice:
						case MasterClass.InvoiceType.SalesReturn:
							if (detail.ItemUnitID == 1)
							{
								if (radioGroup1.SelectedIndex == 0)
									detail.Price = item.FiestUnitPrice1 ?? 0;
								else if (radioGroup1.SelectedIndex == 1)
									detail.Price = item.FiestUnitPrice2 ?? 0;
								else
									detail.Price = item.FiestUnitPrice3 ?? 0;
								detail.CostValue = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.FiestUnitFactor);
							}
							else if (detail.ItemUnitID == 2)
							{
								if (radioGroup1.SelectedIndex == 0)
									detail.Price = item.SecoundUnitPrice1 ?? 0;
								else if (radioGroup1.SelectedIndex == 1)
									detail.Price = item.SecoundUnitPrice2 ?? 0;
								else
									detail.Price = item.SecoundUnitPrice3 ?? 0;
								detail.CostValue = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.SecoundUnitFactor);
							}
							else if (detail.ItemUnitID == 3)
							{
								if (radioGroup1.SelectedIndex == 0)
									detail.Price = item.ThreeUnitPrice1 ?? 0;
								else if (radioGroup1.SelectedIndex == 1)
									detail.Price = item.ThreeUnitPrice2 ?? 0;
								else
									detail.Price = item.ThreeUnitPrice3 ?? 0;
								detail.CostValue = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.ThreeUnitFactor);
							}
							break;
						case MasterClass.InvoiceType.PurchaseInvoice:
						case MasterClass.InvoiceType.PurchaseReturn:
						case MasterClass.InvoiceType.ItemOpenBalance:
						case MasterClass.InvoiceType.ItemDamage:
						case MasterClass.InvoiceType.ItemStoreMove:
							if (detail.ItemUnitID == 1)
							{
								detail.Price = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.FiestUnitFactor);
							}
							else if (detail.ItemUnitID == 2)
							{
								detail.Price = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.SecoundUnitFactor);
							}
							else if (detail.ItemUnitID == 3)
							{
								detail.Price = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.ThreeUnitFactor);
							}
							detail.CostValue = detail.Price;
							spn_TotalRevenue_EditValueChanged(null, null);
							break;
						default:
							throw new NotImplementedException() ;
					}
					


					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Price)], detail.Price));
					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemQty)], detail.ItemQty));
					break;
				case nameof(InvoDInsta.ItemQty):
					if (detail.ItemID == 0) break;
					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Price)], View.GetRowCellValue(index, nameof(InvoDInsta.Price))));
					double Factor = 0;
					if (detail.ItemUnitID == 1)
					{
						Factor =Convert.ToDouble( item.FiestUnitFactor);
					}
					else if (detail.ItemUnitID == 2)
					{
						Factor = Convert.ToDouble(item.SecoundUnitFactor );

					}
					else if (detail.ItemUnitID == 3)
					{
						Factor = Convert.ToDouble(item.ThreeUnitFactor );

					}

					switch (invoiceType)
					{
						case MasterClass.InvoiceType.SalesInvoice:
						case MasterClass.InvoiceType.PurchaseReturn:
						case MasterClass.InvoiceType.SalesReturn:
						case MasterClass.InvoiceType.ItemDamage:
						case MasterClass.InvoiceType.ItemStoreMove:

							var CurrentItemCostPerQty = MasterClass.GetCurrentItemCost(detail.StoreID ?? lkp_Store.EditValue.ToInt(), detail.ItemID);
							var remainingQty = detail.ItemQty * Factor;
							double totalPrice = 0;
							int pos = 0;
							while (remainingQty > 0)
							{
								if (CurrentItemCostPerQty.Count - 1 < pos)
									break;
								double minmumQty = (remainingQty <= CurrentItemCostPerQty[pos].AvalableAmount) ? remainingQty : CurrentItemCostPerQty[pos].AvalableAmount;
								totalPrice += CurrentItemCostPerQty[pos].UnitCostPrice * minmumQty;
								remainingQty = remainingQty - minmumQty;
								pos++;
							}
							detail.TotalCostValue = totalPrice;
							detail.CostValue = detail.TotalCostValue / detail.ItemQty;
							break;
						case MasterClass.InvoiceType.PurchaseInvoice:
						case MasterClass.InvoiceType.ItemOpenBalance:
							break;
						default:
							throw new NotImplementedException();
					}
				

					break;
				case nameof(InvoDInsta.Price):
				case nameof(InvoDInsta.Discount):
				case nameof(InvoDInsta.AddTax): 
					detail.DiscountValue = detail.Discount * (detail.ItemQty * detail.Price);
					detail.AddTaxVal = detail.AddTax * (detail.ItemQty * detail.Price);
					GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.DiscountValue)], detail.DiscountValue));
					switch (invoiceType)
					{ 
						case MasterClass.InvoiceType.SalesInvoice:
						case MasterClass.InvoiceType.SalesReturn:
							break;
						case MasterClass.InvoiceType.PurchaseInvoice:
						case MasterClass.InvoiceType.ItemOpenBalance:
						case MasterClass.InvoiceType.PurchaseReturn:
						case MasterClass.InvoiceType.ItemDamage:
						case MasterClass.InvoiceType.ItemStoreMove:
							detail.CostValue = detail.Price;
							spn_TotalRevenue_EditValueChanged(null, null);
							break;
						default:
							throw new NotImplementedException();
					} 
					break;
				case nameof(InvoDInsta.DiscountValue):
				case nameof(InvoDInsta.AddTaxVal):
					if (View.FocusedColumn.FieldName == nameof(InvoDInsta.DiscountValue))
					{
						detail.Discount = detail.DiscountValue / (detail.ItemQty * detail.Price);
					}
					if (View.FocusedColumn.FieldName == nameof(InvoDInsta.AddTaxVal))
					{
						detail.AddTaxVal = detail.AddTaxVal / detail.TotalPrice;
					}
					detail.TotalPrice = (detail.ItemQty * detail.Price)+ detail.AddTaxVal  - detail.DiscountValue;
					break;
			}
			SharedLog.ItemID = 0;

		}
		//MasterClass.AccountBalance CustomerBalance;
		private void glkp_Customer_EditValueChanged(object sender, EventArgs e)
		{
			//DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			//var cus = (from c in db.Sl_Customers
			//		   where c.ID == Convert.ToInt32(LookUpEdit_PartID.EditValue)
			//		   select c).FirstOrDefault();
			//if (cus == null) return;
			//txt_Address.Text = cus.Address;
			//txt_City.Text = cus.City;
			//txt_MaxCredit.Text = cus.MaxCredit.ToString();
			//txt_Mobile.Text = cus.Mobile;
			//txt_Phone.Text = cus.Phone;
			//CustomerBalance = MasterClass.GetAccountBalance(cus.Account);
			//txt_BalanceBefore.Text = Math.Abs(CustomerBalance.Balance).ToString();
			//txt_BBType.Text = (CustomerBalance.Balance >= 0) ? LangResource.Debit : LangResource.Credit;
			//if (IsNew)
			//{
			//	txt_BalanceAfter.Text = Math.Abs((CustomerBalance.Balance + Convert.ToDouble(spn_Remains.EditValue))).ToString();
			//	txt_BAType.Text = ((CustomerBalance.Balance - Convert.ToDouble(spn_Remains.EditValue)) >= 0) ? LangResource.Debit : LangResource.Credit;
			//}
			//else
			//{
			//	txt_BalanceAfter.Text = txt_BalanceBefore.Text;
			//	txt_BAType.Text = txt_BBType.Text;
			//}



		}

		private void glkp_Customer_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
				return;

			string btnName = e.Button.Tag.ToString();
			if (btnName == "Add")
			{
				DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
				if (invoiceType == MasterClass.InvoiceType.SalesInvoice)
					new PL.Customers_frm().ShowDialog();
				RefreshData();
			}
		}

		private void glkp_Customer_Popup(object sender, EventArgs e)
		{
			MasterClass.RestoreGridLookUpEditViewLayout(sender, this.Name);

		}
		private MemoryStream rep_layout = new MemoryStream();
		private void glkp_Customer_CloseUp(object sender, CloseUpEventArgs e)
		{
			MasterClass.SaveGridLookUpEditViewLayout(sender, this.Name);
		}

	  

	  

		private void lkp_Store_EditValueChanged(object sender, EventArgs e)
		{
			if (lkp_Store.EditValue.ValidAsIntNonZero())
				if (invoiceType == MasterClass.InvoiceType.ItemStoreMove)
				{
					DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
					lkp_ToStore .Properties.DataSource = db.Stores.Where(x => x.StoreID != Convert.ToInt32(lkp_Store.EditValue)).Select(x => new { ID = x.StoreID, Name = x.StoreName }).ToList();
					 
				}
			UpdateItemsBalanceFromGridView();
		}
		void UpdateItemsBalanceFromGridView()
		{
			bool _ChangeIsMade = this.ChangesMade;
			GridView View = GridView_Items;
			for (int i = 0; i <= GridView_Items.RowCount - 1; i++)
			{
				int h = i;
				if (View.GetRowCellValue(h, "ItemID") == null || View.GetRowCellValue(h, "ItemID") == DBNull.Value) return;
				if (View.Columns["CurrentBalance"].Visible != false  )
				{
					ViewItemBalanceInGrid(View.GetRowCellValue(h, "ItemID").ToInt(), lkp_Store.EditValue.ToInt());

				}

			}
			this.ChangesMade = _ChangeIsMade;

		}
		void CalculateItemBalanceInRow(int h)
		{
			GridView View = GridView_Items;
			if (View.GetRowCellValue(h, "ItemID") == null || View.GetRowCellValue(h, "ItemID") == DBNull.Value) return;
			ViewItemBalanceInGrid(View.GetRowCellValue(h, "ItemID").ToInt(), lkp_Store.EditValue.ToInt());


		}


		private void spn_Precent_EditValueChanged(object sender, EventArgs e)
		{
			SpinEdit spn_Precent = sender as SpinEdit;
			if (spn_Precent == null) return;
			SpinEdit spn_Val = null;
			switch (spn_Precent.Name)
			{
			 
				case nameof(spn_Discount):
					spn_Val = spn_DiscountVal;
					break;
			}
			if (spn_Val == null) return;
			if (spn_Val.IsEditorActive) return;
			Double Prst = Convert.ToDouble(spn_Precent.EditValue);
			Double Val = Convert.ToDouble(spn_Val.EditValue);
			Double Total = Convert.ToDouble(spn_Total.EditValue);
			spn_Val.EditValue = (Total * Prst);


		}
		void CalculateNetVal()
		{
			Double Total = Convert.ToDouble(spn_Total.EditValue); 
			Double Discount = Convert.ToDouble(spn_DiscountVal.EditValue);
			Double OtherCharges = Convert.ToDouble(spn_TotalRevenue.EditValue);
			Double Net =  (Total  + OtherCharges)   - Discount;
			spn_Net.EditValue = Net;
	
		}


		private void spn_Total_EditValueChanged(object sender, EventArgs e)
		{
			spn_Precent_EditValueChanged(spn_Discount, null);
			CalculateNetVal();
		}

		private void spn_Val_EditValueChanged(object sender, EventArgs e)
		{
			CalculateNetVal();
			SpinEdit spn_Val = sender as SpinEdit;
			if (spn_Val == null) return;
			if (!spn_Val.IsEditorActive) return;
			SpinEdit spn_Precent = null;
			switch (spn_Val.Name)
			{
				 
				case nameof(spn_DiscountVal):
					spn_Precent = spn_Discount;
					break;
			}
			if (spn_Precent == null) return;

			Double Prst = Convert.ToDouble(spn_Precent.EditValue);
			Double Val = Convert.ToDouble(spn_Val.EditValue);
			Double Total = Convert.ToDouble(spn_Total.EditValue);
			spn_Precent.EditValue = (Val / Total);


		}
		bool IsPaidChangedByUser=false ;

		private void spn_Net_EditValueChanged(object sender, EventArgs e)
		{
			 
			spn_Remains.EditValue = Convert.ToDouble(spn_Net.EditValue) - Convert.ToDouble(spn_Paid.EditValue);
			spn_Remains_EditValueChanged(null, null);
		}

		private void spn_Paid_EditValueChanged(object sender, EventArgs e)
		{
			spn_Remains.EditValue =Math.Abs( Convert.ToDouble(spn_Net.EditValue) - (Convert.ToDouble(spn_Paid.EditValue)));

			//if (IsNew)
			//{
			//	txt_BalanceAfter.Text = Math.Abs((CustomerBalance.Balance + Convert.ToDouble(spn_Remains.EditValue))).ToString();
			//	txt_BAType.Text = ((CustomerBalance.Balance - Convert.ToDouble(spn_Remains.EditValue)) >= 0) ? LangResource.Debit : LangResource.Credit;
			//}
			//else
			//{
			//	txt_BalanceAfter.Text = txt_BalanceBefore.Text;
			//	txt_BAType.Text = txt_BBType.Text;
			//}
		}

	 

 

		private void spn_Remains_EditValueChanged(object sender, EventArgs e)
		{
			if (Convert.ToDouble(spn_Remains.EditValue) == 0)
				spn_Remains.BackColor = System.Drawing.Color.LightGreen;
			else
				spn_Remains.BackColor = System.Drawing.Color.FromArgb(255, 128, 128);

			//if (spn_Remains.EditValue.ToInt() <= 0)
			//	ComboBoxEdit_PayStatus.SelectedIndex = 2; //مسدده
			//else if (spn_Remains.EditValue.ToInt() > 0 && spn_Remains.EditValue.ToInt() < spn_Net.EditValue.ToInt())
			//	ComboBoxEdit_PayStatus.SelectedIndex = 1; //مسدده جزئياً
			//else if (spn_Remains.EditValue.ToInt() > 0 && spn_Remains.EditValue.ToInt() == spn_Net.EditValue.ToInt())
			//	ComboBoxEdit_PayStatus.SelectedIndex = 0; //غير مسدده  
			//else
			//	ComboBoxEdit_PayStatus.SelectedIndex = -1;
		}
		public static bool CheckIfInvoiceCanBeDeleted(int id, int invoiceType, out string _Message)
		{
			DAL.DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
			_Message = ("تم حذف فاتوره رقم @  بنجاح").Replace("@", id.ToString());
			//if (db.Inv_Invoices.Where(x => x.Source == invoiceType && x.SourceID == id).Count() > 0)
			//{
			//	_Message = LangResource.InvoiceDeleteFaild.Replace("@", id.ToString()) + Environment.NewLine + LangResource.CantDeleteInvoiceRelatedBills;
			//	return false;
			//}


			return true;
		}

		 

		private void GridView_Items_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
		{
			ColumnView columnView = sender as ColumnView;
			int h = e.RowHandle;
			GridView view = sender as GridView;
			DAL.Inv_InvoiceDetail detail = view.GetRow(h) as DAL.Inv_InvoiceDetail ;
			if (e.RowHandle < 0) return;
			if (detail.ExpDate.HasValue)
			{
				if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, nameof(InvoDInsta.ExpDate))) <= DateTime.Now)
					e.Appearance.BackColor = System.Drawing.Color.Red;
				else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, nameof(InvoDInsta.ExpDate))).AddDays(-15) <= DateTime.Now)
					e.Appearance.BackColor = System.Drawing.Color.DarkOrange;
				else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, nameof(InvoDInsta.ExpDate))).AddDays(-30) <= DateTime.Now)
					e.Appearance.BackColor = System.Drawing.Color.Orange;
				else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, nameof(InvoDInsta.ExpDate))).AddDays(-45) <= DateTime.Now)
					e.Appearance.BackColor = System.Drawing.Color.Yellow;
				else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, nameof(InvoDInsta.ExpDate))).AddDays(-60) <= DateTime.Now)
					e.Appearance.BackColor = System.Drawing.Color.LightYellow;
			}
		}

		private void spn_Net_DoubleClick(object sender, EventArgs e)
		{
			IsPaidChangedByUser = false;
			spn_Net_EditValueChanged(sender, new EventArgs());
		}

		public override void Print()
		{
			//if (ChangesMade)
			//	Save();

			//if (ChangesMade)
			//	return;
				Print(new List<int>() { Invoice.ID }, this.Name, invoiceType);

		}



		public static void Delete(List<int> ids, string CallerForm, MasterClass.InvoiceType _invoiceType )

		{
		 
			DAL.DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
			List<int> PassList = new List<int>();
			string log = string.Empty;
			foreach (int item in ids)
			{
				string Msg = string.Empty;
				if (CheckIfInvoiceCanBeDeleted(item, (int)_invoiceType, out Msg))
					PassList.Add(item);
				log += Msg + Environment.NewLine;
			}
			ids = PassList;

			int movementTypes;
			switch (_invoiceType )
			{
				case MasterClass.InvoiceType.SalesInvoice:
					movementTypes = (int)MasterClass.TreasuryMovementTypes.SalesInvoice;
					break;
				case MasterClass.InvoiceType.PurchaseInvoice:
					movementTypes = (int)MasterClass.TreasuryMovementTypes.PurchaseInvoice ;
					break;
				case MasterClass.InvoiceType.PurchaseReturn:
					movementTypes = (int)MasterClass.TreasuryMovementTypes.PurchaseReturn;
					break;
				case MasterClass.InvoiceType.SalesReturn:
					movementTypes = (int)MasterClass.TreasuryMovementTypes.SalesReturn;
					break;
				default:
					movementTypes =0 ;
					break;
			}

			if(movementTypes > 0)
			{
				db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == (movementTypes).ToString() && ids.Contains(Convert.ToInt32(x.VoucherID))));
				db.SubmitChanges();
			}

			
			db.Inv_StoreLogs.DeleteAllOnSubmit(from l in db.Inv_StoreLogs where l.Type == (int)_invoiceType && (from d in db.Inv_InvoiceDetails where ids.Contains(d.InvoiceID) select d.ID).ToList().Contains((int)l.TypeID) select l);
			db.SubmitChanges();
			db.Inv_InvoiceDetails.DeleteAllOnSubmit(from d in db.Inv_InvoiceDetails where ids.Contains(d.InvoiceID) select d);
			db.SubmitChanges();
			db.Inv_Invoices.DeleteAllOnSubmit(db.Inv_Invoices.Where(c => ids.Contains(c.ID)));
			db.SubmitChanges();
			frm_LogViewer.ViewLog("سجل حذف الفواتير", log);
	 

		}

		public static object PrintDataSource(List<int> ids, MasterClass.InvoiceType invoiceType)
		{

			DAL.DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
			//var parts = db.Customers.Select(x => new { ID = x.CustomerID, Name = x.CustomerName, Type = (int)MasterClass.PartTypes.Customer });
			//parts.Concat(db.Suppliers.Select(x => new { ID = x.SupplierID , Name = x.SupplierID , Type = (int)MasterClass.PartTypes.Vendor  }));
			var Invoice = (from h in db.Inv_Invoices
						   join b in db.Stores on h.StoreID equals b.StoreID  
						   from user in db.UserPermissions .Where(x => x.ID == h.UserID).DefaultIfEmpty()
						   where ids.Contains(h.ID)
						   select new
						   {
							   h.ID,
							   h.Code,
							   h.PartType,
							   h.PartID,
							   PartTypeText = "",
							   Customer = "", 
							   Store = b.StoreName ,
							   h.Date, 
							   h.Notes,
							   h.DiscountRatio,
							   h.DiscountValue,
							   
							   h.Total,
							   h.TotalRevenue, 
							   h.Net,
							   NetText = "",
							    h.Paid  ,
							   Remains = h.Net - h.Paid,
							   h.Car,
							   h.Driver,
							   h.Destination,
							   h.ShippingAddress,
							  City ="",
							  Address = "",
							  Mobile = "",
							   Phone = "",
							   user.UserName,
							   ProductsCount= db.Inv_InvoiceDetails.Where(x => x.InvoiceID == h.ID).Select(x=>x.ItemID).Distinct().Count(),
							   QtyCount = db.Inv_InvoiceDetails.Where(x => x.InvoiceID == h.ID).Sum (x => x.ItemQty) ,
							   Products = db.Inv_InvoiceDetails.Where(x => x.InvoiceID == h.ID).Select(x => new
							   {
								   x.InvoiceID,
								   Index = 0,
								   x.ItemID, 
								   Item = db.Prodecuts .Where(u => u.ProdecutID  == x.ItemID).SingleOrDefault().ProdecutName ,
								   Unit = "",
								   UnitID = x.ItemUnitID, 
								   x.ItemQty,
								   x.Price,
								   x.Discount,
								   x.DiscountValue,
								   x.AddTax ,
								   x.AddTaxVal ,
								   x.TotalPrice,
								   Color = db.Inv_Colors.Where(u => u.ID == x.Color).SingleOrDefault().Name,
								   x.ExpDate,
								   Serial =string.IsNullOrEmpty(x.Serial )? x.ItemID.ToString():x.Serial  ,
								   Size = db.Inv_Sizes.Where(u => u.ID == x.Size).SingleOrDefault().Name,
								 
							   }).ToList()
						   }).ToList();

			var parts = new[]
				 {
					  new { ID = 1 ,Name = "مورد"  },
					  new { ID = 2 ,Name = "عميل" },
					  new { ID = 3 ,Name = ""  },
					  new { ID = 4 ,Name =  ""},
					};


			Invoice.ForEach(x =>
			{
				x.Set(i => i.NetText, MasterClass.ConvertMoneyToText(x.Net.ToString()));
				x.Set(i => i.PartTypeText, parts.Single(p => p.ID == x.PartType).Name);
				 
				if (x.PartType == (int)MasterClass.PartTypes.Customer)
				{
					var customer = db.Customers.Single(cus => cus.CustomerID == x.PartID);
					x.Set(i => i.Customer,customer.CustomerName );
					x.Set(i => i.Address, customer.Address);
					x.Set(i => i.Phone, customer.Phone);
					x.Set(i => i.Mobile, customer.Phone2);

				}
				else if (x.PartType == (int)MasterClass.PartTypes.Vendor)
				{
					var vendor = db.Suppliers.Single(cus => cus.SupplierID == x.PartID);
					x.Set(i => i.Customer, vendor.SupplierName);
					x.Set(i => i.Address, vendor.Address);
					x.Set(i => i.Phone, vendor.Phone);
					x.Set(i => i.Mobile, vendor.Phone2);
				}

				int Index = 1;
				x.Products.ForEach(p =>
				{
					p.Set(i => i.Index, Index++);
					var product = db.Prodecuts.Single(pro => pro.ProdecutID == p.ItemID);
					switch (p.UnitID)
					{
						case 1: p.Set(i => i.Unit, product.FiestUnit); break;
						case 2: p.Set(i => i.Unit, product.SecoundUnit); break;
						case 3: p.Set(i => i.Unit, product.ThreeUnit); break;
					}
				});

			});


			return Invoice;
		}
		  

		public static void Print(List<int> ids, string CallerForm, MasterClass.InvoiceType invoiceType)
		{
		 
			List<int> IdsToRemove = new List<int>();
			DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
			RPT.rpt_Inv_Invoice.Print(PrintDataSource(ids, invoiceType), invoiceType);
			  
		}
		 

		private void LookUpEdit_PartID_EditValueChanging(object sender, ChangingEventArgs e)
		{
			//var pays = GridView_Pays.DataSource as Collection<Acc_Pay>;

			//if (pays != null && pays.Count(x => x.PayType == (byte)MasterClass.PayTypes.CashNote) != 0 && IsLoadingData == false)
			//{
			//	XtraMessageBox.Show(LangResource.CantChangePartAfterAddingCashNotes);
			//	e.Cancel = true;
			//}

		}

		private void frm_Inv_Invoice_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.F1)
			{
				simpleButton1.PerformClick();
			}
		}

		private void simpleButton1_Click(object sender, EventArgs e)
		{
			var items = GridView_Items.DataSource as Collection<Inv_InvoiceDetail>;
			if (items != null)
				spn_Paid.EditValue = items.Sum(x => x.TotalPrice);
		}

		private void spn_TotalRevenue_EditValueChanged(object sender, EventArgs e)
		{
			double Expenses = Convert.ToDouble(spn_TotalRevenue.EditValue);
			var items = GridView_Items.DataSource as Collection<Inv_InvoiceDetail>;
			if (items == null) return;
			if (IsLoadingData == true) return;

			switch (invoiceType)
			{
			 
				case MasterClass.InvoiceType.SalesInvoice:
				case MasterClass.InvoiceType.PurchaseReturn:
				case MasterClass.InvoiceType.SalesReturn:
				case MasterClass.InvoiceType.ItemStoreMove:
					break; 
				case MasterClass.InvoiceType.PurchaseInvoice:
				case MasterClass.InvoiceType.ItemOpenBalance:
				case MasterClass.InvoiceType.ItemDamage:
					var totalPrice = items.Sum(x => x.TotalPrice);
					var totalItemsQty = items.Sum(x => x.ItemQty );
					var PriceUnit = Expenses / totalPrice;
					var QtyUnit = Expenses / totalItemsQty;
					foreach (var item in items)
					{
						var PriceByPriceUnit = item.Price + (item.Price * PriceUnit);
						var PriceByQtyUnit = item.Price +  QtyUnit;
						var Avg = (PriceByQtyUnit + PriceByPriceUnit) / 2;
						item.CostValue = PriceByQtyUnit;
						item.TotalCostValue = item.CostValue * item.ItemQty;
					}
					break;
				 
				default:
					throw new NotImplementedException();
			}
		}

		private void LookUpEdit_SourceID_Properties_Click(object sender, EventArgs e)
		{
			//MessageBox.Show("");
		}

		private void LookUpEdit_SourceID_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			MasterClass.InvoiceType InvoiceReturnType;
			switch (invoiceType)
			{ 
				case MasterClass.InvoiceType.PurchaseReturn:
					InvoiceReturnType = MasterClass.InvoiceType.PurchaseInvoice;
					break;
				case MasterClass.InvoiceType.SalesReturn :
					InvoiceReturnType = MasterClass.InvoiceType.SalesInvoice ;
					break;
				default:
					throw new NotImplementedException ();
			}
			var frm = new frm_Inv_InvoiceList(InvoiceReturnType, true  );
			frm.ShowDialog();
			LookUpEdit_SourceID.EditValue  = frm_Inv_InvoiceList.SelectedInvoiceID;
			if (LookUpEdit_SourceID.EditValue.ValidAsIntNonZero())
			{
				DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
				var list = db.Inv_InvoiceDetails.Where(x => x.InvoiceID == Convert.ToInt32(LookUpEdit_SourceID.EditValue)).ToList();
				var invoice = db.Inv_Invoices.SingleOrDefault(x => x.ID == Convert.ToInt32(LookUpEdit_SourceID.EditValue));

				IsLoadingData = true;
				LookUpEdit_PartID.EditValue = invoice.PartID;
		        lkp_Store.EditValue  = invoice.StoreID ;

				var items = GridView_Items.DataSource as Collection<Inv_InvoiceDetail>;
				

				GridView_Items.SelectAll();
				GridView_Items.DeleteSelectedRows();
				list.ForEach(item =>
				{
					items.Add(new Inv_InvoiceDetail()
					{
						AddTax = item.AddTax,
						AddTaxVal = item.AddTaxVal,
						Color = item.Color,
						CostValue = item.CostValue,
						Discount = item.Discount,
						DiscountValue = item.DiscountValue,
						ExpDate = item.ExpDate,
						ItemID = item.ItemID,
						ItemQty = item.ItemQty,
						ItemUnitID = item.ItemUnitID,
						Price = item.Price,
						Serial = item.Serial,
						Size = item.Size,
						StoreID = item.StoreID,
						TotalCostValue = item.TotalCostValue,
						TotalPrice = item.TotalPrice,
					});
				});
				GridView_Items_RowUpdated(null, null);
			    IsLoadingData = false;
			}
		}

		private void LookUpEdit_SourceID_EditValueChanged(object sender, EventArgs e)
		{
			 
				LookUpEdit_PartID.ReadOnly = LookUpEdit_SourceID.ValidAsIntNonZero();
			 
		}

		private void textEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
		{
			textEdit1.Text = "";
		}

		private void textEdit1_ButtonPressed(object sender, ButtonPressedEventArgs e)
		{
			 
			Double Total = Convert.ToDouble(spn_Total.EditValue);
			Double Discount = Convert.ToDouble(spn_DiscountVal.EditValue);
			Double OtherCharges = Convert.ToDouble(spn_TotalRevenue.EditValue);
			Double Net = (Total + OtherCharges) - Discount;
			spn_Net.EditValue = Net;
			var TotalCost = ((Collection<Inv_InvoiceDetail>)GridView_Items.DataSource).Sum(x => (double?)x.TotalCostValue) ?? 0;
			textEdit1.Text = (Net - TotalCost).ToString();
		}

		private void frm_Inv_Invoice_Load(object sender, EventArgs e)
		{
			this.Load += (_sender, _e) => { MasterClass.RestoreGridLayout(gridView1, this); };
			this.FormClosing += (_sender, _e) => { MasterClass.SaveGridLayout(gridView1, this); };
		}
	}
}
