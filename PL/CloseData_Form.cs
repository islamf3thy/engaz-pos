﻿using ByStro.BL;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class CloseData_Form : Form
    {
        public CloseData_Form()
        {
            InitializeComponent();
        }
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        Boolean CompleteCustomers = true;
        private void CloseData_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);

        }
        private void butData_Click(object sender, EventArgs e)
        {
            Customers();
            Suppliers();
            //if (!BWorkerCustomers.IsBusy)
            //{

            //    BWorkerCustomers.RunWorkerAsync(); 
            //}
        }

        private void Customers()
        {
            Customers_cls cls = new Customers_cls();
            DataTable dt_cus = cls.Search_Customers("");

            for (int i = 0; i < dt_cus.Rows.Count; i++)
            {
                double balenceDebit = 0;
                double balenceCredit = 0;
                DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(dt_cus.Rows[i]["CustomerID"].ToString(), "Cus");//Cus_Trans_cls.Cus_Balence(dt_cus.Rows[i]["CustomerID"].ToString());                                                                                                   //{
                if (Convert.ToDouble(dt.Rows[0]["balence"].ToString()) > 0)
                {
                    balenceDebit = Convert.ToDouble(dt.Rows[0]["balence"].ToString());
                    balenceCredit = 0;
                }
                else
                {
                    balenceCredit = (Convert.ToDouble(dt.Rows[0]["Credit"].ToString()) - Convert.ToDouble(dt.Rows[0]["Debit"].ToString()));
                    balenceDebit = 0;
                }

                cls.Update_Customers(dt_cus.Rows[i]["CustomerID"].ToString(), balenceDebit, balenceCredit);
            }
        }

        private void Suppliers()
        {
            Suppliers_cls cls = new Suppliers_cls();
            DataTable dt_cus = cls.Search_Suppliers("");

            for (int i = 0; i < dt_cus.Rows.Count; i++)
            {
                double balenceDebit = 0;
                double balenceCredit = 0;
                DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(dt_cus.Rows[i]["SupplierID"].ToString(), "Supp"); // Cus_Trans_cls.Sup_Balence(dt_cus.Rows[i]["SupplierID"].ToString());
                if (dt.Rows[0]["balence"] == DBNull.Value == false)
                {
                    if (Convert.ToDouble(dt.Rows[0]["balence"].ToString()) > 0)
                    {
                        balenceDebit = Convert.ToDouble(dt.Rows[0]["balence"].ToString());
                        balenceCredit = 0;
                    }
                    else
                    {
                        balenceCredit = (Convert.ToDouble(dt.Rows[0]["Credit"].ToString()) - Convert.ToDouble(dt.Rows[0]["Debit"].ToString()));
                        balenceDebit = 0;
                    }
                    cls.Update_Suppliers(dt_cus.Rows[i]["SupplierID"].ToString(), balenceDebit, balenceCredit);
                }
            }
        }
        private void BWorkerCustomers_DoWork(object sender, DoWorkEventArgs e)
        {
         
        }


      




        

        private void BWorkerCustomers_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
 
                progressBar.Value = e.ProgressPercentage;
                lblPrcent.Text = string.Format("{0} %", e.ProgressPercentage);
                progressBar.Update();
            }
            catch (Exception)
            {


            }
        }

        private void BWorkerCustomers_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (  CompleteCustomers == true)
            {
              
                    lblMessage.Text = "Successfully Clear Data  Completed ...";
                    lblMessage.ForeColor = Color.Green;
                    this.ControlBox = true;
                    butData.Enabled = true;
                }
                else
                {
                    lblMessage.Text = "Error Clear Data  Error ...";
                    lblMessage.BackColor = Color.Red;
                    lblMessage.ForeColor = Color.White;
                    this.ControlBox = true;
                    butData.Enabled = true;
                }
            
          
        }


        




    }
}
