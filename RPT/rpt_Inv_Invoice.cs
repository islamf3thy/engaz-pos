﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.IO;
using ByStro.Clases;

namespace ByStro.RPT
{
     
    public partial class rpt_Inv_Invoice : rpt_Master
    {
        public rpt_Inv_Invoice(/*MasterClass.InvoiceType invoiceType*/ )
        {
            InitializeComponent();


            lbl_PrinDate.Text = DateTime.Now.ToString();
            lbl_UserName.Text = CurrentSession.user.UserName;
            lbl_companyName.Text = CurrentSession.Company.CombanyName;
            lbl_Cpmpany_Phone.Text = CurrentSession.Company.PhoneNamber;
             
        }
        public override void LoadData()
        {

            



            cell_Index.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Index"));
            cell_Item.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Item"));
            cell_Unit.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Unit"));
            cell_Qty.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "ItemQty"));
            cell_Price.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Price"));

            cell_Dicount.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Discount"));
            cell_DiscountValue.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "DiscountValue"));

            cell_Total.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "TotalPrice"));
            cell_Color.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Color"));
            cell_Expire.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "ExpDate"));
            cell_Serial.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Serial"));
            cell_Size.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Size"));
            Cell_Vat .ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "AddTax"));
            Cell_VATValue.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "AddTaxVal"));

            lbl_ID.DataBindings.Add("Text", DataSource, "ID");
            lbl_Code.DataBindings.Add("Text", DataSource, "Code");
            lbl_PartName.DataBindings.Add("Text", DataSource, "Customer");
            lbl_PartType.DataBindings.Add("Text", DataSource, "PartTypeText");
            lbl_Attintion.DataBindings.Add("Text", DataSource, "AttintionMs");
            lbl_Store.DataBindings.Add("Text", DataSource, "Store");
            lbl_Date.DataBindings.Add("Text", DataSource, "Date"); 
            lbl_Notes.DataBindings.Add("Text", DataSource, "Notes");
            lbl_Discount.DataBindings.Add("Text", DataSource, "DiscountRatio");
            lbl_DicountValue.DataBindings.Add("Text", DataSource, "DiscountValue");
            lbl_Total.DataBindings.Add("Text", DataSource, "Total");
            lbl_OtherCharges.DataBindings.Add("Text", DataSource, "TotalRevenue");
           
            lbl_Net.DataBindings.Add("Text", DataSource, "Net");
            lbl_NetAsString.DataBindings.Add("Text", DataSource, "NetText");
            lbl_Paid.DataBindings.Add("Text", DataSource, "Paid");
            lbl_Remains.DataBindings.Add("Text", DataSource, "Remains");
            lbl_Driver.DataBindings.Add("Text", DataSource, "Driver");
            lbl_ShipTo.DataBindings.Add("Text", DataSource, "Distnation");
           
            lbl_InsertUser.DataBindings.Add("Text", DataSource, "UserName");

            lbl_City.DataBindings.Add("Text", DataSource, "City");
            lbl_Address.DataBindings.Add("Text", DataSource, "Address");
            lbl_Phone.DataBindings.Add("Text", DataSource, "Phone");
            lbl_Mobile.DataBindings.Add("Text", DataSource, "Mobile");
            lbl_QtyCount.DataBindings.Add("Text", DataSource, "QtyCount");
            lbl_ProductsCount.DataBindings.Add("Text", DataSource, "ProductsCount");
            lbl_ShipTo.DataBindings.Add("Text", DataSource, "ShippingAddress");
            



            try
            {
                xrPictureBox1.Image = MasterClass.GetImageFromByte(CurrentSession.Company.Imge.ToArray());
            }
            catch (Exception)
            {
            }

            base.LoadData();
        }
        public static void Print(object ds, MasterClass.InvoiceType invoiceType)
        {
            rpt_Inv_Invoice report = new rpt_Inv_Invoice();
            report.Name = string.Format("rpt_{0}", nameof(invoiceType));
            

            switch (invoiceType)
            {
     
                case MasterClass.InvoiceType.SalesInvoice:
                    report.lbl_rptName.Text = "فاتوره مبيعات";
                    break;
                case MasterClass.InvoiceType.PurchaseInvoice:
                    report.lbl_rptName.Text = "فاتوره مشتريات";
                    break;
                case MasterClass.InvoiceType.PurchaseReturn :
                    report.lbl_rptName.Text = "فاتوره مردود مشتريات";
                    break;
                case MasterClass.InvoiceType.SalesReturn:
                    report.lbl_rptName.Text = "فاتوره مردود مبيعات";
                    break;
                default:
                    throw new NotImplementedException();
            }



            report.LoadTemplete();



            report.DataSource = ds;
            report.DataMember = "";

            report.DetailReport.DataSource = report.DataSource;
            report.DetailReport.DataMember = "Products";

            //     report.DetailReport_Expence .DataSource = report.DataSource;
            //  report.DetailReport_Expence.DataMember = "HeadOtherCharges";
            report.LoadData();
            report.ShowPreview(); 


            //switch (CurrentSession.user.WhenPrintShowMode)
            //{
            //    case 0: report.ShowPreview(); break;
            //    case 1: report.PrintDialog(); break;
            //    case 2: report.Print(); break;
            //    default: report.PrintDialog(); break;
            //}

        }


    }
}
