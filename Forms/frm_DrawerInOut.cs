﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using ByStro.DAL;
using ByStro.Clases;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;

namespace ByStro.Forms
{
    
    public partial class frm_DrawerInOut : frm_Master 
    {
        DrawerInOut inOut;
        
        public frm_DrawerInOut()
        {
            InitializeComponent();
            
            New();
        }
      
        private bool ValidData()
        {


            if (spinEdit1.EditValue .ValidAsIntNonZero() == false )
            {
                spinEdit1.ErrorText = "*";
                spinEdit1.Focus();
                return false;
            }
            return true;

        
        }
        public override void Save()
        {

            if (!ValidData()) { return; }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew)
            {
                inOut = new DrawerInOut();
                db.DrawerInOuts.InsertOnSubmit(inOut);
            }
            else
            {
                inOut = db.DrawerInOuts.Where(s => s.ID == inOut.ID).First();
            }
            SetData();
            db.SubmitChanges();

            db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.DrawerInOut).ToString() && x.VoucherID == inOut .ID.ToString()));
            var msg = "";
            msg = string.Format("{0} نقدي رقم {1}", inOut.IsIn ? "ايداع" : "سحب", inOut.ID.ToString());
            TreasuryMovement movement = new TreasuryMovement()
            {
                ID = ByStro.Clases.MasterClass.GetNewTreasuryMovementID(),
                AccountID ="1",
                ISCusSupp = "0",
                VoucherID = inOut.ID.ToString(),
                Type = ((int)MasterClass.TreasuryMovementTypes.DrawerInOut ).ToString(),
                VoucherType = "وارد منصرف نقديه",
                VoucherDate = inOut.Date,
                Description = msg,
                Income = inOut.IsIn? inOut.Amount :0,
                Export = !inOut.IsIn ? inOut.Amount : 0,
                Credit = 0,
                Debit = 0,
                ISShow = true,
                UserAdd = Properties.Settings.Default.UserID,
                Remark = msg,
            };
            db.TreasuryMovements.InsertOnSubmit(movement);
            db.SubmitChanges();
            base.Save();
            RefreshData();
        }
        public override void Delete()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (IsNew) return;
            
            if (XtraMessageBox.Show(text: "تأكيد الحذف", caption: "هل تريد الحذف ؟", buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.DrawerInOut).ToString() && x.VoucherID == inOut.ID.ToString()));
                db.SubmitChanges(); 
                inOut = db.DrawerInOuts.Where(c => c.ID == inOut.ID).First();
                db.DrawerInOuts.DeleteOnSubmit(inOut);
                db.SubmitChanges();
                base.Delete();
                New();
            }
            RefreshData();
        }
        void SetData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            inOut.IsIn  =Convert.ToBoolean( comboBoxEdit1.SelectedIndex );
            inOut.Date = dateEdit1.DateTime;
            inOut.Amount = Convert.ToDouble(spinEdit1 .EditValue); 
        }
        void GetDate()
        {
            comboBoxEdit1.SelectedIndex = Convert.ToInt32( inOut.IsIn );
            dateEdit1.EditValue = inOut.Date ;
            spinEdit1.EditValue= inOut.Amount ;
            textEdit1.Text = inOut.ID.ToString();
        }
        public override void New()
        {

            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            inOut = new DrawerInOut ()
            { 
                Date = DateTime.Now, 
            };
            base.New();
            GetDate();
            IsNew = true;
            ChangesMade = false;
        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            gridControl1.DataSource = db.DrawerInOuts.ToList();
            base.RefreshData();
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            layoutControl1.AllowCustomization = false;
            btn_Print.Visibility =btn_List .Visibility = BarItemVisibility.Never;
             
           
            gridView1.Columns[nameof(inOut.Amount)].Caption = "المبلغ";
            gridView1.Columns[nameof(inOut.ID)].Caption = "كود";
            gridView1.Columns[nameof(inOut.IsIn )].Caption = "النوع";
            gridView1.Columns[nameof(inOut.IsIn)].ColumnEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit(); 
            gridView1.Columns[nameof(inOut.Date )].Caption = "التاريخ";
            gridView1.DoubleClick += GridControl1_DoubleClick;
            gridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
            #region DataChanged
             
            spinEdit1.EditValueChanged += DataChanged;
            dateEdit1 .EditValueChanged += DataChanged; 
            #endregion 
        }

        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
          if(e.Column.FieldName == "IsIn")
            {
                if (Convert.ToBoolean(e.Value) == true)
                    e.DisplayText = "وارد";
                else
                    e.DisplayText = "منصرف";

            }
        }

        private void GridControl1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);

            if (info.InRow || info.InRowCell)
            {
                inOut = view.GetRow(info.RowHandle) as DrawerInOut;
                GetDate();
                IsNew = false;

            }
        }
        public override void Print()
        {
        

        }
    }
}
