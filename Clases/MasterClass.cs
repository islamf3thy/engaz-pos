﻿using ByStro.DAL;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using System.Windows.Forms;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI; 
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout; 
using System.Data; 
using System.Reflection;
using System.Resources; 
using DevExpress.XtraEditors.Controls; 
using DevExpress.XtraBars.Navigation; 
using DevExpress.XtraEditors.Repository;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
namespace ByStro.Clases
{
   public static  class MasterClass
    {
        public static string EmptyFieldErrorText { get => "هذا الحقل مطلوب"; }

        public static void RestoreGridLayout(GridView grid, Form frm)
        {
            try
            {

                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\ERP\\XmlLayouts";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string xmlFile = path + "\\" + frm.Name + "_" + CurrentSession.user.ID + "_" + grid.Name + ".xml";
                if (System.IO . File.Exists(xmlFile))
                {
                    grid.RestoreLayoutFromXml(xmlFile);
                }

            }
            catch
            {
            }
        }
        public static void SaveGridLayout(GridView grid, Form frm)
        {
            try
            {

                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\ERP\\XmlLayouts";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                string xmlFile = path + "\\" + frm.Name + "_" + CurrentSession.user.ID + "_" + grid.Name + ".xml";
                //(grid.MainView as GridView).ActiveFilter.Clear();
                grid.SaveLayoutToXml(xmlFile);
            }
            catch
            {
            }
        }
        public static void RestoreGridLookUpEditViewLayout(object sender, string frmName)
        {
            GridLookUpEdit glk = sender as GridLookUpEdit;
            if (glk == null) return;
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\ERP\\XmlLayouts\\" + frmName + "_" + CurrentSession.user.ID + "_" + glk.Name + ".xml";
            if (System.IO.File.Exists(path))
                glk.Properties.View.RestoreLayoutFromXml(path);
        }
        public static void SaveGridLookUpEditViewLayout(object sender, string frmName)
        {
            GridLookUpEdit glk = sender as GridLookUpEdit;
            if (glk == null) return;
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\ERP\\XmlLayouts\\" + frmName + "_" + CurrentSession.user.ID + "_" + glk.Name + ".xml";
            glk.Properties.View.SaveLayoutToXml(path);
        }
        public struct AvalableItemQtyWithCost
        {
            public double UnitCostPrice;
            public double AvalableAmount;
        }
        public static List<AvalableItemQtyWithCost> GetCurrentItemCost(int storeID, int itemID)
        {

            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);


            var TotalSold = dbc.Inv_StoreLogs.Where(x => x.ItemID == itemID && x.StoreID == storeID).Select(x => x.ItemQuOut).Sum();
            var MainLog = dbc.Inv_StoreLogs.Where(x => x.ItemID == itemID && x.StoreID == storeID && x.ItemQuOut == 0
           && (dbc.Inv_StoreLogs.Where(y => y.ItemID == itemID && y.StoreID == storeID && y.date <= x.date).Select(y => y.ItemQuIN).Sum())
           > TotalSold).OrderBy(d => d.date).Select(x => new { x.BuyPrice, x.ItemQuIN });
            var list = MainLog.ToList();
            var reteurnList = new List<AvalableItemQtyWithCost>();
            list.ForEach(l => reteurnList.Add(new AvalableItemQtyWithCost() { AvalableAmount = l.ItemQuIN ?? 0, UnitCostPrice = l.BuyPrice ?? 0 }));
            return reteurnList;



        }
        public static bool AskForDelete()
        {

            if (XtraMessageBox.Show("هل تريد الحذف ؟ ", "تأكيد عمليه حذف ", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) return true;
            else return false;

        }

        public enum PartTypes
        {
            Vendor = 1,
            Customer = 2,
            Employee = 3,
            Account = 4,
        }

        public enum InvoiceType
        {

            ItemOpenBalance = SystemProssess.ItemOpenBalance,
            ItemDamage = SystemProssess.ItemDamage,
            ItemAdd = SystemProssess.ItemAdd,
            ItemTake = SystemProssess.ItemTake,
            SalesInvoice = SystemProssess.SalesInvoice,
            SalesRequest = SystemProssess.SalesRequest,
            SalesOrder = SystemProssess.SalesOrder,
            SalesReturn = SystemProssess.SalesReturn,
            PurchaseInvoice = SystemProssess.PurchaseInvoice,
            PurchaseRequest = SystemProssess.PurchaseRequest,
            PurchaseOrder = SystemProssess.PurchaseOrder,
            PurchaseReturn = SystemProssess.PurchaseReturn,
            PurchasePriceOffer = SystemProssess.PurchasePriceOffer,
            SalesPriceOffer = SystemProssess.SalesPriceOffer,
            ItemStoreMove = SystemProssess.ItemStoreMove,

        }
        public enum SystemProssess
        {
            OpenBalance = 0,       //0   D
            ItemOpenBalance = 1,      //1 
            ItemDamage = 2,       //2 
            ItemAdd = 3,           //3
            ItemTake = 4,          //4 
            ItemStoreMove = 5,      //5
            SalesInvoice = 6,       //6   D
            SalesRequest = 7,      //7 
            SalesOrder = 8,      //8
            SalesReturn = 9,    //9   C
            PurchaseInvoice = 10,  //10  C
            PurchaseRequest = 11,    //11
            PurchaseOrder = 12,    //12
            PurchaseReturn = 13,  //13  D
            Revenues = 14,  //14  D
            Expenses = 15,  //15  C
            CashNoteIn = 16,  //16  D
            CashNoteOut = 17,  //17  C
            CashTransfer = 18,  //18  C
            ProductionOrder = 19,  //19  C
            ManualJournal = 20,  //20  
            HR_Pay = 21,         //21
            HR_loan = 22,        //22
            SalesPriceOffer = 23,        //22
            Without = 24,        //24
            DirectPay = 25,        //24
            DirectPostToStore = 26,        //24
            DirectWithdrawFromStore = 27,        //24
            PurchasePriceOffer = 28
        }
        public static string[] SystemProssessCaption = new string[]
        {
            "رصيد افتتاحي",
            "رصيد افتتاحي للاصناف",
            "سند هالك اصناف",
            "اذن اضافه اصناف",
            "اذن صرف اصناف",
            "اذن تحويل بين مخازن",
            "فاتوره مبيعات",
            "طلب بيع",
            "امر بيع",
            "مردود مبيعات",
            "فاتوره مشتريات",
            "طلب شراء",
            "امر شراء",
            "مردود مشتريات",
            "ايرادات",
            "مصروفات",
            "سند قبض نقدي",
            "سند دفع نقدي",
            "تحويل نقدي",
        };
        public static void OpenProccessByID(int ProccessID)
        {
            switch (ProccessID)
            {
                default:
                    break;
            }
        }

        public static ItemLog SearchForItem(string Barcode, int storeID, bool IsForDefualtBuy = false)
        {
            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);
            ItemLog log = new ItemLog();

            if (Barcode.Contains("*"))
            {
                var barcodeArray = Barcode.Split(new char[] { Convert.ToChar("*") }, StringSplitOptions.RemoveEmptyEntries);
                if(barcodeArray.Length == 2)
                {
                    int detailID = 0;
                    int sourceID = 0;
                    int.TryParse(barcodeArray[0], out sourceID);
                    int.TryParse(barcodeArray[1], out detailID);
                    if(detailID > 0)
                    {
                        var detail = dbc.Inv_InvoiceDetails.SingleOrDefault(x => x.ID == detailID);
                        if(detail != null)
                        {
                            log = new ItemLog()
                            {
                                ItemID = detail.ItemID,
                                Color = detail.Color,
                                Size = detail.Size,
                                StoreID = detail.StoreID.Value,
                                UnitID = detail.ItemUnitID,
                                Serial = detail.Serial,
                                ExpirDate = detail.ExpDate

                            };
                                goto ItemFound;
                        }

                    }
                }
            }
            int UOMIndex = 1;
            var firstUOM = dbc.Prodecuts.FirstOrDefault(x => x.FiestUnitBarcode == Barcode);
            if (firstUOM == null)
            {
                firstUOM = dbc.Prodecuts.SingleOrDefault(x => x.SecoundUnitBarcode == Barcode);
                UOMIndex = 2;
            }
            if (firstUOM == null)
            {
                firstUOM = dbc.Prodecuts.SingleOrDefault(x => x.ThreeUnitBarcode == Barcode);
                UOMIndex = 3; 
            }
            // ,i.Inv_Item.Color   ,i.Inv_Item.Serial ,i.Inv_Item.Expier  , i.Inv_Item.Size };dbc.Inv_Items.Where (x=>x.ID == i.ItemID ).Single()
            if (firstUOM != null )
            {
                
                log = new ItemLog()// assing 
                {
                    ItemID = (int)firstUOM.ProdecutID,
                    UnitID = UOMIndex,
                };
                // get the next item for sell info from the store log table 
                var NextItemForSell = GetNextItemForSell(log.ItemID , storeID, IsForDefualtBuy);
                log.Color = NextItemForSell.Color;
                log.ExpirDate = NextItemForSell.ExpirDate;
                log.Size = NextItemForSell.Size;
                log.Batch = NextItemForSell.Batch;
                goto ItemFound;
            }
            var serialLog = dbc.Inv_StoreLogs.FirstOrDefault(x => x.Serial == Barcode);
            if (serialLog!=null )
            {

                log = new ItemLog()// assing 
                {
                    ItemID =  serialLog.ItemID.Value  ,
                    UnitID = 1, 
                    Color  = serialLog.Color ,
                    Size = serialLog.Size ,
                    StoreID = serialLog.StoreID.Value ,
                    Serial = serialLog.Serial ,
                };
                goto ItemFound;

            }
           

        ItemFound:
            return log;

        }
        public static ItemLog GetNextItemForSell(int itemid, int storeID, bool IsForDefualtBuy = false)
        {
            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);

            ItemLog log = new ItemLog();

            var TotalSold = dbc.Inv_StoreLogs.Where(x => x.ItemID == itemid && x.StoreID == storeID).Select(x => x.ItemQuOut).Sum();
            var MainLog =
            dbc.Inv_StoreLogs.Where(x => x.ItemID == itemid && x.StoreID == storeID && x.ItemQuOut == 0
            && (dbc.Inv_StoreLogs.Where(y => y.ItemID == itemid && y.StoreID == storeID && y.date <= x.date).Select(y => y.ItemQuIN).Sum()) > TotalSold).OrderBy(d => d.date);
            var list = MainLog.ToList();
            var itemlog = MainLog.FirstOrDefault();

            if (itemlog != null) // if we got the next item for sell assign
            {

                log.Color = itemlog.Color;
                log.ExpirDate = itemlog.ExpDate;
                log.Size = itemlog.Size;
                log.Batch = itemlog.Batch;
                //Debug.Print(dbc.GetCommand(itemlog).CommandText);
            }
            return log;

        }
        public struct ItemLog
        {
            public int ItemID;
            public int UnitID;
            public int StoreID;
            public double BuyPrice;
            public double SellPrice;

            public DateTime? ExpirDate;
            public int? Color;
            public int? Size;
            public string Batch;
            public string Serial;
        }

        public static bool IsNumber(this object value)
        {
            int numint = 0;
            double numdouble = 0;
            if (value == null) return false;
            bool flag = int.TryParse(value.ToString(), out numint);
            if (flag == false)
                flag = double.TryParse(value.ToString(), out numdouble);

            return value is sbyte
          || value is byte
          || value is short
          || value is ushort
          || value is int
          || value is uint
          || value is long
          || value is ulong
          || value is float
          || value is double
          || value is decimal
          || flag;
        }
        /// <summary>
        /// Get This object as int 
        /// (will throw an exption  if it is not of type Number )
        /// </summary>
        /// <param name="throwExption"> if false will return 0 if object is not of type number </param>
        /// <returns></returns>
        public static Int32 ToInt(this object value, bool throwExption = false)
        {
            if (value.IsNumber() == false && throwExption)
            {
                throw new InvalidOperationException("Value is not a number.");
            }
            else
            {
                if (value.IsNumber())
                {
                    return Convert.ToInt32(value);

                }
                else return 0;
            }
        }
        public static void Saved( )
        {
            XtraMessageBoxArgs args = new XtraMessageBoxArgs();
            args.AutoCloseOptions.ShowTimerOnDefaultButton = true;
            args.AutoCloseOptions.Delay = 1500;
            args.Caption = "";
            args.Text = "تم الحفظ بنجاح";
            args.DefaultButtonIndex = 0;
            args.Buttons = new DialogResult[] { DialogResult.OK };
            args.MessageBeepSound = MessageBeepSound.Information;
            XtraMessageBox.Show(args);



            // XtraMessageBox.Show(LangResource.SavedSuccessfully, "", MessageBoxButtons.OK, MessageBoxIcon.Information );

        }
        public static bool ValidAsIntNonZero(this object value)
        {
            return (value.IsNumber() && value.ToInt() > 0);
        }
        public static System.Drawing.Image GetImageFromByte(Byte[] ByteArray)
        {
            System.Drawing.Image img;
            try
            {
                Byte[] imgbyte = ByteArray;
                MemoryStream strm = new MemoryStream(imgbyte, false);
                img = System.Drawing.Image.FromStream(strm);
            }
            catch { img = null; }
            return img;
        }
        public static string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";
            string str1 = "";
            foreach (char c in Number)
                str1 = !char.IsDigit(c) ? "" : str1 + c.ToString();
            if (str1 == string.Empty)
                return Number + "1";
            string str2 = (Convert.ToInt64(str1.Insert(0, "1")) + 1L).ToString();
            string str3 = str2[0] != '1' ? str2.Remove(0, 1).Insert(0, "1") : str2.Remove(0, 1);
            int startIndex = Number.LastIndexOf(str1);
            Number = Number.Remove(startIndex);
            Number = Number.Insert(startIndex, str3);
            return Number;
        }
        public static int GetNewTreasuryMovementID()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String); 
            try
            {
                return (int)db.TreasuryMovements.Max(n => n.ID) + 1; 
            }
            catch
            {
                return (int)1; 
            }
        }

        
        public  enum TreasuryMovementTypes
        {
            InstalmentPay = 1,
            InstalmentAdvancedPay = 2,
            DrawerInOut = 3,
            DeliveryOrderCollect = 4,
            InstalmentBenfites = 5,
            SalesInvoice = 6,
            PurchaseInvoice = 7,
            SalesReturn = 8,
            PurchaseReturn = 9,


        }
        #region MoneyToText


        public static string ConvertMoneyToText(string value)
        {
         
          return ConvertMoneyToArabicText(value);

        }
        static string ConvertMoneyToArabicText(string value)
        {
            
            double result = 0.0;
            if (!double.TryParse(value, out result))
                return "";
            string accum = "";
            double rv1 = (double)(int)(result / 1000000.0);
            if (rv1 > 2.0)
                accum = NumToStr1(rv1, accum);
            if (rv1 >= 3.0 && rv1 < 10.0)
                accum += melion[3];
            else if (rv1 == 2.0)
                accum += melion[2];
            else if (rv1 == 1.0 || rv1 >= 10.0 && rv1 <= 999.0)
                accum += melion[1];
            double rv2 = (double)(int)((result - (double)((int)(result / 1000000.0) * 1000000)) / 1000.0);
            if (result != (double)((int)(result / 1000000.0) * 1000000) && result > 1000000.0)
                accum += " و";
            if (rv2 > 2.0)
                accum = NumToStr1(rv2, accum);
            if (rv2 >= 3.0 && rv2 < 10.0)
                accum += alf[3];
            else if (rv2 == 2.0)
                accum += alf[2];
            else if (rv2 == 1.0 || rv2 >= 10.0 && rv2 <= 999.0)
                accum += alf[1];
            double rv3 = (double)(int)(result - (double)((int)(result / 1000.0) * 1000) + 0.0001);
            if (result != (double)((int)(result / 1000.0) * 1000) && result > 1000.0 && rv3 != 0.0)
                accum += " و ";
            if (rv3 >= 2.0 && result != 2.0)
                accum = NumToStr1(rv3, accum);
            if (result > 0.999)
                accum = result >= 11.0 || rv3 <= 2.0 ? (result != 2.0 ? accum + "  جنيه " : accum + "  جنيهات  ") : accum + "  جنيهات  ";
            double rv4 = (double)(int)((result - (double)(int)(result + 0.0001) + 0.0001) * 1000.0) / 10.0;
            if (rv4 >= 1.0 && result > 0.99)
                accum += " و";
            int num = 2;
            if (num == 2)
            {
                if (rv4 > 2.9)
                    accum = NumToStr1(rv4, accum);
                if (rv4 >= 1.0)
                    accum = rv4 < 2.0 || rv4 >= 2.99 ? (rv4 >= 11.0 || rv4 <= 2.9 ? accum + " قرش " : accum + " قروش "  ) : accum + " قرشان " ;
            }
            if (num == 3)
            {
                double rv5 = rv4 * 10.0;
                accum = NumToStr1(rv5, accum);
                if (rv5 >= 1.0)
                    accum = rv5 != 2.0 ? (rv5 >= 11.0 || rv5 <= 2.0 ? accum + " قرش " : accum + " قروش ") : accum + " قرشان ";
            }
            return accum;
        }
        public static string NumToStr1(double rv, string accum)
        {
            if (rv >= 100.0)
            {
                int index = (int)(rv / 100.0);
                accum += meat[index];
            }
            int index1 = (int)(rv - (double)((int)(rv / 100.0) * 100));
            if (index1 != 0 && rv > 99.0)
                accum += " و";
            int index2 = index1 - index1 / 10 * 10;
            if (index1 < 13 && (uint)index1 > 0U)
                accum += ahad[index1];
            if (index1 > 12 && (uint)index2 > 0U)
                accum += ahad2[index2];
            if (index1 > 10 && index1 < 20)
                accum += ahad2[10];
            if (index1 > 19)
            {
                if ((uint)index2 > 0U)
                    accum += " و";
                accum += asharat[index1 / 10];
            }
            return accum;
        }
        private static string[] ahad = new string[13]
        {
      "",
      "واحد",
      "إثنين",
      "ثلاثة",
      "أربعة",
      "خمسة",
      "ستة",
      "سبعة",
      " ثمانية",
      " تسعة",
      " عشرة",
      " أحد",
      " اثنى"
        };
        private static string[] ahad2 = new string[13]
        {
      "",
      "واحد",
      "إثنين",
      "ثلاثة",
      "أربعة",
      "خمسة",
      "ستة",
      "سبعة",
      "ثمانية",
      "تسعة",
      " عشر",
      " أحد",
      " اثنى"
        };
        private static string[] asharat = new string[10]
        {
      "",
      "واحد",
      "عشرون",
      "ثلاثون",
      "أربعون",
      "خمسون",
      "ستون",
      "سبعون",
      "ثمانون",
      "تسعون"
        };
        private static string[] meat = new string[10]
        {
      "",
      "مائة",
      "مائتين",
      "ثلاثمائة",
      "أربعمائة",
      "خمسمائة",
      "ستمائة",
      "سبعمائة",
      "ثمانمائة",
      "تسعمائة"
        };
        private static string[] melion = new string[4]
        {
      "",
      " مليون",
      " مليونان",
      " ملايين"
        };
        private static string[] alf = new string[4]
        {
      "",
      " ألف",
      " ألفين",
      " آلاف"
        };
        #endregion
    }
}
