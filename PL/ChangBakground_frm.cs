﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ChangBakground_frm : Form
    {
        public ChangBakground_frm()
        {
            InitializeComponent();
        }

     
        private void AddPhoto()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Image Files | *.jpg; *.BMP;*.png; *.bmp; *.gif | All Files (*.*) | *.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    p_box.Image = Image.FromFile(ofd.FileName);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        DataAccessLayer dal = new DataAccessLayer();

        // Insert
        public void InsertChangBakground(int ID, Byte[] Photo)
        {
            dal.Execute_SQL("insert into ChangBakground(ID ,Photo )Values (@ID ,@Photo )", CommandType.Text,
            dal.Parameter("@ID", SqlDbType.Int, ID),
             dal.Parameter("@Photo", SqlDbType.Image, Photo));
        }

        //Update
        public void UpdateChangBakground(int ID, Byte[] Photo)
        {
            dal.Execute_SQL("Update ChangBakground Set ID=@ID ,Photo=@Photo  where ID=@ID", CommandType.Text,
            dal.Parameter("@ID", SqlDbType.Int, ID),
            dal.Parameter("@Photo", SqlDbType.Image, Photo));
        }



        private void p_box_Click(object sender, EventArgs e)
        {
            //AddPhoto();
        }

   

        private void button1_Click(object sender, EventArgs e)
        {

        }
        public bool changImge = false;
        private void ChangBakground_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                changBakground();

            }
            catch
            {

               
            }
        }

        private void changBakground()
        {
            try
            {
                if (Properties.Settings.Default.PhotoNumber == 1)
                {
                   
                     p_box.Image = Properties.Resources.SalesManger;
                 
                     
                }
                else if (Properties.Settings.Default.PhotoNumber == 4)
                {
                    p_box.Image = Properties.Resources.Background_4;
                }
                else if (Properties.Settings.Default.PhotoNumber == 5)
                {
                    p_box.Image = Properties.Resources.Background_5;
                }
                else if (Properties.Settings.Default.PhotoNumber == 6)
                {
                    p_box.Image = Properties.Resources.Background_6;
                }
               
                else
                {
                     p_box.Image = Properties.Resources.SalesManger;
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }











        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    DataTable dt = dal.ExecteRader("select * from ChangBakground where ID='1'", CommandType.Text);
            //    #region Update Photo
            //    p_box.Image = Properties.Resources.sssss;
            //    System.IO.MemoryStream ms = new System.IO.MemoryStream();
            //    p_box.Image.Save(ms, p_box.Image.RawFormat);
            //    byte[] byteimge = null;
            //    byteimge = ms.ToArray();

            //    #endregion
            //    if (dt.Rows.Count == 0)
            //    {
            //        InsertChangBakground(1, byteimge);
            //    }
            //    else
            //    {
            //        UpdateChangBakground(1, byteimge);
            //    }
            //    changImge = true;
            //    MessageBox.Show("تم ارجاع الخلفية الافتراضية", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //DataTable dt = dal.ExecteRader("select * from ChangBakground where ID='1'", CommandType.Text);
                //#region Update Photo
                //System.IO.MemoryStream ms = new System.IO.MemoryStream();
                //p_box.Image.Save(ms, p_box.Image.RawFormat);
                //byte[] byteimge = null;
                //byteimge = ms.ToArray();

                //#endregion
                //if (dt.Rows.Count == 0)
                //{
                //    InsertChangBakground(1, byteimge);
                //}
                //else
                //{
                //    UpdateChangBakground(1, byteimge);
                //}
                Main_frm frm = this.Owner as Main_frm;            
                Properties.Settings.Default.PhotoNumber = PhotoNumber;
                Properties.Settings.Default.Save();
               // changImge = true;
                frm.changBakground();
               // MessageBox.Show("تم تغيير الخلفية بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        int PhotoNumber = 0;
        private void lbl1_Click(object sender, EventArgs e)
        {
            try
            {
                
                    p_box.Image = Properties.Resources.SalesManger;
               
                PhotoNumber = 1;
            }
            catch
            {
            }
        }

       
        private void lbl4_Click(object sender, EventArgs e)
        {
            try
            {
                p_box.Image = Properties.Resources.Background_4;
                PhotoNumber = 4;
            }
            catch
            {
            }
        }

        private void lbl5_Click(object sender, EventArgs e)
        {
            try
            {
                p_box.Image = Properties.Resources.Background_5;
                PhotoNumber = 5;
            }
            catch
            {
            }
        }

        private void lbl6_Click(object sender, EventArgs e)
        {
            try
            {
                p_box.Image = Properties.Resources.Background_6;
                PhotoNumber = 6;
            }
            catch
            {
            }
        }

     

      

        private void ChangBakground_frm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Escape)
            {
                this.Close();
            }
            else if (e.KeyCode==Keys.F2)
            {
                toolStripButton1_Click(null,null);
            }
        }
    }
}