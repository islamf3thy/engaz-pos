﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ReportVat_frm : Form
    {
        public ReportVat_frm()
        {
            InitializeComponent();
        }

        private void ReportVat_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
        }
    }
}
