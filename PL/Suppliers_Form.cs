﻿using System;
using System.Windows.Forms;
using System.Data;

namespace ByStro.PL
{
    public partial class Suppliers_Form : Form
    {
        public Suppliers_Form()
        {
            InitializeComponent();
        }
        Suppliers_cls cls = new Suppliers_cls();
        private void Company_Add_Form_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingForm(this);
                // DataAccessLayer.Geet_Company_Fill(combCompeny);
                btnNew_Click(null, null);
            }
            catch
            {

                throw;
            }

        }





        private void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            txtSupplierName.BackColor = System.Drawing.Color.White;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtSupplierName.Text = "";
                txtNationalID.Text = "";
                txtAddress.Text = "";
                txtTaxFileNumber.Text = "";
                txtRegistrationNo.Text = "";
                txtStartContract.Text = "";
                txtEndContract.Text = "";
                txtRemarks.Text = "";
                txtPhone.Text = "";
                txtPhone2.Text = "";
                txtAccountNumber.Text = "";
                txtFax.Text = "";
                txtEmail.Text = "";
                txtWebSite.Text = "";
                txtMaximum.Text = "";
                rb1.Checked = true;
                R1.Checked = true;
                txtSupplierID.Text = cls.MaxID_Suppliers();
                tabControl1.SelectedIndex = 0;
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                chUseVat.Checked = false;
                txtSupplierName.Focus();
                if (Application.OpenForms["BillBay_frm"] != null)
                {
                  //  ((BillBay_frm)Application.OpenForms["BillBay_frm"]).FillCombAccount() ;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSupplierName.Text.Trim() == "")
                {
                    txtSupplierName.BackColor = System.Drawing.Color.Pink;
                    txtSupplierName.Focus();
                    return;
                }
               
                // Search where Customer Name

                DataTable DtSearch = cls.NameSearch_Supplier(txtSupplierName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtSupplierName.BackColor = System.Drawing.Color.Pink;
                    txtSupplierName.Focus();
                    return;
                }
                String Notifications = "1";

                if (R1.Checked == true)
                {
                    Notifications = "1";
                }
                else if (R2.Checked == true)
                {
                    Notifications = "2";
                }
                else if (R3.Checked == true)
                {
                    Notifications = "3";
                }


               



                txtSupplierID.Text = cls.MaxID_Suppliers();
                cls.InsertSuppliers(txtSupplierID.Text, txtSupplierName.Text, txtNationalID.Text,txtAddress.Text, txtTaxFileNumber.Text, txtRegistrationNo.Text, txtStartContract.Text, txtEndContract.Text, txtRemarks.Text, txtPhone.Text, txtPhone2.Text, txtAccountNumber.Text, txtFax.Text, txtEmail.Text, txtWebSite.Text, txtMaximum.Text, Notifications,chUseVat.Checked, Properties.Settings.Default.UserID, rb1.Checked);
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSupplierName.Text.Trim() == "")
                {
                    txtSupplierName.BackColor = System.Drawing.Color.Pink;
                    txtSupplierName.Focus();
                    return;
                }
               
                String Notifications = "1";

                if (R1.Checked == true)
                {
                    Notifications = "1";
                }
                else if (R2.Checked == true)
                {
                    Notifications = "2";
                }
                else if (R3.Checked == true)
                {
                    Notifications = "3";
                }

      

                cls.UpdateSuppliers(txtSupplierID.Text, txtSupplierName.Text, txtNationalID.Text, txtAddress.Text, txtTaxFileNumber.Text, txtRegistrationNo.Text, txtStartContract.Text, txtEndContract.Text, txtRemarks.Text, txtPhone.Text, txtPhone2.Text, txtAccountNumber.Text, txtFax.Text, txtEmail.Text, txtWebSite.Text, txtMaximum.Text, Notifications,chUseVat.Checked, rb1.Checked);
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
          
                //if (cls.NODeleteSuppliers(txtSupplierID.Text).Rows.Count > 0)
                //{
                //    Mass.NoDelete();
                //    return;
                //}

                


                if (Mass.Delete() == true)
                {
                    cls.DeleteSuppliers(txtSupplierID.Text);
                    btnNew_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {
                Suppliers_Search_Form frm = new Suppliers_Search_Form();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }

                if (frm.DGV1.Rows.Count == 0)
                    return;

                DataTable dt = cls.Details_Suppliers(frm.DGV1.CurrentRow.Cells["SupplierID"].Value.ToString());
                DataRow Dr = dt.Rows[0];


          txtSupplierID.Text = Dr["SupplierID"].ToString();
          txtSupplierName.Text = Dr["SupplierName"].ToString();
 
          txtNationalID.Text = Dr["NationalID"].ToString();
          txtAddress.Text = Dr["Address"].ToString();
          txtTaxFileNumber.Text = Dr["TaxFileNumber"].ToString();
          txtRegistrationNo.Text = Dr["RegistrationNo"].ToString();
          txtStartContract.Text = Dr["StartContract"].ToString();
          txtEndContract.Text = Dr["EndContract"].ToString();
          txtRemarks.Text = Dr["Remarks"].ToString();
          txtPhone.Text = Dr["Phone"].ToString();
          txtPhone2.Text = Dr["Phone2"].ToString();
          txtAccountNumber.Text = Dr["AccountNumber"].ToString();
          txtFax.Text = Dr["Fax"].ToString();
          txtEmail.Text = Dr["Email"].ToString();
          txtWebSite.Text = Dr["WebSite"].ToString();
          txtMaximum.Text = Dr["Maximum"].ToString();
  

                if (Convert.ToBoolean(Dr["Status"]) == true)
                {
                    rb1.Checked = true;
                }
                else
                {
                    rb2.Checked = true;
                }


                if (Dr["CreditLimit"].ToString() == "1")
                {
                    R1.Checked = true;
                }
                else if (Dr["CreditLimit"].ToString() == "2")
                {
                    R2.Checked = true;
                }
                else if (Dr["CreditLimit"].ToString() == "3")
                {
                    R3.Checked = true;
                }



                chUseVat.Checked =Convert.ToBoolean( Dr["UseVat"].ToString());
                




                //=========================================================================
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtMaximum_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtNationalID_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void Suppliers_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }
    }
}
