﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


    class BillMaintenance_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_Maintenance_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from Maintenance_Main", CommandType.Text);
    }


    // Insert
    public void Insert_Maintenance_Main(string MainID, DateTime MyDate, string CustomerID, string Remarks, String TypeKind, String TotalInvoice, String Discount, String NetInvoice, String PaidInvoice, String RestInvoise, String TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Maintenance_Main(MainID ,MyDate ,CustomerID ,Remarks ,TypeKind ,TotalInvoice ,Discount ,NetInvoice ,PaidInvoice ,RestInvoise ,TreasuryID ,UserAdd )Values (@MainID ,@MyDate ,@CustomerID ,@Remarks ,@TypeKind ,@TotalInvoice ,@Discount ,@NetInvoice ,@PaidInvoice ,@RestInvoise ,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@TypeKind", SqlDbType.NVarChar, TypeKind),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@PaidInvoice", SqlDbType.Float, PaidInvoice),
        Parameter("@RestInvoise", SqlDbType.Float, RestInvoise),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Maintenance_Main(string MainID, DateTime MyDate, string CustomerID, string Remarks, String TypeKind, string TotalInvoice, string Discount, string NetInvoice, string PaidInvoice, string RestInvoise)
    {
        Execute_SQL("Update Maintenance_Main Set MainID=@MainID ,MyDate=@MyDate ,CustomerID=@CustomerID ,Remarks=@Remarks ,TypeKind=@TypeKind ,TotalInvoice=@TotalInvoice ,Discount=@Discount ,NetInvoice=@NetInvoice ,PaidInvoice=@PaidInvoice ,RestInvoise=@RestInvoise  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@TypeKind", SqlDbType.NVarChar, TypeKind),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@PaidInvoice", SqlDbType.Float, PaidInvoice),
        Parameter("@RestInvoise", SqlDbType.Float, RestInvoise));
    }

    //Delete
    public void Delete_Maintenance_Main(string MainID)
    {
        Execute_SQL("Delete  From Maintenance_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }

    //Details ID
    public DataTable Details_Maintenance_Main(string MainID)
    {
        return ExecteRader(@"SELECT        dbo.Maintenance_Main.*, dbo.Customers.CustomerName, dbo.Customers.Phone
FROM            dbo.Customers INNER JOIN
                         dbo.Maintenance_Main ON dbo.Customers.CustomerID = dbo.Maintenance_Main.CustomerID Where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }


    public DataTable Details_Maintenance_Details(string MainID)
    {
        return ExecteRader(@"SELECT        dbo.Maintenance_Details.*, dbo.DamagedType.DamagedTypeName, dbo.Currency.CurrencyName
FROM            dbo.DamagedType INNER JOIN
                         dbo.Maintenance_Details ON dbo.DamagedType.DamagedTypeID = dbo.Maintenance_Details.DamagedTypeID INNER JOIN
                         dbo.Currency ON dbo.Maintenance_Details.CurrencyID = dbo.Currency.CurrencyID
Where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }

  
    //MaxID
    public String MaxID_Maintenance_Details()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from Maintenance_Details", CommandType.Text);
    }



    // Insert
  

    // Insert
    public void Insert_Maintenance_Details(string MainID, string DamagedTypeID, string Quantity, string Price, string TotalPrice, string CurrencyID, string CurrencyRate, string CurrencyPrice, string CurrencyTotal)
    {
        Execute_SQL("insert into Maintenance_Details(ID ,MainID ,DamagedTypeID ,Quantity ,Price ,TotalPrice ,CurrencyID ,CurrencyRate ,CurrencyPrice ,CurrencyTotal )Values (@ID ,@MainID ,@DamagedTypeID ,@Quantity ,@Price ,@TotalPrice ,@CurrencyID ,@CurrencyRate ,@CurrencyPrice ,@CurrencyTotal )", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, MaxID_Maintenance_Details()),
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@DamagedTypeID", SqlDbType.NVarChar, DamagedTypeID),
        Parameter("@Quantity", SqlDbType.NVarChar, Quantity),
        Parameter("@Price", SqlDbType.NVarChar, Price),
        Parameter("@TotalPrice", SqlDbType.NVarChar, TotalPrice),
        Parameter("@CurrencyID", SqlDbType.NVarChar, CurrencyID),
        Parameter("@CurrencyRate", SqlDbType.NVarChar, CurrencyRate),
        Parameter("@CurrencyPrice", SqlDbType.NVarChar, CurrencyPrice),
        Parameter("@CurrencyTotal", SqlDbType.NVarChar, CurrencyTotal));
    }





    //Delete
    public void Delete_Maintenance_Details(string MainID)
    {
        Execute_SQL("Delete  From Maintenance_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }


    public DataTable Load_Maintenance_Main(DateTime MyDate, DateTime MyDate2, int UserAdd)
    {
        string str = @"SELECT        dbo.Maintenance_Main.*, dbo.Customers.CustomerName, dbo.UserPermissions.EmpName, dbo.Customers.Phone
FROM            dbo.Customers INNER JOIN
                         dbo.Maintenance_Main ON dbo.Customers.CustomerID = dbo.Maintenance_Main.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Maintenance_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Maintenance_Main.MyDate >=@MyDate ) AND (dbo.Maintenance_Main.MyDate <=@MyDate2) AND (dbo.Maintenance_Main.UserAdd =@UserAdd)";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));

    }


    public DataTable Load_Maintenance_Main(DateTime MyDate, DateTime MyDate2)
    {
        string str = @"SELECT        dbo.Maintenance_Main.*, dbo.Customers.CustomerName, dbo.UserPermissions.EmpName, dbo.Customers.Phone
FROM            dbo.Customers INNER JOIN
                         dbo.Maintenance_Main ON dbo.Customers.CustomerID = dbo.Maintenance_Main.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Maintenance_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Maintenance_Main.MyDate >=@MyDate ) AND (dbo.Maintenance_Main.MyDate <=@MyDate2)";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));

    }


    public DataTable Search_Maintenance_Main(DateTime MyDate, DateTime MyDate2, string Search)
    {
        string str = @"SELECT        dbo.Maintenance_Main.*, dbo.Customers.CustomerName, dbo.UserPermissions.EmpName, dbo.Customers.Phone
FROM            dbo.Customers INNER JOIN
                         dbo.Maintenance_Main ON dbo.Customers.CustomerID = dbo.Maintenance_Main.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Maintenance_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Maintenance_Main.MyDate >=@MyDate ) AND (dbo.Maintenance_Main.MyDate <=@MyDate2)  and convert(nvarchar,dbo.Maintenance_Main.MainID)+dbo.Customers.CustomerName like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@Search", SqlDbType.NVarChar, Search));

    }




    public DataTable Search_Maintenance_Main(DateTime MyDate, DateTime MyDate2, int UserAdd, string Search)
    {
        string str = @"SELECT        dbo.Maintenance_Main.*, dbo.Customers.CustomerName, dbo.UserPermissions.EmpName, dbo.Customers.Phone
FROM            dbo.Customers INNER JOIN
                         dbo.Maintenance_Main ON dbo.Customers.CustomerID = dbo.Maintenance_Main.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Maintenance_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Maintenance_Main.MyDate >=@MyDate ) AND (dbo.Maintenance_Main.MyDate <=@MyDate2) AND (dbo.Maintenance_Main.UserAdd =@UserAdd)  and convert(nvarchar,dbo.Maintenance_Main.MainID)+dbo.Customers.CustomerName like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@Search", SqlDbType.NVarChar, Search),
          Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));

    }



}

