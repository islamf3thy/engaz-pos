﻿using ByStro.RPT;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MaintenanceReturnSearch_frm : Form
    {
        public MaintenanceReturnSearch_frm()
        {
            InitializeComponent();
        }
        Maintenance_cls cls = new Maintenance_cls();
       public Boolean LoadData = false;
        private void CustomersPayment_FormAR_Load(object sender, EventArgs e)
        {

            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_Maintenance("", datefrom.Value, datetto.Value, 1);
                txtSearch.Text = "";
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }



        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_Maintenance(txtSearch.Text, datefrom.Value, datetto.Value,1);
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void datefrom_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_Maintenance("", datefrom.Value, datetto.Value,1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void datetto_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_Maintenance("", datefrom.Value, datetto.Value, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (DGV1.Rows.Count==0)
            {
                return;
            }
            LoadData = true;
            Close();
        }


        string DeviceStatusName;
        //private void btnPrint_Click(object sender, EventArgs e)
        //{
        //    if (DGV1.Rows.Count==0)
        //    {
        //        return;  
        //    }
        //    if (DGV1.CurrentRow.Cells["DeviceStatus"].Value.ToString()=="0")
        //    {
        //        DeviceStatusName = "قيد الصيانة";
        //    }
        //    else if (DGV1.CurrentRow.Cells["DeviceStatus"].Value.ToString() == "1")
        //    {
        //        DeviceStatusName =  "تم الاصلاح";
        //    }
        //    else
        //    {
        //        DeviceStatusName = "لا يمكن اصلاحة";
        //    }

        //     try
        //    {

        //        if (BillSetting_cls.PrintSize == 1)
        //        {
        //            PrintSizeA4();
        //        }
        //        else if (BillSetting_cls.PrintSize == 2)
        //        {
        //            PrintSize1_2A4();
        //        }
        //        else
        //        {
        //            PrintSize80MM();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}


       





        //private void PrintSizeA4()
        //{
        //    if (DGV1.Rows.Count==0)
        //    {
        //        return; 
        //    }

        //    BillSetting_cls cls_print = new BillSetting_cls();

        //    DataTable dt_Print = cls.Details_Maintenance(DGV1.CurrentRow.Cells["MaintenanceID"].Value.ToString());

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new RptMaintenance_A4();
        //    report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, this.Text);
        //    report.SetParameterValue(1, DeviceStatusName);

        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //   // frm.ShowDialog();


        //}

        //private void PrintSize1_2A4()
        //{
        //    if (DGV1.Rows.Count == 0)
        //    {
        //        return;
        //    }

        //    BillSetting_cls cls_print = new BillSetting_cls();

        //    DataTable dt_Print = cls.Details_Maintenance(DGV1.CurrentRow.Cells["MaintenanceID"].Value.ToString());

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new RptMaintenance();
        //    report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, this.Text);
        //    report.SetParameterValue(1, DeviceStatusName);

        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //   // frm.ShowDialog();


        //}


        //private void PrintSize80MM()
        //{
        //    if (DGV1.Rows.Count==0)
        //    {
        //        return; 
        //    }

        //    BillSetting_cls cls_print = new BillSetting_cls();

        //    DataTable dt_Print = cls.Details_Maintenance(DGV1.CurrentRow.Cells["MaintenanceID"].Value.ToString());

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new RptMaintenance80MM();
        //    report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, this.Text);
        //    report.SetParameterValue(1, DeviceStatusName);

        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //  //  frm.ShowDialog();


        //}



       

    }
}
