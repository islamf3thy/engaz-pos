﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ByStro.Clases;

namespace ByStro.RPT
{
    public partial class rpt_SalesInvoice : rpt_Master
    {
        public rpt_SalesInvoice()
        {
            InitializeComponent();
            lbl_PrinDate.Text = DateTime.Now.ToString();
            lbl_UserName.Text = CurrentSession.user.UserName;
            lbl_companyName.Text = CurrentSession.Company.CombanyName ;
            lbl_Cpmpany_Phone.Text = CurrentSession.Company.PhoneNamber ;
         


        }
        public override void LoadData()
        {

            cell_Index.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Index"));
            cell_Item.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Item"));
            cell_Unit.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Unit"));
            cell_Qty.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "ItemQty"));
            cell_Price.ExpressionBindings.Add(
            new ExpressionBinding("BeforePrint", "Text", "Price"));
            cell_Dicount.ExpressionBindings.Add(
                      new ExpressionBinding("BeforePrint", "Text", "Discount"));
            cell_Total.ExpressionBindings.Add(
           new ExpressionBinding("BeforePrint", "Text", "TotalPrice"));
            lbl_ID.DataBindings.Add("Text", DataSource, "ID");
            xrBarCode1.DataBindings.Add("Text", DataSource, "ID");
            lbl_PartName.DataBindings.Add("Text", DataSource, "Customer");
            lbl_PartType.DataBindings.Add("Text", DataSource, "PartTypeText");
            lbl_Date.DataBindings.Add("Text", DataSource, "Date");
            lbl_Notes.DataBindings.Add("Text", DataSource, "Notes");
            lbl_DicountValue.DataBindings.Add("Text", DataSource, "DiscountValue");
            lbl_Total.DataBindings.Add("Text", DataSource, "Total");
            lbl_Net.DataBindings.Add("Text", DataSource, "Net");
            lbl_NetAsString.DataBindings.Add("Text", DataSource, "NetText");
            lbl_Paid.DataBindings.Add("Text", DataSource, "Paid");
            lbl_Remains.DataBindings.Add("Text", DataSource, "Remains");
            lbl_InsertUser.DataBindings.Add("Text", DataSource, "UserName");
            lbl_City.DataBindings.Add("Text", DataSource, "City");
            lbl_Address.DataBindings.Add("Text", DataSource, "Address");
            lbl_Phone.DataBindings.Add("Text", DataSource, "Phone");
            lbl_Mobile.DataBindings.Add("Text", DataSource, "Mobile");
            try
            {
                xrPictureBox1.Image = MasterClass.GetImageFromByte(CurrentSession.Company.Imge .ToArray());
            }
            catch (Exception)
            {
            }

            base.LoadData();
        }
        public static void Print(object ds )
        {
            rpt_SalesInvoice report = new rpt_SalesInvoice();  
            report.LoadTemplete();



            report.DataSource = ds;
            report.DataMember = "";

            report.DetailReport.DataSource = report.DataSource;
            report.DetailReport.DataMember = "Products";

            //     report.DetailReport_Expence .DataSource = report.DataSource;
            //  report.DetailReport_Expence.DataMember = "HeadOtherCharges";
            report.LoadData();

            //switch (CurrentSession.user.WhenPrintShowMode)
            //{
            //    case 0: report.ShowPreview(); break;
            //    case 1: report.PrintDialog(); break;
            //    case 2: report.Print(); break;
            //    default: report.PrintDialog(); break;
            //}
             report.Print();

        }


    }
}
