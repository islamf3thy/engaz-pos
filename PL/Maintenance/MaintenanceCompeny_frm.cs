﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class MaintenanceCompeny_frm : Form
    {
        public MaintenanceCompeny_frm()
        {
            InitializeComponent();
        }
        MaintenanceCompeny_cls cls = new MaintenanceCompeny_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);

        }


        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
          
        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtCompenyName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

  

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //"Conditional"
                if (txtCompenyName.Text.Trim() == "")
                {
                    txtCompenyName.BackColor = Color.Pink;
                    txtCompenyName.Focus();
                    return;
                }

                // " Search TextBox "
                DataTable DtSearch = cls.NameSearch_MaintenanceCompeny(txtCompenyName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCompenyName.BackColor = Color.Pink;
                    txtCompenyName.Focus();
                    return;
                }

                txtCompenyID.Text = cls.MaxID_MaintenanceCompeny();
                cls.Insert_MaintenanceCompeny(txtCompenyID.Text, txtCompenyName.Text.Trim(),txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // "Conditional"
                if (txtCompenyName.Text.Trim() == "")
                {
                    txtCompenyName.BackColor = Color.Pink;
                    txtCompenyName.Focus();
                    return;
                }

                //"Conditional"
             


                cls.Update_MaintenanceCompeny(txtCompenyID.Text, txtCompenyName.Text.Trim(), txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                                // fill combBox combParent
            
                //=============================================================================================

                txtCompenyID.Text = cls.MaxID_MaintenanceCompeny();
                txtCompenyName.Text = "";
                txtRemark.Text ="";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;

                if (Application.OpenForms["DeviceType_frm"]!=null)
                {
                    ((DeviceType_frm)Application.OpenForms["DeviceType_frm"]).FillCombMaintenanceCompeny();
                }



                txtCompenyName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                #region " Search TextBox "



                //DataTable DtItem = cls.Search_Category_Delete( txtDeviceTypeID.Text);
                //if (DtItem.Rows.Count > 0)
                //{
                //    MessageBox.Show("لا يمكنك حذف مجموعة يوجد بداخلها اصناف ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    return;
                //}

                #endregion

                if (Mass.Delete() == true)
                {
                    cls.Delete_MaintenanceCompeny( txtCompenyID.Text);
                    btnNew_Click(null, null); 
                    LoadData = true;
                }  
             
            
            



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
           

            try
            {
                MaintenanceCompenySearch_frm frm = new MaintenanceCompenySearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata==false)
                {
                    return;
                }

                DataTable dt = cls.Details_MaintenanceCompeny(frm.DGV1.CurrentRow.Cells["CompenyID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtCompenyID.Text = Dr["CompenyID"].ToString();
                txtCompenyName.Text = Dr["CompenyName"].ToString();

                txtRemark.Text =Dr["Remark"].ToString();
     
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

     

      



  

       












    }
}
