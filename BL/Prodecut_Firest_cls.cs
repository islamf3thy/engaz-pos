﻿
using System;
using System.Data;

class Prodecut_Firest_cls : DataAccessLayer
    {

    //MaxID
    public String MaxID_Firist_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from Firist_Main", CommandType.Text);
    }

    // Insert
    public void InsertFirist_Main(string MainID, DateTime MyDate, string Remarks, string TotalInvoice, int UserAdd)
    {
        Execute_SQL("insert into Firist_Main(MainID ,MyDate ,Remarks ,TotalInvoice ,UserAdd )Values (@MainID ,@MyDate ,@Remarks ,@TotalInvoice ,@UserAdd )", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void UpdateFirist_Main(string MainID, DateTime MyDate, string Remarks, string TotalInvoice)
    {
        Execute_SQL("Update Firist_Main Set MainID=@MainID ,MyDate=@MyDate ,Remarks=@Remarks ,TotalInvoice=@TotalInvoice  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice));
    }

    //Delete
    public void DeleteFirist_Main(string MainID)
    {
        Execute_SQL("Delete  From Firist_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //Details ID
    public DataTable Details_Firist_Main(string MainID)
    {
        return ExecteRader("Select MainID ,MyDate ,Remarks ,TotalInvoice ,UserAdd  from Firist_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //Details ID
    public DataTable Details_Firist_Main()
    {
        return ExecteRader("Select MainID ,MyDate ,Remarks ,TotalInvoice ,UserAdd  from Firist_Main", CommandType.Text);
    }


    // ==============================================================================================================================================================================================




    //MaxID
    private String MaxID_Firist_Details()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from Firist_Details", CommandType.Text);
    }
    // Insert
    public void InsertFirist_Details(string MainID, string ProdecutID, string Unit, string UnitFactor, string UnitOperating, string Quantity, string Price, string TotalPrice, string StoreID, Boolean Recived)
    {
        Execute_SQL("insert into Firist_Details(ID ,MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity ,Price ,TotalPrice ,StoreID ,Recived )Values (@ID ,@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity ,@Price ,@TotalPrice ,@StoreID ,@Recived )", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, MaxID_Firist_Details()),
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@Unit", SqlDbType.NVarChar, Unit),
        Parameter("@UnitFactor", SqlDbType.Float, UnitFactor),
        Parameter("@UnitOperating", SqlDbType.Float, UnitOperating),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@Recived", SqlDbType.Bit, Recived));
    }

    //Delete
    public void DeleteFirist_Details(string MainID)
    {
        Execute_SQL("Delete  From Firist_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    //Details ID
    public DataTable Details_Firist_Details(string MainID)
    {
        return ExecteRader(@"SELECT        dbo.Firist_Details.ID, dbo.Firist_Details.MainID, dbo.Firist_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Firist_Details.Unit, dbo.Firist_Details.UnitFactor, dbo.Firist_Details.UnitOperating, dbo.Firist_Details.Quantity, 
                         dbo.Firist_Details.Price, dbo.Firist_Details.TotalPrice, dbo.Firist_Details.StoreID, dbo.Stores.StoreName, dbo.Firist_Details.Recived
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Firist_Details ON dbo.Prodecuts.ProdecutID = dbo.Firist_Details.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Firist_Details.StoreID = dbo.Stores.StoreID
WHERE(dbo.Firist_Details.MainID =@MainID)", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    //Details ID
    public DataTable Search_Firist_Details(string ProdecutID,string UnitFactor, string StoreID)
    {
        return ExecteRader(@"Select ProdecutID, UnitFactor ,StoreID  from Firist_Details where ProdecutID=@ProdecutID and UnitFactor=@UnitFactor and StoreID=@StoreID", CommandType.Text,
               Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
                              Parameter("@UnitFactor", SqlDbType.NVarChar, UnitFactor),
            Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }



    



}

