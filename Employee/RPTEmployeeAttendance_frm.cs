﻿using ByStro.RPT;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class RPTEmployeeAttendance_frm : XtraForm
    {
        public RPTEmployeeAttendance_frm()
        {
            InitializeComponent();
        }

        Employee_Attendance_cls cls = new Employee_Attendance_cls();

 
        List<GridColumn> IntColumns()
        {


            GridColumn EmployeeName = new GridColumn();
            GridColumn MyDate = new GridColumn();
            GridColumn Hower_Come = new GridColumn();
            GridColumn Hower_Go = new GridColumn();
            GridColumn Hower_Work = new GridColumn();
   


        
            EmployeeName.FieldName = "EmployeeName";
            EmployeeName.Caption = "اسم الموظف";
            EmployeeName.Name = "EmployeeName"; 

            MyDate.FieldName = "MyDate";
            MyDate.Caption = "التاريخ";
            MyDate.Name = "MyDate";

            Hower_Come.FieldName = "Hower_Come";
            Hower_Come.Caption = "ساعة الحضور";
            Hower_Come.Name = "Hower_Come";
            Hower_Go.FieldName = "Hower_Go";
            Hower_Go.Caption = "ساعة الانصراف";
            Hower_Go.Name = "Hower_Go";
            Hower_Work.FieldName = "Hower_Work";
            Hower_Work.Caption = "عدد ساعات العمل";
            Hower_Work.Name = "Hower_Work";




            return new List<GridColumn>()
            { MyDate, Hower_Come, Hower_Go, Hower_Work  };


        }
        private void EmployeeAttendance_frm_Load(object sender, EventArgs e)
        {

            using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {

                Employee_cls Employee_cls = new Employee_cls();
                LookUpEdit_PartID.Properties.DataSource = db.Employees.Select(x=>new {x.EmployeeID , x.EmployeeName }).ToList();
                LookUpEdit_PartID.Properties.DisplayMember = "EmployeeName";
                LookUpEdit_PartID.Properties.ValueMember = "EmployeeID";

            }


            // FILL Combo Store item
            gridView1.Columns.Clear();
            int Index = 0;
            foreach (var item in IntColumns())
            {
                gridView1.Columns.Add(item);
                item.VisibleIndex = Index;
                item.OptionsColumn.AllowEdit = false;
                Index++;
            }

        }
      

        public void Sum_Hower_Work()
        {
            int namberday = 0;
            TimeSpan t1 = TimeSpan.Parse("00:00:00");
            try
            {

                for (int i = 0; i < gridView1.RowCount ; i++)
                {
                    namberday += 1;
                    t1 += TimeSpan.Parse(gridView1.GetRowCellValue(i, "Hower_Work" ) .ToString() );
                }

                txt_1 .Caption  = namberday.ToString();

                txt_2.Caption  = Convert.ToString(t1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }





        }


 

       

        private void EmployeeAttendance_frm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                btnSave_Click(null, null);
            }
        }

   

        private void btnSave_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if ((LookUpEdit_PartID.EditValue is int id && id > 0) == false)
            {
                LookUpEdit_PartID.ErrorText = "يجب اختيار الموظف";

                return;
            }

             

            gridControl1.DataSource = cls.Search_RPT_Employee_Attendance(LookUpEdit_PartID.EditValue.ToString(), datefrom.DateTime, false);
            Sum_Hower_Work();


        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                btnSave_Click(null, null);
            else
                RPT.rpt_GridReport.Print(gridControl1, this.Text + " : " + LookUpEdit_PartID.Text,

                 " " + stlb1.Caption + " " + txt_1.Caption +
                 " " + stlb2.Caption + " " + txt_2.Caption +
                 " التاريخ :  " + datefrom.Text
                    , true);
        }
    }
}
