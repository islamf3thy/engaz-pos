﻿using System;
using System.Data;

class Employee_Holiday_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_Employee_Holiday()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from Employee_Holiday", CommandType.Text);
    }


    // Insert
    public void Insert_Employee_Holiday(string ID, DateTime MyDate, string EmployeeID, string HolidayID, string DayNumber, string Remarks, int UserAdd)
    {
        Execute_SQL("insert into Employee_Holiday(ID ,MyDate ,EmployeeID ,HolidayID ,DayNumber ,Remarks ,UserAdd )Values (@ID ,@MyDate ,@EmployeeID ,@HolidayID ,@DayNumber ,@Remarks ,@UserAdd )", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@HolidayID", SqlDbType.Int, HolidayID),
        Parameter("@DayNumber", SqlDbType.SmallInt, DayNumber),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Employee_Holiday(string ID, DateTime MyDate, string EmployeeID, string HolidayID, string DayNumber, string Remarks)
    {
        Execute_SQL("Update Employee_Holiday Set ID=@ID ,MyDate=@MyDate ,EmployeeID=@EmployeeID ,HolidayID=@HolidayID ,DayNumber=@DayNumber ,Remarks=@Remarks  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@HolidayID", SqlDbType.Int, HolidayID),
        Parameter("@DayNumber", SqlDbType.SmallInt, DayNumber),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }


    //Delete
    public void Delete_Employee_Holiday(string ID)
    {
        Execute_SQL("Delete  From Employee_Holiday where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID));
    }

    //Details ID
    public DataTable Details_Employee_Holiday(string ID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Holiday.ID, dbo.Employee_Holiday.MyDate, dbo.Employee_Holiday.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Holiday.HolidayID, dbo.HolidayType.HolidayName, 
                         dbo.Employee_Holiday.DayNumber, dbo.Employee_Holiday.Remarks, dbo.Employee_Holiday.UserAdd
FROM            dbo.HolidayType INNER JOIN
                         dbo.Employee_Holiday ON dbo.HolidayType.HolidayID = dbo.Employee_Holiday.HolidayID INNER JOIN
                         dbo.Employees ON dbo.Employee_Holiday.EmployeeID = dbo.Employees.EmployeeID
WHERE(dbo.Employee_Holiday.ID = @ID)", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID));
    }



    //Search 
    public DataTable Search__Employee_Holiday(DateTime MyDate ,DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Holiday.ID, dbo.Employee_Holiday.MyDate, dbo.Employee_Holiday.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Holiday.HolidayID, dbo.HolidayType.HolidayName, 
                         dbo.Employee_Holiday.DayNumber, dbo.Employee_Holiday.Remarks, dbo.Employee_Holiday.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.HolidayType INNER JOIN
                         dbo.Employee_Holiday ON dbo.HolidayType.HolidayID = dbo.Employee_Holiday.HolidayID INNER JOIN
                         dbo.Employees ON dbo.Employee_Holiday.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Holiday.UserAdd = dbo.UserPermissions.ID
            Where dbo.Employee_Holiday.MyDate>= @MyDate And dbo.Employee_Holiday.MyDate<= @MyDate2", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    public DataTable Search__Employee_Holiday(DateTime MyDate, DateTime MyDate2,string EmployeeID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Holiday.ID, dbo.Employee_Holiday.MyDate, dbo.Employee_Holiday.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Holiday.HolidayID, dbo.HolidayType.HolidayName, 
                         dbo.Employee_Holiday.DayNumber, dbo.Employee_Holiday.Remarks, dbo.Employee_Holiday.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.HolidayType INNER JOIN
                         dbo.Employee_Holiday ON dbo.HolidayType.HolidayID = dbo.Employee_Holiday.HolidayID INNER JOIN
                         dbo.Employees ON dbo.Employee_Holiday.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Holiday.UserAdd = dbo.UserPermissions.ID
            Where dbo.Employee_Holiday.MyDate>= @MyDate And dbo.Employee_Holiday.MyDate<= @MyDate2 and  dbo.Employee_Holiday.EmployeeID=@EmployeeID", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@MyDate2", SqlDbType.Date, MyDate2),
           Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID));
    }








}

