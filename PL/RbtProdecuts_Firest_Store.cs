﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class RbtProdecuts_Firest_Store : Form
    {
        public RbtProdecuts_Firest_Store()
        {
            InitializeComponent();
        }
        RbtProdecuts_Firest_Store_cls cls = new RbtProdecuts_Firest_Store_cls();
        private void BalanseItemes_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                // FILL Combo Store item
                Store_cls Store_cls = new Store_cls();
                combStore.DataSource = Store_cls.Search_Stores("");
                combStore.DisplayMember = "StoreName";
                combStore.ValueMember = "StoreID";
                combStore.Text = "";
                txtStoreID.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }









 

        

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                if (combStore.Text.Trim()==""||txtStoreID.Text.Trim()=="")
                {
                    dt= cls.Search_Firist_Details();
                }
                else
                {
                    dt= cls.Search_Firist_Details(txtStoreID.Text);
                }
              
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
                lblCount.Text = DGV1.Rows.Count.ToString();
                sumTotal();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }

        private void sumTotal()
        {
            try
            {
                double sum = 0;
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    sum +=Convert.ToDouble( DGV1.Rows[i].Cells["TotalPrice"].Value.ToString());
                }
                lblSum.Text = sum.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void combStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;
                txtStoreID.Text = combStore.SelectedValue.ToString();

            }
            catch
            {

                txtStoreID.Text = "";

            }
        }

        private void combStore_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;

                if (combStore.Text == "")
                {
                    txtStoreID.Text = "";
                }
                else
                {
                    txtStoreID.Text = combStore.SelectedValue.ToString();
                }
            }
            catch
            {
                txtStoreID.Text = "";
            }
        }
    }
}
