﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.DAL;
using System.Data.SqlClient;

namespace ByStro.Forms
{
    public partial class frm_CreatNewCompany : DevExpress.XtraEditors.XtraForm
    {
        public frm_CreatNewCompany()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
            builder.InitialCatalog = textEdit6.Text;
            DBDataContext db = new DBDataContext(builder.ConnectionString);
            if (db.DatabaseExists())
            {
                XtraMessageBox.Show("قاعده البيانات موجوده مسبقا , يرجي تغيير اسم قاعده البيانات ");
                return;

            }
            simpleButton1.Enabled = false; 
            layoutControlItem8.Visibility =  DevExpress.XtraLayout.Utils.LayoutVisibility.Always ;
            progressBar1.Value = 10;
            this.Invalidate(true);
            db.CreateDatabase();
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.UserPermissions.InsertOnSubmit(new UserPermission()
            {
                UserPassword = "admin",
                UserName = "admin",
                TypeUser = true,
                Status = true,
                ID = 1,
                EmpName = "adminstrator",
                CS1 = true,
                CS2 = true,
                CS3 = true,
                CS4 = true,
                CS5 = true,
                CS6 = true,
                CS7 = true,
                CS8 = true,
                CS9 = true,
                CS10 = true,
                CS11 = true,
                CS12 = true,
                CS13 = true,
                CS14 = true,
                CS15 = true,
                CS16 = true,
                CS17 = true,
                CS18 = true,
                CS19 = true,
                CS20 = true,
                CS21 = true,
                CS22 = true,
                CS23 = true,
                CS24 = true,
                CS25 = true,
                CS26 = true,
                CS27 = true,
                CS28 = true,
                CS29 = true,
                CS30 = true,
                CS31 = true,
                CS32 = true,
                CS33 = true,
                CS34 = true,
                CS35 = true,
                CS36 = true,
                CS37 = true,
                CS38 = true,
                CS39 = true,
                CS40 = true,
                CS41 = true,
                CS42 = true,
                CS43 = true,
                CS44 = true,
                CS45 = true,
                CS46 = true,
                CS47 = true,
                CS48 = true,
                CS49 = true,
                CS50 = true,
                CS51 = true,
                CS52 = true,
                CS53 = true,
                CS54 = true,
                CS55 = true,
                CS56 = true,
                CS57 = true,
                CS58 = true,
                CS59 = true,
                CS60 = true,
                CS61 = true,
                CS62 = true,
                CS63 = true,
                CS64 = true,
                CS65 = true,
                CS66 = true,
                CS67 = true,
                CS68 = true,
                CS69 = true,
                CS70 = true,
                CS71 = true,
                CS72 = true,
                CS73 = true,
                CS74 = true,
                CS75 = true,
                CS76 = true,
                CS77 = true,
                CS78 = true,
                CS79 = true,
                CS80 = true,
                CS81 = true, 
                
            });
            db.Stores.InsertOnSubmit(new Store()
            {
                StoreID = 1,
                StoreName = "المخزن الافتراضي", 
                Note="",
                phone= "",
                phone2 = "",
                Place= "",
                

            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.CombanyDatas.InsertOnSubmit(new CombanyData()
            {
                CombanyName = textEdit1.Text,
                PhoneNamber = textEdit3.Text,
                CombanyDescriptone = textEdit2.Text,
                MyName = textEdit3.Text ,
                Title ="",
                BillRemark ="",
                MaintenanceRemark = "",
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.StoreUsers.InsertOnSubmit(new StoreUser()
            {
                ID = 1,
                StoreID = 1,
                UserID = 1 
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Categories.InsertOnSubmit(new Category()
            {
                CategoryID = 1,
                CategoryName = "مجموعه افتراضيه",
                Remark ="", 
                
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Suppliers.InsertOnSubmit(new Supplier()
            {
                SupplierID = 1,
                SupplierName = "مورد افتراضي",
                Status = true,
                UserAdd = 1,
                CreditLimit = 1,
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Customers.InsertOnSubmit(new Customer()
            {
                CustomerID = 1,
                CustomerName = "عميل افتراضي",
                CreditLimit = 1,
                SalesLavel = "1",
                ISCusSupp = false,
                Status = true,
                UserAdd = 1,
                AccountNumber="",
                Address="",
                Email="",
                EndContract="",
                Fax="",
                NationalID="",
                WebSite = "",
                Maximum = "",
                Phone = "",
                Phone2 = "",
                Remarks="",
                TaxFileNumber ="",
                SupplierID="",


            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            db.Units.InsertOnSubmit(new Unit()
            {
                UnitID = 1,
                UnitName = "قطعه"
            });
            progressBar1.Value += 10;
            this.Invalidate(true);
          
            db.SystemSettings.InsertOnSubmit(new SystemSetting() { DivideWeightBy = 2, ItemCodeLength = 7, UseScalBarcode = true, WeightCodeLength = 6 });
            db.Settings.InsertOnSubmit(new Setting()
            {
                CategoryId = "1",
                CustomerID = "1",
                SettingID = 1,
                UseVat = false,
                StoreID = "1",
                UnitId = "1",
                UseCategory = true,
                UseCustomerDefault = true,
                UseStoreDefault = true,
                UseUnit = true,
                Vat = 0,
                PrintSize = 0,
                ShowMessageSave = true,
                ShowMessageQty = true,
                kindPay = "0",
                UsekindPay = false,
                UsingFastInput =true,
                UseCrrencyDefault = false,
                NotificationCustomers =true,
                NotificationProdect=true,
                MaxBalance = "10000",
                

            });
            progressBar1.Value += 10;
            this.Invalidate(true);
            XtraMessageBox.Show("تم انشاء قاعده البيانات بنجاح"); 
            db.SubmitChanges();
        }
    }
}