﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;

namespace ByStro.PL
{
    public partial class PayType_Trans_frm : Form
    {
        public PayType_Trans_frm()
        {
            InitializeComponent();
        }

        PayType_cls cls = new PayType_cls();
        DataAccessLayer DataAccessLayer = new DataAccessLayer();
        private void CustomersTrans_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            this.MaximizeBox = true;
            ERB_Setting.SettingDGV(DGV1);
            FillPayType();
            DGV1.DataSource = null;
            DGV1.Rows.Clear();
            txt_1.Text = "0";
            txt_2.Text = "0";
            txt_3.Text = "0";
        }
        DataTable dt_Print = new DataTable();



        public void FillPayType()
        {
            string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
            combPayType.Text = null;
     

            // FILL Combo Store item
            UserPermissions_CLS Store_cls = new UserPermissions_CLS();
            combUser.DataSource = Store_cls.Search_UserPermissions();
            combUser.DisplayMember = "EmpName";
            combUser.ValueMember = "ID";
            combUser.Text = null;

        }



        private void btnNew_Click(object sender, EventArgs e)
        {



            try
            {

                if (combPayType.SelectedIndex < 0)
                {
                    combPayType.BackColor = Color.Pink;
                    combPayType.Focus();
                    return;
                }

                Decimal x = 0;
                Decimal y = 0;


                DataTable Dt2 = new DataTable();
                if (combUser.SelectedIndex <0)
                {
                    Dt2 = cls.PayType_trans(datefrom.Value, datetto.Value, combPayType.SelectedValue.ToString());
                }
                else
                {
                    Dt2 = cls.PayType_trans(datefrom.Value, datetto.Value, combPayType.SelectedValue.ToString(),combUser.SelectedValue.ToString());
                }
                //===========================================================================================




                DataTable Dt = new DataTable();
                if (combUser.SelectedIndex < 0)
                {
                    Dt = cls.PayType_trans(datefrom.Value, combPayType.SelectedValue.ToString());
                }
                else
                {
                    Dt = cls.PayType_trans(datefrom.Value, combPayType.SelectedValue.ToString(), combUser.SelectedValue.ToString());
                }
                Decimal Sumincome = 0;
                Decimal SumExport = 0;
                if (Dt.Rows[0][0] == DBNull.Value == false)
                    Sumincome = Convert.ToDecimal(Dt.Rows[0][0]);

                if (Dt.Rows[0][1] == DBNull.Value == false)
                    SumExport = Convert.ToDecimal(Dt.Rows[0][1]);

                //====================================================================================

                SqlDataAdapter da_Print = new SqlDataAdapter(@"SELECT  dbo.PayType_trans.*, dbo.UserPermissions.EmpName FROM   dbo.UserPermissions INNER JOIN  dbo.PayType_trans ON dbo.UserPermissions.ID = dbo.PayType_trans.UserID
                Where PayID='0' ORDER BY VoucherDate", DataAccessLayer.connSQLServer);
                dt_Print = new DataTable();

                da_Print.Fill(dt_Print);
                da_Print.Dispose();
                dt_Print.Clear();
                DataRow DR = dt_Print.NewRow();
                //DR["VoucherCode"] = "0";
                //DR["VoucherType"] = "رصيد سابق";
                //DR["VoucherDate"] = DateTime.Now.ToString("yyyy/MM/dd");
                //DR["Statement"] = "رصيد الفترة السابقة";
                //DR["Debit"] = Sumincome.ToString();
                //DR["Credit"] = SumExport.ToString();
                //dt_Print.Rows.Add(DR);
                //x += Convert.ToDecimal(Sumincome.ToString());
                //y += Convert.ToDecimal(SumExport.ToString());



                for (int i = 0; i < Dt2.Rows.Count; i++)
                {
                    DR = dt_Print.NewRow();
                    //DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherCode"] = Dt2.Rows[i]["VoucherCode"];
                    DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherDate"] = Dt2.Rows[i]["VoucherDate"];
                    DR["Statement"] = Dt2.Rows[i]["Statement"];
                    DR["Debit"] = Dt2.Rows[i]["Debit"];
                    DR["Credit"] = Dt2.Rows[i]["Credit"];
                    DR["EmpName"] = Dt2.Rows[i]["EmpName"];
                    dt_Print.Rows.Add(DR);
                    x += Convert.ToDecimal(Dt2.Rows[i]["Debit"]);
                    y += Convert.ToDecimal(Dt2.Rows[i]["Credit"]);
                }


                txt_1.Text = x.ToString();
                txt_2.Text = y.ToString();
                Decimal D = x - y;
                txt_3.Text = D.ToString();
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt_Print;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Massage", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        //private void toolStripButton1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (DGV1.Rows.Count == 0)
        //        {
        //            btnNew_Click(null, null);
        //        }

        //        #region "Print"
        //        Reportes_Form frm = new Reportes_Form();
        //        PayTypeTransCR report = new PayTypeTransCR();
        //        report.Database.Tables["PayType_trans"].SetDataSource(dt_Print);
        //        report.SetParameterValue(0, datefrom.Value);
        //        report.SetParameterValue(1, datetto.Value);
        //        report.SetParameterValue(2, txt_1.Text);
        //        report.SetParameterValue(3, txt_2.Text);
        //        report.SetParameterValue(4, txt_3.Text);
        //        report.SetParameterValue(5, combPayType.Text);
        //        frm.crystalReportViewer1.ReportSource = report;
        //        frm.crystalReportViewer1.Refresh();
        //        frm.ShowDialog();
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }







        //}

        private void combPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;
        }
    }
}
