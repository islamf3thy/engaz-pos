﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Number_frm : Form
    {
        public Number_frm()
        {
            InitializeComponent();
        }
        public Boolean ISOK = false;
        private void Number_frm_Load(object sender, EventArgs e)
        {
            ISOK = false;
        }
        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + button12.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + ".";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            try
            {

                txtQ.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "1";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "2";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "3";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "4";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "5";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "6";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "7";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "8";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "9";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnx_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q.Substring(0, txtQ.Text.Length - 1);

            }
            catch //(Exception ex)
            {
              //  MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtQ.Text.Trim()!="")
            {
                ISOK = true;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

    
    }
}
