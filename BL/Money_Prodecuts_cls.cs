﻿
using System;
using System.Data;

class Money_Prodecuts_cls : DataAccessLayer
{
    //Details ID
    public DataTable Money_Prodects_Sales_Details(DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Sales_Details.ID, dbo.Sales_Details.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, 
                         dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.BayPrice, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, 
                         dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.StoreID, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Sales_Details.TotalBayPrice, dbo.Stores.StoreName
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Sales_Details ON dbo.Sales_Main.MainID = dbo.Sales_Details.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID
                  Where MyDate>= @MyDate And MyDate<= @MyDate2", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
               Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    public DataTable Money_Prodects_Sales_Details(DateTime MyDate, DateTime MyDate2,string StoreID)
    {
        return ExecteRader(@"SELECT        dbo.Sales_Details.ID, dbo.Sales_Details.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, 
                         dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.BayPrice, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, 
                         dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.StoreID, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Sales_Details.TotalBayPrice, dbo.Stores.StoreName
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Sales_Details ON dbo.Sales_Main.MainID = dbo.Sales_Details.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID
                  Where MyDate>= @MyDate And MyDate<= @MyDate2 and dbo.Sales_Details.StoreID=@StoreID", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@MyDate2", SqlDbType.Date, MyDate2),
               Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }



    public DataTable Money_Prodects(DateTime? MyDate =null, DateTime? MyDate2 = null, int? StoreID = null, int? CategoryID = null, int? ProdecutID = null)
    {
        return ExecteRader(@"ProductProfiles", CommandType.StoredProcedure,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
               Parameter("@MyDate2", SqlDbType.Date, MyDate2),
               Parameter("@StoreID", SqlDbType.Int, StoreID),
                 Parameter("@CategoryID", SqlDbType.Int, CategoryID),
                 Parameter("@ProdecutID", SqlDbType.Int, ProdecutID));
    }


    public void createstore()
    {
        Execute_SQL(@"
CREATE	PROCEDURE [dbo].[ProductProfiles]
    @MyDate	date=NULL,
    @MyDate2	date=NULL,
    @StoreID int = NULL,
    @CategoryID int = NULL,
    @ProdecutID int = NULL
	AS

SELECT        dbo.Sales_Details.ID, dbo.Sales_Details.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, 
                         dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, 
                         dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.BayPrice, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, 
                         dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.StoreID, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, 
                         dbo.Sales_Details.TotalBayPrice, dbo.Stores.StoreName, dbo.Prodecuts.CategoryID
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Sales_Details ON dbo.Sales_Main.MainID = dbo.Sales_Details.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID

		 where	 dbo.Sales_Main. MyDate >=@MyDate AND   dbo.Sales_Main. MyDate<=@MyDate2
			  
	AND  (@StoreID IS NULL OR  dbo.Sales_Details.StoreID = @StoreID)
	AND  (@CategoryID IS NULL OR  dbo.Prodecuts.CategoryID = @CategoryID)
	AND  (@ProdecutID IS NULL OR  dbo.Prodecuts.ProdecutID = @ProdecutID)", CommandType.Text);
    
    }







}

