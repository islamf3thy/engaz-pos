﻿
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;
using DevExpress.XtraGrid.Columns;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Linq;
using ByStro.DAL;
using ByStro.Clases;

namespace ByStro.PL
{
    public partial class RbtCustomer_frm : XtraForm
    {
        public bool IsForCustomers { get; set; }

        public RbtCustomer_frm(bool _isForCustomers)
        {
            InitializeComponent();
            IsForCustomers = _isForCustomers;
            this.Text = "ارصده حسابات : " + (IsForCustomers ? "العملاء" : "الموردين");
            barButtonItem1.PerformClick();
            gridView1.Columns["AccountID"].Caption = "رقم الحساب";
            gridView1.Columns["Debit"].Caption = "المدين";
            gridView1.Columns["Credit"].Caption = "الدائن";
            gridView1.Columns["Account"].Caption = "اسم الحساب";
            gridView1.Columns["Balance"].Caption = "الرصيد";
            gridView1.RowCellStyle += GridView1_RowCellStyle;
            gridView1.OptionsBehavior.Editable = false;
        }

        private void GridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
           if (e.Column.FieldName == "Balance")
            {
                if(e.CellValue is double d && d > 0)
                {
                 e.Appearance.BackColor =    DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
                }
                else
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger ;

                }
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var db = new DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var query = from mov in db.TreasuryMovements.Where(x => x.ISCusSupp == (IsForCustomers ? "Cus" : "Supp"))
                            group mov by new { mov.AccountID, mov.ISCusSupp } into acc
                            from cus in db.Customers.Where(x => x.CustomerID.ToString() == acc.Key.AccountID).DefaultIfEmpty()
                            from supp in db.Suppliers.Where(x => x.SupplierID.ToString() == acc.Key.AccountID).DefaultIfEmpty()
                            select new
                            {
                                acc.Key.AccountID,
                                Account = (acc.Key.ISCusSupp == "Cus") ? cus.CustomerName : (acc.Key.ISCusSupp == "Supp") ? supp.SupplierName : "Unkown",
                                Debit = acc.Sum(x => x.Debit),
                                Credit = acc.Sum(x => x.Credit),
                                Balance = acc.Sum(x => x.Credit ?? 0) - acc.Sum(x => x.Debit ?? 0)
                            };
                gridControl1.DataSource = query.ToList();
                txt_1.Caption = query.Count().ToString();
                txt_2.Caption = query.Sum(x => x.Debit).ToString();
                txt_3.Caption = query.Sum(x => x.Credit).ToString();
                txt_4.Caption =( (query.Sum(x => (double?)x.Balance ))??0).ToString();

            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)  
                RPT.rpt_GridReport.Print(gridControl1, this.Text , 
                 " " + stlb1.Caption + " " + txt_1.Caption +
                 " " + stlb2.Caption + " " + txt_2.Caption +
                 " " + stlb3.Caption + " " + txt_3.Caption +
                 " " + stlb4.Caption + " " + txt_4.Caption
                    , true);
        }

        private void RbtCustomer_frm_Load(object sender, EventArgs e)
        {
            this.Load += (_sender, _e) => { MasterClass.RestoreGridLayout(gridView1, this); };
            this.FormClosing += (_sender, _e) => { MasterClass.SaveGridLayout(gridView1, this); };
        }
    }
}
