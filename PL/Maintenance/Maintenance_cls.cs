﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


class Maintenance_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_Maintenance()
    {
        return Execute_SQL("select ISNULL (MAX(MaintenanceID)+1,1) from Maintenance", CommandType.Text);
    }


    // Insert
    public void Insert_Maintenance(String MaintenanceID, DateTime RecivedData, DateTime ReturnData, String SerialNumber, String DeviceTypeID, String DamagedTypeID, String DeviceStatus, String CustomerID, Double TotalPrice, String Remark, int UserID)
    {
        Execute_SQL("insert into Maintenance(MaintenanceID ,RecivedData ,ReturnData ,SerialNumber ,DeviceTypeID ,DamagedTypeID ,DeviceStatus ,CustomerID ,TotalPrice ,Remark ,UserID )Values (@MaintenanceID ,@RecivedData ,@ReturnData ,@SerialNumber ,@DeviceTypeID ,@DamagedTypeID ,@DeviceStatus ,@CustomerID ,@TotalPrice ,@Remark ,@UserID )", CommandType.Text,
        Parameter("@MaintenanceID", SqlDbType.Int, MaintenanceID),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
        Parameter("@ReturnData", SqlDbType.Date, ReturnData),
        Parameter("@SerialNumber", SqlDbType.NVarChar, SerialNumber),
        Parameter("@DeviceTypeID", SqlDbType.Int, DeviceTypeID),
        Parameter("@DamagedTypeID", SqlDbType.Int, DamagedTypeID),
        Parameter("@DeviceStatus", SqlDbType.SmallInt, DeviceStatus),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@Remark", SqlDbType.NText, Remark),
        Parameter("@UserID", SqlDbType.Int, UserID));
    }





    //Update
    public void Update_Maintenance(string MaintenanceID, DateTime RecivedData, DateTime ReturnData, String SerialNumber, string DeviceTypeID, string DamagedTypeID, string DeviceStatus, String CustomerID, Double TotalPrice, string Remark)
    {
        Execute_SQL("Update Maintenance Set MaintenanceID=@MaintenanceID ,RecivedData=@RecivedData ,ReturnData=@ReturnData ,SerialNumber=@SerialNumber ,DeviceTypeID=@DeviceTypeID ,DamagedTypeID=@DamagedTypeID ,DeviceStatus=@DeviceStatus ,CustomerID=@CustomerID ,TotalPrice=@TotalPrice,Remark=@Remark  where MaintenanceID=@MaintenanceID", CommandType.Text,
        Parameter("@MaintenanceID", SqlDbType.Int, MaintenanceID),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
        Parameter("@ReturnData", SqlDbType.Date, ReturnData),
        Parameter("@SerialNumber", SqlDbType.NVarChar, SerialNumber),
        Parameter("@DeviceTypeID", SqlDbType.Int, DeviceTypeID),
        Parameter("@DamagedTypeID", SqlDbType.Int, DamagedTypeID),
        Parameter("@DeviceStatus", SqlDbType.SmallInt, DeviceStatus),
        Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }


    //Update
    public void Update_Maintenance(string MaintenanceID, DateTime ReturnData, int DeviceStatus, Double TotalPrice, string Remark)
    {
        Execute_SQL("Update Maintenance Set ReturnData=@ReturnData ,DeviceStatus=@DeviceStatus ,TotalPrice=@TotalPrice,Remark=@Remark  where MaintenanceID=@MaintenanceID", CommandType.Text,
        Parameter("@MaintenanceID", SqlDbType.Int, MaintenanceID),
        Parameter("@ReturnData", SqlDbType.Date, ReturnData),
        Parameter("@DeviceStatus", SqlDbType.Int, DeviceStatus),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }






    //Delete
    public void Delete_Maintenance(string MaintenanceID)
    {
        Execute_SQL("Delete  From Maintenance where MaintenanceID=@MaintenanceID", CommandType.Text,
        Parameter("@MaintenanceID", SqlDbType.NVarChar, MaintenanceID));
    }



    string Query = @"SELECT  dbo.Maintenance.*, dbo.DeviceType.DeviceTypeName, dbo.MaintenanceCompeny.CompenyName, dbo.Customers.CustomerName, 
                         dbo.DamagedType.DamagedTypeName, dbo.UserPermissions.EmpName
FROM            dbo.DeviceType INNER JOIN
                         dbo.Maintenance ON dbo.DeviceType.DeviceTypeID = dbo.Maintenance.DeviceTypeID INNER JOIN
                         dbo.DamagedType ON dbo.Maintenance.DamagedTypeID = dbo.DamagedType.DamagedTypeID INNER JOIN
                         dbo.UserPermissions ON dbo.Maintenance.UserID = dbo.UserPermissions.ID INNER JOIN
                         dbo.Customers ON dbo.Maintenance.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.MaintenanceCompeny ON dbo.DeviceType.CompenyID = dbo.MaintenanceCompeny.CompenyID ";





    //Details ID
    public DataTable Details_Maintenance(string MaintenanceID)
    {
        return ExecteRader(Query + @"Where MaintenanceID=@MaintenanceID", CommandType.Text,
        Parameter("@MaintenanceID", SqlDbType.NVarChar, MaintenanceID));
    }

    public DataTable Search_Maintenance(string search,DateTime RecivedData,DateTime RecivedData2,String DeviceStatus)
    {

        string QueryText = Query + @"Where RecivedData >= @RecivedData And RecivedData <= @RecivedData2 And dbo.Maintenance.DeviceStatus=@DeviceStatus 
AND  dbo.DeviceType.DeviceTypeName+dbo.DamagedType.DamagedTypeName+dbo.Customers.CustomerName like '%'+@search+ '%'";

        return ExecteRader(QueryText, CommandType.Text,
Parameter("@search", SqlDbType.NVarChar, search),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
             Parameter("@RecivedData2", SqlDbType.Date, RecivedData2),
              Parameter("@DeviceStatus", SqlDbType.NVarChar, DeviceStatus));
    }


    public DataTable Search_Maintenance(string search, DateTime RecivedData, DateTime RecivedData2)
    {
        return ExecteRader(Query+@"Where RecivedData >= @RecivedData And RecivedData <= @RecivedData2
AND  dbo.DeviceType.DeviceTypeName+dbo.DamagedType.DamagedTypeName+dbo.Customers.CustomerName like '%'+@search+ '%'
", CommandType.Text,
Parameter("@search", SqlDbType.NVarChar, search),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
             Parameter("@RecivedData2", SqlDbType.Date, RecivedData2));
    }
    public DataTable Search_MaintenanceUser(string search, DateTime RecivedData, DateTime RecivedData2, String DeviceStatus,string UserID)
    {
        return ExecteRader(Query+@"Where RecivedData >= @RecivedData And RecivedData <= @RecivedData2 And dbo.Maintenance.DeviceStatus=@DeviceStatus and UserID=@UserID
AND  dbo.DeviceType.DeviceTypeName+dbo.DamagedType.DamagedTypeName+dbo.Customers.CustomerName like '%'+@search+ '%'
", CommandType.Text,
Parameter("@search", SqlDbType.NVarChar, search),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
             Parameter("@RecivedData2", SqlDbType.Date, RecivedData2),
              Parameter("@DeviceStatus", SqlDbType.NVarChar, DeviceStatus),
                Parameter("@UserID", SqlDbType.NVarChar, UserID));
    }

    public DataTable Search_MaintenanceUser(string search, DateTime RecivedData, DateTime RecivedData2, string UserID)
    {
        return ExecteRader(Query+ @"Where RecivedData >= @RecivedData And RecivedData <= @RecivedData2 and UserID=@UserID
AND  dbo.DeviceType.DeviceTypeName+dbo.DamagedType.DamagedTypeName+dbo.Customers.CustomerName like '%'+@search+ '%'
", CommandType.Text,
Parameter("@search", SqlDbType.NVarChar, search),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
             Parameter("@RecivedData2", SqlDbType.Date, RecivedData2),
                Parameter("@UserID", SqlDbType.NVarChar, UserID));
    }
    public DataTable Search_Maintenance(string search, DateTime RecivedData, DateTime RecivedData2, int DeviceStatus)
    {
        return ExecteRader(Query + @"Where RecivedData >= @RecivedData And RecivedData <= @RecivedData2 And dbo.Maintenance.DeviceStatus >=@DeviceStatus
AND  dbo.DeviceType.DeviceTypeName+dbo.DamagedType.DamagedTypeName+dbo.Customers.CustomerName like '%'+@search+ '%'
", CommandType.Text,
Parameter("@search", SqlDbType.NVarChar, search),
        Parameter("@RecivedData", SqlDbType.Date, RecivedData),
             Parameter("@RecivedData2", SqlDbType.Date, RecivedData2),
              Parameter("@DeviceStatus", SqlDbType.NVarChar, DeviceStatus));
    }


}

