﻿using System;
using System.Data;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using ByStro.DAL;
using System.Windows.Forms;
using ByStro.Clases;
using System.Drawing;
using DevExpress.XtraEditors.Repository;

namespace ByStro.Forms
{
    public partial class frm_DeliveryOrderList : frm_Master
    {
        bool IsDone;
        public frm_DeliveryOrderList(bool _IsDone)
        {
            InitializeComponent();
            InitializeComponent();
            IsDone = _IsDone;
        }
        private void frm_PayInstallment_SizeChanged(object sender, EventArgs e)
        {

            if (this.WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            this.Text = IsDone? "اوامر التوصيل المنتهيه" : "اوامر التوصيل الجاريه";
            var insta = new DeliveryOrder();
            gridView1.OptionsBehavior.Editable = false;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.Columns[nameof (insta .DriverID)].Caption = "السائق";
            gridView1.Columns[nameof(insta.HasReturned)].Caption = "تم التوصيل";
            gridView1.Columns[nameof(insta.ID)].Caption = "كود";
            gridView1.Columns[nameof(insta.Orders)].Visible =false ;
            gridView1.Columns[nameof(insta.OrdersAmount)].Caption = "قيمه الفواتير";
            gridView1.Columns[nameof(insta.OrdersCount)].Caption = "عدد الفواتير";
            gridView1.Columns[nameof(insta.OutDate)].Caption = "تاريخ الخروج";
            gridView1.Columns[nameof(insta.RecivedAmount)].Caption = "المبلغ المحصل";
            gridView1.Columns[nameof(insta.ReturnDate)].Caption = "تاريخ التوصيل";
            gridView1.Columns[nameof(insta.RecivedAmount)].Summary.Clear();
            gridView1.Columns[nameof(insta.RecivedAmount)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(insta.RecivedAmount), "الاجمالي{0}");
            gridView1.Columns[nameof(insta.OrdersCount)].Summary.Clear();
            gridView1.Columns[nameof(insta.OrdersCount)].Summary.Add(DevExpress.Data.SummaryItemType.Count, nameof(insta.OrdersCount), "العدد{0}");
            btn_Delete.Visibility =
            btn_List.Visibility =
            btn_Print.Visibility =
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            base.RefreshData();
            var source = db.DeliveryOrders .Where(x => x.HasReturned  == IsDone);
            gridControl1.DataSource = source.ToList();
            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            repo.DataSource = db.Drivers.Select(x => new { x.ID, x.Name }).ToList();
            repo.ValueMember = "ID";
            repo.DisplayMember = "Name";
            gridControl1.RepositoryItems.Add(repo);
            gridView1.Columns["DriverID"].ColumnEdit = repo;
        }
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);

            if (info.InRow || info.InRowCell)
            {
                var order = view.GetRow(info.RowHandle) as DeliveryOrder ;
                if(order != null)
                {
                    new frm_DeliveryOrder(order.ID).Show();
                }
           
            }

        }
        public override void New()
        {
            new frm_DeliveryOrder().Show();
        }

        private void frm_DeliveryOrderList_Load(object sender, EventArgs e)
        {
            this.Load += (_sender, _e) => { MasterClass.RestoreGridLayout(gridView1, this); };
            this.FormClosing += (_sender, _e) => { MasterClass.SaveGridLayout(gridView1, this); };
        }
    }
}
