﻿using ByStro.Clases;
using ByStro.DAL;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_PayInstallment : frm_Master
    {
        private static frm_PayInstallment _instance;
        public static frm_PayInstallment Instance
        {
            get
            {

                if (_instance == null || _instance.IsDisposed) _instance = new frm_PayInstallment();
                return _instance;
            }
        }
        public frm_PayInstallment()
        {
            InitializeComponent();
            New();
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
          
            

        }
        public override void Print()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String); 
            var detail = db.Instalment_Details.Single(x => x.ID == Convert.ToInt32(lookUpEdit1.EditValue));
            var installment = db.Instalments.Single(x => x.ID == detail.InstalmentID);
            frm_Instalment .Print(new List<int>() { installment.ID });
        }
        public override void RefreshData()
        {
            base.RefreshData();
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            LookUpEdit_Customer.Properties.DataSource = db.Customers.Select(x => new { ID = x.CustomerID, Name = x.CustomerName ,Phone = x.Phone  }).ToList();
            LookUpEdit_Customer.Properties.DisplayMember = "Name";
            LookUpEdit_Customer.Properties.ValueMember = "ID";
            lookUpEdit1.Properties.ValueMember = "ID";
            lookUpEdit1.Properties.DisplayMember = "ID";



        }
        public override void New()
        {
           
            dateEdit1.DateTime = DateTime.Now;

            textEdit1.Text =
            textEdit11.Text =
            textEdit111.Text = "";
            dateEdit11.EditValue =  
            LookUpEdit_Customer.EditValue  =
            lookUpEdit1.EditValue = null;
            base.New();

        }

        private void frm_PayInstallment_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
        }
        int? ID;
        internal void LoadItem(int? id)
        {
            if (id == null)
                New();
            else
            {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String); 
                ID = id;
                var detail = db.Instalment_Details.Single(x => x.ID == id);
                var installment = db.Instalments.Single(x => x.ID == detail.InstalmentID);
                LookUpEdit_Customer.EditValue = installment.Customer;
                lookUpEdit1.EditValue = id;
                dateEdit1.EditValue = detail.DatePaid.HasValue ? detail.DatePaid.Value: DateTime.Now; 
            }
        }
        public override void Save()
        {

            if (lookUpEdit1.EditValue.ValidAsIntNonZero() == false) {
                lookUpEdit1.ErrorText = Clases.MasterClass.EmptyFieldErrorText;
                lookUpEdit1.Focus();
                return;
            }
            if (dateEdit1.DateTime.Year < 1950)
            {
                dateEdit1.ErrorText = Clases.MasterClass.EmptyFieldErrorText;
                dateEdit1.Focus();
                return;
            }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            Instalment_Detail instalmentDetail;

            instalmentDetail = db.Instalment_Details .Where(s => s.ID == Convert.ToInt32(lookUpEdit1.EditValue)).First();
            instalmentDetail.DatePaid = dateEdit1.DateTime;
            instalmentDetail.IsPaid = true;
            if(db.Instalment_Details.Where(s => s.InstalmentID == instalmentDetail.InstalmentID).Count ()> instalmentDetail.number)
            {
                var nextinstalmentDetail = db.Instalment_Details.Where(s => s.InstalmentID == instalmentDetail.InstalmentID && s.number > instalmentDetail.number).FirstOrDefault();
                if(nextinstalmentDetail!=null)
                {
                    var diff = Convert.ToDouble(textEdit1.EditValue) - instalmentDetail.Amount;
                    nextinstalmentDetail.Amount -= diff;
                }

            }

            instalmentDetail.Amount = Convert.ToDouble(textEdit1.EditValue);
            db.SubmitChanges();
            db.TreasuryMovements.DeleteAllOnSubmit(db.TreasuryMovements.Where(x => x.Type == ((int)MasterClass.TreasuryMovementTypes.InstalmentPay).ToString() && x.VoucherID == instalmentDetail.ID.ToString()));
            TreasuryMovement movement = new TreasuryMovement()
            {
                ID = ByStro.Clases.MasterClass. GetNewTreasuryMovementID(),
                AccountID =LookUpEdit_Customer.EditValue.ToInt().ToString(),
                ISCusSupp = "Cus",
                VoucherID = instalmentDetail.ID.ToString(),
                Type = ((int)MasterClass.TreasuryMovementTypes.InstalmentPay).ToString(),
                VoucherType = "سداد دفعه قسط",
                VoucherDate = dateEdit1.DateTime ,
                Description = string.Format("سداد دفعه قسط رقم {0} لملف قسط رقم {1} لعميل {2}",instalmentDetail.ID ,instalmentDetail.InstalmentID , LookUpEdit_Customer .Text ),
                Income = instalmentDetail.Amount ,
                Export = 0 ,
                Credit = instalmentDetail.Amount,
                Debit = 0,
                ISShow = true  ,
                UserAdd = Properties.Settings.Default.UserID,
                Remark = string.Format("سداد دفعه قسط رقم {0} لملف قسط رقم {1} لعميل {2}", instalmentDetail.ID, instalmentDetail.InstalmentID, LookUpEdit_Customer.Text),
            };
            db.TreasuryMovements.InsertOnSubmit(movement);
            db.SubmitChanges();
            MarkInstalmentState(instalmentDetail.InstalmentID);
            base.Save();
        }
        public static  void MarkInstalmentState(int ID)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var installment = db.Instalments.Single(x => x.ID == ID);
            var totalPaid = db.Instalment_Details.Where(x => x.InstalmentID == installment.ID && x.IsPaid == true ).Sum(x => ((double?)x.Amount)) ?? 0;
            if (totalPaid == installment.Amount)
                installment.ISPaid = true;
            else if (totalPaid == 0 )
                installment.ISPaid = false;
            else
                installment.ISPaid = false;

         
            db.SubmitChanges();


        }
        private void LookUpEdit_Customer_EditValueChanged(object sender, EventArgs e)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (LookUpEdit_Customer.EditValue.ValidAsIntNonZero())
            {
                var sources = db.Instalments.Where(i => db.Instalment_Details.Where(d=>d.IsPaid== false ).Select(d=>d.InstalmentID).Contains(i.ID) && i.Customer == Convert.ToInt32(LookUpEdit_Customer.EditValue)).Select(x =>new 
                {InstamentID = x.ID ,
                    ID = db.Instalment_Details.Where(d => d.InstalmentID == x.ID && d.IsPaid == false).First().ID ,
                    Amount =db.Instalment_Details.Where(d => d.InstalmentID == x.ID && d.IsPaid == false).First().Amount ,
                    DueDate = db.Instalment_Details.Where(d => d.InstalmentID == x.ID && d.IsPaid == false ).First().DueDate   }).ToList();
                if(sources == null || sources.Count() == 0)
                {
                    XtraMessageBox.Show("هذا العميل لا يوجد له اي اقساط مستحقه ");
                    lookUpEdit1.Properties.DataSource = null;
                }
                else
                {
                    lookUpEdit1.Properties.DataSource = sources;
                    lookUpEdit1.Properties.PopulateColumns();
                    if(lookUpEdit1.Properties.Columns.Count > 0)
                    {
                    lookUpEdit1.Properties.Columns["ID"].Caption = "كود";
                    lookUpEdit1.Properties.Columns["Amount"].Caption = "المبلغ";
                    lookUpEdit1.Properties.Columns["DueDate"].Caption = "تاريخ الاستحقاق";
                    lookUpEdit1.Properties.Columns["InstamentID"].Caption = "رقم الملف";
                    }

                }



            }
            else
                lookUpEdit1 .Properties.DataSource = null;
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpEdit1.EditValue.ValidAsIntNonZero())
            {
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                var detail = db.Instalment_Details.Single(x => x.ID == Convert.ToInt32(lookUpEdit1.EditValue));
                var installment = db.Instalments.Single(x => x.ID == detail.InstalmentID);
                var totalPaid = db.Instalment_Details.Where(x => x.InstalmentID == installment.ID && x.IsPaid == true && x.ID != Convert.ToInt32(lookUpEdit1.EditValue)).Sum(x =>((double ?) x.Amount))??0;
                var remainng = db.Instalment_Details.Where(x => x.InstalmentID == installment.ID && x.IsPaid == false).Count();
                dateEdit11.DateTime = detail.DueDate;
                textEdit1.Text = detail.Amount.ToString();
                textEdit11.Text = (installment.Amount - totalPaid).ToString();
                textEdit111.Text = remainng.ToString();
            }
            
        }
        public override void OpenList()
        {
            base.OpenList();
            new frm_PayInstallmentList(true).Show();
        }
    }
}
