﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class IncomStatement_frm : Form
    {
        public IncomStatement_frm()
        {
            InitializeComponent();
        }
        Menu_Meny_cls cls = new Menu_Meny_cls();

        private void IncomStatement_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Sales();
            Expense();
            Salary();
            Maintenance();
            if (BillSetting_cls.UseVat==true)
            {
                textBox6.Text = BillSetting_cls.Vat.ToString();
            }
            else
            {
                textBox6.Text = "0";
            }
        }




        private void Sales()
        {
            try
            {
                DataTable dt = cls.Search_Sales(dateTimePicker1.Value);
                DataTable dt_DiscoundBay = cls.Search_Bay(dateTimePicker1.Value);
                txtSumSales.Text = dt.Rows[0]["NetInvoice"].ToString();
                txtSumRSales.Text=dt.Rows[0]["ReturnInvoise"].ToString();

                txtSales_NetInvoice.Text = (Convert.ToDouble(dt.Rows[0]["NetInvoice"].ToString()) - Convert.ToDouble(dt.Rows[0]["ReturnInvoise"])).ToString();

                //Search_Bay
               double x =Convert.ToDouble( dt.Rows[0]["TotalBayPrice"].ToString());
               double y = Convert.ToDouble(dt.Rows[0]["ReturnBayPrice"].ToString());
               double w= Convert.ToDouble(dt_DiscoundBay.Rows[0]["Discount"].ToString());
                textBox1.Text = (x-y-w).ToString();
                textBox8.Text = (Convert.ToDouble(txtSales_NetInvoice.Text) - Convert.ToDouble(textBox1.Text)).ToString("0.00");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Expense()
        {
            try
            {
                DataTable dt = cls.Search_Expenses(dateTimePicker1.Value);
                textBox2.Text = dt.Rows[0]["ExpenseValue"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Salary()
        {
            try
            {
                DataTable dt = cls.Search_Salary(dateTimePicker1.Value);
                txtTotalSalary.Text = dt.Rows[0]["NetSalary"].ToString();
                Double salesnet = Convert.ToDouble(textBox8.Text);
                double expence = Convert.ToDouble(txtTotalSalary.Text) + Convert.ToDouble(textBox2.Text);
                textBox4.Text = (salesnet- expence).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void Maintenance()
        {
            try
            {
                DataTable dt = cls.Search_Maintenance(dateTimePicker1.Value);

                txtMaintenance.Text = (dt.Rows[0][0]).ToString();
                double x=Convert.ToDouble(textBox4.Text);
                textBox4.Text = (x + Convert.ToDouble(txtMaintenance.Text)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox6.Text.Trim()!="")
            {
                double x = Convert.ToDouble(textBox6.Text) * Convert.ToDouble(textBox4.Text) / 100;
                textBox5.Text = x.ToString();
                textBox7.Text = (Convert.ToDouble(textBox4.Text) - Convert.ToDouble(textBox5.Text)).ToString();
            }
            else
            {
                textBox5.Text = "0";
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            try
            {
            textBox7.Text = (Convert.ToDouble(textBox4.Text) - Convert.ToDouble(textBox5.Text)).ToString();

            }
            catch 
            {
                return;
            }

        }
    }
}
