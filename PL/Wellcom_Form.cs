﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Wellcom_Form : Form
    {
        public Wellcom_Form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Wellcom_Form_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = Color.Yellow;
            label1.ForeColor = Color.DarkRed;
        }

        private void Wellcom_Form_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = Color.DarkRed;
            label1.ForeColor = Color.Yellow;
        }

        private void Wellcom_Form_Load(object sender, EventArgs e)
        {

        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = Color.Yellow;
            label1.ForeColor = Color.DarkRed;
        }
    }
}
