﻿using System;
using System.Data;

class WorkPartCompany_cls : DataAccessLayer
    {


    //MaxID
    public String MaxID_WorkPartCompany()
    {
        return Execute_SQL("select ISNULL (MAX(WorkPartCompanyID)+1,1) from WorkPartCompany", CommandType.Text);
    }

    // Insert
    public void Insert_WorkPartCompany(String WorkPartCompanyID, String WorkPartCompanyName, String Remarks)
    {
        Execute_SQL("insert into WorkPartCompany(WorkPartCompanyID ,WorkPartCompanyName ,Remarks )Values (@WorkPartCompanyID ,@WorkPartCompanyName ,@Remarks )", CommandType.Text,
        Parameter("@WorkPartCompanyID", SqlDbType.Int, WorkPartCompanyID),
        Parameter("@WorkPartCompanyName", SqlDbType.NVarChar, WorkPartCompanyName),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }

    //Update
    public void Update_WorkPartCompany(String WorkPartCompanyID, String WorkPartCompanyName, String Remarks)
    {
        Execute_SQL("Update WorkPartCompany Set WorkPartCompanyID=@WorkPartCompanyID ,WorkPartCompanyName=@WorkPartCompanyName ,Remarks=@Remarks  where WorkPartCompanyID=@WorkPartCompanyID", CommandType.Text,
        Parameter("@WorkPartCompanyID", SqlDbType.Int, WorkPartCompanyID),
        Parameter("@WorkPartCompanyName", SqlDbType.NVarChar, WorkPartCompanyName),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }

    //Delete
    public void Delete_WorkPartCompany(int WorkPartCompanyID)
    {
        Execute_SQL("Delete  From WorkPartCompany where WorkPartCompanyID=@WorkPartCompanyID", CommandType.Text,
        Parameter("@WorkPartCompanyID", SqlDbType.Int, WorkPartCompanyID));
    }







    //Details ID
    public DataTable Details_WorkPartCompany(string WorkPartCompanyID)
    {
        return ExecteRader("Select *  from WorkPartCompany Where WorkPartCompanyID=@WorkPartCompanyID", CommandType.Text,
        Parameter("@WorkPartCompanyID", SqlDbType.NVarChar, WorkPartCompanyID));
    }


    //Details ID
    public DataTable NameSearch__WorkPartCompany(String WorkPartCompanyName)
    {
        return ExecteRader("Select WorkPartCompanyName  from WorkPartCompany Where WorkPartCompanyName=@WorkPartCompanyName", CommandType.Text,
        Parameter("@WorkPartCompanyName", SqlDbType.NVarChar, WorkPartCompanyName));
    }



    //Search 
    public DataTable Search_WorkPartCompany(string Search)
    {
        return ExecteRader("Select *  from WorkPartCompany Where convert(nvarchar,WorkPartCompanyID)+WorkPartCompanyName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}

