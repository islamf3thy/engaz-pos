﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public class Search_cls
{



    public static void FillcombSoreUsers(ComboBox comb, bool ISNULL = true)
    {
        Store_cls store_Cls = new Store_cls();
        DataTable dt = store_Cls.Search_StoreUsers(ByStro.Properties.Settings.Default.UserID);
        comb.DataSource = dt;
        comb.DisplayMember = "StoreName";
        comb.ValueMember = "StoreID";
        comb.Text = null;
        if (ISNULL == false)
            if (dt.Rows.Count > 0) comb.SelectedIndex = 0;


    }

    public static void FillcombUsers(ComboBox comb, bool ISNULL = true)
    {
        UserPermissions_CLS userPermissions_CLS = new UserPermissions_CLS();
        DataTable dt = userPermissions_CLS.Search_UserPermissions("");
        comb.DataSource = dt;
        comb.DisplayMember = "EmpName";
        comb.ValueMember = "ID";
        comb.Text = null;
        if (ISNULL == false)
            if (dt.Rows.Count > 0) comb.SelectedIndex = 0;

    }



    public static void FillcombPayType(ComboBox comb, bool ISNULL = true)
    {
        PayType_cls PayType_cls = new PayType_cls();
        DataTable dt = PayType_cls.Search__PayType("");
        comb.DataSource = dt;
        comb.DisplayMember = "PayTypeName";
        comb.ValueMember = "PayTypeID";
        comb.Text = null;
        if (ISNULL == false)
            if (dt.Rows.Count > 0) comb.SelectedIndex = 0;

    }






}

