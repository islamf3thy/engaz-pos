﻿using System;
using System.Data;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class Employee_Additions_Search_frm : Form
    {
        public Employee_Additions_Search_frm()
        {
            InitializeComponent();
        }
        public Boolean loaddata = false;
        public Boolean search = false;
        public String SearchType = "ALL";
        Employee_Additions_cls cls = new Employee_Additions_cls();
        private void AccountEnd_Form_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);

                Employee_cls Employee_cls = new Employee_cls();
                combEmployee.DataSource = Employee_cls.Search_Employees("");
                combEmployee.DisplayMember = "EmployeeName";
                combEmployee.ValueMember = "EmployeeID";
                combEmployee.Text = "";
                txtEmployeeID.Text = "";
   
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
     
        }


    
        private void AccountEnd_Form_KeyDown(object sender, KeyEventArgs e)
        {
        
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void DGV1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                String Header = DGV1.Columns[e.ColumnIndex].Name;
                if (Header == "C1")
                {
                    loaddata = true;
                    ID_Load = DGV1.CurrentRow.Cells["Additions_ID"].Value.ToString();
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGV1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    DGV1.Rows[i].Cells[0].Value = Properties.Resources.Open_Folder_48px;
                }
            }
            catch
            {


            }
        }
        public String ID_Load = "";
        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
              try
            {
                if (DGV1.SelectedRows.Count==1)
                {
                    loaddata = true;
                    ID_Load = DGV1.CurrentRow.Cells["Additions_ID"].Value.ToString();
                    Close();
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
            }
        }

        private void combEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
            }
            catch 
            {
                txtEmployeeID.Text = "";
            }
        }

        private void combEmployee_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combEmployee.Text=="")
                {
                    txtEmployeeID.Text = "";
                }
                else
                {
                txtEmployeeID.Text = combEmployee.SelectedValue.ToString();

                }
            }
            catch
            {
                txtEmployeeID.Text = "";
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                if (txtEmployeeID.Text.Trim()=="")
                {
                   dt= cls.Search__Employee_Additions(dateTimePicker1.Value,dateTimePicker2.Value);
                }
                else
                {
                    dt = cls.Search__Employee_Additions(txtEmployeeID.Text, dateTimePicker1.Value, dateTimePicker2.Value);

                }
                //Load 
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;

                lblCount.Text = DGV1.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
