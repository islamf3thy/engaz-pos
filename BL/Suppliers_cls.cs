﻿
using System;
using System.Data;

class Suppliers_cls : DataAccessLayer
{



    public String MaxID_Suppliers()
    {
        return Execute_SQL("select ISNULL (MAX(SupplierID)+1,1) from Suppliers", CommandType.Text);
    }


    // Insert
    public void InsertSuppliers(String SupplierID, String SupplierName, String NationalID, String Address, String TaxFileNumber, String RegistrationNo, String StartContract, String EndContract, String Remarks, String Phone, String Phone2, String AccountNumber, String Fax, String Email, String WebSite, String Maximum, String CreditLimit,Boolean UseVat, int UserAdd, Boolean Status)
    {
        Execute_SQL("insert into Suppliers(SupplierID ,SupplierName ,NationalID ,Address ,TaxFileNumber ,RegistrationNo ,StartContract ,EndContract ,Remarks ,Phone ,Phone2 ,AccountNumber ,Fax ,Email ,WebSite ,Maximum ,CreditLimit,UseVat ,UserAdd ,Status )Values (@SupplierID ,@SupplierName ,@NationalID ,@Address ,@TaxFileNumber ,@RegistrationNo ,@StartContract ,@EndContract ,@Remarks ,@Phone ,@Phone2 ,@AccountNumber ,@Fax ,@Email ,@WebSite ,@Maximum ,@CreditLimit,@UseVat ,@UserAdd ,@Status )", CommandType.Text,
        Parameter("@SupplierID", SqlDbType.Int, SupplierID),
        Parameter("@SupplierName", SqlDbType.NVarChar, SupplierName),
        Parameter("@NationalID", SqlDbType.NVarChar, NationalID),
        Parameter("@Address", SqlDbType.NVarChar, Address),
        Parameter("@TaxFileNumber", SqlDbType.NVarChar, TaxFileNumber),
        Parameter("@RegistrationNo", SqlDbType.NVarChar, RegistrationNo),
        Parameter("@StartContract", SqlDbType.NVarChar, StartContract),
        Parameter("@EndContract", SqlDbType.NVarChar, EndContract),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Phone2", SqlDbType.NVarChar, Phone2),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Email", SqlDbType.NVarChar, Email),
        Parameter("@WebSite", SqlDbType.NVarChar, WebSite),
        Parameter("@Maximum", SqlDbType.NVarChar, Maximum),
        Parameter("@CreditLimit", SqlDbType.SmallInt, CreditLimit),
          Parameter("@UseVat", SqlDbType.Bit, UseVat),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd),
        Parameter("@Status", SqlDbType.Bit, Status));
    }



    //Update
    public void UpdateSuppliers(String SupplierID, String SupplierName, String NationalID, String Address, String TaxFileNumber, String RegistrationNo, String StartContract, String EndContract, String Remarks, String Phone, String Phone2, String AccountNumber, String Fax, String Email, String WebSite, String Maximum, String CreditLimit,Boolean UseVat, Boolean Status)
    {
        Execute_SQL("Update Suppliers Set SupplierID=@SupplierID ,SupplierName=@SupplierName ,NationalID=@NationalID ,Address=@Address ,TaxFileNumber=@TaxFileNumber ,RegistrationNo=@RegistrationNo ,StartContract=@StartContract ,EndContract=@EndContract ,Remarks=@Remarks ,Phone=@Phone ,Phone2=@Phone2 ,AccountNumber=@AccountNumber ,Fax=@Fax ,Email=@Email ,WebSite=@WebSite ,Maximum=@Maximum ,CreditLimit=@CreditLimit,UseVat=@UseVat ,Status=@Status  where SupplierID=@SupplierID", CommandType.Text,
        Parameter("@SupplierID", SqlDbType.Int, SupplierID),
        Parameter("@SupplierName", SqlDbType.NVarChar, SupplierName),
        Parameter("@NationalID", SqlDbType.NVarChar, NationalID),
        Parameter("@Address", SqlDbType.NVarChar, Address),
        Parameter("@TaxFileNumber", SqlDbType.NVarChar, TaxFileNumber),
        Parameter("@RegistrationNo", SqlDbType.NVarChar, RegistrationNo),
        Parameter("@StartContract", SqlDbType.NVarChar, StartContract),
        Parameter("@EndContract", SqlDbType.NVarChar, EndContract),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Phone2", SqlDbType.NVarChar, Phone2),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Email", SqlDbType.NVarChar, Email),
        Parameter("@WebSite", SqlDbType.NVarChar, WebSite),
        Parameter("@Maximum", SqlDbType.NVarChar, Maximum),
        Parameter("@CreditLimit", SqlDbType.SmallInt, CreditLimit),
        Parameter("@UseVat", SqlDbType.Bit, UseVat),
        Parameter("@Status", SqlDbType.Bit, Status));
    }







    public DataTable NameSearch_Supplier(String SupplierName)
    {
        return ExecteRader("Select SupplierID,SupplierName  from Suppliers where SupplierName=@SupplierName", CommandType.Text,
        Parameter("@SupplierName", SqlDbType.NVarChar, SupplierName));
    }












    //Delete
    public void DeleteSuppliers(string SupplierID)
    {
        Execute_SQL("Delete  From Suppliers where SupplierID=@SupplierID", CommandType.Text,
        Parameter("@SupplierID", SqlDbType.Int, SupplierID));
    }

    //Details ID
    public DataTable NODeleteSuppliers(String SupplierID)
    {
        return ExecteRader("Select SupplierID  from Suppliers_trans Where SupplierID=@SupplierID", CommandType.Text,
        Parameter("@SupplierID", SqlDbType.Int, SupplierID));
    }




    //Details ID
    public DataTable Details_Suppliers(string SupplierID)
    {
        return ExecteRader("Select* from Suppliers where SupplierID=@SupplierID", CommandType.Text,
        Parameter("@SupplierID", SqlDbType.Int, SupplierID));
    }



    //Search
    public DataTable Search_Suppliers(String Search)
    {
        return ExecteRader("Select *  from Suppliers where convert(nvarchar,SupplierID)+SupplierName like '%'+ @Search + '%'  ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }





    //Update
    public void Update_Suppliers(string SupplierID, Double Income, Double Export)
    {
        Execute_SQL("Update Suppliers Set SupplierID=@SupplierID ,Income=@Income ,Export=@Export  where SupplierID=@SupplierID", CommandType.Text,
        Parameter("@SupplierID", SqlDbType.Int, SupplierID),
        Parameter("@Income", SqlDbType.Float, Income),
        Parameter("@Export", SqlDbType.Float, Export));
    }








}

