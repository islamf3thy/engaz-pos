﻿using System;
using System.Windows.Forms;
using ByStro.BL;
namespace ByStro.PL
{
    public partial class B_ClearAllData_Form : Form
    {
        public B_ClearAllData_Form()
        {
            InitializeComponent();
        }
        ClearData_cls cls = new ClearData_cls();
        struct DataParamter
        {
            //public int Process;
            //public int Detay;

        }
        //private DataParamter _inptParameter;  


        public Boolean LoadData = false;
       

        private void B_ClearAllData_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            lblMessage.Text = "";

        }

        private void btnNew_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("سوف يتم فقد كل الداتا الموجودة : هل متأكد من القيام بهذة العميلة", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }



            lblMessage.Text = "Please Wait ...";
            cls.ClearData();
            progressBar1.Value = 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            progressBar1.Value += 10;
            lblMessage.Text = "Successfully Completed ...";
            MessageBox.Show("سوف يتم تسجيل الخروج", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            LoadData = true;
            Close();
        }
    }
}
