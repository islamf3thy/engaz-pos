﻿namespace ByStro.PL
{
    partial class CustomersTrans_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomersTrans_Form));
            this.Label4 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.datefrom = new System.Windows.Forms.DateTimePicker();
            this.datetto = new System.Windows.Forms.DateTimePicker();
            this.Label2 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.txtAccountID = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtAccountName = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stlb1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txt_1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stlb2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txt_2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stlb3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txt_3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(78, 33);
            this.Label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(35, 17);
            this.Label4.TabIndex = 130;
            this.Label4.Text = "من :";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(291, 35);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(38, 17);
            this.Label1.TabIndex = 129;
            this.Label1.Text = "الي :";
            // 
            // datefrom
            // 
            this.datefrom.CustomFormat = "dd /MM /yyyy";
            this.datefrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datefrom.Location = new System.Drawing.Point(123, 31);
            this.datefrom.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.datefrom.Name = "datefrom";
            this.datefrom.Size = new System.Drawing.Size(161, 24);
            this.datefrom.TabIndex = 1;
            // 
            // datetto
            // 
            this.datetto.CustomFormat = "dd /MM /yyyy";
            this.datetto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetto.Location = new System.Drawing.Point(335, 31);
            this.datetto.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.datetto.Name = "datetto";
            this.datetto.Size = new System.Drawing.Size(171, 24);
            this.datetto.TabIndex = 2;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(28, 61);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label2.Size = new System.Drawing.Size(90, 17);
            this.Label2.TabIndex = 119;
            this.Label2.Text = "اسم  العميل :";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(939, 25);
            this.toolStrip1.TabIndex = 692;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.Image = global::ByStro.Properties.Resources.Search_20;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(58, 22);
            this.btnNew.Text = "عرض";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ByStro.Properties.Resources.Print_25;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(62, 22);
            this.toolStripButton1.Text = "طباعة";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // txtAccountID
            // 
            this.txtAccountID.Location = new System.Drawing.Point(546, 56);
            this.txtAccountID.Name = "txtAccountID";
            this.txtAccountID.Size = new System.Drawing.Size(10, 24);
            this.txtAccountID.TabIndex = 695;
            this.txtAccountID.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(511, 57);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 25);
            this.button3.TabIndex = 694;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtAccountName
            // 
            this.txtAccountName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtAccountName.Location = new System.Drawing.Point(123, 58);
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.ReadOnly = true;
            this.txtAccountName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAccountName.Size = new System.Drawing.Size(383, 24);
            this.txtAccountName.TabIndex = 693;
            this.txtAccountName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stlb1,
            this.txt_1,
            this.stlb2,
            this.txt_2,
            this.stlb3,
            this.txt_3});
            this.statusStrip1.Location = new System.Drawing.Point(0, 465);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(16, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(939, 22);
            this.statusStrip1.TabIndex = 717;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stlb1
            // 
            this.stlb1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stlb1.Name = "stlb1";
            this.stlb1.Size = new System.Drawing.Size(96, 17);
            this.stlb1.Text = "حركات العميل :";
            // 
            // txt_1
            // 
            this.txt_1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txt_1.Name = "txt_1";
            this.txt_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_1.Size = new System.Drawing.Size(17, 17);
            this.txt_1.Text = "0";
            // 
            // stlb2
            // 
            this.stlb2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stlb2.Name = "stlb2";
            this.stlb2.Size = new System.Drawing.Size(63, 17);
            this.stlb2.Text = "المسدد :";
            // 
            // txt_2
            // 
            this.txt_2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txt_2.Name = "txt_2";
            this.txt_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_2.Size = new System.Drawing.Size(17, 17);
            this.txt_2.Text = "0";
            // 
            // stlb3
            // 
            this.stlb3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stlb3.Name = "stlb3";
            this.stlb3.Size = new System.Drawing.Size(105, 17);
            this.stlb3.Text = "الرصيــد الحالي :";
            // 
            // txt_3
            // 
            this.txt_3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txt_3.Name = "txt_3";
            this.txt_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_3.Size = new System.Drawing.Size(17, 17);
            this.txt_3.Text = "0";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(0, 88);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(939, 374);
            this.gridControl1.TabIndex = 718;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            // 
            // CustomersTrans_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(939, 487);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.txtAccountID);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtAccountName);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.datefrom);
            this.Controls.Add(this.datetto);
            this.Controls.Add(this.Label2);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CustomersTrans_Form";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "كشف حساب العميل ";
            this.Load += new System.EventHandler(this.CustomersTrans_Form_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DateTimePicker datefrom;
        internal System.Windows.Forms.DateTimePicker datetto;
        internal System.Windows.Forms.Label Label2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.TextBox txtAccountID;
        public System.Windows.Forms.TextBox txtAccountName;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel stlb1;
        private System.Windows.Forms.ToolStripStatusLabel txt_1;
        private System.Windows.Forms.ToolStripStatusLabel stlb2;
        private System.Windows.Forms.ToolStripStatusLabel txt_2;
        private System.Windows.Forms.ToolStripStatusLabel stlb3;
        private System.Windows.Forms.ToolStripStatusLabel txt_3;

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}