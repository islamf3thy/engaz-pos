﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ByStro.DAL;
using ByStro.PL;
using ByStro.Properties;
using DevExpress.LookAndFeel;

namespace ByStro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool result;
            var mutex = new System.Threading.Mutex(true,"1241024445555",out result);
            if (!result)
            {
                return; 
            }
            //try
            //{
            string Lang = "ar-EG";
            DevExpress.XtraEditors.WindowsFormsSettings.RightToLeft = DevExpress.Utils.DefaultBoolean.True;
            CultureInfo cultureInfo = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(Lang); 
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);




            UserLookAndFeel.Default.SkinName = Settings.Default["ApplicationSkinName"].ToString();
         
            
            
            
            
            
            new Forms.SplashScreen1().ShowDialog();
            Application.Run(new LoginUser_Form());
           // Application.Run(new File.frmStart());

            // this is a commint 

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }

        #region DBUpdate
        public const int DBVersion = 3;
        public static void CheckForDBUpdate()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            if (db.DatabaseExists() == false) return;
            //try
            //{
            //    var x = db.Instalments.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\AddInstalment.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.BarcodeTemplates.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\CreatBarcodeTemplateTable.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.Drivers.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\DeliverySystem.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.DrawerInOuts.Count();
            //}
            //catch (Exception)
            //{
            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\DrawerInOut.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}
            //try
            //{
            //    var x = db.SystemSettings.Count();
            //}
            //catch (Exception)
            //{

            //    string cmd = System.IO.File.ReadAllText(Environment.CurrentDirectory + "\\DAL\\DB Update\\SystemSettings.sql");
            //    db.ExecuteCommand(cmd, new object[0]);
            //}

            try
            {
               
                var maxV = db.ST_Updates.Max(x =>(Int32?) x.DBVer)??0;
                if (maxV < DBVersion)
                {
                    var allFiles = System.IO.Directory.GetFiles(Environment.CurrentDirectory + "\\DAL\\DB Update", "Update*", System.IO.SearchOption.TopDirectoryOnly);
                    allFiles.ToList().ForEach(f =>
                    {
                        var FullName = System.IO.Path.GetFileName(f);
                        var num = Convert.ToInt32(FullName.Substring(6, FullName.Length - 10));
                        if (num > maxV)
                        {
                            string cmd = System.IO.File.ReadAllText(f);
                            db.ExecuteCommand(cmd, new object[0]);
                            db.ST_Updates.InsertOnSubmit(new ST_Update()
                            {
                                DBVer = num,
                                UpdateDate = DateTime.Now,
                            });
                            db.SubmitChanges();
                        }
                    });
                }

            }
            catch (Exception ex )
            {
                MessageBox.Show("UpdateError \n" + ex.Message);
            }
        }
        
        #endregion
    }
}
