﻿using ByStro.Clases;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_Report : XtraForm
    {
        public   enum ReportType
        {
            ProductMovment ,
            ProductBalance,
            ProductExpire,
            ProductReachedReorderLevel,

        }
        ReportType reportType;
        public  string FilterString;
        public frm_Report(ReportType _reportType  )
        {
            InitializeComponent();
            reportType = _reportType;
        }

        private void btn_Print_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RPT.rpt_GridReport.Print(this.gridControl1, this.Text, FilterString, true);
        }
    }

   
}
