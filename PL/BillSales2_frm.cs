﻿using ByStro.DAL; 
using DevExpress.XtraEditors;
using System;
using System.Data;
using System.Drawing; 
using System.Windows.Forms;
using System.Linq;


namespace ByStro.PL
{
    public partial class BillSales2_frm : Form
    {
        public BillSales2_frm()
        {
            InitializeComponent();
        }
        Sales_CLS cls = new Sales_CLS();
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        AVG_cls AVG_cls = new AVG_cls();
        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();

        string TransID = "0";
        string TreasuryID = "0";
        // Open New Bill
        public Boolean OpenNewBill = false;
        int UserID;
        // INDEX
        int PayType ; string StrPayType;

        DataTable dtShow; int pos = 0;
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            this.MaximizeBox = true;
            ERB_Setting.SettingDGV(DGV1);

            try
            {

        

            UserID = Properties.Settings.Default.UserID;
            FillCombAccount();
            FillProdecut();
            FillCurrency();
            FillPayType();
            btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }
        public void FillProdecut()
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);

        }
        public void FillCombAccount()
        {
            string cusName = combAccount.Text;
            string cusID = txtAccountID.Text;
            Customers_cls CUS = new Customers_cls();
            combAccount.DataSource = CUS.Search_Customers("");
            combAccount.DisplayMember = "CustomerName";
            combAccount.ValueMember = "CustomerID";
            combAccount.Text = cusName;
            txtAccountID.Text = cusID;

        }

        public void FillPayType()
        {
            string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
            combPayType.Text = cusName;

        }


     

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName.BackColor = Color.WhiteSmoke;
                    txtStoreID.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();
                    txtSearch.Text = "";
                    txtSearch.Focus();



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


















        private void DGV1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            lastQty();
        }

        double last_Qty = 0;
        void lastQty()
        {
            try
            {
                last_Qty = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }








        Prodecut_cls Prodecut_CLS = new Prodecut_cls();

        DataTable dt_Prodecut = new DataTable();
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                ItemsSearchBill_frm frm = new ItemsSearchBill_frm();
                frm.ShowDialog(this);
                if (frm.loaddata==true)
                {
                    txtSearch.Text = frm.DGV1.CurrentRow.Cells["ProdecutName"].Value.ToString();
             
                Search_KeyDown();
                return;
                }
            }


            if (e.KeyCode == Keys.Enter)
            {
                if (txtStoreID.Text == "")
                {
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    combUnit.Focus();
                    return;
                }

                if (txtSearch.Text.Trim() == "")
                {
                    cleartxt();
                    return;
                }

                Search_KeyDown();
                

                //SumitemdDGV();
            }

        }

        private void Search_KeyDown()
        {
            #region "تحميل الباركود واكود الصنف واسم الصنف في مكانة  السليم"

            //====================================================================================
            try
            {

                dt_Prodecut = Prodecut_CLS.SearchBill_Prodecuts(txtSearch.Text, txtStoreID.Text);

                String ProdecutName = txtSearch.Text;
                txtProdecutID.Text = dt_Prodecut.Rows[0]["ProdecutID"].ToString();
                txtSearch.Text = dt_Prodecut.Rows[0]["ProdecutName"].ToString();
                txtBalence.Text = dt_Prodecut.Rows[0]["Balence"].ToString();

                combUnit.Items.Clear();

                if (dt_Prodecut.Rows[0]["FiestUnit"].ToString() != "")
                {
                    combUnit.Items.Add(dt_Prodecut.Rows[0]["FiestUnit"].ToString());
                }
                if (dt_Prodecut.Rows[0]["SecoundUnit"].ToString() != "")
                {
                    combUnit.Items.Add(dt_Prodecut.Rows[0]["SecoundUnit"].ToString());
                }
                if (dt_Prodecut.Rows[0]["ThreeUnit"].ToString() != "")
                {
                    combUnit.Items.Add(dt_Prodecut.Rows[0]["ThreeUnit"].ToString());
                }
                txtQuantity.Text = "1";



                #region "ملأ الكومبوبوكس بالوحدات"

                if (dt_Prodecut.Rows[0]["FiestUnitBarcode"].ToString() == ProdecutName)
                {
                    combUnit.SelectedIndex = 0;

                }
                else if (dt_Prodecut.Rows[0]["SecoundUnitBarcode"].ToString() == ProdecutName)
                {
                    combUnit.SelectedIndex = 1;

                }
                else if (dt_Prodecut.Rows[0]["ThreeUnitBarcode"].ToString() == ProdecutName)
                {
                    combUnit.SelectedIndex = 2;
                }
                else
                {
                    if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "1")
                    {
                        combUnit.SelectedIndex = 0;

                    }
                    else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "2")
                    {
                        combUnit.SelectedIndex = 1;

                    }
                    else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "3")
                    {
                        combUnit.SelectedIndex = 2;
                    }
                    else
                    {
                        combUnit.SelectedIndex = 0;
                    }
                }

                combUnit.Focus();
                combUnit.Select();

                #endregion
                if (BillSetting_cls.UsingFastInput == true)
                {

                    txtSearch.Focus();
                }


            }
            catch
            {
                Mass.No_Fouind();
                txtSearch.Text = "";
                txtSearch.Focus();
            }


            //==================================================================================================================

            #endregion
        }


        private void combUnit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQuantity.Focus();
            }
        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPrice.Focus();
            }
        }

        private void txtPrise_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }





        #region" اضافة الي الداتا قرد فيو   "
        //string AVG_Price = "0";

        public void AddItem_DGV()
        {
            try
            {
                if (txtSearch.Text == "" || txtProdecutID.Text == "")
                {
                    txtSearch.BackColor = Color.Pink;
                    txtSearch.Focus();
                    return;
                }

                if (combCurrency.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combCurrency.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                {
                    MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCurrencyRate.Focus();
                    return;
                }

                if (combUnit.Text == "")
                {
                    combUnit.BackColor = Color.Pink;
                    combUnit.Focus();
                    return;
                }
                if (txtQuantity.Text == "" || Convert.ToDouble(txtQuantity.Text) <= Convert.ToDouble(0))
                {
                    txtQuantity.BackColor = Color.Pink;
                    txtQuantity.Focus();
                    return;
                }


                if (txtPrice.Text == "" || Convert.ToDouble(txtPrice.Text) <= Convert.ToDouble(0))
                {
                    txtPrice.BackColor = Color.Pink;
                    txtPrice.Focus();
                    return;
                }



                if (txtVAT.Text == "")
                {
                    txtVAT.BackColor = Color.Pink;
                    txtVAT.Focus();
                    return;
                }


                if (txtStoreID.Text == "")
                {
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    return;
                }



                //=================Search Prodect use in table or no===============================================================================================

                DataTable Dt_StoreSearch = Store_Prodecut_cls.Details_Store_Prodecut(txtProdecutID.Text, txtStoreID.Text);
                if (Dt_StoreSearch.Rows.Count == 0)
                {
                    MessageBox.Show("الصنف المدخل غير موجود في المخزن المحدد", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //================================================================================================================

                #region"الكمية غير متوفرة"

                if (DGV1.Rows.Count == 0)
                {
                    if (Convert.ToDouble(txtBalence.Text) < (Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtUnitFactor.Text)))
                    {
                        MessageBox.Show("الكمية غير متوفرة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtQuantity.BackColor = Color.Pink;
                        txtQuantity.Focus();
                        return;
                    }
                }

                else
                {
                    double X = 0;
                    double U = Convert.ToDouble(txtBalence.Text);
                    for (int i = 0; i < DGV1.Rows.Count; i++)
                    {
                        if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == txtStoreID.Text)
                        {
                            // الكمية الموجودة في المخزن
                            X += Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value);
                        }
                    }

                    X += Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtUnitFactor.Text);
                    if (X > U)
                    {
                        MessageBox.Show("الكمية غير متوفرة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtQuantity.Focus();
                        return;
                    }

                }
                #endregion

                #region "سعر البيع اقل من سعر التكلفة "
                Double BayPrice = Convert.ToDouble(txtProdecutAvg.Text);
                Double SalesPrice = Convert.ToDouble(txtPrice.Text)*double.Parse(txtCurrencyRate.Text);
                if (BayPrice > SalesPrice)
                {
                    if (MessageBox.Show("سعر البيـع اقل من سعر الشـراء بمقدار :  " + (SalesPrice - BayPrice).ToString() + Environment.NewLine + "هل تريد الاستمــرار ؟؟ ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        return;
                    }

                }
                else if (BayPrice == SalesPrice)
                {
                    if (MessageBox.Show("سعر البيع يساوي سعر الشراء : هل تريد البيع بسعر التكلفة", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        return;
                    }
                }

                #endregion

                //================================================================================================================

                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["Unit"].Value.ToString() == combUnit.Text && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == txtStoreID.Text)
                    {
                        if (BillSetting_cls.ShowMessageQty == true)
                        {
                          
                            Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                            DGV1.Rows[i].Cells["Quantity"].Value = +qty;
                            DGV1.Rows[i].Selected = true;


                            Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["CurrencyPrice"].Value);
                            Double MainVat_ = Convert.ToDouble(DGV1.Rows[i].Cells["MainVat"].Value.ToString());
                            Double total = x * MainVat_;
                            double vat_ = total / 100;

                            DGV1.Rows[i].Cells["CurrencyVat"].Value = (vat_).ToString();
                            DGV1.Rows[i].Cells["CurrencyTotal"].Value = x + vat_;
                            //

                            DGV1.Rows[i].Cells["Vat"].Value = (vat_ * double.Parse(DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString())).ToString();
                            DGV1.Rows[i].Cells["Price"].Value = (Convert.ToDouble(DGV1.Rows[i].Cells["CurrencyPrice"].Value) * double.Parse(DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString())).ToString();

                            Double Q = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);

                            DGV1.Rows[i].Cells["TotalPrice"].Value = double.Parse(DGV1.Rows[i].Cells["Vat"].Value.ToString()) + Q;


                            // الحصول علي اجمالي سعر التكلفة
                            DGV1.Rows[i].Cells["TotalBayPrice"].Value = (Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["BayPrice"].Value));


                            if (OpenNewBill == false)
                            {
                                if (btnSave.Enabled == true)
                                {
                                    cls.UpdateSales_Details(DGV1.Rows[i].Cells["ID"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["Vat"].Value.ToString(), DGV1.Rows[i].Cells["MainVat"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["TotalBayPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());
                                    DGV1.Rows[i].Selected = true;
                                }
                            }
                            SumDgv();
                            cleartxt();
                            return;

                        }
                        else
                        {
                            if (MessageBox.Show("هذ الصنف سبق وتم اضافتة ضمن الفاتورة  : هل تريد اضافة  " + txtQuantity.Text + "  الي الكمية ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                                DGV1.Rows[i].Cells["Quantity"].Value = +qty;
                                DGV1.Rows[i].Selected = true;


                                Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["CurrencyPrice"].Value);
                                Double MainVat_ = Convert.ToDouble(DGV1.Rows[i].Cells["MainVat"].Value.ToString());
                                Double total = x * MainVat_;
                                double vat_ = total / 100;

                                DGV1.Rows[i].Cells["CurrencyVat"].Value = (vat_).ToString();
                                DGV1.Rows[i].Cells["CurrencyTotal"].Value = x + vat_;
                                //

                                DGV1.Rows[i].Cells["Vat"].Value = (vat_ * double.Parse(DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString())).ToString();
                                DGV1.Rows[i].Cells["Price"].Value = (Convert.ToDouble(DGV1.Rows[i].Cells["CurrencyPrice"].Value) * double.Parse(DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString())).ToString();
                                Double Q = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);

                                DGV1.Rows[i].Cells["TotalPrice"].Value = double.Parse(DGV1.Rows[i].Cells["Vat"].Value.ToString()) + Q;



                                // الحصول علي اجمالي سعر التكلفة
                                DGV1.Rows[i].Cells["TotalBayPrice"].Value = (Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["BayPrice"].Value));

                                if (OpenNewBill == false)
                                {
                                    if (btnSave.Enabled == true)
                                    {
                                        cls.UpdateSales_Details(DGV1.Rows[i].Cells["ID"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["Vat"].Value.ToString(), DGV1.Rows[i].Cells["MainVat"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["TotalBayPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());
                                        DGV1.Rows[i].Selected = true;
                                    }
                                }
                                SumDgv();
                                cleartxt();
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }


                    }
                }

                String TotalBayPrice = (Convert.ToDouble(txtProdecutAvg.Text) * Convert.ToDouble(txtQuantity.Text)).ToString();

                if (OpenNewBill == false)
                {
                    if (btnSave.Enabled == true)
                    {
                        cls.InsertSales_Details("0", txtProdecutID.Text, combUnit.Text, txtUnitFactor.Text, txtUnitOperating.Text, txtQuantity.Text, "0", txtProdecutAvg.Text, TotalBayPrice,(double .Parse(txtPrice.Text)*double.Parse(txtCurrencyRate.Text)).ToString() ,(double.Parse(txtVAT.Text)*double.Parse(txtCurrencyRate.Text)).ToString(), txtMainVat.Text,(double.Parse(txtTotalPrice.Text)*double.Parse(txtCurrencyRate.Text)) .ToString(), "0", txtStoreID.Text, false,combCurrency.SelectedValue.ToString(),txtCurrencyRate.Text,txtPrice.Text,txtVAT.Text,txtTotalPrice.Text);
                        // Load Data
                        DGV1.AutoGenerateColumns = false;
                        DGV1.DataSource = cls.Details_Sales_DetailsRecived(false);
                        if (DGV1.Rows.Count >0)
                        {
                        DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;

                        }
                    }
                    else
                    {
                        AddRowDgv(txtProdecutID.Text, txtSearch.Text, combUnit.Text, txtUnitFactor.Text, txtUnitOperating.Text, txtQuantity.Text, "0", txtProdecutAvg.Text, TotalBayPrice, (double.Parse(txtPrice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), (double.Parse(txtVAT.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtMainVat.Text, (double.Parse(txtTotalPrice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStoreID.Text, txtStoreName.Text,combCurrency.SelectedValue.ToString(),txtCurrencyRate.Text,txtPrice.Text,txtVAT.Text,txtTotalPrice.Text,combCurrency.Text);
                    }
                }
                else
                {
                    AddRowDgv(txtProdecutID.Text, txtSearch.Text, combUnit.Text, txtUnitFactor.Text, txtUnitOperating.Text, txtQuantity.Text, "0", txtProdecutAvg.Text, TotalBayPrice, (double.Parse(txtPrice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), (double.Parse(txtVAT.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtMainVat.Text, (double.Parse(txtTotalPrice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStoreID.Text, txtStoreName.Text, combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, txtPrice.Text, txtVAT.Text, txtTotalPrice.Text, combCurrency.Text);

                }

                lblCount.Text = DGV1.Rows.Count.ToString();
                SumDgv();
                cleartxt();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        int nRow;

        private void SumDgv()
        {
            double sum = 0;
            double sum_Bay = 0;
            Double M_vat = 0;
            double CurrencyRate_ = 1;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                sum += Convert.ToDouble(DGV1.Rows[i].Cells["TotalPrice"].Value);
                sum_Bay += Convert.ToDouble(DGV1.Rows[i].Cells["TotalBayPrice"].Value);
                M_vat += Convert.ToDouble(DGV1.Rows[i].Cells["Vat"].Value);
            }


            if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim())&& txtCurrencyRate.Text!="0")
            {
                CurrencyRate_ = Convert.ToDouble(txtCurrencyRate.Text);
            }

            // nRow = DGV1.CurrentCell.RowIndex;
            txtTotalInvoice.Text = (sum).ToString();
            txtTotalBayPrice.Text = (sum_Bay).ToString();
            txtVatValue.Text = (M_vat).ToString();

            if (PayType == 0)
            {
                txtpaid_Invoice.Text = txtNetInvoice.Text;
            }
            else if (PayType == 1)
            {
                txtpaid_Invoice.Text = "0";
            }
            else
            {
                txtpaid_Invoice.Text = txtSumPayType.Text;
            }
            txtRestInvoise.Text = (double.Parse(txtNetInvoice.Text) - double.Parse(txtpaid_Invoice.Text)).ToString();

        }
        private void AddRowDgv(string ProdecutID, string ProdecutName, string Unit, string UnitFactor, string UnitOperating, string Quantity, string XQuantity, string BayPrice, string TotalBayPrice, string Price, string Vat, string MainVat, string TotalPrice, string StoreID, string StoreName, string CurrencyID, string CurrencyRate, string CurrencyPrice, String CurrencyVat, string CurrencyTotal,string CurrencyName)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
                DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["XQuantity"].Value = XQuantity;
                DGV1.Rows[lastRows].Cells["BayPrice"].Value = BayPrice;
                DGV1.Rows[lastRows].Cells["TotalBayPrice"].Value = TotalBayPrice;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["Vat"].Value = Vat;
                DGV1.Rows[lastRows].Cells["MainVat"].Value = MainVat;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;
                DGV1.Rows[lastRows].Cells["StoreID"].Value = StoreID;
                DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;

                DGV1.Rows[lastRows].Cells["CurrencyID"].Value = CurrencyID;
                DGV1.Rows[lastRows].Cells["CurrencyVat"].Value = CurrencyVat;
                DGV1.Rows[lastRows].Cells["CurrencyPrice"].Value = CurrencyPrice;
                DGV1.Rows[lastRows].Cells["CurrencyRate"].Value = CurrencyRate;
                DGV1.Rows[lastRows].Cells["CurrencyTotal"].Value = CurrencyTotal;
                DGV1.Rows[lastRows].Cells["CurrencyName"].Value = CurrencyName;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        private void cleartxt()
        {
            txtProdecutID.Text = "";
            txtSearch.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtTotalPrice.Text = "";
            combUnit.Items.Clear();
            txtVAT.Text = "";
            txtProdecutAvg.Text = "0";
            txtBalence.Text = "0";
            txtSearch.Focus();

        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            txtQuantity.BackColor = Color.White;
            SumTotal_txt();

        }


        private void SumTotal_txt()
        {
            try
            {



                if (txtQuantity.Text == "")
                {
                    txtVAT.Text = "0";
                    txtTotalPrice.Text = "0";
                    txtQuantity.Focus();
                    return;
                }

                if (txtPrice.Text == "")
                {
                    txtVAT.Text = "0";
                    txtTotalPrice.Text = "0";
                    txtPrice.Focus();
                    return;
                }

                if (txtVAT.Text == "")
                {
                    txtVAT.Text = "0";
                    txtTotalPrice.Text = "0";
                    txtVAT.Focus();
                    return;
                }

                double x = Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtPrice.Text);
                // decimal y = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtPrice.Text);

                // الكل مظروب في قيمة الخصم وناخد القيمة  ونقسمها علي  100

                // MainVat


                Double total = x * Convert.ToDouble(txtMainVat.Text);
                double vat_ = total / 100;
                txtVAT.Text = (vat_).ToString();




                //   x = x * Convert.ToDouble(txtVAT.Text) / 100;
                txtTotalPrice.Text = Convert.ToString(x + vat_);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }
        private void txtPrise_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveBill(false);
        }


        private void SaveBill(Boolean print)
        {

            try
            {


                if (lblCount.Text == "0")
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }

                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }
                }

                if (PayType == 0)
                {
                    if (DGVPayType.Rows.Count == 0)
                    {
                        AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtNetInvoice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtNetInvoice.Text);
                        ClearPayType();
                        SumPayType();
                    }
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }



                txtMainID.Text = cls.MaxID_Sales_Main();
                //===InsertCustomers_trans============================================================================================================================================================================================================================================================
                ////===Treasury Movement_============================================================================================================================================================================================================================================================

                double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
                if (PayType == 1)
                {
                    paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
                }
                    TreasuryID = TreasuryMovement_CLS.MaxID_TreasuryMovement();
                TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, "فاتورة مبيعات " + " : " + combAccount.Text, paidInvoice.ToString(), "0", this.Text + " : " + StrPayType, txtNetInvoice.Text, paidInvoice.ToString(), UserID);
              //  TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value,  combAccount.Text,txtCurrencyID.Text,txtCurrencyRate.Text, paidInvoice.ToString(),(paidInvoice*Convert.ToDouble(txtCurrencyRate.Text)).ToString(), "0","0", this.Text + " : " + combPayKind.Text, txtNetInvoice.Text, (Convert.ToDouble(txtNetInvoice.Text) * Convert.ToDouble(txtCurrencyRate.Text)).ToString(), paidInvoice.ToString(), (Convert.ToDouble(paidInvoice) * Convert.ToDouble(txtCurrencyRate.Text)).ToString(), UserID,true);

                //====Bay_Main===========================================================================================================================================================================================================================================================


                cls.InsertSales_Main(txtMainID.Text, D1.Value, txtAccountID.Text, txtNote.Text, StrPayType, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), paidInvoice, Convert.ToDouble(txtRestInvoise.Text), "0", txtTotalBayPrice.Text, "0", (Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtTotalBayPrice.Text)).ToString(), TransID, TreasuryID, double.Parse(txtMainVat.Text), double.Parse(txtVatValue.Text),"0", Properties.Settings.Default.UserID);


                //====InsertSales_Details===========================================================================================================================================================================================================================================================


                if (OpenNewBill == false)
                {
                    // dt_Prodecut.Clear();
                    for (int i = 0; i < DGV1.Rows.Count; i++)
                    {
                        cls.UpdateSales_Details(DGV1.Rows[i].Cells["ID"].Value.ToString(), txtMainID.Text, true);
                        AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), false);
                    }

                }
                else
                {
                    //dt_Prodecut.Clear();
                    for (int i = 0; i < DGV1.Rows.Count; i++)
                    {
                        //DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString()
                        cls.InsertSales_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), "0", DGV1.Rows[i].Cells["BayPrice"].Value.ToString(), DGV1.Rows[i].Cells["TotalBayPrice"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["Vat"].Value.ToString(), DGV1.Rows[i].Cells["MainVat"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), "0", DGV1.Rows[i].Cells["StoreID"].Value.ToString(), true, DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());
                        AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), false);
                    }

                }


                #region // Pay
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = "عميل" + " / " + combAccount.Text;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), 0, Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion









                if (print == true)
                {
                    //Task.Run(() => PrintBill());
                   
                        PrintBill();
                   
                }

                if (BillSetting_cls.ShowMessageSave == false)
                {
                    MessageSaved_frm frm = new MessageSaved_frm();
                    frm.ShowDialog();
                }
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                var invoice = db.Sales_Mains.SingleOrDefault(x => x.MainID == Convert.ToInt64(txtMainID.Text));
                if (invoice.RestInvoise > 0 && XtraMessageBox.Show(text: "هل تريد انشاء ملف قسط لهذه الفاتوره", caption: "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var frm = new Forms.frm_Instalment(invoice.CustomerID.Value, Convert.ToInt32(invoice.MainID));
                    frm.Show();
                }
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void PrintBill()
        {
        //   BillSales_frm.PrintBill(Convert.ToInt32(txtMainID.Text));
        }

        
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {


                DataTable dt_Rsales = cls.Details_Sales_Main(txtMainID.Text);
                if (DataAccessLayer.CS_12 == false)
                {
                    MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (dt_Rsales.Rows.Count == 0)
                {
                    MessageBox.Show("رقم الفاتورة  غير موجود يرجي التأكد من رقم الفاتورة المدخل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Convert.ToDouble(dt_Rsales.Rows[0]["ReturnInvoise"].ToString()) > 0)
                {
                    MessageBox.Show("لا يمكنك التعديل  بسبب تم  عمل مرتجع علي الفاتورة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (DGV1.Rows.Count == 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }


                if (PayType == 0)
                {
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }

                MessageEditShow_frm MessageEditShow_frm = new MessageEditShow_frm();
                MessageEditShow_frm.ShowDialog(this);
                if (MessageEditShow_frm.ISEdit==true)
                {
                    UpdateBill();
                    btnNew_Click(null, null);
                }





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void UpdateBill()
        {
            //===Treasury Movement_============================================================================================================================================================================================================================================================
       
            
            Double paid = Convert.ToDouble(txtNetInvoice.Text);
            if (PayType != 0)
            {
                paid = Convert.ToDouble(txtpaid_Invoice.Text);
            }
            TreasuryMovement_CLS.UpdateTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, "فاتورة مبيعات " + " : " + combAccount.Text, paid.ToString(), "0", this.Text + " : " + StrPayType, txtNetInvoice.Text, paid.ToString());
                
            //====Update Suppliers_trans========================================================================================================================================================================================================================================================

            double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
            if (PayType == 1)
            {
                paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
            }

            cls.UpdateSales_Main(txtMainID.Text, D1.Value, txtAccountID.Text, txtNote.Text, StrPayType, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), paidInvoice, Convert.ToDouble(txtRestInvoise.Text), txtTotalBayPrice.Text, (Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtTotalBayPrice.Text)).ToString(), TreasuryID, double.Parse(txtMainVat.Text), double.Parse(txtVatValue.Text), "0");
            //====Delete Quentity ========================================================================================================================================================================================================================================================
            DataTable dt = cls.Details_Sales_Details(txtMainID.Text);


            cls.Delete_Sales_Details(txtMainID.Text);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(), false);
            }

            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                cls.InsertSales_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), "0", DGV1.Rows[i].Cells["BayPrice"].Value.ToString(), DGV1.Rows[i].Cells["TotalBayPrice"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["Vat"].Value.ToString(), DGV1.Rows[i].Cells["MainVat"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), "0", DGV1.Rows[i].Cells["StoreID"].Value.ToString(), true, DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());
                AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), false);

            }



            #region // Pay
            PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);
            for (int i = 0; i < DGVPayType.Rows.Count; i++)
            {
                string Statement = "عميل" + " / " + combAccount.Text;
                if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                {
                    Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                }
                PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), 0, Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
            }
            #endregion

            if (BillSetting_cls.ShowMessageSave == false)
            {
                MessageEdit_frm frm = new MessageEdit_frm();
                frm.ShowDialog();
            }
        }
        //======================================================================


        //#endregion




        private void txtSum_TextChanged(object sender, EventArgs e)
        {

            //try
            //{

            //    if (txtSum.Text.Trim() != "")
            //    {
            //        txtArbic.Text = DAL.DataAccessLeayr.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtSum.Text), 2, "", "", true, true);

            //    }
            //    else
            //    {
            //        txtArbic.Text = "";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                cleartxt();
              //  return;
            }

            //DataTable dt = Prodecut_CLS.Search_Prodecuts(txtSearch.Text);
            //loadaccount_CLS.ItemsLoadtxt(txtSearch,dt);


        }


        private void combUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combUnit.SelectedIndex == 0)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["FiestUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["FiestUnitOperating"].ToString();
                // Sales Price for Unit
                double price=0 ;
                if (SalesLevel == 1)
                {
                    price = double.Parse(dt_Prodecut.Rows[0]["FiestUnitPrice1"].ToString());
                }
                else if (SalesLevel == 2)
                {
                    price = double.Parse(dt_Prodecut.Rows[0]["FiestUnitPrice2"].ToString());
                }
                else if (SalesLevel == 3)
                {
                    price = double.Parse(dt_Prodecut.Rows[0]["FiestUnitPrice3"].ToString());
                }

                if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                {
                    double Rate = double.Parse(txtCurrencyRate.Text.Trim());
                    txtPrice.Text = (price / Rate).ToString();
                }
                else
                {
                    txtPrice.Text = (price).ToString();
                }


            }
            else if (combUnit.SelectedIndex == 1)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["SecoundUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString();
                //  txtPrice.Text = (Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutBayPrice"].ToString()) * Convert.ToDouble(txtUnitFactor.Text)).ToString();
                // Sales Price for Unit
                double price = 0;
                //if (SalesLevel1.Checked == true)
                //{
                //    price = double.Parse(dt_Prodecut.Rows[0]["SecoundUnitPrice1"].ToString());
                //}
                //else if (SalesLevel2.Checked == true)
                //{
                //    price = double.Parse(dt_Prodecut.Rows[0]["SecoundUnitPrice2"].ToString());
                //}
                //else if (SalesLevel3.Checked == true)
                //{
                //    price = double.Parse(dt_Prodecut.Rows[0]["SecoundUnitPrice3"].ToString());
                //}


                if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                {
                    double Rate = double.Parse(txtCurrencyRate.Text.Trim());
                    txtPrice.Text = (price / Rate).ToString();
                }
                else
                {
                    txtPrice.Text = (price).ToString();
                }

            }
            else if (combUnit.SelectedIndex == 2)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["ThreeUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString();
                //txtPrice.Text = (Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutBayPrice"].ToString()) * Convert.ToDouble(txtUnitFactor.Text)).ToString();
                // Sales Price for Unit
                double price = 0;
                //if (SalesLevel1.Checked == true)
                //{
                //    price = double.Parse(dt_Prodecut.Rows[0]["ThreeUnitPrice1"].ToString());
                //}
                //else if (SalesLevel2.Checked == true)
                //{
                //    price = double.Parse(dt_Prodecut.Rows[0]["ThreeUnitPrice2"].ToString());
                //}
                //else if (SalesLevel3.Checked == true)
                //{
                //    price = double.Parse(dt_Prodecut.Rows[0]["ThreeUnitPrice3"].ToString());
                //}
                if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                {
                    double Rate = double.Parse(txtCurrencyRate.Text.Trim());
                    txtPrice.Text = (price / Rate).ToString();
                }
                else
                {
                    txtPrice.Text = (price).ToString();
                }
            }
            txtVAT.Text = dt_Prodecut.Rows[0]["DiscoundSale"].ToString();
            txtProdecutAvg.Text = (Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutAvg"].ToString()) * Convert.ToDouble(txtUnitFactor.Text)).ToString();
            // Change Unit
            changeUnit();
            if (BillSetting_cls.UsingFastInput == true)
            {
                AddItem_DGV();
                txtSearch.Focus();
            }
        }



        // change unit 
        private void changeUnit()
        {
            double U = Convert.ToDouble(txtBalence.Text);
            double Unit_DGV = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == txtStoreID.Text)
                {
                    // الكمية الموجودة في المخزن
                    Unit_DGV += Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value);
                }
            }
          //  textBox1.Text = ((Convert.ToDouble(txtBalence.Text) - Unit_DGV) / Convert.ToDouble(txtUnitFactor.Text)).ToString();

        }
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
              //  BillSales_Search_frm frm = new BillSales_Search_frm();
         //       frm.Text = this.Text;
         //       frm.ShowDialog(this);
         //       if (frm.LoadData == false)
         //       {
         //           return;
         //       }
         //
         //       GetDataSearch(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtVAT.Focus();
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            txtPrice.BackColor = Color.White;
            SumTotal_txt();

        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DGV1.Rows.Count > 0)
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }
        }





        private void button3_Click(object sender, EventArgs e)
        {
            Customers_frm cus = new Customers_frm();
            cus.ShowDialog();

        }


        int SalesLevel;
        private void BalenceCustomer(string CustomerID)
        {
            try
            {
               // txtCustomersBlanse.Text = "0";
                if (CustomerID.Trim() != "" || CustomerID != "System.Data.DataRowView")
                {
                    DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(txtAccountID.Text, "Cus");//Cus_Trans_cls.Cus_Balence(dt_cus.Rows[i]["CustomerID"].ToString());
                                                                                                    //  = Cus_Trans_cls.Cus_Balence(txtAccountID.Text);
                    //txtCustomersBlanse.Text = "0";
                    //if (dt.Rows[0]["balence"] == DBNull.Value == false)
                    //{
                    //    txtCustomersBlanse.Text = dt.Rows[0]["balence"].ToString();
                    //}
                    //=========================================================================================================
                    Customers_cls Customers_cls = new Customers_cls();
                    DataTable dt_cus = Customers_cls.Details_Customers(CustomerID);
                    txtPhone.Text = dt_cus.Rows[0]["Phone"].ToString();
                    if (dt_cus.Rows[0]["SalesLavel"].ToString() == "1")
                    {
                        SalesLevel = 1;
                    }
                    else if (dt_cus.Rows[0]["SalesLavel"].ToString() == "2")
                    {
                        SalesLevel = 2;
                    }
                    else if (dt_cus.Rows[0]["SalesLavel"].ToString() == "3")
                    {
                        SalesLevel = 3;
                    }

                }
            }
            catch
            {

            }

        }

      
        private void txtVAT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }





        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
                DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["Vat"].Value = Vat;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;
                DGV1.Rows[lastRows].Cells["StoreID"].Value = StoreID;
                DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void txtChange(object sender, EventArgs e)
        {
            SumTotalTXT();
        }

        private void txtpaid_Invoice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Double paid = Convert.ToDouble(txtSumPayType.Text);
                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) - paid).ToString();
            }
            catch
            {
                return;
            }
        }

      


        private Double QtyDGV1(String ProdecutID, String StoreID)
        {
            Double XQuantity = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == ProdecutID && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == StoreID)
                {
                    // الكمية الموجودة في الداتا جرد فيو
                    XQuantity += Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value);
                }
            }
            return XQuantity;
        }

        private void DGV1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Prodecut_cls Prodecut_CLS = new Prodecut_cls();
                DataTable dt_Quantity = Prodecut_CLS.Edit_Quentity_GDV_Bill_Prodecuts(DGV1.CurrentRow.Cells["ProdecutID"].Value.ToString(), DGV1.CurrentRow.Cells["StoreID"].Value.ToString());
                // الكميات محولة من الداتا جرد فيو
                Double Quantity_In_DGV1 = QtyDGV1(DGV1.CurrentRow.Cells["ProdecutID"].Value.ToString(), DGV1.CurrentRow.Cells["StoreID"].Value.ToString());
                //الرصيد المتاح في المخزن
                Double TotalQuantity = Convert.ToDouble(dt_Quantity.Rows[0]["Balence"].ToString());
                if (btnUpdate.Enabled == true)
                {
                    TotalQuantity = Convert.ToDouble(dt_Quantity.Rows[0]["Balence"].ToString()) + (Convert.ToDouble(DGV1.CurrentRow.Cells["XQuantity"].Value.ToString()) * Convert.ToDouble(DGV1.CurrentRow.Cells["UnitFactor"].Value));
                }


                //المقارنة بين الكمية الموجودة في المخزن والكميات المدخلة
                if (Quantity_In_DGV1 > TotalQuantity)
                {
                    MessageBox.Show(" تم تجاوز الكمية الموجودة بمقدار :   " + ((TotalQuantity - Quantity_In_DGV1) / Convert.ToDouble(DGV1.CurrentRow.Cells["UnitFactor"].Value)).ToString() + "", "الكمية الحالية غير متوفرة ", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    DGV1.CurrentRow.Cells["Quantity"].Value = last_Qty;
                    return;
                }

                //// ضرب الكمية المعدلة في السعر
                //Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                //Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());

                //Double total = x * MainVat_;
                //double vat_ = total / 100;

                //DGV1.CurrentRow.Cells["Vat"].Value = (vat_).ToString();
                //DGV1.CurrentRow.Cells["TotalPrice"].Value = x + vat_;


                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value);
                Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());
                Double total = x * MainVat_;
                double vat_ = total / 100;

                DGV1.CurrentRow.Cells["CurrencyVat"].Value = (vat_).ToString();
                DGV1.CurrentRow.Cells["CurrencyTotal"].Value = x + vat_;
                //

                DGV1.CurrentRow.Cells["Vat"].Value = (vat_ * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                DGV1.CurrentRow.Cells["Price"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value) * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();

                Double Q = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                DGV1.CurrentRow.Cells["TotalPrice"].Value = double.Parse(DGV1.CurrentRow.Cells["Vat"].Value.ToString()) + Q;
       


                // الحصول علي اجمالي سعر التكلفة
                DGV1.CurrentRow.Cells["TotalBayPrice"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["BayPrice"].Value));

                // الفاتوة المفتحوحة لو تساوي FALSE يدرج البيانات في قاعدة البيانات 
                if (OpenNewBill == false)
                {
                    if (btnSave.Enabled == true)
                    {
                        cls.UpdateSales_Details(DGV1.CurrentRow.Cells["ID"].Value.ToString(), DGV1.CurrentRow.Cells["Quantity"].Value.ToString(), DGV1.CurrentRow.Cells["Price"].Value.ToString(), DGV1.CurrentRow.Cells["Vat"].Value.ToString(), DGV1.CurrentRow.Cells["MainVat"].Value.ToString(), DGV1.CurrentRow.Cells["TotalPrice"].Value.ToString(), DGV1.CurrentRow.Cells["TotalBayPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyID"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyVat"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyTotal"].Value.ToString());
                    }
                }
                // الاجمالي
                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + "الحقل يجب ان يحتوي علي قيمة ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVAT_TextChanged(object sender, EventArgs e)
        {
            txtVAT.BackColor = Color.White;
            SumTotal_txt();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataAccessLayer.CS_13 == false)
                {
                    MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                MessageDelete_frm frm = new MessageDelete_frm();
                frm.ShowDialog(this);
                if (frm.ISDelete==true)
                {
                    //====Delete Quentity ========================================================================================================================================================================================================================================================
                    DataTable dt = cls.Details_Sales_Details(txtMainID.Text);


                    cls.Delete_Sales_Details(txtMainID.Text);
                    TreasuryMovement_CLS.DeleteTreasuryMovement(TreasuryID);
                    // Pay
                    PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);

                    cls.Delete_BillStoreMain(txtMainID.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(), false);
                    }


                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            // Use Fast Inpout

            //===============================================================================================================================================================
            txtMainID.Text = cls.MaxID_Sales_Main();
           
            txtNote.Text = "";
            txtProdecutID.Text = "";
            txtTotalInvoice.Text = "0";
            txtpaid_Invoice.Text = "0";
            txtRestInvoise.Text = "0";
            D1.Value = DateTime.Now;




            if (BillSetting_cls.UseCustomerDefult == true)
            {
                Customers_cls customers_cls = new Customers_cls();
                DataTable dt_customer = customers_cls.Details_Customers(BillSetting_cls.CustomerID);
                if (dt_customer.Rows.Count > 0)
                {
                    txtAccountID.Text = dt_customer.Rows[0]["CustomerID"].ToString();
                    combAccount.Text = dt_customer.Rows[0]["CustomerName"].ToString();
                }
            }
            else
            {
                txtAccountID.Text = "";
                combAccount.Text = "";
            }


            // Customer Defult

            if (BillSetting_cls.UseCrrencyDefault == true)
            {
                Currency_cls Currency = new Currency_cls();
                DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
            }
            else
            {
                combCurrency.Text = null;
                txtCurrencyRate.Text = "";

            }



            //=================================================================================

            // txtpaid_Invoice.Text = "0";
            TransID = "0";
            TreasuryID = "0";
            txtTotalDescound.Text = "0";
            // store defult ===================================================================================



            if (BillSetting_cls.UseStoreDefult == true)
            {
                Store_cls Store_cls = new Store_cls();
                DataTable dt_store = Store_cls.Details_Stores(int.Parse(BillSetting_cls.StoreID));
                txtStoreID.Text = dt_store.Rows[0]["StoreID"].ToString();
                txtStoreName.Text = dt_store.Rows[0]["StoreName"].ToString();
            }
            else
            {
                txtStoreID.Text = "";
                txtStoreName.Text = "";
            }





            if (BillSetting_cls.UseVat == true)
            {
                txtMainVat.Text = BillSetting_cls.Vat;
            }
            else
            {
                txtMainVat.Text = "0";
            }

            //===============================================================================================================================================================
            // Open New Bill
            if (OpenNewBill == false)
            {
                // Load Data
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Details_Sales_DetailsRecived(false);
                //DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                SumDgv();
            }
            else
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
            }
            lblCount.Text = DGV1.Rows.Count.ToString();
            //===============================================================================================================================================================
            btnSave.Enabled = true;
            btnSave_Print.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            ClearPayType();

            #region UsekindPay

            //===============================================================================================================================================================


            if (BillSetting_cls.UsekindPay == true)
            {
                PayType_cls PayType_cls = new PayType_cls();
                DataTable dtPayType = PayType_cls.Details_PayType(BillSetting_cls.PayTypeID);
                if (dtPayType.Rows.Count > 0)
                {
                    combPayType.Text = dtPayType.Rows[0]["PayTypeName"].ToString();
                }
            }
            else
            {
                combPayType.Text = null;
            }



            //===============================================================================================================================================================

            #endregion


            txtSumPayType.Text = "0";
            // txtRestInvoise.Text = "0";
            DGVPayType.DataSource = null;
            DGVPayType.Rows.Clear();
            cleartxt();

            //===================================================================================================================================================================
            DataTable dtData = new DataTable();
            if (DataAccessLayer.TypeUser == true)
            {
                dtData = cls.Search_Sales_Main();
            }
            else
            {
                dtData = cls.Search_Sales_Main(Properties.Settings.Default.UserID);
            }

            dtShow = dtData;
            pos = dtShow.Rows.Count;
            ShowData(pos);

            PayType = BillSetting_cls.kindPay;
            if (PayType == 0)
            {
                btnPayCash_Click(null, null);
            }
            else 
            {
                btnPayAgel_Click(null, null);

            }
        


        }


        public void ShowData(int index)
        {
            try
            {
                GetDataSearch(dtShow.Rows[index]["MainID"].ToString());
            }
            catch
            {
                return;
            }
        }

    private void  GetDataSearch(string MainID)
        {
            try
            {
         
                btnSave.Enabled = false;
                btnSave_Print.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
                DataTable dt = cls.Details_Sales_Main(MainID);
                DataRow Dr = dt.Rows[0];
                txtMainID.Text = Dr["MainID"].ToString();
                D1.Value = Convert.ToDateTime(Dr["MyDate"]);
                txtAccountID.Text = Dr["CustomerID"].ToString();
                combAccount.Text = Dr["CustomerName"].ToString();
                txtPhone.Text = Dr["Phone"].ToString();
                txtNote.Text = Dr["Note"].ToString();

                StrPayType = Dr["TypeKind"].ToString();
                // kkkkkkkkkkkkkkkkkkkkkkkkkkkkk




                txtMainVat.Text = Dr["Vat"].ToString();
                txtTotalInvoice.Text = Dr["TotalInvoice"].ToString();
                txtTotalDescound.Text = Dr["Discount"].ToString();
                txtNetInvoice.Text = Dr["NetInvoice"].ToString();
                txtpaid_Invoice.Text = Dr["PaidInvoice"].ToString();
                txtRestInvoise.Text = Dr["RestInvoise"].ToString();

                TransID = Dr["TransID"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();


                // txtVatValue .Text= Dr["VatValue"].ToString();

                //=========================================================================================================================================================================
                dt.Clear();
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                //=========================================================================================================================================================================
                dt = cls.Details_Sales_Details(MainID);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["BayPrice"].ToString(), dt.Rows[i]["TotalBayPrice"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["MainVat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString(), dt.Rows[i]["CurrencyID"].ToString(), dt.Rows[i]["CurrencyRate"].ToString(), dt.Rows[i]["CurrencyPrice"].ToString(), dt.Rows[i]["CurrencyVat"].ToString(), dt.Rows[i]["CurrencyTotal"].ToString(), dt.Rows[i]["CurrencyName"].ToString());
                }


                // PyeType
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(MainID, txtBillTypeID.Text);
                for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                {
                    AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Debit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                }
                SumPayType();



                lblCount.Text = DGV1.Rows.Count.ToString();
                SumDgv();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                button21_Click(null, null);
            }
        }

        private void BillSales_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (btnSave_Print.Enabled == true)
                    {
                        btnSave_Print_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F10)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }


                else if (e.KeyCode == Keys.F5)
                {
                    button5_Click(null, null);
                }
               

                if (e.KeyCode == Keys.F4)
                {
                    if ( tabControl1.SelectedIndex==0)
                    {
                         tabControl1.SelectedIndex = 1;
                    txtPayValue.Focus();
                    }
                    else
                    {
                        tabControl1.SelectedIndex = 0;
                        txtSearch.Focus();
                    }
                   
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }



        private void اعادةتحميلالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);
        }

        private void اضافةفاتورةفوريةToolStripMenuItem_Click(object sender, EventArgs e)
        {
       //     BillSales_frm frm = new BillSales_frm();
            //frm.BackColor = Color.Tomato;
            //frm.Show();
        }

        private void CustomerTrsnse_Click(object sender, EventArgs e)
        {
            try
            {
                if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }

                CustomersTrans_Form frm = new CustomersTrans_Form();
                frm.txtAccountID.Text = txtAccountID.Text;
                frm.txtAccountName.Text = combAccount.Text;
                frm.AddEdit = "Bay";
                frm.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        private void CS_44_Click(object sender, EventArgs e)
        {

            MessageBox.Show((Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtTotalBayPrice.Text)).ToString(), "", MessageBoxButtons.OK);
        }

        private void combAccount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                txtAccountID.Text = "";
                combAccount.Text = "";
            }
        }

        private void txtStoreName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                txtStoreID.Text = "";
                txtStoreName.Text = "";
            }
        }

        private void combAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combAccount.BackColor = Color.White;
                txtAccountID.Text = combAccount.SelectedValue.ToString();

            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void combAccount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combAccount.BackColor = Color.White;
                if (combAccount.Text.Trim() == "")
                {
                    txtAccountID.Text = "";
                }
                else
                {
                    txtAccountID.Text = combAccount.SelectedValue.ToString();
                }
            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void txtAccountID_TextChanged(object sender, EventArgs e)
        {
            BalenceCustomer(txtAccountID.Text);
        }


        private void btnSave_Print_Click(object sender, EventArgs e)
        {
            SaveBill(true);
        }

       

        private void AddDgv_QuentityStore(DataGridView dgv, string ProdecutID, string StoreName, string Balence, string ChangeAllUnit)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                dgv.Rows[lastRows].Cells["StoreName"].Value = StoreName;
                dgv.Rows[lastRows].Cells["Balence"].Value = Balence;
                dgv.Rows[lastRows].Cells["Change_Unit"].Value = ChangeAllUnit;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private string ChangeUnit(string Quentity, string FiestUnit, string FiestUnitFactor, string SecoundUnit, string SecoundUnitOperating, string SecoundUnitFactor, string ThreeUnit, string ThreeUnitOperating, string ThreeUnitFactor)
        {
            if (Quentity == "" || Quentity == "0")
            {
                return "";
            }
            string Resalet = "";
            if (ThreeUnitOperating != "")
            {
                decimal n = Convert.ToDecimal(Quentity) / Convert.ToDecimal(ThreeUnitOperating);
                decimal H = Math.Floor(n);
                decimal b = Convert.ToDecimal(H) * Convert.ToDecimal(ThreeUnitOperating);
                decimal namber3 = Convert.ToDecimal(Quentity) - Convert.ToDecimal(b);
                decimal F = Convert.ToDecimal(H) / Convert.ToDecimal(SecoundUnitOperating);
                decimal namber1 = Math.Floor(F);
                decimal Fd = Convert.ToDecimal(namber1) * Convert.ToDecimal(SecoundUnitOperating);
                decimal namer2 = Convert.ToDecimal(H) - Convert.ToDecimal(Fd);
                Resalet = namber1 + "  " + FiestUnit + "  // " + namer2 + "  " + SecoundUnit + "  // " + namber3 + " " + ThreeUnit;
                //======================================================================================================================================================================================================================================================

            }

            else if (SecoundUnitOperating != "")
            {

                var y = Convert.ToDouble(Quentity) / Convert.ToDouble(SecoundUnitOperating);
                var Hnamber1 = Math.Floor(y);
                var dFd = Convert.ToDouble(Hnamber1) * Convert.ToDouble(SecoundUnitOperating);
                var hnamer2 = Convert.ToDouble(Quentity) - Convert.ToDouble(dFd);
                Resalet = Hnamber1 + " " + FiestUnit + "  // " + hnamer2 + " " + SecoundUnit;
                //======================================================================================================================================================================================================================================================

            }
            else if (FiestUnit != "")
            {
                Resalet = Quentity + "  " + FiestUnit;
                //======================================================================================================================================================================================================================================================
            }

            return Resalet;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayType.SelectedIndex >= 0)
                {
                    txtPayValue.Focus();
                }

            }
        }

        private void txtValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtPayValue.Text.Trim()))
                {
                    txtStatement.Focus();
                }

            }
        }

        private void ClearPayType()
        {
            combPayType.Text = null;
            txtPayValue.Text = "";
            txtStatement.Text = "";
            combPayType.Focus();
        }

        private void SumPayType()
        {
            try
            {

                double Sum = 0;
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    Sum += Convert.ToDouble(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString());
                }
                txtSumPayType.Text = Sum.ToString();
                txtpaid_Invoice.Text = txtSumPayType.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {


                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }

                    if (combPayType.SelectedIndex < 0)
                    {
                        combPayType.BackColor = Color.Pink;
                        combPayType.Focus();
                        return;
                    }
                    if (txtPayValue.Text.Trim() == "")
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    if (double.Parse(txtPayValue.Text) == 0)
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    for (int i = 0; i < DGVPayType.Rows.Count; i++)
                    {
                        if (DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString() == combPayType.SelectedValue.ToString())
                        {
                            combPayType.BackColor = Color.Pink;
                            combPayType.Focus();
                            return;
                        }
                    }
                    AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text,(double.Parse( txtPayValue.Text)* double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text,combCurrency.SelectedValue.ToString(),combCurrency.Text,txtCurrencyRate.Text,txtPayValue.Text);
                    ClearPayType();
                    SumPayType();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void DGVPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
       
 
      
        }

      

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;

        }

        private void txtPayValue_TextChanged(object sender, EventArgs e)
        {
            txtPayValue.BackColor = Color.White;
        }

        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            PrintBill();
        }





        private void SumTotalTXT()
        {
            try
            {

                Double total = Convert.ToDouble(txtTotalInvoice.Text);
                Double Discount = 0;
                if (!string.IsNullOrEmpty(txtTotalDescound.Text))
                {
                    Discount = Convert.ToDouble(txtTotalDescound.Text);
                }
                txtNetInvoice.Text = (Convert.ToDouble(txtTotalInvoice.Text) - Discount).ToString();
                Double paid = Convert.ToDouble(txtSumPayType.Text);
                txtpaid_Invoice.Text = paid.ToString();
                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) - paid).ToString();

                if (btnSave.Enabled)
                {
                    if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        txtPayValue.Text = (double.Parse(txtNetInvoice.Text) / double.Parse(txtCurrencyRate.Text)).ToString();

                    }
                    else
                    {
                        txtPayValue.Text = txtNetInvoice.Text;
                    }
                }


            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message.ToString());
                // return;
            }

        }

        private void combAccount_KeyDown_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (string.IsNullOrEmpty(combAccount.Text.Trim()))
                    {
                        return;
                    }
                    Customers_cls cls = new Customers_cls();
                    DataTable dt = cls.Search_Customers(combAccount.Text);
                    if (dt.Rows.Count > 0)
                    {
                        combAccount.Text = dt.Rows[0]["CustomerName"].ToString();
                    }
                    else
                    {

                        Customers_frm cus = new Customers_frm();
                        cus.txtPhone.Text = combAccount.Text; //txtCustomerName.Text = "";
                        cus.ClearData = false;
                        cus.ShowDialog(this);
                        if (cus.SaveData == true)
                        {
                            combAccount.Text = cus.txtCustomerName.Text;
                        }

                    }
                }
            }
            catch
            {


            }
        }

     
        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void combCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }
        }

        private void txtNetInvoice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV1.SelectedRows.Count == 0)
                {
                    return;
                }

                MessageDelete_frm frm = new MessageDelete_frm();
                frm.ShowDialog(this);


                if (frm.ISDelete == true)
                {
                    foreach (DataGridViewRow r in DGV1.SelectedRows)
                    {
                        if (OpenNewBill == false)
                        {
                            if (btnSave.Enabled == true)
                            {
                                cls.DeleteSales_Details_UseID(DGV1.CurrentRow.Cells["ID"].Value.ToString());
                                // Load Data
                                DGV1.AutoGenerateColumns = false;
                                DGV1.DataSource = cls.Details_Sales_DetailsRecived(false);
                            }
                            else
                            {
                                DGV1.Rows.Remove(r);
                            }
                        }
                        else
                        {
                            DGV1.Rows.Remove(r);
                        }

                    }
                    lblCount.Text = DGV1.Rows.Count.ToString();
                    SumDgv();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count==0)
                {
                    return;
                }
                Double qty = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) + 1;
                DGV1.CurrentRow.Cells["Quantity"].Value = +qty;
            


                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value);
                Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());
                Double total = x * MainVat_;
                double vat_ = total / 100;

                DGV1.CurrentRow.Cells["CurrencyVat"].Value = (vat_).ToString();
                DGV1.CurrentRow.Cells["CurrencyTotal"].Value = x + vat_;
                //   CurrentRow
                 
                DGV1.CurrentRow.Cells["Vat"].Value = (vat_ * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                DGV1.CurrentRow.Cells["Price"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value) * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                Double Q = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                DGV1.CurrentRow.Cells["TotalPrice"].Value = double.Parse(DGV1.CurrentRow.Cells["Vat"].Value.ToString()) + Q;



                // الحصول علي اجمالي سعر التكلفة
                DGV1.CurrentRow.Cells["TotalBayPrice"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["BayPrice"].Value));

                if (OpenNewBill == false)
                {
                    if (btnSave.Enabled == true)
                    {
                        cls.UpdateSales_Details(DGV1.CurrentRow.Cells["ID"].Value.ToString(), DGV1.CurrentRow.Cells["Quantity"].Value.ToString(), DGV1.CurrentRow.Cells["Price"].Value.ToString(), DGV1.CurrentRow.Cells["Vat"].Value.ToString(), DGV1.CurrentRow.Cells["MainVat"].Value.ToString(), DGV1.CurrentRow.Cells["TotalPrice"].Value.ToString(), DGV1.CurrentRow.Cells["TotalBayPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyID"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyVat"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyTotal"].Value.ToString());
                        DGV1.CurrentRow.Selected = true;
                    }
                }
                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
               
                Double qty = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) -1;
                if (qty <=0)
                {
                    return;
                }
                DGV1.CurrentRow.Cells["Quantity"].Value = +qty;



                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value);
                Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());
                Double total = x * MainVat_;
                double vat_ = total / 100;

                DGV1.CurrentRow.Cells["CurrencyVat"].Value = (vat_).ToString();
                DGV1.CurrentRow.Cells["CurrencyTotal"].Value = x + vat_;
                //   CurrentRow

                DGV1.CurrentRow.Cells["Vat"].Value = (vat_ * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                DGV1.CurrentRow.Cells["Price"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value) * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                Double Q = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                DGV1.CurrentRow.Cells["TotalPrice"].Value = double.Parse(DGV1.CurrentRow.Cells["Vat"].Value.ToString()) + Q;



                // الحصول علي اجمالي سعر التكلفة
                DGV1.CurrentRow.Cells["TotalBayPrice"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["BayPrice"].Value));

                if (OpenNewBill == false)
                {
                    if (btnSave.Enabled == true)
                    {
                        cls.UpdateSales_Details(DGV1.CurrentRow.Cells["ID"].Value.ToString(), DGV1.CurrentRow.Cells["Quantity"].Value.ToString(), DGV1.CurrentRow.Cells["Price"].Value.ToString(), DGV1.CurrentRow.Cells["Vat"].Value.ToString(), DGV1.CurrentRow.Cells["MainVat"].Value.ToString(), DGV1.CurrentRow.Cells["TotalPrice"].Value.ToString(), DGV1.CurrentRow.Cells["TotalBayPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyID"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyVat"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyTotal"].Value.ToString());
                        DGV1.CurrentRow.Selected = true;
                    }
                }
                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            try
            {
                if (nRow < DGV1.RowCount)
                {
                    DGV1.Rows[nRow].Selected = false;
                    DGV1.Rows[++nRow].Selected = true;
                }
            }
            catch 
            {
                DGV1.Rows[0].Selected = false;

            }
           
        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                if (nRow > DGV1.RowCount)
                {
                    DGV1.Rows[nRow].Selected = false;
                    DGV1.Rows[--nRow].Selected = true;
                }
            }
            catch
            {
                DGV1.Rows[DGV1.RowCount].Selected = false;

            }
           
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + button12.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q +".";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            try
            {
              
                txtQ.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "1";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "2";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "3";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "4";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "5";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "6";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "7";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "8";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q + "9";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnx_Click(object sender, EventArgs e)
        {
            try
            {
                String Q = txtQ.Text;
                txtQ.Text = Q.Substring(0, txtQ.Text.Length-1);

            }
            catch// (Exception ex)
            {
               // MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                if (txtQ.Text.Trim() == "")
                {
                    return;
                }
                Double qty = Convert.ToDouble(txtQ.Text) ;
                DGV1.CurrentRow.Cells["Quantity"].Value = +qty;



                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value);
                Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());
                Double total = x * MainVat_;
                double vat_ = total / 100;

                DGV1.CurrentRow.Cells["CurrencyVat"].Value = (vat_).ToString();
                DGV1.CurrentRow.Cells["CurrencyTotal"].Value = x + vat_;
                //   CurrentRow

                DGV1.CurrentRow.Cells["Vat"].Value = (vat_ * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                DGV1.CurrentRow.Cells["Price"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value) * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                Double Q = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                DGV1.CurrentRow.Cells["TotalPrice"].Value = double.Parse(DGV1.CurrentRow.Cells["Vat"].Value.ToString()) + Q;



                // الحصول علي اجمالي سعر التكلفة
                DGV1.CurrentRow.Cells["TotalBayPrice"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["BayPrice"].Value));

                if (OpenNewBill == false)
                {
                    if (btnSave.Enabled == true)
                    {
                        cls.UpdateSales_Details(DGV1.CurrentRow.Cells["ID"].Value.ToString(), DGV1.CurrentRow.Cells["Quantity"].Value.ToString(), DGV1.CurrentRow.Cells["Price"].Value.ToString(), DGV1.CurrentRow.Cells["Vat"].Value.ToString(), DGV1.CurrentRow.Cells["MainVat"].Value.ToString(), DGV1.CurrentRow.Cells["TotalPrice"].Value.ToString(), DGV1.CurrentRow.Cells["TotalBayPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyID"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyPrice"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyVat"].Value.ToString(), DGV1.CurrentRow.Cells["CurrencyTotal"].Value.ToString());
                        DGV1.CurrentRow.Selected = true;
                    }
                }
                SumDgv();
                txtQ.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVatValue_Click(object sender, EventArgs e)
        {
           
        }

        private void txtTotalDescound_Click(object sender, EventArgs e)
        {
            Number_frm frm = new Number_frm();
            frm.ShowDialog(this);
            if (frm.ISOK == true)
            {
                txtTotalDescound.Text = frm.txtQ.Text;
            }

        }

        private void txtPayValue_Click(object sender, EventArgs e)
        {
            Number_frm frm = new Number_frm();
            frm.ShowDialog(this);
            if (frm.ISOK == true)
            {
                txtPayValue.Text = frm.txtQ.Text;
            }

        }



        private void PAY()
        {
            btnPayCash.BackColor = Color.Yellow;
            btnPayAgel.BackColor = Color.Yellow;
        }

        private void btnPayAgel_Click(object sender, EventArgs e)
        {
            PAY();
            btnPayAgel.BackColor = Color.LightSalmon;
            //tabControl1.TabPages.Remove(tabPage2);
            txtpaid_Invoice.Text = "0";
            PayType = 1;
            StrPayType = "اجل";
        }

        private void btnPayCash_Click(object sender, EventArgs e)
        {
            PAY();
            btnPayCash.BackColor = Color.LightSalmon;
           // tabControl1.TabPages.Remove(tabPage2);
            PayType = 0;
            txtpaid_Invoice.Text = txtNetInvoice.Text;
            StrPayType = "نقدي";
        }



        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                pos++;
                if (pos < dtShow.Rows.Count)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اكبر سند  ");
                    pos = dtShow.Rows.Count;
                    //ShowData(pos);
                }
            }
            catch
            {
                return;
            }




        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                pos--;
                if (pos >= 0)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اضغر سند ");

                }
            }
            catch
            {
                return;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.SelectedRows.Count == 0)
                {
                    return;
                }

                MessageDelete_frm frm = new MessageDelete_frm();
                frm.ShowDialog(this);


                if (frm.ISDelete == true)
                {

                            foreach (DataGridViewRow r in DGVPayType.SelectedRows)
                            {

                                DGVPayType.Rows.Remove(r);

                            }
                            SumPayType();
                        

                    

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString()) + 1;
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString()) - 1;
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
#endregion