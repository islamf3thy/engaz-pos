﻿using ByStro.PL;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Employee_Deduction_frm : Form
    {
        public Employee_Deduction_frm()
        {
            InitializeComponent();
        }
        Employee_Deduction_cls cls = new Employee_Deduction_cls();
        //TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();

        private void Discound_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingForm(this);
                btnNew_Click(null, null);

                LoadData();
                combEmployee.Text = "";
                txtEmployeeID.Text = "";
                combEmployee.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }



        public void LoadData()
        {

            Employee_cls Employee_cls = new Employee_cls();
            string EmployeeName = combEmployee.Text;
            string EmployeeID = txtEmployeeID.Text;
            combEmployee.DataSource = Employee_cls.Search_Employees("");
            combEmployee.DisplayMember = "EmployeeName";
            combEmployee.ValueMember = "EmployeeID";
            combEmployee.Text = EmployeeName;
            txtEmployeeID.Text = EmployeeID;
            //======================================================================================
            string DeductionType_Name = combDeductionType.Text;
            string DeductionType_ID = txtDeductionType_ID.Text;
            Employee_DeductionType_cls Employee_DeductionType_cls = new Employee_DeductionType_cls();
            combDeductionType.DataSource = Employee_DeductionType_cls.Search__Employee_DeductionType("");
            combDeductionType.DisplayMember = "DeductionType_Name";
            combDeductionType.ValueMember = "DeductionType_ID";
               combDeductionType.Text= DeductionType_Name;
               txtDeductionType_ID.Text= DeductionType_ID;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

            
                //======================================================================================

                combDeductionType.Text = "";
                txtDeductionType_ID.Text = "";
                //======================================================================================
                txtDeduction_ID.Text = cls.MaxID_Employee_Deduction();
                D1.Value = DateTime.Now;
                txtDeduction_Value.Text = "";
                txtRemarks.Text = "";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                combDeductionType.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtEmployeeID.Text.Trim() == "")
                {
                    combEmployee.BackColor = Color.Pink;
                    combEmployee.Focus();
                    return;
                }

                // condetion
                else if (txtDeductionType_ID.Text.Trim() == "")
                {
                    combDeductionType.BackColor = Color.Pink;
                    combDeductionType.Focus();
                    return;
                }
                // condetion
                else if (txtDeduction_Value.Text.Trim() == "")
                {
                    txtDeduction_Value.BackColor = Color.Pink;
                    txtDeduction_Value.Focus();
                    return;
                }
                else if (Convert.ToDouble(txtDeduction_Value.Text.Trim()) == 0)
                {
                    txtDeduction_Value.BackColor = Color.Pink;
                    txtDeduction_Value.Focus();
                    return;
                }
                txtDeduction_ID.Text = cls.MaxID_Employee_Deduction();
               // txtTreasuryID.Text = cls_Treasury_Movement.MaxID_TreasuryMovement();
                cls.Insert_Employee_Deduction(txtDeduction_ID.Text, D1.Value, txtEmployeeID.Text, txtDeductionType_ID.Text, txtDeduction_Value.Text , txtTreasuryID.Text, txtRemarks.Text, Properties.Settings.Default.UserID);
               // cls_Treasury_Movement.InsertTreasuryMovement(txtTreasuryID.Text,txtEmployeeID.Text,"Emp", txtDeduction_ID.Text, txtTypeID.Text, combDeductionType.Text, D1.Value,combEmployee.Text, "0", txtDeduction_Value.Text,txtRemarks.Text,"0","0", Properties.Settings.Default.UserID);

                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtEmployeeID.Text.Trim() == "")
                {
                    combEmployee.BackColor = Color.Pink;
                    combEmployee.Focus();
                    return;
                }
                // condetion
               else if (txtDeduction_ID.Text.Trim() == "")
                {
                    combDeductionType.BackColor = Color.Pink;
                    combDeductionType.Focus();
                    return;
                }
                
                // condetion
                else if (txtDeduction_Value.Text.Trim() == "")
                {
                    txtDeduction_Value.BackColor = Color.Pink;
                    txtDeduction_Value.Focus();
                    return;
                }
                // condetion
                else if (txtDeduction_ID.Text.Trim() == "")
                {
                    combDeductionType.BackColor = Color.Pink;
                    combDeductionType.Focus();
                    return;
                }
                else if (Convert.ToDouble(txtDeduction_Value.Text.Trim()) == 0)
                {
                    MessageBox.Show("يرجي ادخال قيمة مناسبة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtDeduction_Value.BackColor = Color.Pink;
                    txtDeduction_Value.Focus();
                    return;
                }
                cls.Update_Employee_Deduction(txtDeduction_ID.Text, D1.Value, txtEmployeeID.Text, txtDeductionType_ID.Text, txtDeduction_Value.Text, txtTreasuryID.Text, txtRemarks.Text);

                //cls_Treasury_Movement.UpdateTreasuryMovement(txtTreasuryID.Text, "", "NO", txtDeduction_ID.Text, txtTypeID.Text, combDeductionType.Text, D1.Value, combEmployee.Text, "0", txtDeduction_Value.Text, txtRemarks.Text, "0", "0");


                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_Employee_Deduction(txtDeduction_ID.Text);
                    //cls_Treasury_Movement.DeleteTreasuryMovement(txtTreasuryID.Text);
                    btnNew_Click(null, null);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {


            try
            {
                Employee_Deduction_Search_frm frm = new Employee_Deduction_Search_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_Employee_Deduction(frm.DGV1.CurrentRow.Cells["Deduction_ID"].Value.ToString());
                    txtDeduction_ID.Text = Dt.Rows[0]["Deduction_ID"].ToString();
                    txtEmployeeID.Text = Dt.Rows[0]["EmployeeID"].ToString();
                    combEmployee.Text = Dt.Rows[0]["EmployeeName"].ToString();
                    D1.Value = Convert.ToDateTime(Dt.Rows[0]["MyDate"]);
                    txtTreasuryID.Text =Dt.Rows[0]["TreasuryID"].ToString();
                    txtDeduction_Value.Text = Dt.Rows[0]["Deduction_Value"].ToString();
                    txtDeductionType_ID.Text = Dt.Rows[0]["DeductionType_ID"].ToString();
                    combDeductionType.Text = Dt.Rows[0]["DeductionType_Name"].ToString();
                    
                    txtRemarks.Text = Dt.Rows[0]["Remarks"].ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                txtEmployeeID.Text = "";
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtDeduction_Value.BackColor = Color.White;

                if (txtDeduction_Value.Text == "0" || txtDeduction_Value.Text == "")
                {
                    txtArbic.Text = "";
                }
                else
                {
                    txtArbic.Text = DataAccessLayer.horof.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtDeduction_Value.Text), 3, "", "", true, true);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void combEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combEmployee.BackColor = Color.White;
                txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
            }
            catch
            {
                txtEmployeeID.Text = "";
                return;
            }
        }

        private void combEmployee_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combEmployee.Text.Trim() == "")
                {
                    txtEmployeeID.Text = "";

                }
                else
                {
                    combEmployee.BackColor = Color.White;
                    txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
                }

            }
            catch
            {
                txtEmployeeID.Text = "";
                return;
            }
        }

        private void combDeductionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combDeductionType.BackColor = Color.White;
                txtDeductionType_ID.Text = combDeductionType.SelectedValue.ToString();
            }
            catch
            {
                txtDeductionType_ID.Text = "";
                return;
            }
        }

        private void combDeductionType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combDeductionType.Text.Trim() == "")
                {
                    txtDeductionType_ID.Text = "";

                }
                else
                {
                    combDeductionType.BackColor = Color.White;
                    txtDeductionType_ID.Text = combDeductionType.SelectedValue.ToString();
                }

            }
            catch
            {
                txtDeductionType_ID.Text = "";
                return;
            }
        }
    }
}
