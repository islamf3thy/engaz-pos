﻿using System;
using System.Data;

class Employee_AdditionsType_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Employee_AdditionsType()
    {
        return Execute_SQL("select ISNULL (MAX(AdditionsType_ID)+1,1) from Employee_AdditionsType", CommandType.Text);
    }


    // Insert
    public void Insert_Employee_AdditionsType(string AdditionsType_ID, String AdditionsType_Name, string Remarks, int UserAdd)
    {
        Execute_SQL("insert into Employee_AdditionsType(AdditionsType_ID ,AdditionsType_Name ,Remarks ,UserAdd )Values (@AdditionsType_ID ,@AdditionsType_Name ,@Remarks ,@UserAdd )", CommandType.Text,
        Parameter("@AdditionsType_ID", SqlDbType.Int, AdditionsType_ID),
        Parameter("@AdditionsType_Name", SqlDbType.NVarChar, AdditionsType_Name),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Employee_AdditionsType(string AdditionsType_ID, String AdditionsType_Name, string Remarks)
    {
        Execute_SQL("Update Employee_AdditionsType Set AdditionsType_ID=@AdditionsType_ID ,AdditionsType_Name=@AdditionsType_Name ,Remarks=@Remarks  where AdditionsType_ID=@AdditionsType_ID", CommandType.Text,
        Parameter("@AdditionsType_ID", SqlDbType.Int, AdditionsType_ID),
        Parameter("@AdditionsType_Name", SqlDbType.NVarChar, AdditionsType_Name),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }

    //Delete
    public void Delete_Employee_AdditionsType(string AdditionsType_ID)
    {
        Execute_SQL("Delete  From Employee_AdditionsType where AdditionsType_ID=@AdditionsType_ID", CommandType.Text,
        Parameter("@AdditionsType_ID", SqlDbType.Int, AdditionsType_ID));
    }

    //Details ID
    public DataTable NODelete_Employee_AdditionsType(String AdditionsType_ID)
    {
        return ExecteRader("Select AdditionsType_ID  from Employee_Additions Where AdditionsType_ID=@AdditionsType_ID", CommandType.Text,
        Parameter("@AdditionsType_ID", SqlDbType.Int, AdditionsType_ID));
    }


    //Details ID
    public DataTable Details_Employee_AdditionsType(string AdditionsType_ID)
    {
        return ExecteRader("Select *  from Employee_AdditionsType Where AdditionsType_ID=@AdditionsType_ID", CommandType.Text,
        Parameter("@AdditionsType_ID", SqlDbType.Int, AdditionsType_ID));
    }

    //Details ID
    public DataTable NameSearch__Employee_AdditionsType(String AdditionsType_Name)
    {
        return ExecteRader("Select *  from Employee_AdditionsType Where AdditionsType_Name=@AdditionsType_Name", CommandType.Text,
        Parameter("@AdditionsType_Name", SqlDbType.NVarChar, AdditionsType_Name));
    }

    //Search 
    public DataTable Search__Employee_AdditionsType(string Search)
    {
        return ExecteRader("Select *  from Employee_AdditionsType Where convert(nvarchar,AdditionsType_ID)+AdditionsType_Name like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}