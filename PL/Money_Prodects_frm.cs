﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Money_Prodects_frm : Form
    {
        public Money_Prodects_frm()
        {
            InitializeComponent();
        }
        Money_Prodecuts_cls cls = new Money_Prodecuts_cls();

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
              //  DataTable dt = new DataTable(); 
                //if ( txtStoreID.Text.Trim() == "" &&  txtProdectID.Text.Trim() == "")
                //{
                //    dt = cls.Money_Prodects_Sales_Details(D1.Value, D2.Value);
                //}
                //else if ( txtStoreID.Text.Trim() == "" &&  txtProdectID.Text.Trim() != "")
                //{
                //    dt = cls.Money_Prodects(D1.Value, D2.Value, txtProdectID.Text);
                //}
                //else if ( txtStoreID.Text.Trim() != "" &&  txtProdectID.Text.Trim() == "")
                //{
                //    dt = cls.Money_Prodects_Sales_Details(D1.Value, D2.Value, txtStoreID.Text);
                //}
                //else if ( txtStoreID.Text.Trim() != "" &&  txtProdectID.Text.Trim() != "")
                //{
                //    dt = cls.Money_Prodects_Sales_Details(D1.Value, D2.Value, txtProdectID.Text, txtStoreID.Text);
                //}
                int? StoreID=null ; int? CategoryID = null; int? ProdecutID = null;
                if (combStore.SelectedItem !=null)
                {
                    StoreID = int.Parse(combStore.SelectedValue.ToString());
                }
                if (combCategory.SelectedItem != null)
                {
                    CategoryID = int.Parse(combCategory.SelectedValue.ToString());
                }
                if (combProdect.SelectedItem != null)
                {
                    ProdecutID = int.Parse(combProdect.SelectedValue.ToString());
                }


                DataTable  dt = cls.Money_Prodects(D1.Value, D2.Value, StoreID, CategoryID, ProdecutID);
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
                lblCount.Text = DGV1.Rows.Count.ToString();
                sum();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Money_Prodects_frm_Load(object sender, EventArgs e)
        {
            try
            {
              cls.  createstore();
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                this.MaximizeBox = true;
                ERB_Setting.SettingDGV(DGV1);
                // FILL Combo Group item
                Prodecut_cls Prodecut_cls = new Prodecut_cls();
                combProdect.DataSource = Prodecut_cls.Search_Prodecuts("");
                combProdect.DisplayMember = "ProdecutName";
                combProdect.ValueMember = "ProdecutID";
                combProdect.Text = null;
      

                // FILL Combo Store item
                Store_cls Store_cls = new Store_cls();
                combStore.DataSource = Store_cls.Search_Stores("");
                combStore.DisplayMember = "StoreName";
                combStore.ValueMember = "StoreID";
                combStore.Text = null;
        


                
                // FILL Combo Store item
                Category_cls category_clse = new Category_cls();
                combCategory.DataSource = category_clse.Search_Category("");
                combCategory.DisplayMember = "CategoryName";
                combCategory.ValueMember = "CategoryID";
                combCategory.Text = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGV1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
          
        }


        void sum()
        {
            double sum = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                DGV1.Rows[i].Cells["Money"].Value = Convert.ToDouble(DGV1.Rows[i].Cells["TotalPrice"].Value.ToString()) - Convert.ToDouble(DGV1.Rows[i].Cells["ReturnTotalPrice"].Value.ToString()) - Convert.ToDouble(DGV1.Rows[i].Cells["TotalBayPrice"].Value.ToString());
                sum += Convert.ToDouble(DGV1.Rows[i].Cells["Money"].Value);
            }

            lblSum.Text = sum.ToString("0.00");
        }
        private void combStore_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void combStore_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void combProdect_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }

        private void combProdect_TextChanged(object sender, EventArgs e)
        {
          
        }
    }
}
