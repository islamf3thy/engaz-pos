﻿
class ClearData_cls : DataAccessLayer
{

    public void ClearData()
    {
        Execute_SQL("sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'", System.Data.CommandType.Text);
        Execute_SQL("sp_MSForEachTable 'DELETE FROM ?' ", System.Data.CommandType.Text);
        Execute_SQL(" sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'", System.Data.CommandType.Text);
    }

}

