﻿using System;
using System.Data;
using System.Drawing;

using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillChangeStore_frm : Form
    {
        public BillChangeStore_frm()
        {
            InitializeComponent();
        }
        BillStore_cls cls = new BillStore_cls();
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        AVG_cls AVG_cls = new AVG_cls();
        Boolean Recived = true;
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {

            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);
            FillProdecut();
            btnNew_Click(null, null);
        }

        public void FillProdecut()
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);

        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName.BackColor = Color.WhiteSmoke;
                    txtStoreID.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }










        private void SUMQTY()
        {
            try
            {
                double QTY = 0;
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    QTY += double.Parse(DGV1.Rows[i].Cells["Quantity"].Value.ToString());
                }
                lblQTY.Text = QTY.ToString();
            }
            catch
            {


            }

        }







        private void DGV1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            lastQty();
        }
        double last_Qty = 0;
        void lastQty()
        {
            try
            {
                last_Qty = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }








        DataTable dt_Prodecut = new DataTable();
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {



            if (e.KeyCode == Keys.Enter)
            {

                if (txtSearch.Text.Trim() == "")
                {
                    cleartxt();
                    return;
                }
                else if (txtStoreID.Text == "")
                {
                    MessageBox.Show("يرجي تحديد المخزن الذي تريد التحويل منة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    return;
                }
                else if (txtStoreID2.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي تحديد المخزن الذي تريد التحويل اليه", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    txtStoreName2.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }
                else if (txtStoreID.Text == txtStoreID2.Text)
                {
                    MessageBox.Show("لا يمكنك التحويل الي نفس المخزن", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                #region "تحميل الباركود واكود الصنف واسم الصنف في مكانة  السليم"

                //====================================================================================
                try
                {
                    Prodecut_cls Prodecut_CLS = new Prodecut_cls();
                    dt_Prodecut = Prodecut_CLS.SearchBill_Prodecuts(txtSearch.Text, txtStoreID.Text);

                    String ProdecutName = txtSearch.Text;
                    txtProdecutID.Text = dt_Prodecut.Rows[0]["ProdecutID"].ToString();
                    txtSearch.Text = dt_Prodecut.Rows[0]["ProdecutName"].ToString();
                    txtBalence.Text = dt_Prodecut.Rows[0]["Balence"].ToString();

                    combUnit.Items.Clear();

                    if (dt_Prodecut.Rows[0]["FiestUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["FiestUnit"].ToString());
                    }
                    if (dt_Prodecut.Rows[0]["SecoundUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["SecoundUnit"].ToString());
                    }
                    if (dt_Prodecut.Rows[0]["ThreeUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["ThreeUnit"].ToString());
                    }
                    txtQuantity.Text = "1";



                    #region "ملأ الكومبوبوكس بالوحدات"

                    if (dt_Prodecut.Rows[0]["FiestUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 0;

                    }
                    else if (dt_Prodecut.Rows[0]["SecoundUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 1;

                    }
                    else if (dt_Prodecut.Rows[0]["ThreeUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 2;
                    }
                    else
                    {
                        if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "1")
                        {
                            combUnit.SelectedIndex = 0;

                        }
                        else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "2")
                        {
                            combUnit.SelectedIndex = 1;

                        }
                        else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "3")
                        {
                            combUnit.SelectedIndex = 2;
                        }
                        else
                        {
                            combUnit.SelectedIndex = 0;
                        }
                    }

                    combUnit.Focus();
                    combUnit.Select();

                    #endregion
                    if (BillSetting_cls.UsingFastInput == true)
                    {

                        txtSearch.Focus();
                    }


                }
                catch
                {
                    Mass.No_Fouind();
                    txtSearch.Text = "";
                    txtSearch.Focus();
                }


                //==================================================================================================================

                #endregion

                //SumitemdDGV();
            }


        }

        private void combUnit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQuantity.Focus();
            }
        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPrice.Focus();
            }
        }

        private void txtPrise_KeyDown(object sender, KeyEventArgs e)
        {

        }





        #region" اضافة الي الداتا قرد فيو   "
        //string AVG_Price = "0";

        public void AddItem_DGV()
        {
            try
            {
                if (txtSearch.Text == "" || txtProdecutID.Text == "")
                {
                    txtSearch.BackColor = Color.Pink;
                    txtSearch.Focus();
                    return;
                }

                if (combUnit.Text == "")
                {
                    combUnit.BackColor = Color.Pink;
                    combUnit.Focus();
                    return;
                }
                if (txtQuantity.Text == "" || Convert.ToDouble(txtQuantity.Text) <= Convert.ToDouble(0))
                {
                    txtQuantity.BackColor = Color.Pink;
                    txtQuantity.Focus();
                    return;
                }


                if (txtStoreID.Text == "")
                {
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    return;
                }


                //=================Search Prodect use in table or no===============================================================================================

                DataTable Dt_StoreSearch = Store_Prodecut_cls.Details_Store_Prodecut(txtProdecutID.Text, txtStoreID2.Text);
                if (Dt_StoreSearch.Rows.Count == 0)
                {
                    if (MessageBox.Show("الصنف المدخل غير موجود في المخزن : هل تريد اضافتة", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Store_Prodecut_cls.InsertStore_Prodecut(txtProdecutID.Text, txtStoreID2.Text, "0", "0", "0");
                    }


                }

                //================================================================================================================
                #region"الكمية غير متوفرة"


                if (DGV1.Rows.Count == 0)
                {
                    if (Convert.ToDouble(txtBalence.Text) < (Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtUnitFactor.Text)))
                    {
                        MessageBox.Show("الكمية غير متوفرة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtQuantity.BackColor = Color.Pink;
                        txtQuantity.Focus();
                        return;
                    }
                }

                else
                {
                    double X = 0;
                    double U = Convert.ToDouble(txtBalence.Text);
                    for (int i = 0; i < DGV1.Rows.Count; i++)
                    {
                        if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == txtStoreID.Text)
                        {
                            // الكمية الموجودة في المخزن
                            X += Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value);
                        }
                    }

                    X += Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtUnitFactor.Text);
                    if (X > U)
                    {
                        MessageBox.Show("الكمية غير متوفرة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtQuantity.Focus();
                        return;
                    }

                }
                #endregion

                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["Unit"].Value.ToString() == combUnit.Text)
                    {
                        if (BillSetting_cls.ShowMessageQty == true)
                        {
                            Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                            DGV1.Rows[i].Cells["Quantity"].Value = +qty;

                            cleartxt();
                            return;

                        }
                        else
                        {
                            if (MessageBox.Show("هذ الصنف سبق وتم اضافتة ضمن الفاتورة  : هل تريد اضافة  " + txtQuantity.Text + "  الي الكمية ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                                DGV1.Rows[i].Cells["Quantity"].Value = +qty;


                                cleartxt();
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }


                    }
                }



                AddRowDgv(txtProdecutID.Text, txtSearch.Text, combUnit.Text, txtUnitFactor.Text, txtUnitOperating.Text, txtQuantity.Text, "0", txtPrice.Text, txtTotalPrice.Text, txtStoreID.Text);
                lblCount.Text = DGV1.Rows.Count.ToString();
                SUMQTY();

                cleartxt();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }


        private void AddRowDgv(string ProdecutID, string ProdecutName, string Unit, string UnitFactor, string UnitOperating, string Quantity, String XQuantity, string Price, string TotalPrice, String StoreID)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
                DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["XQuantity"].Value = XQuantity;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;
                DGV1.Rows[lastRows].Cells["StoreID"].Value = StoreID;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        private void cleartxt()
        {
            txtProdecutID.Text = "";
            txtSearch.Text = "";
            txtQuantity.Text = "";
            combUnit.Items.Clear();
            txtPrice.Text = "";
            txtTotalPrice.Text = "";
            txtBalence.Text = "";
            txtSearch.Focus();

        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            txtQuantity.BackColor = Color.White;
            SumTotal_txt();
        }

        private void SumTotal_txt()
        {
            try
            {



                if (txtQuantity.Text == "")
                {
                    txtQuantity.Focus();
                    return;
                }

                if (txtPrice.Text == "")
                {
                    txtPrice.Focus();
                    return;
                }


                txtTotalPrice.Text = (Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtPrice.Text)).ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.Rows.Count == 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                if (txtStoreID.Text == "")
                {
                    MessageBox.Show("يرجي تحديد المخزن الذي تريد التحويل منة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    return;
                }
                if (txtStoreID2.Text == "")
                {
                    MessageBox.Show("يرجي تحديد المخزن الذي تريد التحويل اليه", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    txtStoreName2.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }
                if (txtStoreID.Text == txtStoreID2.Text)
                {
                    MessageBox.Show("لا يمكنك التحويل الي نفس المخزن", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }



                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }


                txtMainID.Text = cls.MaxID_ChangeStore_Main();
                cls.InsertChangeStore_Main(txtMainID.Text, D1.Value, txtRemarks.Text, txtStoreID.Text, txtStoreID2.Text, Properties.Settings.Default.UserID);
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.InsertChangeStore_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), txtStoreID.Text, false, Recived);
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), txtStoreID.Text, true);
                    cls.InsertChangeStore_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), txtStoreID2.Text, true, Recived);
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), txtStoreID2.Text, true);

                }

                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }
                btnNew_Click(null, null);



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.Rows.Count == 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (txtStoreID.Text == "")
                {
                    MessageBox.Show("يرجي تحديد المخزن الذي تريد التحويل منة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    return;
                }
                if (txtStoreID2.Text == "")
                {
                    MessageBox.Show("يرجي تحديد المخزن الذي تريد التحويل اليه", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    txtStoreName2.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }
                if (txtStoreID.Text == txtStoreID2.Text)
                {
                    MessageBox.Show("لا يمكنك التحويل الي نفس المخزن", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }

                cls.UpdateChangeStore_Main(txtMainID.Text, D1.Value, txtRemarks.Text, txtStoreID.Text, txtStoreID2.Text);
                DataTable dt = cls.Details_ChangeStore_Details(txtMainID.Text);


                cls.Delete_ChangeStore_Details(txtMainID.Text);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(), true);
                }
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.InsertChangeStore_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), txtStoreID.Text, false, Recived);
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), txtStoreID.Text, true);
                    cls.InsertChangeStore_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), txtStoreID2.Text, true, Recived);
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), txtStoreID2.Text, true);

                }
                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Update();
                }
                btnNew_Click(null, null);



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }





        //======================================================================


        #endregion




        private void txtSum_TextChanged(object sender, EventArgs e)
        {

            //try
            //{

            //    if (txtSum.Text.Trim() != "")
            //    {
            //        txtArbic.Text = DAL.DataAccessLeayr.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtSum.Text), 2, "", "", true, true);

            //    }
            //    else
            //    {
            //        txtArbic.Text = "";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                cleartxt();

            }
        }


        private void combUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combUnit.SelectedIndex == 0)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["FiestUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["FiestUnitOperating"].ToString();
            }
            else if (combUnit.SelectedIndex == 1)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["SecoundUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString();
            }
            else if (combUnit.SelectedIndex == 2)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["ThreeUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString();

            }
            txtPrice.Text = (Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutAvg"].ToString()) * Convert.ToDouble(txtUnitFactor.Text)).ToString("0.00");


            if (BillSetting_cls.UsingFastInput == true)
            {
                AddItem_DGV();
                txtSearch.Focus();
            }
        }









        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DGV1.Rows.Count > 0)
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }
        }











        private void txtVAT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }






        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.SelectedRows.Count >= 1)
                {
                    if (Mass.Delete() == true)
                    {
                        foreach (DataGridViewRow r in DGV1.SelectedRows)
                        {
                            DGV1.Rows.Remove(r);
                        }
                        lblCount.Text = DGV1.Rows.Count.ToString();
                        SUMQTY();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void DGV1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Prodecut_cls Prodecut_CLS = new Prodecut_cls();
                DataTable dt_Quantity = Prodecut_CLS.Edit_Quentity_GDV_Bill_Prodecuts(DGV1.CurrentRow.Cells["ProdecutID"].Value.ToString(), DGV1.CurrentRow.Cells["StoreID"].Value.ToString());
                // الكميات محولة من الداتا جرد فيو
                Double Quantity_In_DGV1 = QtyDGV1(DGV1.CurrentRow.Cells["ProdecutID"].Value.ToString(), DGV1.CurrentRow.Cells["StoreID"].Value.ToString());
                //الرصيد المتاح في المخزن
                Double TotalQuantity = Convert.ToDouble(dt_Quantity.Rows[0]["Balence"].ToString());
                if (btnUpdate.Enabled == true)
                {
                    TotalQuantity = Convert.ToDouble(dt_Quantity.Rows[0]["Balence"].ToString()) + (Convert.ToDouble(DGV1.CurrentRow.Cells["XQuantity"].Value.ToString()) * Convert.ToDouble(DGV1.CurrentRow.Cells["UnitFactor"].Value));
                }


                //المقارنة بين الكمية الموجودة في المخزن والكميات المدخلة
                if (Quantity_In_DGV1 > TotalQuantity)
                {
                    MessageBox.Show(" تم تجاوز الكمية الموجودة بمقدار :   " + ((TotalQuantity - Quantity_In_DGV1) / Convert.ToDouble(DGV1.CurrentRow.Cells["UnitFactor"].Value)).ToString() + "", "الكمية الحالية غير متوفرة ", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    DGV1.CurrentRow.Cells["Quantity"].Value = last_Qty;
                    return;
                }




                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                DGV1.CurrentRow.Cells["TotalPrice"].Value = x.ToString();
                SUMQTY();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private Double QtyDGV1(String ProdecutID, String StoreID)
        {
            Double XQuantity = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == ProdecutID && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == StoreID)
                {
                    // الكمية الموجودة في الداتا جرد فيو
                    XQuantity += Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value);
                }
            }
            return XQuantity;
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    DataTable dt = cls.Details_ChangeStore_Details(txtMainID.Text);

                    cls.DeleteChangeStore_Main(txtMainID.Text);
                    cls.Delete_ChangeStore_Details(txtMainID.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(), true);
                    }
                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        DataTable dtShow;
        int pos = 0;
        private void btnNew_Click(object sender, EventArgs e)
        {
            // Use Fast Inpout

            //===============================================================================================================================================================
            txtMainID.Text = cls.MaxID_ChangeStore_Main();
            DGV1.Rows.Clear();
            txtRemarks.Text = "";
            txtProdecutID.Text = "";
            lblCount.Text = DGV1.Rows.Count.ToString();
            dtShow = cls.Search_ChangeStore_Main();
            pos = dtShow.Rows.Count;
            ShowData(pos);
            DGV1.DataSource = null;
            DGV1.Rows.Clear();
            lblQTY.Text = "0";
            btnSave.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            cleartxt();

        }



        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                toolStripButton3_Click(null, null);
            }
        }

        private void BillBay_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                pos--;
                if (pos >= 0)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اضغر سند ");

                }
            }
            catch
            {
                return;
            }
        }



        public void ShowData(int index)
        {
            try
            {
                GetDataSearch(dtShow.Rows[index]["MainID"].ToString());
            }
            catch
            {
                return;
            }
        }


        private void GetDataSearch(String ID)
        {
            DataTable dt = cls.Details_ChangeStore_Main(ID);
            DataRow Dr = dt.Rows[0];
            txtMainID.Text = Dr["MainID"].ToString();
            D1.Value = Convert.ToDateTime(Dr["MyDate"]);
            txtRemarks.Text = Dr["Remarks"].ToString();

            txtStoreID.Text = Dr["StoreID"].ToString();
            txtStoreName.Text = cls.Details_Stores(Dr["StoreID"].ToString()).Rows[0]["StoreName"].ToString();
            txtStoreID2.Text = Dr["StoreID2"].ToString();
            txtStoreName2.Text = cls.Details_Stores(Dr["StoreID2"].ToString()).Rows[0]["StoreName"].ToString();
            //=========================================================================================================================================================================
            dt.Clear();
            DGV1.DataSource = null;
            DGV1.Rows.Clear();
            //=========================================================================================================================================================================
            dt = cls.Details_ChangeStore_Details(txtMainID.Text, false);
            DataRow Dr2 = dt.Rows[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString());
            }

            lblCount.Text = DGV1.Rows.Count.ToString();
            btnSave.Enabled = false;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                pos++;
                if (pos < dtShow.Rows.Count)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اكبر سند  ");
                    pos = dtShow.Rows.Count;
                    //ShowData(pos);
                }
            }
            catch
            {
                return;
            }
        }


        private void DGV1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            txtPrice.BackColor = Color.White;
            SumTotal_txt();
        }

        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTotalPrice.Focus();
            }
        }

        private void txtTotalPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName2.BackColor = Color.WhiteSmoke;
                    txtStoreID2.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName2.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
