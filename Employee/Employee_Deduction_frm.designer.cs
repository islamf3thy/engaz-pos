﻿namespace ByStro.PL
{
    partial class Employee_Deduction_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Employee_Deduction_frm));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnGetData = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDeduction_Value = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDeduction_ID = new System.Windows.Forms.TextBox();
            this.combDeductionType = new System.Windows.Forms.ComboBox();
            this.combEmployee = new System.Windows.Forms.ComboBox();
            this.txtDeductionType_ID = new System.Windows.Forms.TextBox();
            this.txtArbic = new System.Windows.Forms.TextBox();
            this.txtTreasuryID = new System.Windows.Forms.TextBox();
            this.txtTypeID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(540, 234);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(96, 30);
            this.btnClose.TabIndex = 66;
            this.btnClose.Text = "اغلاق";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGetData
            // 
            this.btnGetData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnGetData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetData.ForeColor = System.Drawing.Color.White;
            this.btnGetData.Location = new System.Drawing.Point(540, 202);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(96, 30);
            this.btnGetData.TabIndex = 65;
            this.btnGetData.Text = "بحث";
            this.btnGetData.UseVisualStyleBackColor = false;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(540, 170);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(96, 30);
            this.btnDelete.TabIndex = 64;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(540, 138);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(96, 30);
            this.btnUpdate.TabIndex = 63;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(540, 106);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 30);
            this.btnSave.TabIndex = 62;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.White;
            this.btnNew.Location = new System.Drawing.Point(540, 74);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(96, 30);
            this.btnNew.TabIndex = 61;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label7.Location = new System.Drawing.Point(36, 72);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 17);
            this.label7.TabIndex = 76;
            this.label7.Text = "اسم الموظف :";
            // 
            // txtDeduction_Value
            // 
            this.txtDeduction_Value.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDeduction_Value.Location = new System.Drawing.Point(36, 188);
            this.txtDeduction_Value.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDeduction_Value.Name = "txtDeduction_Value";
            this.txtDeduction_Value.Size = new System.Drawing.Size(120, 24);
            this.txtDeduction_Value.TabIndex = 79;
            this.txtDeduction_Value.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            this.txtDeduction_Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label5.Location = new System.Drawing.Point(36, 168);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 17);
            this.label5.TabIndex = 82;
            this.label5.Text = "قيمة الاستقطاع :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label9.Location = new System.Drawing.Point(36, 220);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 17);
            this.label9.TabIndex = 88;
            this.label9.Text = "ملاحظات :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRemarks.Location = new System.Drawing.Point(36, 240);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(451, 61);
            this.txtRemarks.TabIndex = 87;
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtEmployeeID.Location = new System.Drawing.Point(582, 24);
            this.txtEmployeeID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.ReadOnly = true;
            this.txtEmployeeID.Size = new System.Drawing.Size(10, 24);
            this.txtEmployeeID.TabIndex = 90;
            this.txtEmployeeID.Visible = false;
            this.txtEmployeeID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.Location = new System.Drawing.Point(36, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 92;
            this.label2.Text = "اسم الاستقطاع :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label3.Location = new System.Drawing.Point(295, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 96;
            this.label3.Text = "التاريخ :";
            // 
            // D1
            // 
            this.D1.CustomFormat = "";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D1.Location = new System.Drawing.Point(292, 44);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(195, 24);
            this.D1.TabIndex = 95;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label4.Location = new System.Drawing.Point(37, 22);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 17);
            this.label4.TabIndex = 100;
            this.label4.Text = "رقم الاستقطاع :";
            // 
            // txtDeduction_ID
            // 
            this.txtDeduction_ID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDeduction_ID.Location = new System.Drawing.Point(36, 44);
            this.txtDeduction_ID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDeduction_ID.Name = "txtDeduction_ID";
            this.txtDeduction_ID.ReadOnly = true;
            this.txtDeduction_ID.Size = new System.Drawing.Size(251, 24);
            this.txtDeduction_ID.TabIndex = 99;
            // 
            // combDeductionType
            // 
            this.combDeductionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combDeductionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combDeductionType.FormattingEnabled = true;
            this.combDeductionType.Location = new System.Drawing.Point(36, 140);
            this.combDeductionType.Name = "combDeductionType";
            this.combDeductionType.Size = new System.Drawing.Size(451, 24);
            this.combDeductionType.TabIndex = 103;
            this.combDeductionType.SelectedIndexChanged += new System.EventHandler(this.combDeductionType_SelectedIndexChanged);
            this.combDeductionType.TextChanged += new System.EventHandler(this.combDeductionType_TextChanged);
            // 
            // combEmployee
            // 
            this.combEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combEmployee.FormattingEnabled = true;
            this.combEmployee.Location = new System.Drawing.Point(36, 92);
            this.combEmployee.Name = "combEmployee";
            this.combEmployee.Size = new System.Drawing.Size(451, 24);
            this.combEmployee.TabIndex = 104;
            this.combEmployee.SelectedIndexChanged += new System.EventHandler(this.combEmployee_SelectedIndexChanged);
            this.combEmployee.TextChanged += new System.EventHandler(this.combEmployee_TextChanged);
            // 
            // txtDeductionType_ID
            // 
            this.txtDeductionType_ID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDeductionType_ID.Location = new System.Drawing.Point(540, 12);
            this.txtDeductionType_ID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDeductionType_ID.Name = "txtDeductionType_ID";
            this.txtDeductionType_ID.ReadOnly = true;
            this.txtDeductionType_ID.Size = new System.Drawing.Size(10, 24);
            this.txtDeductionType_ID.TabIndex = 105;
            this.txtDeductionType_ID.Visible = false;
            // 
            // txtArbic
            // 
            this.txtArbic.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtArbic.Location = new System.Drawing.Point(160, 188);
            this.txtArbic.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtArbic.Name = "txtArbic";
            this.txtArbic.ReadOnly = true;
            this.txtArbic.Size = new System.Drawing.Size(327, 24);
            this.txtArbic.TabIndex = 110;
            // 
            // txtTreasuryID
            // 
            this.txtTreasuryID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTreasuryID.Location = new System.Drawing.Point(608, 12);
            this.txtTreasuryID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTreasuryID.Name = "txtTreasuryID";
            this.txtTreasuryID.ReadOnly = true;
            this.txtTreasuryID.Size = new System.Drawing.Size(10, 24);
            this.txtTreasuryID.TabIndex = 111;
            this.txtTreasuryID.Visible = false;
            // 
            // txtTypeID
            // 
            this.txtTypeID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTypeID.Location = new System.Drawing.Point(395, 12);
            this.txtTypeID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTypeID.Name = "txtTypeID";
            this.txtTypeID.ReadOnly = true;
            this.txtTypeID.Size = new System.Drawing.Size(58, 24);
            this.txtTypeID.TabIndex = 112;
            this.txtTypeID.Text = "EmpDed";
            this.txtTypeID.Visible = false;
            // 
            // Employee_Deduction_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(668, 326);
            this.Controls.Add(this.txtTypeID);
            this.Controls.Add(this.txtTreasuryID);
            this.Controls.Add(this.txtArbic);
            this.Controls.Add(this.txtDeductionType_ID);
            this.Controls.Add(this.combEmployee);
            this.Controls.Add(this.combDeductionType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDeduction_ID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEmployeeID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDeduction_Value);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnGetData);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNew);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Employee_Deduction_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل استقطاع";
            this.Load += new System.EventHandler(this.Discound_frm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Discound_frm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnGetData;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtDeduction_Value;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtRemarks;
        public System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker D1;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtDeduction_ID;
        private System.Windows.Forms.ComboBox combDeductionType;
        private System.Windows.Forms.ComboBox combEmployee;
        public System.Windows.Forms.TextBox txtDeductionType_ID;
        public System.Windows.Forms.TextBox txtArbic;
        public System.Windows.Forms.TextBox txtTreasuryID;
        public System.Windows.Forms.TextBox txtTypeID;
    }
}