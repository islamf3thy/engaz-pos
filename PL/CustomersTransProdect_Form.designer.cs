﻿namespace ByStro.PL
{
    partial class CustomersTransProdect_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomersTransProdect_Form));
            this.Label4 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.datefrom = new System.Windows.Forms.DateTimePicker();
            this.datetto = new System.Windows.Forms.DateTimePicker();
            this.Label2 = new System.Windows.Forms.Label();
            this.DGV1 = new System.Windows.Forms.DataGridView();
            this.MainID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnTotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmpName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.txtAccountID = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtAccountName = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txt_1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(78, 33);
            this.Label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(35, 17);
            this.Label4.TabIndex = 130;
            this.Label4.Text = "من :";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(291, 35);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(38, 17);
            this.Label1.TabIndex = 129;
            this.Label1.Text = "الي :";
            // 
            // datefrom
            // 
            this.datefrom.CustomFormat = "dd /MM /yyyy";
            this.datefrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datefrom.Location = new System.Drawing.Point(123, 31);
            this.datefrom.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.datefrom.Name = "datefrom";
            this.datefrom.Size = new System.Drawing.Size(161, 24);
            this.datefrom.TabIndex = 1;
            // 
            // datetto
            // 
            this.datetto.CustomFormat = "dd /MM /yyyy";
            this.datetto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetto.Location = new System.Drawing.Point(335, 31);
            this.datetto.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.datetto.Name = "datetto";
            this.datetto.Size = new System.Drawing.Size(171, 24);
            this.datetto.TabIndex = 2;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(28, 61);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label2.Size = new System.Drawing.Size(90, 17);
            this.Label2.TabIndex = 119;
            this.Label2.Text = "اسم  العميل :";
            // 
            // DGV1
            // 
            this.DGV1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.DGV1.AllowUserToAddRows = false;
            this.DGV1.AllowUserToDeleteRows = false;
            this.DGV1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MainID,
            this.MyDate,
            this.ProdecutName,
            this.Unit,
            this.Quantity,
            this.ReturnQuantity,
            this.Price,
            this.Vat,
            this.TotalPrice,
            this.ReturnTotalPrice,
            this.StoreName,
            this.EmpName});
            this.DGV1.EnableHeadersVisualStyles = false;
            this.DGV1.GridColor = System.Drawing.Color.DarkGray;
            this.DGV1.Location = new System.Drawing.Point(0, 85);
            this.DGV1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.DGV1.Name = "DGV1";
            this.DGV1.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGV1.RowHeadersVisible = false;
            this.DGV1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.DGV1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV1.Size = new System.Drawing.Size(939, 380);
            this.DGV1.TabIndex = 114;
            // 
            // MainID
            // 
            this.MainID.DataPropertyName = "MainID";
            this.MainID.HeaderText = "رقم الفاتورة";
            this.MainID.Name = "MainID";
            this.MainID.ReadOnly = true;
            this.MainID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MainID.Width = 120;
            // 
            // MyDate
            // 
            this.MyDate.DataPropertyName = "MyDate";
            this.MyDate.FillWeight = 120F;
            this.MyDate.HeaderText = "التاريخ";
            this.MyDate.Name = "MyDate";
            this.MyDate.ReadOnly = true;
            // 
            // ProdecutName
            // 
            this.ProdecutName.DataPropertyName = "ProdecutName";
            this.ProdecutName.HeaderText = "اسم الصنف";
            this.ProdecutName.Name = "ProdecutName";
            this.ProdecutName.ReadOnly = true;
            this.ProdecutName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProdecutName.Width = 190;
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "Unit";
            this.Unit.HeaderText = "الوحدة";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            this.Quantity.HeaderText = "الكمية";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ReturnQuantity
            // 
            this.ReturnQuantity.DataPropertyName = "ReturnQuantity";
            this.ReturnQuantity.HeaderText = "كمية المرتجع";
            this.ReturnQuantity.Name = "ReturnQuantity";
            this.ReturnQuantity.ReadOnly = true;
            this.ReturnQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "السعر";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Price.Width = 120;
            // 
            // Vat
            // 
            this.Vat.DataPropertyName = "Vat";
            this.Vat.HeaderText = "الضريبة";
            this.Vat.Name = "Vat";
            this.Vat.ReadOnly = true;
            this.Vat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Vat.Width = 90;
            // 
            // TotalPrice
            // 
            this.TotalPrice.DataPropertyName = "TotalPrice";
            this.TotalPrice.HeaderText = "اجمالي السعر";
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.ReadOnly = true;
            this.TotalPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalPrice.Width = 120;
            // 
            // ReturnTotalPrice
            // 
            this.ReturnTotalPrice.DataPropertyName = "ReturnTotalPrice";
            this.ReturnTotalPrice.HeaderText = "اجمالي المرتجع";
            this.ReturnTotalPrice.Name = "ReturnTotalPrice";
            this.ReturnTotalPrice.ReadOnly = true;
            this.ReturnTotalPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnTotalPrice.Width = 120;
            // 
            // StoreName
            // 
            this.StoreName.DataPropertyName = "StoreName";
            this.StoreName.HeaderText = "المخزن";
            this.StoreName.Name = "StoreName";
            this.StoreName.ReadOnly = true;
            this.StoreName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StoreName.Width = 150;
            // 
            // EmpName
            // 
            this.EmpName.DataPropertyName = "EmpName";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.EmpName.DefaultCellStyle = dataGridViewCellStyle3;
            this.EmpName.HeaderText = "المستخدم";
            this.EmpName.Name = "EmpName";
            this.EmpName.ReadOnly = true;
            this.EmpName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EmpName.Width = 160;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(939, 25);
            this.toolStrip1.TabIndex = 692;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.Image = global::ByStro.Properties.Resources.Search_20;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(58, 22);
            this.btnNew.Text = "عرض";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtAccountID
            // 
            this.txtAccountID.Location = new System.Drawing.Point(546, 56);
            this.txtAccountID.Name = "txtAccountID";
            this.txtAccountID.Size = new System.Drawing.Size(10, 24);
            this.txtAccountID.TabIndex = 695;
            this.txtAccountID.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(511, 57);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 25);
            this.button3.TabIndex = 694;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtAccountName
            // 
            this.txtAccountName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtAccountName.Location = new System.Drawing.Point(123, 58);
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.ReadOnly = true;
            this.txtAccountName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAccountName.Size = new System.Drawing.Size(383, 24);
            this.txtAccountName.TabIndex = 693;
            this.txtAccountName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.txt_1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 467);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(16, 0, 1, 0);
            this.statusStrip1.Size = new System.Drawing.Size(939, 22);
            this.statusStrip1.TabIndex = 717;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(109, 17);
            this.toolStripStatusLabel1.Text = "اجمالي الحركات :";
            // 
            // txt_1
            // 
            this.txt_1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txt_1.Name = "txt_1";
            this.txt_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_1.Size = new System.Drawing.Size(17, 17);
            this.txt_1.Text = "0";
            // 
            // CustomersTransProdect_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(939, 489);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.txtAccountID);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtAccountName);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.datefrom);
            this.Controls.Add(this.datetto);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.DGV1);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CustomersTransProdect_Form";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "كشف حساب عميل تفصيلي";
            this.Load += new System.EventHandler(this.CustomersTrans_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DateTimePicker datefrom;
        internal System.Windows.Forms.DateTimePicker datetto;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.DataGridView DGV1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.TextBox txtAccountID;
        public System.Windows.Forms.TextBox txtAccountName;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel txt_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MainID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vat;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnTotalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmpName;
    }
}