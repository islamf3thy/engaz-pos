﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MessageEditShow_frm : Form
    {
        public MessageEditShow_frm()
        {
            InitializeComponent();
        }

        public Boolean ISEdit = false;
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ISEdit = true;
            this.Close();
        }

        private void MessageDelete_frm_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }
    }
}
