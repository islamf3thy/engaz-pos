﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace ByStro.PL
{
    public partial class Customers_frm : Form
    {
        public Customers_frm()
        {
            InitializeComponent();
        }
        Customers_cls cls = new Customers_cls();
        public Boolean ClearData = true;
        public Boolean SaveData = false;

        private void Company_Add_Form_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingForm(this);
                // DataAccessLayer.Geet_Company_Fill(combCompeny);

                btnNew_Click(null, null);
           
               
            }
            catch
            {

              
            }

        }





        private void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            txtCustomerName.BackColor = System.Drawing.Color.White;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
               
                comboBox1.SelectedIndex = 0;
                txtNationalID.Text = "";
                txtAddress.Text = "";
                txtTaxFileNumber.Text = "";
                txtRegistrationNo.Text = "";
                txtStartContract.Text = "";
                txtEndContract.Text = "";
                txtRemarks.Text = "";
                if (ClearData == true)
                {
                    txtCustomerName.Text = "";
                    txtPhone.Text = "";
                }
                
                txtPhone2.Text = "";
                txtAccountNumber.Text = "";
                txtFax.Text = "";
                txtEmail.Text = "";
                txtWebSite.Text = "";
                txtMaximum.Text = "";
                rb1.Checked = true;
                R1.Checked = true;
                txtCustomerID.Text = cls.MaxID_Customers_ID();
                tabControl1.SelectedIndex = 0;
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;

                chISCusSupp.Checked = false;
                txtSupplierID.Text = "";
                txtSupplierName.Text = "";
                txtSupplierName.BackColor = Color.WhiteSmoke;

                txtCustomerName.Focus();


                if (Application.OpenForms["BillSales_frm"] != null)
                {
                //    ((BillSales_frm)Application.OpenForms["BillSales_frm"]).FillCombAccount();

                }

                if (Application.OpenForms["CustomersFirst_frm"] != null)
                {
                    ((CustomersFirst_frm)Application.OpenForms["CustomersFirst_frm"]).FillCombAccount();

                }
                if (Application.OpenForms["MaintenanceRecived_frm"] != null)
                {
                    ((MaintenanceRecived_frm)Application.OpenForms["MaintenanceRecived_frm"]).FillCustomerName();

                }

                if (Application.OpenForms["BillMaintenance_frm"] != null)
                {
                    ((BillMaintenance_frm)Application.OpenForms["BillMaintenance_frm"]).FillCombAccount();

                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCustomerName.Text.Trim() == "")
                {
                    txtCustomerName.BackColor = System.Drawing.Color.Pink;
                    txtCustomerName.Focus();
                    return;
                }
               
                // Search where Customer Name

                DataTable DtSearch = cls.NameSearch_Customers(txtCustomerName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtCustomerName.BackColor = System.Drawing.Color.Pink;
                    txtCustomerName.Focus();
                    return;
                }
                String Notifications = "1";

                if (R1.Checked == true)
                {
                    Notifications = "1";
                }
                else if (R2.Checked == true)
                {
                    Notifications = "2";
                }
                else if (R3.Checked == true)
                {
                    Notifications = "3";
                }

                string SalesLavel = "1";
                if (comboBox1.SelectedIndex == 0)
                {
                    SalesLavel = "1";
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    SalesLavel = "2";
                }
                else  if (comboBox1.SelectedIndex == 2)
                    {
                    SalesLavel = "3";
                }
                if (chISCusSupp.Checked)
                {
                    if (string.IsNullOrEmpty(txtSupplierID.Text))
                    {
                        tabControl1.SelectedIndex = 3;
                        txtSupplierName.BackColor = Color.Pink;
                        txtSupplierName.Focus();
                        return;
                    }
                }



               
                txtCustomerID.Text = cls.MaxID_Customers_ID();
                cls.InsertCustomers(txtCustomerID.Text, txtCustomerName.Text, SalesLavel, txtNationalID.Text, txtAddress.Text, txtTaxFileNumber.Text, txtRegistrationNo.Text, txtStartContract.Text, txtEndContract.Text, txtRemarks.Text, txtPhone.Text, txtPhone2.Text, txtAccountNumber.Text, txtFax.Text, txtEmail.Text, txtWebSite.Text, txtMaximum.Text, Notifications,chISCusSupp.Checked,txtSupplierID.Text, Properties.Settings.Default.UserID, rb1.Checked);
                //===InsertCustomers_trans============================================================================================================================================================================================================================================================
                if (ClearData==false)
                {
                    SaveData = true;

                    this.Close();
                }


                btnNew_Click(null, null);

              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCustomerName.Text.Trim() == "")
                {
                    txtCustomerName.BackColor = System.Drawing.Color.Pink;
                    txtCustomerName.Focus();
                    return;
                }
               
                String Notifications = "1";

                if (R1.Checked == true)
                {
                    Notifications = "1";
                }
                else if (R2.Checked == true)
                {
                    Notifications = "2";
                }
                else if (R3.Checked == true)
                {
                    Notifications = "3";
                }
                string SalesLavel = "1";
                if (comboBox1.SelectedIndex == 0)
                {
                    SalesLavel = "1";
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    SalesLavel = "2";
                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    SalesLavel = "3";
                }
                if (chISCusSupp.Checked)
                {
                    if (string.IsNullOrEmpty(txtSupplierID.Text))
                    {
                        tabControl1.SelectedIndex = 3;
                        txtSupplierName.BackColor = Color.Pink;
                        txtSupplierName.Focus();
                        return;
                    }
                }

                cls.UpdateCustomers(txtCustomerID.Text,txtCustomerName.Text, SalesLavel, txtNationalID.Text, txtAddress.Text, txtTaxFileNumber.Text, txtRegistrationNo.Text, txtStartContract.Text, txtEndContract.Text, txtRemarks.Text, txtPhone.Text, txtPhone2.Text, txtAccountNumber.Text, txtFax.Text, txtEmail.Text, txtWebSite.Text, txtMaximum.Text, Notifications,chISCusSupp.Checked,txtSupplierID.Text, rb1.Checked);
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (cls.NODeleteCustomers(txtCustomerID.Text).Rows.Count > 0)
                {
                    Mass.NoDelete();
                    return;
                }





                if (Mass.Delete() == true)
                {
                    cls.DeleteCustomers(txtCustomerID.Text);
                    btnNew_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {
                CustomersSearch_frm frm = new CustomersSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }

                if (frm.DGV1.Rows.Count == 0)
                    return;

                DataTable dt = cls.Details_Customers(frm.DGV1.CurrentRow.Cells["CustomerID"].Value.ToString());
                DataRow Dr = dt.Rows[0];


          txtCustomerID.Text = Dr["CustomerID"].ToString();
          txtCustomerName.Text = Dr["CustomerName"].ToString();
 
          txtNationalID.Text = Dr["NationalID"].ToString();
          txtAddress.Text = Dr["Address"].ToString();
          txtTaxFileNumber.Text = Dr["TaxFileNumber"].ToString();
          txtRegistrationNo.Text = Dr["RegistrationNo"].ToString();
          txtStartContract.Text = Dr["StartContract"].ToString();
          txtEndContract.Text = Dr["EndContract"].ToString();
          txtRemarks.Text = Dr["Remarks"].ToString();
          txtPhone.Text = Dr["Phone"].ToString();
          txtPhone2.Text = Dr["Phone2"].ToString();
          txtAccountNumber.Text = Dr["AccountNumber"].ToString();
          txtFax.Text = Dr["Fax"].ToString();
          txtEmail.Text = Dr["Email"].ToString();
          txtWebSite.Text = Dr["WebSite"].ToString();
          txtMaximum.Text = Dr["Maximum"].ToString();


                if (string.IsNullOrEmpty(Dr["ISCusSupp"].ToString() ))
                {
                    chISCusSupp.Checked = false;
                }
                else
                {
                    chISCusSupp.Checked =(Boolean) Dr["ISCusSupp"];
                }
                
                if (string.IsNullOrEmpty(Dr["SupplierID"].ToString()))
                {
                    txtSupplierID.Text = "";
                    txtSupplierName.Text = "";
                }
                else
                {
                    Suppliers_cls Suppliers_cls = new Suppliers_cls();
                    DataTable dt_supp=    Suppliers_cls.Details_Suppliers(Dr["SupplierID"].ToString());
                    txtSupplierID.Text = dt_supp.Rows[0]["SupplierID"].ToString();
                    txtSupplierName.Text = dt_supp.Rows[0]["SupplierName"].ToString();
                }




                if (Convert.ToBoolean(Dr["Status"]) == true)
                {
                    rb1.Checked = true;
                }
                else
                {
                    rb2.Checked = true;
                }


                if (Dr["CreditLimit"].ToString() == "1")
                {
                    R1.Checked = true;
                }
                else if (Dr["CreditLimit"].ToString() == "2")
                {
                    R2.Checked = true;
                }
                else if (Dr["CreditLimit"].ToString() == "3")
                {
                    R3.Checked = true;
                }




              
                if (Dr["SalesLavel"].ToString() == "1")
                {
                    comboBox1.SelectedIndex = 0;
                }
                else if (Dr["SalesLavel"].ToString() == "2")
                {
                    comboBox1.SelectedIndex = 1;
                }
                else if (Dr["SalesLavel"].ToString() == "3")
                {
                    comboBox1.SelectedIndex = 2;
                }

                //=========================================================================
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtMaximum_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtNationalID_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void Customers_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtDebit_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void chISCusSupp_CheckedChanged(object sender, EventArgs e)
        {
            if (chISCusSupp.Checked)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                Suppliers_Search_Form frm = new Suppliers_Search_Form();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    txtSupplierName.BackColor = System.Drawing.Color.White;
                    txtSupplierID.Text = frm.DGV1.CurrentRow.Cells["SupplierID"].Value.ToString();
                    txtSupplierName.Text = frm.DGV1.CurrentRow.Cells["SupplierName"].Value.ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
    }
}
