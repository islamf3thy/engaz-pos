﻿using System; 
using System.Data; 
using System.Linq; 
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls; 
using System.IO;
using System.Linq.Expressions;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils.Menu;
using System.Collections.ObjectModel;
using DevExpress.XtraReports.UserDesigner; 
using ByStro.RPT;
using ByStro.Clases;
using ByStro.DAL;

namespace ByStro.Forms
{
    public partial class frm_BarcodeTemplates : XtraForm
    {
        RPT.rtp_Barcode report;
        public frm_BarcodeTemplates()
        {
            InitializeComponent();
          
        }


        private void GridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as DAL.BarcodeTemplate;

            if (report == null)
                report = new RPT.rtp_Barcode();
            MemoryStream stream = new MemoryStream();
            report.SaveLayout(stream);
            row.Template = new System.Data.Linq.Binary(stream.ToArray());
            report = null;
        }

        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = e.Row as DAL.BarcodeTemplate;
            if (row == null) return;
            if (string.IsNullOrEmpty(row.Name))
            {
                e.Valid = false;
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Name)], "برجاء ادخال الاسم ");

            }
        }

        DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);






        private void frm_BarcodeTemplates_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (gridView1.HasColumnErrors)
            {
                e.Cancel = true;
                return;
            }
            db.SubmitChanges();
        }

        private void frm_BarcodeTemplates_Load(object sender, EventArgs e)
        {


            gridControl1.DataSource = db.BarcodeTemplates.Select<DAL.BarcodeTemplate, DAL.BarcodeTemplate>((Expression<Func<DAL.BarcodeTemplate, DAL.BarcodeTemplate>>)
                 (x => x));
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            gridView1.InitNewRow += GridView1_InitNewRow;
            gridView1.PopupMenuShowing += GridView1_PopupMenuShowing;
            gridView1.Columns["ID"].Visible =
            //gridView1.Columns["Rows"].Visible =
            //gridView1.Columns["Coulmns"].Visible =
            //gridView1.Columns["MGR_T"].Visible =
            //gridView1.Columns["MGR_L"].Visible =
            //gridView1.Columns["MGR_R"].Visible =
            //gridView1.Columns["MGR_B"].Visible =
            //gridView1.Columns["PG_W"].Visible =
            //gridView1.Columns["PG_H"].Visible =
            //gridView1.Columns["PG_H"].Visible =
            //gridView1.Columns["PaperType"].Visible =
            //gridView1.Columns["Printer"].Visible =
            gridView1.Columns["Template"].Visible = false;
            gridView1.Columns["Name"].Caption = "اسم النموذج";
            gridView1.Columns["IsDefault"].Caption = "افتراضي";
            gridView1.Columns["IsDefault"].OptionsColumn.AllowEdit = false;
            gridView1.CellValueChanged += GridView1_CellValueChanged;
            gridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            gridView1.RowCellClick += GridView1_RowCellClick;
            //foreach (int num in Enum.GetValues(typeof(PaperKind)))
            //    this.cb_Papers.Properties.Items.Add(new ImageComboBoxItem(Enum.GetName(typeof(PaperKind), (object)num), (object)Enum.GetName(typeof(PaperKind), (object)num), -1));
            //foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            //    comboBoxEdit1.Properties.Items.Add(strPrinter);


        }

        private void GridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Column.FieldName == "IsDefault" )
            {
                var row = gridView1.GetRow(e.RowHandle) as BarcodeTemplate;
                if (row == null) return;
                row.IsDefault = true;
                var list = gridView1.DataSource as Collection<DAL.BarcodeTemplate>;
                for (int i = 0; i < list.Count; i++)
                    if (i != e.RowHandle)
                        list[i].IsDefault = false;

            }
        }

        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
           
            var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            if (row == null) return;
            EditingReport = new RPT.rtp_Barcode(); 
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(row.Template.ToArray(), 0, row.Template.ToArray().Length);
                EditingReport.LoadLayout(stream);
               
            }
        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            db.SubmitChanges();
        }

        private void GridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == GridMenuType.Row)
            {
                var menu = e.Menu as GridViewMenu;
                var itm = new DXMenuItem("تعديل التصميم");
                itm.Click += Itm_Click;
                menu.Items.Add(itm);

                var copy = new DXMenuItem("نسخ");
                copy.Click += Copy_Click;
                menu.Items.Add(copy);

                var delete = new DXMenuItem("حذف");
                delete.Click += Delete_Click;
                menu.Items.Add(delete);

                if (report != null)
                {
                    var past = new DXMenuItem("لصق");
                    past.Click += Past_Click;
                    menu.Items.Add(past);
                }
                var reset = new DXMenuItem("اعاده تعيين");
                reset.Click += Reset_Click;
                menu.Items.Add(reset);

            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            if(row != null)
            {
                if(XtraMessageBox.Show("هل تريد الحذف", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    (gridView1.DataSource as Collection<BarcodeTemplate>).Remove(row);
                }
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("هل تريد اعاده تعيين هذا التصميم ؟ ", "", MessageBoxButtons.YesNo) != DialogResult.Yes) return;
            var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            var rpt = new RPT.rtp_Barcode();
            using (MemoryStream stream = new MemoryStream())
            {
                rpt.SaveLayout(stream);
                row.Template = new System.Data.Linq.Binary(stream.ToArray());
            }

        }

        private void Past_Click(object sender, EventArgs e)
        {
            var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            if (report == null)
                report = new RPT.rtp_Barcode();
            using (MemoryStream stream = new MemoryStream())
            {
                report.SaveLayout(stream);
                row.Template = new System.Data.Linq.Binary(stream.ToArray());
                report = null;
            }
        }

        private void Copy_Click(object sender, EventArgs e)
        {
            var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            using (MemoryStream stream = new MemoryStream())
            {


                stream.Write(row.Template.ToArray(), 0, row.Template.ToArray().Length);
                if (report == null)
                    report = new RPT.rtp_Barcode();
                report.LoadLayout(stream);
            }
        }

        private void Itm_Click(object sender, EventArgs e)
        {
            db.SubmitChanges();

            var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            // EditingReport = row.ID;
            EditingReport = new RPT.rtp_Barcode();

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(row.Template.ToArray(), 0, row.Template.ToArray().Length);
                EditingReport.LoadLayout(stream);
            }






            XRDesignMdiController mdiController;
            //  rpt.ShowDesigner();
            XRDesignForm form = new XRDesignForm();
            mdiController = form.DesignMdiController;

            // Handle the DesignPanelLoaded event of the MDI controller,
            // to override the SaveCommandHandler for every loaded report.
            mdiController.DesignPanelLoaded +=
                new DesignerLoadedEventHandler(mdiController_DesignPanelLoaded);

            // Open an empty report in the form.
            mdiController.OpenReport(EditingReport);

            try
            {

            
            // Show the form.
            form.ShowDialog();
          
            }
            catch
            {
            form.ShowDialog();

            }
            finally
            {
                if (mdiController.ActiveDesignPanel != null)
                {
                    mdiController.ActiveDesignPanel.CloseReport();
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    EditingReport.SaveLayout(stream);
                    row.Template = new System.Data.Linq.Binary(stream.ToArray());
                }
                db.SubmitChanges();
            }


        }

        static rtp_Barcode EditingReport;


        void mdiController_DesignPanelLoaded(object sender, DesignerLoadedEventArgs e)
        {
            XRDesignPanel panel = (XRDesignPanel)sender;
            panel.AddCommandHandler(new SaveCommandHandler(panel));
        }
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;

            public SaveCommandHandler(XRDesignPanel panel)
            {
                this.panel = panel;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command,
                object[] args)
            {
                // Save the report.
                Save();
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command,
                ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                                   command == ReportCommand.SaveFileAs);
                return !useNextHandler;
            }

            void Save()
            {


                // Write your custom saving here.
                // ... // For instance:
                //panel.Report.Name = panel.Report.GetType().Name;
                //string Path = rpt_Master.ReportPath + "\\" + panel.Report.Name + ".xml";
                //System.IO.Directory.CreateDirectory(rpt_Master.ReportPath);
                //panel.Report.SaveLayout(Path);
                //// Prevent the "Report has been changed" dialog from being shown.
                panel.ReportState = ReportState.Saved;
            }
        }

        private void spinEdit1101_EditValueChanged(object sender, EventArgs e)
        {
            //SetAndSave();
            //var row = gridView1.GetFocusedRow() as DAL.BarcodeTemplate;
            //using (MemoryStream stream = new MemoryStream())
            //{
            //    EditingReport.SaveLayout(stream);
            //    row.Template = new System.Data.Linq.Binary(stream.ToArray());
            //}
        }
     
    

        private void cb_Papers_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    PaperKind paperKind = (PaperKind)Enum.Parse(typeof(PaperKind), cb_Papers.Text);
            //    layoutControlItem3.Visibility = layoutControlItem2.Visibility = (paperKind == PaperKind.Custom) ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //}
            //catch (Exception)
            //{
            //    cb_Papers.ErrorText = "نوع الورقه غير صحيح";


            //}
            
        }
    }
}
    
