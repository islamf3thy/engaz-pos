﻿using ByStro.RPT;
using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;

using System.Windows.Forms;


namespace ByStro.PL
{
    public partial class BillSalesR_frm : Form
    {
        public BillSalesR_frm()
        {
            InitializeComponent();
            toolStripButton2.Click += Forms.frm_SearchItems.ShowSearchWindow;

        }
        Sales_CLS cls = new Sales_CLS();
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        AVG_cls AVG_cls = new AVG_cls();
        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();

        string TransID = "0";
        string TreasuryID = "0";
        // Open New Bill
        public Boolean OpenNewBill = false;
        int UserID;
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);

            UserID = Properties.Settings.Default.UserID;
            FillCurrency();
            FillPayType();
            btnNew_Click(null, null);
          

        }


        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }


        public void FillPayType()
        {
            //string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
           // combPayType.Text = cusName;

        }
















        private void DGV1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            lastQty();
        }

        double last_Qty = 0;
        void lastQty()
        {
            try
            {
                last_Qty = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }








        Prodecut_cls Prodecut_CLS = new Prodecut_cls();

        DataTable dt_Prodecut = new DataTable();





        #region" اضافة الي الداتا قرد فيو   "
        //string AVG_Price = "0";

        private void SumDgv()
        {
            double sum = 0;
            Double M_vat = 0;
            double byTotalPrise = 0;
            Double QTY = 0;
        

            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                sum += Convert.ToDouble(DGV1.Rows[i].Cells["ReturnTotalPrice"].Value);
                M_vat += Convert.ToDouble(DGV1.Rows[i].Cells["ReturnVat"].Value);
                byTotalPrise += Convert.ToDouble(DGV1.Rows[i].Cells["TotalBayPrice"].Value)* double.Parse(DGV1.Rows[i].Cells["ReturnQuantity"].Value.ToString());
                QTY += double.Parse(DGV1.Rows[i].Cells["ReturnQuantity"].Value.ToString());
          
            }
            txtTotalInvoice.Text = sum.ToString();
            txtVatValue.Text = M_vat.ToString();
            txtReturnTotalBayPrice.Text = byTotalPrise.ToString();
            lblQTY.Text = QTY.ToString();
        }
        private void AddRowDgv(string ID, string ProdecutID, string ProdecutName, string Unit, string UnitFactor, string UnitOperating, string Quantity, string ReturnQuantity, string Price, string BayPrice, string ReturnVat, string MainVat, string ReturnTotalPrice, string StoreID, string StoreName)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ID"].Value = ID;

                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
                DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["ReturnQuantity"].Value = ReturnQuantity;
                DGV1.Rows[lastRows].Cells["BayPrice"].Value = BayPrice;
                DGV1.Rows[lastRows].Cells["TotalBayPrice"].Value = Convert.ToDouble(BayPrice) * Convert.ToDouble(ReturnQuantity);
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["ReturnVat"].Value = ReturnVat;
                DGV1.Rows[lastRows].Cells["MainVat"].Value = MainVat;
                DGV1.Rows[lastRows].Cells["ReturnTotalPrice"].Value = ReturnTotalPrice;
                DGV1.Rows[lastRows].Cells["StoreID"].Value = StoreID;
                DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }




        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveBill(false);
        }


        private void SaveBill(Boolean print)
        {

            try
            {


                if (lblCount.Text == "0")
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }



                if (combPayKind.SelectedIndex == 0)
                {
                    if (DGVPayType.Rows.Count == 0)
                    {
                        AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtNetInvoice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtNetInvoice.Text);
                        ClearPayType();
                        SumPayType();
                    }
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }

                double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
                if (combPayKind.SelectedIndex == 1)
                {
                    paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
                }

                DataTable dt_TreasuryMovement = TreasuryMovement_CLS.Details_TreasuryMovement_Return(txtMainID.Text, txtBillTypeID.Text);
                if (dt_TreasuryMovement.Rows.Count == 0)
                {
                    //TreasuryMovement_CLS.DeleteTreasuryMovement();
                    TreasuryID = TreasuryMovement_CLS.MaxID_TreasuryMovement();
                    TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, txtCustomerName.Text, "0", paidInvoice.ToString(), this.Text + " : " + combPayKind.Text, paidInvoice.ToString(), txtNetInvoice.Text, UserID);

                }
                else
                {
                    TreasuryMovement_CLS.UpdateTreasuryMovement(dt_TreasuryMovement.Rows[0]["ID"].ToString(), txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, txtCustomerName.Text, "0", paidInvoice.ToString(), this.Text + " : " + combPayKind.Text, paidInvoice.ToString(), txtNetInvoice.Text);

                }
                //====Bay_Main=========================================================================================================================================================================================================================================================== 
                double x = Convert.ToDouble(txtMainNetInvoice.Text) - Convert.ToDouble(txtNetInvoice.Text);
                double y = Convert.ToDouble(txtTotalBayPrice.Text) - Convert.ToDouble(txtReturnTotalBayPrice.Text);
                double u = x - y;
                cls.UpdateSales_Main(txtMainID.Text, txtNetInvoice.Text,txtpaid_Invoice.Text, txtReturnTotalBayPrice.Text, (x-y).ToString());
         
                //====InsertSales_Details===========================================================================================================================================================================================================================================================

                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    Double R_Quentity = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value.ToString()) - Convert.ToDouble(DGV1.Rows[i].Cells["ReturnQuantity"].Value.ToString());
                    Double R_ByTotalPrice = R_Quentity * Convert.ToDouble(DGV1.Rows[i].Cells["BayPrice"].Value.ToString());
                    cls.Update_SalesReturn_Details(DGV1.Rows[i].Cells["ID"].Value.ToString(), DGV1.Rows[i].Cells["ReturnQuantity"].Value.ToString(), DGV1.Rows[i].Cells["ReturnVat"].Value.ToString(), DGV1.Rows[i].Cells["ReturnTotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["ReturnCurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["ReturnCurrencyTotal"].Value.ToString(), R_ByTotalPrice.ToString());
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), false);
                }




                #region // Pay
                PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = "عميل" + " / " + txtCustomerName.Text;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text,0, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion










                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }









        private void txtSum_TextChanged(object sender, EventArgs e)
        {

            //try
            //{

            //    if (txtSum.Text.Trim() != "")
            //    {
            //        txtArbic.Text = DAL.DataAccessLeayr.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtSum.Text), 2, "", "", true, true);

            //    }
            //    else
            //    {
            //        txtArbic.Text = "";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }





        // change unit 
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                BillSales_Search_frm frm = new BillSales_Search_frm();
                frm.ReturnInvoice = true;
                frm.Text = this.Text;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }
                btnSave.Enabled = true;

                DataTable dt = cls.Details_Sales_Main(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMainID.Text = Dr["MainID"].ToString();
                D1.Value = DateTime.Now;// Convert.ToDateTime(Dr["MyDate"]);
                txtAccountID.Text = Dr["CustomerID"].ToString();
                txtCustomerName.Text = Dr["CustomerName"].ToString();
                // txtPhone.Text = Dr["Phone"].ToString();
                txtNote.Text = Dr["Note"].ToString();
                combPayKind.Text = Dr["TypeKind"].ToString();
                txtTotalProfits.Text = Dr["TotalProfits"].ToString();
                txtTotalDescound.Text = Dr["Discount"].ToString();
                txtMainNetInvoice.Text = Dr["NetInvoice"].ToString();
                txtTotalBayPrice.Text = Dr["TotalBayPrice"].ToString();
                // txtRestInvoise.Text = Dr["RestInvoise"].ToString();

                TransID = Dr["TransID"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();


                // txtVatValue .Text= Dr["VatValue"].ToString();

                //=========================================================================================================================================================================
                dt.Clear();
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                //=========================================================================================================================================================================
                dt = cls.Details_Sales_Details(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    AddRowDgv(dt.Rows[i]["ID"].ToString(), dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["ReturnQuantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["BayPrice"].ToString(), dt.Rows[i]["ReturnVat"].ToString(), dt.Rows[i]["MainVat"].ToString(), dt.Rows[i]["ReturnTotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString());
                //}


                // PyeType
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString(), txtBillTypeID.Text);
                for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                {
                    AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Credit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                }
                SumPayType();



                lblCount.Text = DGV1.Rows.Count.ToString();
                SumDgv();
                //btnUpdate.Enabled = true;
                //btnDelete.Enabled = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }




        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DGV1.Rows.Count > 0)
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }
        }









        //private void Add_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DGV1.Rows.Add();
        //        DGV1.ClearSelection();
        //        DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
        //        var lastRows = DGV1.Rows.Count - 1;
        //        DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
        //        DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
        //        DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
        //        DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
        //        DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
        //        DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
        //        DGV1.Rows[lastRows].Cells["Price"].Value = Price;
        //        DGV1.Rows[lastRows].Cells["ReturnVat"].Value = ReturnVat;
        //        DGV1.Rows[lastRows].Cells["ReturnTotalPrice"].Value = ReturnTotalPrice;
        //        DGV1.Rows[lastRows].Cells["StoreID"].Value = StoreID;
        //        DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;


        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}


        private void txtChange(object sender, EventArgs e)
        {
            SumTotalTXT();
        }

        private void txtpaid_Invoice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Double paid = Convert.ToDouble(txtSumPayType.Text);
                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) - paid).ToString();
            }
            catch
            {
                return;
            }
        }

        private void combPayKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (combPayKind.SelectedIndex == 0)
            //{
            //    // Double paid = Convert.ToDouble(txtpaid_Invoice.Text) + Convert.ToDouble(txtSumPayType.Text);
            //    txtpaid_Invoice.Text = txtNetInvoice.Text;

            //}
            //else
            //{
            //    txtpaid_Invoice.Text = "0";
            //}
        }


        private Double QtyDGV1(String ProdecutID, String StoreID)
        {
            Double XQuantity = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == ProdecutID && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == StoreID)
                {
                    // الكمية الموجودة في الداتا جرد فيو
                    XQuantity += Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value);
                }
            }
            return XQuantity;
        }

        private void DGV1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                if (Convert.ToDouble(DGV1.CurrentRow.Cells["ReturnQuantity"].Value) > Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value))
                {
                    MessageBox.Show("كمية المرتجع اكبر من الكمية المباعة يرجي ادخال كمية مناسبة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    DGV1.CurrentRow.Cells["ReturnQuantity"].Value = last_Qty;

                }



                // ضرب الكمية المعدلة في السعر
                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["ReturnQuantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value);

                Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());

                Double total = x * MainVat_;
                double vat_ = total / 100;

                DGV1.CurrentRow.Cells["ReturnCurrencyVat"].Value = (vat_).ToString();
                DGV1.CurrentRow.Cells["ReturnCurrencyTotal"].Value = x + vat_;
                Double Rx = Convert.ToDouble(DGV1.CurrentRow.Cells["ReturnQuantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["BayPrice"].Value);
                DGV1.CurrentRow.Cells["TotalBayPrice"].Value = Rx;

                DGV1.CurrentRow.Cells["ReturnVat"].Value = (double.Parse(DGV1.CurrentRow.Cells["ReturnCurrencyVat"].Value.ToString())* double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                 DGV1.CurrentRow.Cells["ReturnTotalPrice"].Value = (double.Parse(DGV1.CurrentRow.Cells["ReturnCurrencyTotal"].Value.ToString()) * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                //اجمالي
                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataAccessLayer.CS_13 == false)
                {
                    MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (Mass.Delete() == true)
                {
                    //====Delete Quentity ========================================================================================================================================================================================================================================================
                    DataTable dt = cls.Details_Sales_Details(txtMainID.Text);


                    cls.Delete_Sales_Details(txtMainID.Text);
                    TreasuryMovement_CLS.DeleteTreasuryMovement(TreasuryID);
                    // Pay
                    PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);

                    cls.Delete_BillStoreMain(txtMainID.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(), false);
                    }


                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            // Use Fast Inpout

            //===============================================================================================================================================================
            txtMainID.Text = "";// cls.MaxID_Sales_Main();
            combPayKind.SelectedIndex = BillSetting_cls.kindPay;
            txtNote.Text = "";
            txtTotalInvoice.Text = "0";
            txtpaid_Invoice.Text = "0";
            txtRestInvoise.Text = "0";
            txtVatValue.Text = "0";
            D1.Value = DateTime.Now;

            lblQTY.Text = "0";


            txtAccountID.Text = "";
            txtCustomerName.Text = "";



            //=================================================================================

            // txtpaid_Invoice.Text = "0";
            TransID = "0";
            TreasuryID = "0";
            txtTotalDescound.Text = "0";
            txtReturnTotalBayPrice.Text = "0";
            txtTotalBayPrice.Text = "0";
            txtMainNetInvoice.Text = "0";
            txtTotalProfits.Text = "0";

            lblCount.Text = "0";
            //===============================================================================================================================================================
            if (BillSetting_cls.UseCrrencyDefault == true)
            {
                Currency_cls Currency = new Currency_cls();
                DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
            }
            else
            {
                combCurrency.Text = null;
                txtCurrencyRate.Text = "";

            }


            btnSave.Enabled = false;
            ClearPayType();
            #region UsekindPay

            //===============================================================================================================================================================


            if (BillSetting_cls.UsekindPay == true)
            {
                PayType_cls PayType_cls = new PayType_cls();
                DataTable dtPayType = PayType_cls.Details_PayType(BillSetting_cls.PayTypeID);
                if (dtPayType.Rows.Count > 0)
                {
                    combPayType.Text = dtPayType.Rows[0]["PayTypeName"].ToString();
                }
            }
            else
            {
                combPayType.Text = null;
            }



            //===============================================================================================================================================================

            #endregion

            txtSumPayType.Text = "0";
            DGV1.DataSource = null;
            DGV1.Rows.Clear();
            DGVPayType.DataSource = null;
            DGVPayType.Rows.Clear();

        }



        private void BillSales_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {

                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }



                else if (e.KeyCode == Keys.F5)
                {
                    button5_Click(null, null);
                }

                if (e.KeyCode == Keys.F4)
                {
                    if (tabControl1.SelectedIndex == 0)
                    {
                        tabControl1.SelectedIndex = 1;
                        txtPayValue.Focus();
                    }
                    else
                    {
                        tabControl1.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }




















        private void AddDgv_QuentityStore(DataGridView dgv, string ProdecutID, string StoreName, string Balence, string ChangeAllUnit)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                dgv.Rows[lastRows].Cells["StoreName"].Value = StoreName;
                dgv.Rows[lastRows].Cells["Balence"].Value = Balence;
                dgv.Rows[lastRows].Cells["Change_Unit"].Value = ChangeAllUnit;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private string ChangeUnit(string Quentity, string FiestUnit, string FiestUnitFactor, string SecoundUnit, string SecoundUnitOperating, string SecoundUnitFactor, string ThreeUnit, string ThreeUnitOperating, string ThreeUnitFactor)
        {
            if (Quentity == "" || Quentity == "0")
            {
                return "";
            }
            string Resalet = "";
            if (ThreeUnitOperating != "")
            {
                decimal n = Convert.ToDecimal(Quentity) / Convert.ToDecimal(ThreeUnitOperating);
                decimal H = Math.Floor(n);
                decimal b = Convert.ToDecimal(H) * Convert.ToDecimal(ThreeUnitOperating);
                decimal namber3 = Convert.ToDecimal(Quentity) - Convert.ToDecimal(b);
                decimal F = Convert.ToDecimal(H) / Convert.ToDecimal(SecoundUnitOperating);
                decimal namber1 = Math.Floor(F);
                decimal Fd = Convert.ToDecimal(namber1) * Convert.ToDecimal(SecoundUnitOperating);
                decimal namer2 = Convert.ToDecimal(H) - Convert.ToDecimal(Fd);
                Resalet = namber1 + "  " + FiestUnit + "  // " + namer2 + "  " + SecoundUnit + "  // " + namber3 + " " + ThreeUnit;
                //======================================================================================================================================================================================================================================================

            }

            else if (SecoundUnitOperating != "")
            {

                var y = Convert.ToDouble(Quentity) / Convert.ToDouble(SecoundUnitOperating);
                var Hnamber1 = Math.Floor(y);
                var dFd = Convert.ToDouble(Hnamber1) * Convert.ToDouble(SecoundUnitOperating);
                var hnamer2 = Convert.ToDouble(Quentity) - Convert.ToDouble(dFd);
                Resalet = Hnamber1 + " " + FiestUnit + "  // " + hnamer2 + " " + SecoundUnit;
                //======================================================================================================================================================================================================================================================

            }
            else if (FiestUnit != "")
            {
                Resalet = Quentity + "  " + FiestUnit;
                //======================================================================================================================================================================================================================================================
            }

            return Resalet;
        }



        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayType.SelectedIndex >= 0)
                {
                    txtPayValue.Focus();
                }

            }
        }

        private void txtValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtPayValue.Text.Trim()))
                {
                    txtStatement.Focus();
                }

            }
        }

        private void ClearPayType()
        {
            combPayType.Text = null;
            txtPayValue.Text = "";
            txtStatement.Text = "";
            combPayType.Focus();
        }

        private void SumPayType()
        {
            try
            {

                double Sum = 0;
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    Sum += Convert.ToDouble(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString());
                }
                txtSumPayType.Text = Sum.ToString();
                txtpaid_Invoice.Text = txtSumPayType.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {

                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }


                    if (combPayType.SelectedIndex < 0)
                    {
                        combPayType.BackColor = Color.Pink;
                        combPayType.Focus();
                        return;
                    }
                    if (txtPayValue.Text.Trim() == "")
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    if (double.Parse(txtPayValue.Text) == 0)
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    for (int i = 0; i < DGVPayType.Rows.Count; i++)
                    {
                        if (DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString() == combPayType.SelectedValue.ToString())
                        {
                            combPayType.BackColor = Color.Pink;
                            combPayType.Focus();
                            return;
                        }
                    }
                    AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text,(double.Parse(txtPayValue.Text)*double.Parse(txtCurrencyRate.Text)) .ToString(), txtStatement.Text,combCurrency.SelectedValue.ToString(),combCurrency.Text,txtCurrencyRate.Text,txtPayValue.Text);
                    ClearPayType();
                    SumPayType();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void DGVPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.SelectedRows.Count == 0)
                {
                    return;
                }
                if (Mass.Delete() == true)
                {
                    foreach (DataGridViewRow r in DGVPayType.SelectedRows)
                    {

                        DGVPayType.Rows.Remove(r);

                    }
                    SumPayType();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;

        }

        private void txtPayValue_TextChanged(object sender, EventArgs e)
        {
            txtPayValue.BackColor = Color.White;
        }

        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }





        private void SumTotalTXT()
        {
            try
            {

                Double total = Convert.ToDouble(txtTotalInvoice.Text);
                Double Discount = 0;
                if (!string.IsNullOrEmpty(txtTotalDescound.Text))
                {
                    Discount = Convert.ToDouble(txtTotalDescound.Text);
                }
                txtNetInvoice.Text = (Convert.ToDouble(txtTotalInvoice.Text) - Discount).ToString();
                Double paid = Convert.ToDouble(txtSumPayType.Text);
                txtpaid_Invoice.Text = paid.ToString();
                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) - paid).ToString();
                //if (btnSave.Enabled)
                //{
                //    if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                //    {
                //        txtPayValue.Text = (double.Parse(txtNetInvoice.Text) / double.Parse(txtCurrencyRate.Text)).ToString();

                //    }
                //    else
                //    {
                //        txtPayValue.Text = txtNetInvoice.Text;
                //    }
                //}
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message.ToString());
                // return;
            }

        }


        #endregion

        private void combCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }

        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
    }
}