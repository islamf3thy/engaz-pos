﻿using ByStro.BL;
using ByStro.Clases;
using ByStro.DAL;
using ByStro.Forms;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class LoginUser_Form : XtraForm
    {
        public LoginUser_Form()
        {

            InitializeComponent();
            try
            {
                backgroundWorker1.RunWorkerAsync();
                DataAccessLayer.Finull_Sarial = BL.Final_key.GetHash(Properties.Settings.Default.computerID);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
        // DataAccessLayer DataAccessLayer = new DataAccessLayer();


        public void LoginUser_Form_Load(object sender, EventArgs e)
        {
            if(CheckIfConnectionUp() == false)
            {
                ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
              //  frm.add = "Main";
                frm.ShowDialog(this);
            }

            //CreateNewCompany_frm frm = new CreateNewCompany_frm();

            //frm.Show();
            simpleButton1.Visible = CheckIfConnectionUp();

            timer1.Start();
           // lblHora.Text = DateTime.Now.ToLongTimeString();
            lblVersion.Text = DataAccessLayer.Version;


             #region"User name and password and chb"
            if (Properties.Settings.Default.RemmberPassword == true)
            {

                txtUserName.Text = Properties.Settings.Default.UserName; 
                txtPassword.Text = Properties.Settings.Default.UserPassword;
                chRememberMe.Checked = true;
            }

            #endregion

            if (Properties.Settings.Default.Finull_Sarial == DataAccessLayer.Finull_Sarial)
            {
                butActivationtheprogram.Enabled = false;
            }


            txtUserName.Focus();


        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Properties.Settings.Default.computerID = BL.ComputerInfo.GetComputerId();
                Properties.Settings.Default.Save();
                //MessageBox.Show("", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void sQLConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            frm.ShowDialog();

        }

        private void sERUELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SerilFull_FORM FRM = new SerilFull_FORM();
            FRM.ShowDialog(this);
        }



        private void LoginUser_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void LoginUser_Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void LoginUser_Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
               // button1.PerformClick(); ;
            }
        }



        private void button1_MouseHover(object sender, EventArgs e)
        {
            //button1.Size = new Size(100, 92);

        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            // button1.BackgroundImageLayout = ImageLayout.Stretch;
            //button1.Size = new Size(90, 85);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckIfConnectionUp() == false)
            {
                XtraMessageBox.Show("لا يمكن الاتصال بالخادم , تاكد من اعدادات الاتصال", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }if(CheckIfDataBaseExist() == false)
            {
                if (XtraMessageBox.Show("تم الاتصال بالخادم بنجاح ولاكن قاعده البيانات غير موجوده ويجب تهئتها اولا قبل استخدام البرنامج  \n هل تريد انشاء قاعده بيانات جديده ؟"
                    , "", MessageBoxButtons.YesNo , MessageBoxIcon.Error) == DialogResult.Yes) {
                    simpleButton1.PerformClick();
                };
                return;
            }

            var p = Properties.Settings.Default;



                #region MyRegion
                CombanyInformatuon_cls CombanyInformatuon_cls = new CombanyInformatuon_cls();
                DataTable dt_CombanyInformatuon = CombanyInformatuon_cls.Details_CombanyData();
                if (dt_CombanyInformatuon.Rows.Count == 0)
                {
                    CombanyInformatuon_FormAR frm_company = new CombanyInformatuon_FormAR();
                    frm_company.ShowDialog(this);
                    return;
                }




            #endregion




            #region"تفعيل البرنامج "
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            Trail_cls sales_CLS = new Trail_cls();
                Double countSales = Convert.ToDouble(db.Inv_Invoices.Where(x=>x.InvoiceType == (int)MasterClass.InvoiceType .SalesInvoice ).Count());
                try
                {
                    if (p.Finull_Sarial != DataAccessLayer.Finull_Sarial)
                    {
                        p.LicenseType = "Trail";
                        p.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                        p.Save();
                        if (countSales >= Final_key.SalesNumber)
                        {
                            this.Hide();
                            SerilFull_FORM FrmAc = new SerilFull_FORM();
                            FrmAc.ShowDialog();
                            Application.Exit();
                            return;
                        }

                    }
                    else
                    {
                        RegistryKey regkey = default(RegistryKey);
                        regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\ByStor", true);
                        string code = "";
                        code = regkey.GetValue("System_Activation").ToString();
                        if (DataAccessLayer.DecryptData(code) == DataAccessLayer.Finull_Sarial)
                        {
                            p.LicenseType = "Full";
                            p.Save();
                        }
                        else
                        {
                            if (countSales >= Final_key.SalesNumber)
                            {
                                this.Hide();
                                SerilFull_FORM FrmAc = new SerilFull_FORM();
                                FrmAc.ShowDialog();
                                Application.Exit();
                                return;
                            }

                        }

                    }
                }
                catch (Exception)
                {

                    p.LicenseType = "Trail";
                    p.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                    p.Save();
                    if (countSales >= Final_key.SalesNumber)
                    {
                        this.Hide();
                        SerilFull_FORM FrmAc = new SerilFull_FORM();
                        FrmAc.ShowDialog();
                        Application.Exit();
                        return;
                    }
                }
                #endregion



                if (txtUserName.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال اسم المستخدم");
                    txtUserName.BackColor = Color.Pink;
                    txtUserName.Focus();
                    return;
                }
                if (txtPassword.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال كلمة المرور");
                    txtPassword.BackColor = Color.Pink;
                    txtPassword.Focus();
                    return;
                }

                String Admin = "User";
                if (txtUserName.Text == "STARTECH")
                {
                    txtPassword.Text = txtPassword.Text.ToUpper();
                }

                if (txtUserName.Text == "STARTECH" && txtPassword.Text.ToUpper() == "ARS1130S1032SS")
                {
                    Admin = "OK";
                    //DataAccessLayer.UserAddEdit = "1";
                    DataAccessLayer.UserNameLogIn = "مهندس محمد ";
                    Properties.Settings.Default.Save();
                    Main_frm frm = new Main_frm();
                    frm.Username.Text = "مهندس محمد";
                    frm.CHClearAllData.Visible = true;
                    frm.Maintenance_frm.Visible = true;
                    txtUserName.Text = ""; txtPassword.Text = "";
                    this.Hide();
                    frm.Show();
               

                }


                if (Admin == "User")
                {
                    #region "User"
                    UserPermissions_CLS cls = new UserPermissions_CLS();
                    DataTable Dt_NewLogIn = cls.Search_NewLogIn_UserPermissions();

                    if (Dt_NewLogIn.Rows.Count == 0)
                    {
                        User_Permetion_frm f_user = new User_Permetion_frm();
                        f_user.ShowDialog(this);
                        return;
                    }

                    //================================================================================================

                    DataTable dt = cls.Search_LogIn_UserPermissions(txtUserName.Text, txtPassword.Text, true);

                    if (dt.Rows.Count > 0)
                    {
                        Main_frm frm = new Main_frm();
                        dynamic Dr = dt.Rows[0];

                        DataAccessLayer.TypeUser = Dr["TypeUser"];

                        frm.Username.Text = Dr["EmpName"];
                        frm.CS_01.Visible = Dr["CS1"];
                        frm.CS_02.Visible = Dr["CS2"];
                        frm.CS_03.Visible = Dr["CS3"];
                        frm.CS_04.Visible = Dr["CS4"];
                        frm.CS_05.Visible = Dr["CS5"];
                        frm.CS_06.Visible = Dr["CS6"];
                        frm.CS_07.Visible = Dr["CS7"];
                        
                        frm.CS_09.Visible = Dr["CS9"];
                        frm.CS_10.Visible = Dr["CS10"];
                        frm.CS_11.Visible = Dr["CS11"];
                        DataAccessLayer.CS_12 = Dr["CS12"];
                        DataAccessLayer.CS_13 = Dr["CS13"];
                        frm.CS_14.Visible = Dr["CS14"];
                        frm.CS_15.Visible = Dr["CS15"];
                        DataAccessLayer.CS_16 = Dr["CS16"];
                        DataAccessLayer.CS_17 = Dr["CS17"];
                        frm.CS_18.Visible = Dr["CS18"];
                        frm.CS_19.Visible = Dr["CS19"];
                        frm.CS_20.Visible = Dr["CS20"];
                        frm.CS_21.Visible = Dr["CS21"];
                       
                        frm.CS_22.Visible = Dr["CS22"];
                        frm.CS_23.Visible = Dr["CS23"];
                        frm.CS_24.Visible = Dr["CS24"];
                        frm.CS_25.Visible = Dr["CS25"];
                        frm.CS25.Visible = Dr["CS25"];

                        frm.CS_26.Visible = Dr["CS26"];
                        frm.CS_27.Visible = Dr["CS27"];
                        frm.CS_28.Visible = Dr["CS28"];
                        frm.CS_29.Visible = Dr["CS29"];
                        frm.CS_30.Visible = Dr["CS30"];
                        frm.CS_31.Visible = Dr["CS31"];
                        frm.CS_32.Visible = Dr["CS32"];
                        frm.CS_33.Visible = Dr["CS33"];
                        frm.CS_34.Visible = Dr["CS34"];
                        frm.CS_35.Visible = Dr["CS35"];
                        frm.CS_36.Visible = Dr["CS36"];
                        frm.CS_37.Visible = Dr["CS37"];
                        frm.CS_38.Visible = Dr["CS38"];
                        frm.CS_40.Visible = Dr["CS39"];
                        frm.CS_39.Visible = Dr["CS40"];
                        frm.CS_41.Visible = Dr["CS41"];
                        frm.CS_42.Visible = Dr["CS42"];
                       
                        DataAccessLayer.CS_44 = Dr["CS44"];
                    
                        frm.CS_46.Visible = Dr["CS46"];
                        frm.CS_47.Visible = Dr["CS47"];
                        frm.CS_48.Visible = Dr["CS48"];
                        frm.CS_49.Visible = Dr["CS49"];
                        frm.CS_50.Visible = Dr["CS50"];
                        frm.CS_51.Visible = Dr["CS51"];
                        frm.CS_52.Visible = Dr["CS52"];
                        frm.CS_53.Visible = Dr["CS53"];
                        frm.CS_54.Visible = Dr["CS54"];
                        frm.CS_55.Visible = Dr["CS55"];
                        frm.CS_56.Visible = Dr["CS56"];
                        frm.CS_57.Visible = Dr["CS57"];
                        frm.CS_58.Visible = Dr["CS58"];
                        frm.CS_59.Visible = Dr["CS59"];
                        frm.CS_60.Visible = Dr["CS60"];
                        frm.CS_61.Visible = Dr["CS61"];
                     
                        frm.CS_62.Visible = Dr["CS62"];
                        //frm.CS_63.Visible = Dr["CS63"];
                        //frm.CS_64.Visible = Dr["CS64"];
                       // frm.CS_65.Visible = Dr["CS65"];
                        frm.CS_66.Visible = Dr["CS66"];
                        frm.CS_67.Visible = Dr["CS67"];
                        frm.CS_68.Visible = Dr["CS68"];
                        frm.CS_69.Visible = Dr["CS69"];
                        frm.CS_70.Visible = Dr["CS70"];
                        frm.CS_71.Visible = Dr["CS71"];
                        frm.CS_72.Visible = Dr["CS72"];
                        frm.CS_73.Visible = Dr["CS73"];


                        frm.CS_74.Visible = Dr["CS74"];
                        frm.CS_75.Visible = Dr["CS75"];
                        frm.CS_76.Visible = Dr["CS76"];
                        frm.CS_77.Visible = Dr["CS77"];
                        frm.CS_78.Visible = Dr["CS78"];
                        frm.CS_79.Visible = Dr["CS79"];
                        frm.CS_80.Visible = Dr["CS80"];
                        frm.CS_81.Visible = Dr["CS81"];

                 




                        Properties.Settings.Default.UserID = Dr["ID"];
                        frm.Username.Text = Dr["EmpName"];
                        Properties.Settings.Default.UserName = Dr["UserName"];
                        Properties.Settings.Default.myDate = DateTime.Now.ToString("yyy/MM/dd");
                        Properties.Settings.Default.Save();
                        // xxxxxxxxx قفل السنة 
                        frm.CS_52.Visible = false;





                   








                        if (chRememberMe.Checked == true)
                        {

                            Properties.Settings.Default.UserName = txtUserName.Text;
                            Properties.Settings.Default.UserPassword = txtPassword.Text;
                            Properties.Settings.Default.RemmberPassword = true;
                            Properties.Settings.Default.Save();
                        }

                        else
                        {

                            Properties.Settings.Default.UserName = "";
                            Properties.Settings.Default.UserPassword = "";
                            Properties.Settings.Default.RemmberPassword = false;
                            Properties.Settings.Default.Save();
                        }


                        SettingInvoice_cls SettingInvoice_cls = new SettingInvoice_cls();
                        DataTable dt_Setting = SettingInvoice_cls.Details_Setting();
                        if (dt_Setting.Rows.Count > 0)
                        {
                            Store_cls store_Cls = new Store_cls();
                            DataTable dt_storeUser = store_Cls.Search_Stores("");
                            BillSetting_cls.BillSetting(int.Parse(dt_Setting.Rows[0]["SettingID"].ToString()), (bool)dt_Setting.Rows[0]["UseStoreDefault"], dt_storeUser.Rows[0]["StoreID"].ToString(), (bool)dt_Setting.Rows[0]["UseCustomerDefault"], dt_Setting.Rows[0]["CustomerID"].ToString(), (bool)dt_Setting.Rows[0]["UsingFastInput"], (bool)dt_Setting.Rows[0]["ShowMessageQty"], (bool)dt_Setting.Rows[0]["ShowMessageSave"], int.Parse(dt_Setting.Rows[0]["kindPay"].ToString()), (bool)dt_Setting.Rows[0]["UseVat"], dt_Setting.Rows[0]["Vat"].ToString(), int.Parse(dt_Setting.Rows[0]["PrintSize"].ToString()), (bool)dt_Setting.Rows[0]["UseCrrencyDefault"], dt_Setting.Rows[0]["CurrencyID"].ToString(), (bool)dt_Setting.Rows[0]["NotificationProdect"], (bool)dt_Setting.Rows[0]["NotificationCustomers"], dt_Setting.Rows[0]["MaxBalance"].ToString(), (bool)dt_Setting.Rows[0]["UsekindPay"], dt_Setting.Rows[0]["PayTypeID"].ToString(),   (bool)dt_Setting.Rows[0]["UseCategory"], dt_Setting.Rows[0]["CategoryId"].ToString(), (bool)dt_Setting.Rows[0]["UseUnit"], dt_Setting.Rows[0]["UnitId"].ToString());
                        }



                        timer1.Stop();

                        this.Hide();
                        frm.Show();
                      
                    }
                    else
                    {
                        MessageBox.Show("ليس لديك صلاحية للعمل علي هذ البرنامج", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Properties.Settings.Default.UserName = "";
                        Properties.Settings.Default.UserPassword = "";
                        Properties.Settings.Default.RemmberPassword = false;
                        Properties.Settings.Default.Save();
                    }
                    #endregion
                }





             





                //MessageBox.Show(p.Trail.ToString());



 

        }

        private void MainForm()
        {
            Main_frm main = new Main_frm();
            main.CS_01.Visible = false;
            main.CS_10.Visible = false;
            main.CS_15.Visible = false;
            main.CS_19.Visible = false;
            main.CS_24.Visible = false;
            main.CS_29.Visible = false;
            main.CS_42.Visible = false;
            main.CS_46.Visible = false;
            main.CS_53.Visible = false;
            main.CS_58.Visible = false;
            main.CS_58.Visible = false;
            main.CS_75.Visible = false;
            main.Username.Visible = false;
            main.Fills.Visible = true;
            
            main.Show();
        }


        private void btnmin_Click(object sender, EventArgs e)
        {         
            WindowState = FormWindowState.Minimized;
        }

   
        private void btncerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("سوف يتم الخروج من البرنامج : هل تريد الاستمرار", "◄ Message | Star Tech ►", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
          //  lblHora.Text = dt.ToString("HH:MM:ss");
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {

                //txtUserName.PasswordChar = "*";
             
                
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            txtUserName.BackColor = Color.White;
            if (txtUserName.Text == "startech" || txtUserName.Text == "STARTECH")
            { 
                txtUserName.Text = txtUserName.Text.ToUpper();
                txtPassword.Focus();
            }
            else
            { 
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            txtPassword.BackColor = Color.White;
        }

        private void lblHora_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new frm_ResetAdminPassWord().ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            new frm_CreatNewCompany().ShowDialog();
        }
        public static  bool CheckIfConnectionUp()
        {
            var sqlbulder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
            sqlbulder.InitialCatalog = "master";
            var sqlcon = new SqlConnection(sqlbulder.ConnectionString ); 
            try
            {
                sqlcon.Open();
                sqlcon.Close();
                return true;
            }
            catch ( Exception ex)
            {
                return false;
            }
            
        }
        public static bool CheckIfDataBaseExist()
        {
            using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                return db.DatabaseExists();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            //  frm.add = "Main";
            frm.ShowDialog(this);
        }
    }
}
