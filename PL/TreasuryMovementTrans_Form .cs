﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;
using DevExpress.XtraGrid.Columns;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Linq;

namespace ByStro.PL
{
    public partial class TreasuryMovementTrans_Form : XtraForm 
    {
        public TreasuryMovementTrans_Form()
        {
            InitializeComponent();
        }

        RbtTreasuryMovement_cls cls = new RbtTreasuryMovement_cls();
        DataAccessLayer DataAccessLayer = new DataAccessLayer();
        private void CustomersTrans_Form_Load(object sender, EventArgs e)
        {
            datefrom.DateTime = DateTime.Now.Date;
            datetto.DateTime = DateTime.Now.Date;
            txt_1.Caption = "0";
            txt_2.Caption = "0";
            txt_3.Caption = "0";
            barButtonItem1.PerformClick();
            
        }
        DataTable dt_Print = new DataTable();

        List<GridColumn> IntColumns()
        {


            GridColumn VoucherID = new GridColumn();
            GridColumn VoucherType = new GridColumn();
            GridColumn VoucherDate = new GridColumn();
            GridColumn Remark = new GridColumn();
            GridColumn Debit = new GridColumn();
            GridColumn Credit = new GridColumn();
            GridColumn EmpName = new GridColumn();


            VoucherID.FieldName = "VoucherID";
            VoucherID.Caption = "رقم السند";
            VoucherID.Name = "VoucherID";
            VoucherID.Width = 150;

            VoucherType.FieldName = "VoucherType";
            VoucherType.Caption = "نوع الحركة";
            VoucherType.Name = "VoucherType";
            VoucherType.Width = 150;

            VoucherDate.FieldName = "VoucherDate";
            VoucherDate.Caption = "تاريخ السند";
            VoucherDate.Name = "VoucherDate";
            VoucherDate.Width = 120;
            // 
            // Remark
            // 
            Remark.FieldName = "Description";
            Remark.Caption = "البيــــــــــان";
            Remark.Name = "Remark";
            Remark.Width = 250;
            // 
            // Debit
            // 
            Debit.FieldName = "Income";
            Debit.Caption = "وارد";
            Debit.Name = "Income";
            Debit.Width = 130;
            // 
            // Credit
            // 
            Credit.FieldName = "Export";
            Credit.Caption = "صادر";
            Credit.Name = "Export";
            Credit.Width = 130;
            // 
            // EmpName
            // 
            EmpName.FieldName = "EmpName";
            //dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            // EmpName.DefaultCellStyle = dataGridViewCellStyle9;
            EmpName.Caption = "المستخدم";
            EmpName.Name = "EmpName";
            EmpName.Width = 150;
            return new List<GridColumn>() { VoucherID, VoucherType, VoucherDate, Remark, Debit, Credit, EmpName };


        }





        private void btnNew_Click(object sender, EventArgs e)
        {



            try
            {



                Decimal x = 0;
                Decimal y = 0;


                DataTable Dt2 = cls.TreasuryMovement( datefrom.DateTime , datetto.DateTime);
                //===========================================================================================
                DataTable Dt = cls.TreasuryMovement2( datefrom.DateTime);

                Decimal Sumincome = 0;
                Decimal SumExport = 0;
                if (Dt.Rows[0][0] == DBNull.Value == false)
                    Sumincome = Convert.ToDecimal(Dt.Rows[0][0]);

                if (Dt.Rows[0][1] == DBNull.Value == false)
                    SumExport = Convert.ToDecimal(Dt.Rows[0][1]);

                //====================================================================================

                SqlDataAdapter da_Print = new SqlDataAdapter(@"SELECT        dbo.TreasuryMovement.*, dbo.UserPermissions.EmpName  FROM  dbo.TreasuryMovement INNER JOIN
                         dbo.UserPermissions ON dbo.TreasuryMovement.UserAdd = dbo.UserPermissions.ID where VoucherID ='0' ORDER BY VoucherDate", DataAccessLayer.connSQLServer);
                dt_Print = new DataTable();

                da_Print.Fill(dt_Print);
                da_Print.Dispose();
                dt_Print.Clear();
                DataRow DR = dt_Print.NewRow();
                DR["VoucherID"] = "0";
                DR["VoucherType"] = "رصيد سابق";
                DR["VoucherDate"] = DateTime.Now.ToString("yyyy/MM/dd");
                DR["Description"] = "رصيد الفترة السابقة";
                DR["income"] = Sumincome.ToString();
                DR["Export"] = SumExport.ToString();
                dt_Print.Rows.Add(DR);
                x += Convert.ToDecimal(Sumincome.ToString());
                y += Convert.ToDecimal(SumExport.ToString());



                for (int i = 0; i < Dt2.Rows.Count; i++)
                {
                    DR = dt_Print.NewRow();
                    //DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherID"] = Dt2.Rows[i]["VoucherID"];
                    DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherDate"] = Dt2.Rows[i]["VoucherDate"];
                    DR["Description"] = Dt2.Rows[i]["Description"];
                    DR["income"] = Dt2.Rows[i]["income"];
                    DR["Export"] = Dt2.Rows[i]["Export"];
                    DR["EmpName"] = Dt2.Rows[i]["EmpName"];
                    dt_Print.Rows.Add(DR);
                    x += Convert.ToDecimal(Dt2.Rows[i]["income"]);
                    y += Convert.ToDecimal(Dt2.Rows[i]["Export"]);
                }


                txt_1.Caption = x.ToString();
                txt_2.Caption = y.ToString();
                Decimal D = y - x;
                txt_3.Caption = D.ToString();
                gridControl1.DataSource = dt_Print;
                gridView1.Columns.Clear();
                int Index = 0;
                foreach (var item in IntColumns())
                {
                    gridView1.Columns.Add(item);
                    item.VisibleIndex = Index;
                    item.OptionsColumn.AllowEdit = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Massage", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                btnNew_Click(null, null);
            else
                RPT.rpt_GridReport.Print(gridControl1, this.Text ,

                 " " + stlb1.Caption + " " + txt_1.Caption +
                 " " + stlb2.Caption + " " + txt_2.Caption +
                 " " + stlb3.Caption + " " + txt_3.Caption
                    , true);

        }



    }
}
