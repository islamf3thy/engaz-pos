﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class Store_frm : Form
    {
        public Store_frm()
        {
            InitializeComponent();
        }
        Store_cls cls = new Store_cls();
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
          //  ERB_Setting.SettingDGV(DGV1);
            btnNew_Click(null, null);
            Search_cls.FillcombUsers(combUser);

        }

        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void txtStoreName_TextChanged(object sender, EventArgs e)
        {
            txtStoreName.BackColor = Color.White;

        }

        private void txtCurrencyVal_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                // "Conditional"
                if (txtStoreName.Text.Trim() == "")
                {
                    txtStoreName.BackColor = Color.Pink;
                    txtStoreName.Focus();
                    return;
                }






                // " Search 
                DataTable DtSearch = cls.NameSearch_Stores(txtStoreName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtStoreName.Focus();
                    return;
                }


                // "Conditional"
                if (DGV1.Rows.Count==0)
                {
                    MessageBox.Show("يرجي ادخال المستخدمين", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        
                    return;
                }




                // Save
                txtID.Text = cls.MaxID_Stores();
                cls.Insert_Stores(int.Parse(txtID.Text), txtStoreName.Text.Trim(), txtphone.Text, txtphone2.Text, txtPlace.Text, txtNote.Text);

                
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                   cls. Insert_StoreUsers(int.Parse(txtID.Text),int.Parse(DGV1.Rows[i].Cells["ID"].Value.ToString()));
                }

                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                txtID.Text = cls.MaxID_Stores();
                txtStoreName.Text = "";
           
                txtphone.Text = "";
                txtphone2.Text = "";
                txtPlace.Text = "";
                txtNote.Text = "";
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtStoreName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                // "Conditional"
                if (txtStoreName.Text.Trim() == "")
                {
                    txtStoreName.BackColor = Color.Pink;

                    txtStoreName.Focus();
                    return;
                }


                // "Conditional"
                if (DGV1.Rows.Count == 0)
                {
                    MessageBox.Show("يرجي ادخال المستخدمين", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return;
                }

                //UPDATE
                cls.Update_Stores(int.Parse(txtID.Text), txtStoreName.Text.Trim(), txtphone.Text, txtphone2.Text, txtPlace.Text, txtNote.Text);

                //
                cls.Delete_StoreUsers(int.Parse(txtID.Text));
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.Insert_StoreUsers(int.Parse(txtID.Text), int.Parse(DGV1.Rows[i].Cells["ID"].Value.ToString()));
                }


                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {



            try
            {
                if (cls.Details_Store_Prodecut(txtID.Text).Rows.Count>0)
                {
                    Mass.NoDelete();
                    return;
                }

                if (Mass.Delete() == true)
                {
                    cls.Delete_StoreUsers(int.Parse(txtID.Text));
                    cls.Delete_Stores(int.Parse(txtID.Text));
                    btnNew_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {

                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;

                }
                DataTable dt = cls.Details_Stores(int.Parse( frm.ID_Load));
                DataRow Dr = dt.Rows[0];
                txtID.Text = Dr["StoreID"].ToString();
                txtStoreName.Text = Dr["StoreName"].ToString();
                txtphone.Text = Dr["phone"].ToString();
                txtphone2.Text = Dr["phone2"].ToString();
                txtPlace.Text = Dr["Place"].ToString();
                txtNote.Text = Dr["Note"].ToString();

                DGV1.DataSource = null;
                DGV1.Rows.Clear();


                DataTable dt_user = cls.Details_StoreUsers(int.Parse(txtID.Text));
                for (int i = 0; i < dt_user.Rows.Count; i++)
                {
                    AddRowDgv(dt_user.Rows[i]["UserID"].ToString(), dt_user.Rows[i]["EmpName"].ToString());
                }




                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
     
                 try
            {
                if (combUser.SelectedItem==null)
                {
                    combUser.BackColor = Color.Pink;
                    combUser.Focus();
                    return;
                }


                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["ID"].Value.ToString()== combUser.SelectedValue.ToString())
                    {
                        //MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        combUser.BackColor = Color.Pink;
                        combUser.Focus();
                        return;
                    }
                }






                AddRowDgv(combUser.SelectedValue.ToString(),combUser.Text);
                combUser.Text = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void AddRowDgv(string ID, string EmpName)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
  
                DGV1.Rows[lastRows].Cells["ID"].Value = ID;
                DGV1.Rows[lastRows].Cells["EmpName"].Value = EmpName;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void CombUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            combUser.BackColor = Color.White;
        }

        private void DGV1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                String Header = DGV1.Columns[e.ColumnIndex].Name;
                if (Header == "del")
                {
                    if (Mass.Delete() == true)
                    {
                        foreach (DataGridViewRow r in DGV1.SelectedRows)
                        {
                            DGV1.Rows.Remove(r);
                        }
                      
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
