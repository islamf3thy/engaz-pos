﻿using System;
using System.Data;

class Customers_Pay_cls : DataAccessLayer
{


    //MaxID
    public String MaxID_Customers_Pay()
    {
        return Execute_SQL("select ISNULL (MAX(PayID)+1,1) from Customers_Pay", CommandType.Text);
    }

    // Insert
    public void InsertCustomers_Pay(string PayID, DateTime PayDate, Double PayValue, string Remarks, string CustomerID,String TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Customers_Pay(PayID ,PayDate ,PayValue ,Remarks ,CustomerID,TreasuryID,UserAdd )Values (@PayID,@PayDate ,@PayValue ,@Remarks ,@CustomerID,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayValue", SqlDbType.Float, PayValue),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void UpdateCustomers_Pay(string PayID, DateTime PayDate, Double PayValue, string Remarks, string CustomerID)
    {
        Execute_SQL("Update Customers_Pay Set PayID=@PayID,PayDate=@PayDate ,PayValue=@PayValue ,Remarks=@Remarks ,CustomerID=@CustomerID  where PayID=@PayID", CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayValue", SqlDbType.Float, PayValue),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID));
    }




    //Delete
    public void DeleteCustomers_Pay(string PayID)
    {
        Execute_SQL("Delete  From Customers_Pay where PayID=@PayID", CommandType.Text,
        Parameter("@PayID", SqlDbType.BigInt, PayID));
    }



    //==================================================================================================================================================================

    //Search
    public DataTable Search_Customers_Pay(string Search, DateTime PayDate, DateTime PayDate2)
    {
        string sql = @"SELECT        dbo.Customers_Pay.*, dbo.Customers.CustomerName, dbo.UserPermissions.EmpName
FROM            dbo.Customers INNER JOIN
                         dbo.Customers_Pay ON dbo.Customers.CustomerID = dbo.Customers_Pay.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Customers_Pay.UserAdd = dbo.UserPermissions.ID
        WHERE  PayDate>= @PayDate And PayDate<= @PayDate2 And convert(nvarchar,dbo.Customers_Pay.PayID)+dbo.Customers.CustomerName like '%'+ @Search + '%'";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search),
        Parameter("@PayDate", SqlDbType.Date, PayDate),
        Parameter("@PayDate2", SqlDbType.Date, PayDate2));
    }

    //Details   Where id
    public DataTable Details_Customers_Pay(String PayID)
    {
        string sql = @"SELECT        dbo.Customers_Pay.*, dbo.Customers.CustomerName
FROM            dbo.Customers INNER JOIN
                         dbo.Customers_Pay ON dbo.Customers.CustomerID = dbo.Customers_Pay.CustomerID
         WHERE  (dbo.Customers_Pay.PayID=@PayID)";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@PayID", SqlDbType.NVarChar, PayID));
    }

}

