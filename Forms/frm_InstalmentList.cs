﻿using System; 
using System.Data; 
using System.Linq; 
using DevExpress.XtraGrid.Views.Grid; 
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using ByStro.DAL;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils.Menu;
using System.Collections.Generic;
using ByStro.Clases;

namespace ByStro.Forms
{
    public partial class frm_InstalmentList : frm_Master
    {
        public   frm_Instalment instalmentInstaice;

        private  static  frm_InstalmentList _instance;

        public static frm_InstalmentList Instance { get {

                if (_instance == null || _instance.IsDisposed ) _instance = new frm_InstalmentList();
                return _instance;
            }
        }
        private frm_InstalmentList()
        {
            InitializeComponent();
       
        }
     
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            gridView1.OptionsBehavior.Editable = false; 
            gridView1.Columns["ID"].Caption = "رقم";
            gridView1.Columns["Code"].Caption = "كود";
            gridView1.Columns["Customer"].Caption = "العميل";
            gridView1.Columns["Date"].Caption = "التاريخ";
            gridView1.Columns["StartDate"].Caption = "تاريخ اول قسط";
            gridView1.Columns["InstallPeriod"].Caption = "مده التقسيط";
            gridView1.Columns["InstalmentsCount"].Caption = "عدد الاقساط";
            gridView1.Columns["Invoice"].Caption = "الفاتوره";
            gridView1.Columns["ISPaid"].Caption = "مسدده";
            gridView1.Columns["Remaining"].Caption = "المتبقي";
            gridView1.Columns["Notes"].Caption = "الملاحظات";
            gridView1.Columns["Amount"].Caption = "المبلغ المقسط";
            gridView1.Columns["Net"].Caption = "المبلغ الاساسي";
            gridView1.Columns["AdvancedPay"].Caption = "المقدم";
            gridView1.Columns["Paid"].Caption = "المسدد";
            gridView1.Columns["BenfitesValue"].Caption = "نسبه الفائده";
            gridView1.Columns["BenfitesValue"].DisplayFormat.FormatType = FormatType.Custom;
            gridView1.Columns["BenfitesValue"].DisplayFormat.FormatString = "{0:P}";
            gridView1.Columns["Amount"].Summary.Clear();
            gridView1.Columns["Amount"].Summary.Add(DevExpress.Data.SummaryItemType.Sum,"Amount","الاجمالي{0}");
            gridView1.Columns["Remaining"].Summary.Clear();
            gridView1.Columns["Remaining"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "Remaining", "اجمالي المتبقي{0}");
            gridView1.Columns["Paid"].Summary.Clear();
            gridView1.Columns["Paid"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "Paid", "اجمالي المدفوع{0}");
            gridView1.Columns["ID"].Summary.Clear();
            gridView1.Columns["ID"].Summary.Add(DevExpress.Data.SummaryItemType.Count, "ID", "العدد{0}");


            btn_Delete.Visibility =
            btn_List.Visibility =
            btn_Print.Visibility =
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            gridControl1.ViewRegistered += gridControl1_ViewRegistered;
        }
        private void gridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {
            GridView view = e.View as GridView;
            view.OptionsView.ShowFooter = false;
            view.OptionsView.ShowViewCaption = true;
            if (view.LevelName == "Details")
            {
                view.ViewCaption = "الاقساط";
                view.Columns["number"].Caption = "رقم";
                view.Columns["Amount"].Caption = "المبلغ";
                view.Columns["DueDate"].Caption = "تاريخ الاستحقاق";
                view.Columns["DatePaid"].Caption = "تاريخ السداد";
                view.Columns["IsPaid"].Caption = "مسدده";

            }

        }
        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            base.RefreshData();

            var list = db.Instalments.Select(x => new
            {
                x.ID,
                x.Code,
                Customer = db.Customers.Single(c => c.CustomerID == x.Customer).CustomerName,
                x.Date,
                x.StartDate,
                x.InstallPeriod,
                x.InstalmentsCount,
                x.Invoice,
                x.ISPaid,
                x.Net,
                x.AdvancedPay ,
                x.BenfitesValue ,
                x.Amount ,
                Paid = db.Instalment_Details.Where(d => d.InstalmentID == x.ID && d.IsPaid == true).Sum(d => (double?)d.Amount),
                Remaining = x.Amount -db. Instalment_Details.Where(d => d.InstalmentID == x.ID && d.IsPaid == true).Sum(d => (double?)d.Amount),
                x.Notes,
                Details=db.Instalment_Details.Where(d => d.InstalmentID == x.ID ).Select (d=> new { d.number, d.Amount, d.DueDate, d.DatePaid, d.IsPaid } ).ToList()
            }).ToList();
            gridControl1.DataSource = list;
        }
        public override void New()
        {
            (new Forms.frm_Instalment()).Show();
        }
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
             
            if (info.InRow || info.InRowCell)
            {
                if(instalmentInstaice == null)
                {
                    instalmentInstaice = new frm_Instalment(Convert.ToInt32(view.GetRowCellValue(info.RowHandle, "ID")));
                    instalmentInstaice.Show();
                }
                else{
                    instalmentInstaice.Close();
                    instalmentInstaice = new frm_Instalment(Convert.ToInt32(view.GetRowCellValue(info.RowHandle, "ID")));
                    instalmentInstaice.Show();
                    this.Close();

                }
            }

        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {

            if(e.MenuType == GridMenuType.Row)
            {
                var menu = e.Menu as GridViewMenu;
                var itm = new DXMenuItem("طباعه");
                itm.ImageOptions.SvgImage = btn_Print.ImageOptions.SvgImage;
                itm.Click += Itm_Click;
                menu.Items.Add(itm);
            }
        
        }

        private void Itm_Click(object sender, EventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0) return; 
            List<int> ids = new List<int>();
            foreach (var item in gridView1.GetSelectedRows())
            {
                ids.Add(Convert.ToInt32(gridView1.GetRowCellValue(item, "ID")));
            }
            frm_Instalment.Print(ids);
        }

        private void frm_InstalmentList_Load(object sender, EventArgs e)
        {
            this.Load += (_sender, _e) => { MasterClass.RestoreGridLayout(gridView1, this); };
            this.FormClosing += (_sender, _e) => { MasterClass.SaveGridLayout(gridView1, this); };
        }
    }
}
